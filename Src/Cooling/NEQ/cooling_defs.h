/* ############################################################
      
     FILE:     cooling_defs.h

     PURPOSE:  contains shared definitions with scope
               limited to the cooling module ONLY

   ############################################################ */

#define frac_Z   1.e-3   /*   = N(Z) / N(H), fractional number density of metals (Z)
                                with respect to hydrogen (H) */ 
#define frac_He  0.082   /*   = N(Z) / N(H), fractional number density of helium (He)
                                with respect to hydrogen (H) */ 

/* ##############################################################
     
                   P R O T O T Y P I N G 

   ############################################################## */

void Check_Species (double *);
void check_rhs(real *rhs);
void find_rates(real T, real Ne, real N, real *v);
real find_N_rho ();

/* ############################################################################

                            New structures

   ############################################################################ */

typedef struct COOLING_COEFFICIENTS
{
  real Lrate[NIONS];  /*  Abundance increase rates. This is to be multiplied with N(x-1) - it's the ionization from x-1 to x */
  real Crate[NIONS];  /*  Abundance decrease rates. This is to be multiplied with N(x) - it's the ionization/recombination from x  */
  real Rrate[NIONS];  /*  Abundance increase rates. This is to be multiplied with N(x+1) - it's the recombination from x+1 to x  */
  real N;                    /*  Total number density                 */
  real Ne;                   /*  Electron number density ( cm^{-3} )  */
  real T;                    /*  Temperature (K)                      */
  real La[NIONS];     /*  The coefficient of N_el in Lrate     */
  real Lb[NIONS];     /*  The coefficient of X_H in Lrate      */
  real Lc[NIONS];     /*  The coefficient of X_He in Lrate     */
  real Ca[NIONS];     /*  The coefficient of N_el in Crate     */
  real Cb[NIONS];     /*  The coefficient of X_H in Crate      */
  real Cc[NIONS];     /*  The coefficient of X_He in Crate     */
  real Ra[NIONS];     /*  The coefficient of N_el in Rrate     */
  real Rb[NIONS];     /*  The coefficient of X_H in Rrate      */
  real Rc[NIONS];     /*  The coefficient of X_He in Rrate     */
  real muN, muD;             /*  The nominator and denominator of the mean molecular weight - used for d\mu computations             */
  real de[NIONS];     /*  The radiative losses read from cooling tables, interpolated for T and N_el                          */
  real de_dne[NIONS]; /*  The slope coefficient in the radiative losses interpolation function of N_el                        */

/* -------- NEW ENTRIES -------- */
  
  real dnel_dX[NIONS];
  real fCH, fRH;
  real dmuN_dX[NIONS];
  real dmuD_dX[NIONS];
  real dLIR_dX[NIONS];

} COOL_COEFF;

/* ############################################################################

        Global variable declaration.  They are defined in cooling.c

   ############################################################################ */

extern real elem_ab[8];   /* Number fractions   */
extern real elem_ab_sol[7];   /* Number fractions, Solar    */
extern real elem_ab_uni[7];   /* Number fractions, Universe */
extern real elem_mass[8];  /*   Atomic mass, in a.m.u.   */
extern int elem_part[31];
extern real ***ion_data;
extern real ar_grid[10];
extern int ion_nT;
extern real rad_rec_z[31];
extern real coll_ion_dE[31];
extern COOL_COEFF CoolCoeffs;



/* ############################################################################

         Cooling Tables generation utility - necessary definitions
    
   ############################################################################ */


#define  NPOINT    5000
#define  MAX_LEV   10


/* ****************************************************
   First, the definitions for the 
   Radiative Losses Tables computation
   **************************************************** */


/* ****************************************************
   nlev  = number of levels involved 
   A     = transition probabilities in sec^(-1)
   wgth  = statistical weights
   dE    = energy of the transition in eV 
   N     = Abundance of the ion;
   Ni    = populations of the level;
           notice that we must have \sum Ni = N
   **************************************************** */

typedef struct ION {
   int      nlev, nTom, isMAP, isCV, isH, isCHEB;                 
   double   N;  
   double   wght[MAX_LEV];        
   double   Ni[MAX_LEV];
   double   **A, **dE;  
   double   ***omega, Tom[8];
} Ion;

double to_ev(double);  /* Temperature conversion  K -> eV */


/* Vector/matrix malloc functions */
double **array_2D (long int, long int);
void free_array_2D (double **);
double *array_1D (long int );
void free_array_1D (double *);
int *iarray_1D (int );
void free_iarray_1D (int *);
double *vector (int npoint);
double **matrix (int nx, int ny);
double ***matrix3 (int nx, int ny, int nz);
void free_vector (double *v);
void free_matrix (double **m);


/*  Lagrange interpolation function */
double lagrange (double *x, double *y, double xp, int n, int ii, int jj); 

/* Linear system solver  */
void Solve_System (Ion *X, double Ne, double T);
void Symmetrize_Coeff (Ion *X);

/* Emission lines definitions  */
void INIT_ATOM(Ion *, int);
void HI_INIT (Ion *HIv);
void HeI_INIT (Ion *HeIv);
void HeII_INIT (Ion *HeIIv);
void CI_INIT (Ion *CIv);
void CII_INIT (Ion *CIIv);
void CIII_INIT (Ion *CIIv);
void CIV_INIT (Ion *CIIv);
void CV_INIT (Ion *CIIv);
void NI_INIT (Ion *NIv);
void NII_INIT (Ion *NIIv);
void NIII_INIT (Ion *NIIIv);
void NIV_INIT (Ion *NIIIv);
void NV_INIT (Ion *NIIIv);
void OI_INIT (Ion *OIv);
void OII_INIT (Ion *OIIv);
void OIII_INIT (Ion *OIIIv);
void OIV_INIT (Ion *OIVv);
void OV_INIT (Ion *OVv);
void NeI_INIT (Ion *NeIv);
void NeII_INIT (Ion *NeIIv);
void NeIII_INIT (Ion *NeIIIv);
void NeIV_INIT (Ion *NeIIIv);
void NeV_INIT (Ion *NeIIIv);
void SI_INIT (Ion *SIIv);
void SII_INIT (Ion *SIIv);
void SIII_INIT (Ion *SIIv);
void SIV_INIT (Ion *SIIv);
void SV_INIT (Ion *SIIv);
void FeI_INIT (Ion *FeIv);
void FeII_INIT (Ion *FeIIv);
void FeIII_INIT (Ion *FeIIIv);

/* -----------------------------------------------
    Configure ionization tables parameters here:
    start temperature, temperature step,
    number of steps, number of ion species
   ----------------------------------------------- */
#define  kB        8.617343e-5   
#define I_TSTART     500.0
#define I_TSTEP       10.0
#define I_NSTEP    40000
#define N_AT          28

/* ---------------------------------------------------------------------
    Configure cooling tables parameters here:
    electron number density range (C_NEMIN to C_NEMAX),
    
   ----------------------------------------------- */

#define C_NEMIN   1.0e-2
#define C_NEMAX   1.0e+8
#define C_TMIN    2000.0
#define C_TMAX    2.0e+5
#define C_TSTEP    0.025
#define C_NESTEP   0.12






