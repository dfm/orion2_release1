/* ############################################################
      
     FILE:     cooling.h

     PURPOSE:  contains common definitions for the 
               whole CODE

     Notice: the order is absolutely important and MUST NOT
             be changed !!!
   ############################################################ */

/* -- YES/NO to INCLUDE or EXCLUDE Iron ions from computations -- */

#define INCLUDE_Fe  NO

#if INCLUDE_Fe == YES
 #define NIONS  31
#else
 #define NIONS  28
#endif


#define HI    (NFLX)
#define HeI   (NFLX + 1)
#define HeII  (NFLX + 2)
#define CI    (NFLX + 3)
#define CII   (NFLX + 4)
#define CIII  (NFLX + 5)
#define CIV   (NFLX + 6)
#define CV    (NFLX + 7)
#define NI    (NFLX + 8)
#define NII   (NFLX + 9)
#define NIII  (NFLX + 10)
#define NIV   (NFLX + 11)
#define NV    (NFLX + 12)
#define OI    (NFLX + 13)
#define OII   (NFLX + 14)
#define OIII  (NFLX + 15)
#define OIV   (NFLX + 16)
#define OV    (NFLX + 17)
#define NeI   (NFLX + 18)
#define NeII  (NFLX + 19)
#define NeIII (NFLX + 20)
#define NeIV  (NFLX + 21)
#define NeV   (NFLX + 22)
#define SI    (NFLX + 23)
#define SII   (NFLX + 24)
#define SIII  (NFLX + 25)
#define SIV   (NFLX + 26)
#define SV    (NFLX + 27)
#if INCLUDE_Fe == YES
 #define FeI    (NFLX + 28)
 #define FeII   (NFLX + 29)
 #define FeIII  (NFLX + 30)
#endif


real GET_MAX_RATE (real *, real *, real);
real MEAN_MOLECULAR_WEIGHT (real *);
real COMP_EQUIL            (real, real, real *);
real find_N_rho ();
void RADIAT (real *, real *);


/*
int Ros4_expl, Ros4_impl, Ros4_sup_dt;
*/








