#include "orion2.h"

#ifndef STS_nu 
 #define STS_nu  0.1
#endif

#define STS_MAX_STEPS 8192

static void COMPUTE_SUBSTEPS(real, real tau[], int);
static real FIND_ROOT(real, real, real);
static real CORRECT_TIMESTEP(int, real);

/* ********************************************************** */
void STS (const Data *d, Time_Step *Dts, Grid *grid)
/* 
 *
 * Main Super-Time-Stepping driver.
 * 
 * Solve a diffusion equation like
 *
 *  dV/dt = R 
 *
 * with R a nonlinear right hand side involving
 * second derivatives.
 * This function is called in an operator 
 * split way. 
 *
 *
 ************************************************************ */
{
  int i, j, k, nv, n, m;
  real N, ts[STS_MAX_STEPS];
  real dtpar, dtad, dxmin, scrh, tsave; 
  real max_coeff, max_coeff_glob;
  static real ****rhs;

  if (rhs == NULL) rhs = Array_4D(NVAR, NZ_TOT, NY_TOT, NX_TOT, double);

  BOUNDARY(d, ALLDIR, grid);
  tsave = glob_time;

  dxmin = grid[IDIR].dl_min;
  #if DIMENSIONS >= 2
   dxmin = dmin(grid[IDIR].dl_min, grid[JDIR].dl_min);
   #if DIMENSIONS == 3
    dxmin = dmin(dxmin, grid[KDIR].dl_min);
   #endif
  #endif

/* -------------------------------------------------
    Compute the parabolic time step by calling 
    the RHS function once outside the main loop.
    By default  CFL = 0.4 < 1/2
   ------------------------------------------------- */

  max_coeff = 0.0;
  #if THERMAL_CONDUCTION == SUPER_TIME_STEPPING
   STS_TC_RHS (d->Vc, rhs, &max_coeff, grid);
  #endif
  #if RESISTIVE_MHD == SUPER_TIME_STEPPING
   STS_RES_RHS (d->Vc, rhs, &max_coeff, grid);
  #endif
  #ifdef PARALLEL
   MPI_Allreduce (&max_coeff, &max_coeff_glob, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
   max_coeff = max_coeff_glob;
  #endif

  dtpar = 0.4*dxmin*dxmin/max_coeff;
  dtad  = delta_t;

/* -------------------------------------------------
    Compute the number of steps needed to fit 
    the supertimestep with the advection one.
   ------------------------------------------------- */

  N = FIND_ROOT(1.0, dtpar, dtad);
  N = floor(N+1.0);
  n = (int)N;
/*
  print1("The number of substeps is %d\n", n);
*/
  if (n > STS_MAX_STEPS){
    print1 (" ! WARNING: NUMBER OF TIMESTEPS IN STS IS > %d\n",STS_MAX_STEPS); 
    print1 (" ! TRY LOWERING THE INITIAL TIMESTEP.\n");
    QUIT_ORION2(1);
  }

/* ------------------------------------------------------
    Correct the timestep in order to agree with an
    integer number of timesteps
   ------------------------------------------------------ */

  dtpar = CORRECT_TIMESTEP(n, dtad);

/* ------------------------------------------------------
    Compute the substeps, the sum of which is the
    supertimestep, equal to the advection one
   ------------------------------------------------------ */

  COMPUTE_SUBSTEPS(dtpar, ts, n);

/*-------------------------------------------------------------*/
/*-----THE STS LOOP FOR THE THERMAL CONDUCTION BEGINS HERE-----*/
/*-------------------------------------------------------------*/

  for (m = 0; m < n; m++){

    BOUNDARY(d, ALLDIR, grid);

    scrh = ts[n-m-1];

    if (n == 1){
      scrh = dtpar;
    }

    #if THERMAL_CONDUCTION == SUPER_TIME_STEPPING
     STS_TC_RHS (d->Vc, rhs, &max_coeff, grid);

     KDOM_LOOP (k){
     JDOM_LOOP (j){
     IDOM_LOOP (i){
       d->Vc[PR][k][j][i] += scrh*rhs[PR][k][j][i];
     }}}
    #endif
    #if RESISTIVE_MHD == SUPER_TIME_STEPPING
     STS_RES_RHS (d->Vc, rhs, &max_coeff, grid);

     KDOM_LOOP (k){
     JDOM_LOOP (j){
     IDOM_LOOP (i){
       d->Vc[BX][k][j][i] += scrh*rhs[BX][k][j][i];
       d->Vc[BY][k][j][i] += scrh*rhs[BY][k][j][i];
       d->Vc[BZ][k][j][i] += scrh*rhs[BZ][k][j][i];
     }}}
    #endif

    glob_time += ts[n-m-1];
  }
  
  glob_time = tsave;
  
}


void COMPUTE_SUBSTEPS(real dtex, real tau[], int ssorder)
{
int i;
real S;

S = 0.0;

for (i = 0; i < ssorder; i++) {
   tau[i] = dtex / ((-1.0+STS_nu)*cos(((2.0*i+1.0)*CONST_PI)/(2.0*ssorder)) + 1.0 + STS_nu);
   S += tau[i];
   }
}



real FIND_ROOT(real x0, real dtr, real dta)
{
real a,b,c;
real Ns, Ns1;
int n;

n = 0;

Ns = x0+1.0;
Ns1 = x0;
   
while(fabs(Ns-Ns1) >= 1.0e-5)
   {
   Ns = Ns1;
   a = (1.0-sqrt(STS_nu))/(1.0+sqrt(STS_nu));
   b = pow(a,2.0*Ns);
   c = (1.0-b)/(1.0+b);
   Ns1 = Ns + (dta - dtr*Ns/(2.0*sqrt(STS_nu))*c)/(dtr/(2.0*sqrt(STS_nu))*(c-2.0*Ns*b*log(a)*(1.0+c)/(1.0+b)));
   /*printf("%d\n", n);*/
   n += 1;
   }

return(Ns);
}



real CORRECT_TIMESTEP(int n0, real dta)
{
real a,b,c;
real dtr;

a = (1.0-sqrt(STS_nu))/(1.0+sqrt(STS_nu));
b = pow(a,2.0*n0);
c = (1.0-b)/(1.0+b);

dtr = dta*2.0*sqrt(STS_nu)/(n0*c);
/*
printf("%le\n", dtr);
*/
return(dtr);
}

#undef STS_MAX_STEPS 
