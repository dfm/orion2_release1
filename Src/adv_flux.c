#include "orion2.h"

#define USE_CMA NO
/* *********************************************************** */
void ADVECT_FLUX (const State_1D *state, int beg, int end, Grid *grid)
/*! 
 *
 * Compute fluxes for passive scalars obeying
 * advection equations of the form:
 *  \f[ q_t + v\cdot \nabla q = 0\f]
 * Fluxes are computed using an upwind selection rule
 * based on the density flux:
 * \f[ 
 *    F(\rho q)_{i+1/2} = \rho q v = \left\{\begin{array}{ll}
 *    F(\rho) q_L   & \quad \textrm{if} \quad F(\rho) > 0 \\ \noalign{\medskip}
 *    F(\rho) q_R   & \quad \textrm{if} \quad F(\rho) < 0 \\ \noalign{\medskip}
 *   \end{array}\right.
 *  \f]
 *
 * \note
 *
 * The CMA (Consistent multi-fluid advection method), may be
 * switched on when the sum of several scalars (e.g. different 
 * fluid phases) must be normalized to 1. Reference:
 *
 * \see
 *
 * "The consistent multi-fluid advection method"
 *  T. Plewa and E. Muller
 *  Astron. Astrophys. 342, 179 (1999)
 *
 ************************************************************* */
{
  int    i, nv;
  real *ts, *flux, *vL, *vR;
  real s, rho;
  real phi;
  static double *sigma, **vi;
  
/* ---------------------------------------------------
          Compute entropy for if necessary. 
   --------------------------------------------------- */

  #if ENTROPY_SWITCH == YES
   if (sigma == NULL){
     sigma = Array_1D(NMAX_POINT, double);
     vi    = Array_2D(NMAX_POINT, NVAR, double);
   }
   for (i = beg; i <= end; i++){
     if (state->flux[i][DN] >= 0.0) {
       vi[i][DN] = state->vL[i][DN];
       vi[i][PR] = state->vL[i][PR];
     }else{
       vi[i][DN] = state->vR[i][DN];
       vi[i][PR] = state->vR[i][PR];
     }        
   }
   ENTROPY(vi, sigma, beg, end);
  #endif
  
/* -- compute scalar's fluxes -- */

  for (i = beg; i <= end; i++){

    flux = state->flux[i];
    vL   = state->vL[i];
    vR   = state->vR[i];

    ts = flux[DN] > 0.0 ? vL:vR;


    for (nv = NFLX; nv < (NFLX + NSCL); nv++){
      flux[nv] = flux[DN]*ts[nv];
    }
    
    #if INCLUDE_COOLING == NEQ

     /* -----   He   -----  */
                                                                                                                                                                                                    
     phi = ts[HeI] + ts[HeII];
                                                                                                                                                                                                    
     for (nv = NFLX+1; nv < NFLX+3; nv++){
       flux[nv] /= phi;
     }
     /* -----   C   -----  */
     phi = 0.0;
     for (nv = NFLX+3; nv < NFLX+8; nv++){
       phi += ts[nv];
     }
     for (nv = NFLX+3; nv < NFLX+8; nv++){
       flux[nv] /= phi;
     }
     /* -----   N   -----  */
     phi = 0.0;
     for (nv = NFLX+8; nv < NFLX+13; nv++){
       phi += ts[nv];
     }
     for (nv = NFLX+8; nv < NFLX+13; nv++){
       flux[nv] /= phi;
     }
                                                                                                                                                                                                    
     /* -----   O   -----  */
     phi = 0.0;
     for (nv = NFLX+13; nv < NFLX+18; nv++){
       phi += ts[nv];
     }
     for (nv = NFLX+13; nv < NFLX+18; nv++){
       flux[nv] /= phi;
     }
     /* -----   Ne   -----  */
     phi = 0.0;
     for (nv = NFLX+18; nv < NFLX+23; nv++){
       phi += ts[nv];
     }
     for (nv = NFLX+18; nv < NFLX+23; nv++){
       flux[nv] /= phi;
     }

     /* -----   S   -----  */

     phi = 0.0;
     for (nv = NFLX+23; nv < NFLX+28; nv++){
       phi += ts[nv];
     }
     for (nv = NFLX+23; nv < NFLX+28; nv++){
       flux[nv] /= phi;
     }
                                                                                                                                                                                                  
    #endif


    #if USE_CMA == YES
     phi = 0.0;
     for (nv = NFLX; nv < (NFLX + NSCL); nv++){
       phi += ts[nv];
     }
     for (nv = NFLX; nv < (NFLX + NSCL); nv++){
       flux[nv] /= phi;
     }
    #endif

    #if ENTROPY_SWITCH == YES

/*
     {
       double SL, SR, eL, eR, vL, vR;
   
       eL = state->uL[i][ENTR];
       eR = state->uR[i][ENTR];
       vL = state->vL[i][V1];
       vR = state->vR[i][V1];

       SL = dmin(0.0,state->SL[i]);
       SR = dmax(0.0,state->SR[i]);

       flux[ENTR] = SL*SR*(eR - eL)
                  + SR*eL*vL - SL*eR*vR;
       flux[ENTR] /= SR - SL;
     }
*/
//     flux[ENTR] = flux[DN]*sigma[i];
    #endif

  }
}
