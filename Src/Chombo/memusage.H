#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*/
#endif

#ifndef _MEMUSAGE_H_
#define _MEMUSAGE_H_

#include "REAL.H"

Real get_memory_usage(void);

void gather_memory_from_procs(Real end_memory,
                              Real &avg_memory,
                              Real &min_memory,
                              Real &max_memory);
#endif
