/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(05/26/09): Add magnetic pressure gradient as another variable
*                     for refinement.
*    2) PS(06/04/09): Add a geometrical refinement criterion.
*    3) PS(09/24/09): Modify refinement on density base on input 
*                     parameteraux[mg].
*    4) PS(08/06/10): Rename Pluto to Orion.
*    5) PS(11/18/10): Add shear flow tagging scheme.
*    6) PS(03/15/11): Add Jeans refinement criterion when gravity is involed
*                     and remove aux[mg] and aux[md].
*/

#include "ParmParse.H"
#include <cstdio>
#include <string>
using std::string;

#include "PatchOrion.H"
#include "LoHiSide.H"

void PatchOrion::GETRELGRAD(FArrayBox&   gFab,
                            FArrayBox&   UFab,
			    const int    index,
			    const Real   abstol,
                            const int    dir,
                            const Box&   b)
{
  CH_assert(m_isDefined);

  int nx, ny, nz, nv;
  int i, j, k;
  int ip, im, jp, jm, kp, km;
  int ioff, joff, koff;
  int ibeg, iend, jbeg, jend, kbeg, kend;
  int ilobeg, iloend, jlobeg, jloend, klobeg, kloend;
  int ihibeg, ihiend, jhibeg, jhiend, khibeg, khiend;
  
  int nvar = UFab.nComp();
  double diff, aver;
  double ***UU[nvar];
  double ***q;
  double ***grad;

  kbeg = kend = 0;

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;

  for (nv=0 ; nv<nvar ; nv ++) {
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));
  }

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(dir));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  ioff = (dir == IDIR);
  joff = (dir == JDIR); 
  koff = (dir == KDIR);

  q = UU[index];   

  for (k = kbeg; k <= kend; k++) { kp = k + koff; km = k - koff;
  for (j = jbeg; j <= jend; j++) { jp = j + joff; jm = j - joff;
  for (i = ibeg; i <= iend; i++) { ip = i + ioff; im = i - ioff;
    
   #if GEOMETRY == CYLINDRICAL
    diff = q[kp][jp][ip]/(ip+0.5) - q[km][jm][im]/(im+0.5);
    aver = q[kp][jp][ip]/(ip+0.5) + q[km][jm][im]/(im+0.5);
   #else
    diff = q[kp][jp][ip] - q[km][jm][im];
    aver = q[kp][jp][ip] + q[km][jm][im];
   #endif

    grad[k][j][i] = 0.;
    if(abs(aver)>abstol) grad[k][j][i] = diff/aver;

  }}}

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}


void PatchOrion::GETSHOCKDETECT(FArrayBox&   gFab,
				FArrayBox&   UFab,
				const int    dir,
				const Box&   b)
{
  CH_assert(m_isDefined);

  int nx, ny, nz, nv;
  int i, j, k;
  int ip, im, jp, jm, kp, km;
  int ioff, joff, koff;
  int ibeg, iend, jbeg, jend, kbeg, kend;
  int ilobeg, iloend, jlobeg, jloend, klobeg, kloend;
  int ihibeg, ihiend, jhibeg, jhiend, khibeg, khiend;
 
  int nvar = UFab.nComp();
  double taup, tau, taum, v1p, v1, v1m, Kp, K, Km, prp, pr, prm, dp, dv;
  double diff, aver;
  double ***UU[nvar];
  double ***grad;


  kbeg = kend = 0;

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;
  
  for (nv=0 ; nv<nvar ; nv ++)
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(dir));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  ioff = (dir == IDIR);
  joff = (dir == JDIR); 
  koff = (dir == KDIR);

  for (k = kbeg; k <= kend; k++) { kp = k + koff; km = k - koff;
  for (j = jbeg; j <= jend; j++) { jp = j + joff; jm = j - joff;
  for (i = ibeg; i <= iend; i++) { ip = i + ioff; im = i - ioff;

    taup = 1.0/UU[DN][kp][jp][ip];
    tau  = 1.0/UU[DN][k ][j ][i ];
    taum = 1.0/UU[DN][km][jm][im];

    v1p = UU[M1][kp][jp][ip]*taup;
    v1  = UU[M1][k ][j ][i ]*tau ;
    v1m = UU[M1][km][jm][im]*taum;

    D_EXPAND(Kp  = 0.5*UU[MX][kp][jp][ip]*UU[MX][kp][jp][ip]; ,
             Kp += 0.5*UU[MY][kp][jp][ip]*UU[MY][kp][jp][ip]; ,
             Kp += 0.5*UU[MZ][kp][jp][ip]*UU[MZ][kp][jp][ip];)

    D_EXPAND(K  = 0.5*UU[MX][k][j][i]*UU[MX][k][j][i]; ,
             K += 0.5*UU[MY][k][j][i]*UU[MY][k][j][i]; ,
             K += 0.5*UU[MZ][k][j][i]*UU[MZ][k][j][i];)

    D_EXPAND(Km  = 0.5*UU[MX][km][jm][im]*UU[MX][km][jm][im]; ,
             Km += 0.5*UU[MY][km][jm][im]*UU[MY][km][jm][im]; ,
             Km += 0.5*UU[MZ][km][jm][im]*UU[MZ][km][jm][im];)

#if EOS == IDEAL
    prp = (gmm - 1.0)*(UU[EN][kp][jp][ip] - Kp*taup);
    pr  = (gmm - 1.0)*(UU[EN][k ][j ][i ] - K *tau);
    prm = (gmm - 1.0)*(UU[EN][km][jm][im] - Km*taum);
#elif EOS == ISOTHERMAL
    prp = C_ISO*C_ISO*UU[DN][kp][jp][ip];
    pr  = C_ISO*C_ISO*UU[DN][k ][j ][i ];
    prm = C_ISO*C_ISO*UU[DN][km][jm][im];
#endif

    dp = fabs(prp - prm)/(dmin(pr,prm));
    dv = v1p - v1m;

    if (dv < 0.1){
      grad[k][j][i] = dp;
    }else{
      grad[k][j][i] = 0.0;
    }    
  }}}

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}


void PatchOrion::GETJEANSNO(FArrayBox&   gFab,
                            FArrayBox&   UFab,
                            const Box&   b)
{
  CH_assert(m_isDefined);

  int nx, ny, nz, nv;
  int i, j, k;
  int ibeg, iend, jbeg, jend, kbeg, kend;
  int ilobeg, iloend, jlobeg, jloend, klobeg, kloend;
  int ihibeg, ihiend, jhibeg, jhiend, khibeg, khiend;
 
  int nvar = UFab.nComp();
  double den;
  double ***UU[nvar];
  double ***q;
  double ***grad;

#if EOS == IDEAL
  double m2, pr, Jeans_factor;
  double gmm1=gmm-1.0;
#elif EOS == ISOTHERMAL
  double Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
    m_dx/(2.0*CONST_PI*C_ISO);
#endif

#ifdef STAGGERED_MHD
  double beta, b2;
#endif

  kbeg = kend = 0;

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;
  
  for (nv=0 ; nv<nvar ; nv ++)
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(0));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  for (k = kbeg; k <= kend; k++) { 
  for (j = jbeg; j <= jend; j++) { 
  for (i = ibeg; i <= iend; i++) { 

    den = dmax(UU[DN][k][j][i], SMALL_DN);
#ifdef STAGGERED_MHD
    b2 = EXPAND(UU[BX][k][j][i]*UU[BX][k][j][i],
		+ UU[BY][k][j][i]*UU[BY][k][j][i],
		+ UU[BZ][k][j][i]*UU[BZ][k][j][i]);
#if EOS == ISOTHERMAL
    Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
      m_dx/(2.0*CONST_PI*C_ISO);
    beta = 2.0 * den * C_ISO * C_ISO / b2;
#endif
#endif

  #if EOS == IDEAL
    m2 = EXPAND(UU[MX][k][j][i]*UU[MX][k][j][i],
       + UU[MY][k][j][i]*UU[MY][k][j][i], + UU[MZ][k][j][i]*UU[MZ][k][j][i]);
    #ifdef STAGGERED_MHD
      pr = gmm1 * (UU[EN][k][j][i] - 0.5*(m2/den + b2));
      beta = 2.0 * pr / b2;
    #else /* STAGGERED_MHD */
      pr = gmm1 * (UU[EN][k][j][i] - 0.5*m2/den);
    #endif /* STAGGERED_MHD */

    if (pr < 0.0) pr = SMALL_PR;

    Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
      m_dx/(2.0*CONST_PI*sqrt(gmm*pr/den));
#endif
    
#ifdef STAGGERED_MHD
    Jeans_factor /= sqrt(1.0 + (0.74/beta)); // ATM magnetic jeans adjustment
#endif
    grad[k][j][i] = Jeans_factor*sqrt(den);
    
    //cout << "jeans no = " << grad[k][j][i] << endl;

  }}}          

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}

void PatchOrion::GETJEANSNO2( FArrayBox&   gFab,
                              FArrayBox&   UFab,
                              const Box&   b)
{
  CH_assert(m_isDefined);

  int nx, ny, nz, nv;
  int i, j, k;
  int ibeg, iend, jbeg, jend, kbeg, kend;
  int ilobeg, iloend, jlobeg, jloend, klobeg, kloend;
  int ihibeg, ihiend, jhibeg, jhiend, khibeg, khiend;
 
  int nvar = UFab.nComp();
  double den, rad, rc, dx,ciso2;
  double x1,x2,x3;
  double ***UU[nvar];
  double ***q;
  double ***grad;

#if EOS == IDEAL
  double m2, pr, Jeans_factor;
  double gmm1=gmm-1.0;
#elif EOS == ISOTHERMAL
  double Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
    m_dx/(2.0*CONST_PI*C_ISO);
#endif

#ifdef STAGGERED_MHD
  double beta, b2;
#endif

  kbeg = kend = 0;

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );
  
  dx = m_dx;
  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;
  
  for (nv=0 ; nv<nvar ; nv ++)
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(0));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  for (k = kbeg; k <= kend; k++) { 
  for (j = jbeg; j <= jend; j++) { 
  for (i = ibeg; i <= iend; i++) { 


    x1 = double(i+0.5)*dx;
    x2 = double(j+0.5)*dx;
    x3 = double(k+0.5)*dx;
    x1 += DOM_XBEG[IDIR];
    x2 += DOM_XBEG[JDIR];
    x3 += DOM_XBEG[KDIR];

    rc = 0.1 * 3.08568025e18;
    rad = sqrt(x1*x1  + x2*x2 + x3*x3);
    den = dmax(2.4e-18*pow(rad/rc,-1.5),SMALL_DN);
    //den = dmax(UU[DN][k][j][i], SMALL_DN);
#ifdef STAGGERED_MHD
    b2 = EXPAND(UU[BX][k][j][i]*UU[BX][k][j][i],
		+ UU[BY][k][j][i]*UU[BY][k][j][i],
		+ UU[BZ][k][j][i]*UU[BZ][k][j][i]);
#if EOS == ISOTHERMAL
    Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
      m_dx/(2.0*CONST_PI*C_ISO);
    beta = 2.0 * den * C_ISO * C_ISO / b2;
#endif
#endif

  #if EOS == IDEAL
    m2 = EXPAND(UU[MX][k][j][i]*UU[MX][k][j][i],
       + UU[MY][k][j][i]*UU[MY][k][j][i], + UU[MZ][k][j][i]*UU[MZ][k][j][i]);
    #ifdef STAGGERED_MHD
      pr = gmm1 * (UU[EN][k][j][i] - 0.5*(m2/den + b2));
      beta = 2.0 * pr / b2;
    #else 
      pr = gmm1 * (UU[EN][k][j][i] - 0.5*m2/den);
    #endif 

    #if BARO_COOL == YES
      ciso2 = CONST_kB*20.0/(2.33*CONST_mp);
      pr = den*ciso2*(1.0+pow(den/1.0e-13,gmm-1.0));
      
      if (pr < 0.0) pr = SMALL_PR;
      Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
      m_dx/(2.0*CONST_PI*sqrt(pr/den));
      beta = 2.0 * pr / b2;
    #else

      if (pr < 0.0) pr = SMALL_PR;

      Jeans_factor = sqrt(4.0*CONST_PI*CONST_G*pow(UNIT_VELOCITY/UNIT_LENGTH,2)*UNIT_DENSITY)*
      m_dx/(2.0*CONST_PI*sqrt(gmm*pr/den));

    #endif
#endif
    
#ifdef STAGGERED_MHD
    Jeans_factor /= sqrt(1.0 + (0.74/beta)); // ATM magnetic jeans adjustment
#endif
    grad[k][j][i] = Jeans_factor*sqrt(den);

  }}}

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}

void PatchOrion::MAGNITUDE(FArrayBox&         gmagFab,
                           FArrayBox&         gFab,
                           const Box&         bbox)
{

 int i, j, k, idim;
 int ioff, joff, koff;
 int ibeg, iend, jbeg, jend, kbeg, kend;
 
 double sum,cur;

 double ***gradmag;
 double ***grad[DIMENSIONS];

 kbeg = kend = 0;

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );


 gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

 D_EXPAND(ibeg = gFab.loVect()[IDIR];
          iend = gFab.hiVect()[IDIR]; ,
          jbeg = gFab.loVect()[JDIR];
          jend = gFab.hiVect()[JDIR]; ,
          kbeg = gFab.loVect()[KDIR];
          kend = gFab.hiVect()[KDIR]; );

 for (idim=0; idim < DIMENSIONS; idim++)
  grad[idim] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(idim));  

 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );


 for (k=kbeg ; k <= kend ; k++) {
 for (j=jbeg ; j <= jend ; j++) {
 for (i=ibeg ; i <= iend ; i++) {

  sum = 0.0;
  for (idim=0; idim < DIMENSIONS; idim++) {
   cur = grad[idim][k][j][i];
   sum = sum + cur*cur; 
  }

  gradmag[k][j][i] = sqrt(sum);

 }}}

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

 D_EXPAND(ibeg = gFab.loVect()[IDIR];
          iend = gFab.hiVect()[IDIR]; ,
          jbeg = gFab.loVect()[JDIR];
          jend = gFab.hiVect()[JDIR]; ,
          kbeg = gFab.loVect()[KDIR];
          kend = gFab.hiVect()[KDIR]; );

 for (idim=0; idim < DIMENSIONS; idim++)
  free_chmatrix(grad[idim],kbeg,kend,jbeg,jend,ibeg,iend);

}


/*:PS Geometrical refinement criterion */
void PatchOrion::DISKTAG(FArrayBox&   UFab,
			 FArrayBox&   tagFab,
			 Real*    xMassive,
			 int         level,
                         const Box&   bbox)
{

 int i, j, k, idim, nv;
 int ioff, joff, koff, nxtot, nytot, nztot;
 int ibeg, iend, jbeg, jend, kbeg, kend;
 
 double x1, x2, x3, dx, r;

 real ***tags; // array of pointers to tag data

 int nvar = UFab.nComp();
 real ***UU[nvar]; // array of pointers to tag data

 // if there is no sink particle in the problem return
 if (xMassive == NULL) return;

 kbeg = kend = 0;

 D_EXPAND(ibeg = tagFab.loVect()[IDIR];
          iend = tagFab.hiVect()[IDIR]; ,
          jbeg = tagFab.loVect()[JDIR];
          jend = tagFab.hiVect()[JDIR]; ,
          kbeg = tagFab.loVect()[KDIR];
          kend = tagFab.hiVect()[KDIR]; );

 tags = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,tagFab.dataPtr(0));

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++) {
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));
  }

 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );

 dx = m_dx;
 nxtot = iend - ibeg + 1;
 nytot = jend - jbeg + 1;
 nztot = kend - kbeg + 1;

 for (k=0 ; k < nztot ; k++) {
 for (j=0 ; j < nytot ; j++) {
 for (i=0 ; i < nxtot ; i++) {

   x1 = double(i + ibeg + 0.5)*dx;
   x2 = double(j + jbeg + 0.5)*dx;
   x3 = double(k + kbeg + 0.5)*dx;

   x1 += DOM_XBEG[IDIR];
   x2 += DOM_XBEG[JDIR];
   x3 += DOM_XBEG[KDIR];
   /* Disk Refinement Criteria */
   r = sqrt((x1-xMassive[IDIR])*(x1-xMassive[IDIR])+(x2-xMassive[JDIR])*(x2-xMassive[JDIR])+(x3-xMassive[KDIR])*(x3-xMassive[KDIR]));
   if (0) {    //r < 500*1.496e13
     tags[k+kbeg][j+jbeg][i+ibeg] = 1.0;
   }
   else{tags[k+kbeg][j+jbeg][i+ibeg] = 0.0;}
 }}}

 D_EXPAND(ibeg = tagFab.loVect()[IDIR];
          iend = tagFab.hiVect()[IDIR]; ,
          jbeg = tagFab.loVect()[JDIR];
          jend = tagFab.hiVect()[JDIR]; ,
          kbeg = tagFab.loVect()[KDIR];
          kend = tagFab.hiVect()[KDIR]; );

 free_chmatrix(tags,kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++) {
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);
  }

}


/* ATL / AS  Refinement criterion based on location */
void PatchOrion::NestedREF(FArrayBox& gmagFab, const Box& bbox, const int m_level, const Real factor, const Real m_domainLength,const Real* Center)
{

 // If 'factor' is zero or greater than 1, we skip everything. (At this point,
 // it shouldn't be identically equal to zero unless something changed in AMRLevelOrion.cpp)
 if(factor==0 || factor < -1.0 || factor > 1.0) return;


 // Most of this shamelessly copied from PS's Zoom In algorithm below
 int i, j, k, idim;
 int ioff, joff, koff, nxtot, nytot, nztot;
 int ibeg, iend, jbeg, jend, kbeg, kend;

 double x1, x2, x3, dx;

 //A pointer array we'll use to access the data
 double ***gradmag;

 // Chombo command that gets the indices in the box of interest
 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 // Using those indices, form an array pointing to the data
 gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

 // Gets some information on the box size
 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );

 // Grid size, cell size
 dx = m_dx;
 nxtot = iend - ibeg + 1;
 nytot = jend - jbeg + 1;
 nztot = kend - kbeg + 1;

 // If factor > 0, we are laying down some SMR. 
 // If factor < 0, we are restricting the locations where AMR can occur
 double Boundary = m_domainLength*fabs(factor)/2.0;
 
 //cout << "Boundary : " << Boundary << " on level " << m_level << "(" << m_domainLength << "," << factor << ")" << endl;

 for (k = kbeg; k <= kend; k++) {
 for (j = jbeg; j <= jend; j++) {
 for (i = ibeg; i <= iend; i++) {
       x1 = double(i)*dx;
       x2 = double(j)*dx;
       x3 = double(k)*dx;

       x1 += DOM_XBEG[IDIR];
       x2 += DOM_XBEG[JDIR];
       x3 += DOM_XBEG[KDIR];
       gradmag[k][j][i] = 0.0;

       bool b1 = fabs(x1-Center[0]) <= Boundary;
       bool b2 = fabs(x2-Center[1]) <= Boundary;
       bool b3 = fabs(x3-Center[2]) <= Boundary;

       if(factor>0) 
       {        
           if( b1 && b2 && b3 )   gradmag[k][j][i] = 10.0; // we shall force refinement!
           else gradmag[k][j][i] = -10.0;  // we shall prevent refinement at this location!
       } else {
	 if( b1 && b2 && b3 )   gradmag[k][j][i] = 0.0; // if other tagging routines want to refine, go for it!
	 else gradmag[k][j][i] = -10.0;                 // we shall prevent refinment at this location!
       }

 }}} // End of triple-for loop

 // Let's free the data
 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );
 free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

}

/* ALR /  Refinement criterion based on location (sphere geomtry) */
void PatchOrion::NestedSphereREF(FArrayBox& gmagFab, const Box& bbox, const int m_level, const Real factor, const Real m_domainLength,const Real* Center)
{

  // Most of this shamelessly copied from PS's Zoom In algorithm below
  int i, j, k, idim;
  int ioff, joff, koff, nxtot, nytot, nztot;
  int ibeg, iend, jbeg, jend, kbeg, kend;

  double x1, x2, x3, dx, r, rc;

  //A pointer array we'll use to access the data
  double ***gradmag;

  // Chombo command that gets the indices in the box of interest
  D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
	   iend = gmagFab.hiVect()[IDIR]; ,
	   jbeg = gmagFab.loVect()[JDIR];
	   jend = gmagFab.hiVect()[JDIR]; ,
	   kbeg = gmagFab.loVect()[KDIR];
	   kend = gmagFab.hiVect()[KDIR]; );

  // Using those indices, form an array pointing to the data
  gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

  // Gets some information on the box size
  D_EXPAND(ibeg = bbox.loVect()[IDIR];
	   iend = bbox.hiVect()[IDIR]; ,
	   jbeg = bbox.loVect()[JDIR];
	   jend = bbox.hiVect()[JDIR]; ,
	   kbeg = bbox.loVect()[KDIR];
	   kend = bbox.hiVect()[KDIR]; );

  // Grid size, cell size
  dx = m_dx;
  nxtot = iend - ibeg + 1;
  nytot = jend - jbeg + 1;
  nztot = kend - kbeg + 1;

  double Boundary = m_domainLength*factor;
  for (k = kbeg; k <= kend; k++) {
    for (j = jbeg; j <= jend; j++) {
      for (i = ibeg; i <= iend; i++) {
	x1 = double(i)*dx;
	x2 = double(j)*dx;
	x3 = double(k)*dx;

	x1 += DOM_XBEG[IDIR];
	x2 += DOM_XBEG[JDIR];
	x3 += DOM_XBEG[KDIR];
	gradmag[k][j][i] = 0.0;
	
	//compute r and "center" r
	r = pow(x1*x1+ x2*x2 + x3*x3, 0.5);
        rc = pow(Center[0]*Center[0] + Center[1]*Center[1] + Center[2]*Center[2], 0.5);
	bool br = fabs(r-rc) <= Boundary;
	//bool b1 = fabs(x1-Center[0]) <= Boundary;
	//bool b2 = fabs(x2-Center[1]) <= Boundary;
	//bool b3 = fabs(x3-Center[2]) <= Boundary;
        
	//if( b1 && b2 && b3 )   gradmag[k][j][i] = 10.0;
	if( br )   gradmag[k][j][i] = 10.0;
	else gradmag[k][j][i] = -10.0; // If outside nested region, prevent refinement from occuring
	                     // (requires the elseif statement in AMRLevelOrion.cpp to be uncommented)
      }}} // End of triple-for loop

  // Let's free the data
  D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
	   iend = gmagFab.hiVect()[IDIR]; ,
	   jbeg = gmagFab.loVect()[JDIR];
	   jend = gmagFab.hiVect()[JDIR]; ,
	   kbeg = gmagFab.loVect()[KDIR];
	   kend = gmagFab.hiVect()[KDIR]; );
  free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

} //End NestedSphere

//ALR: Tag cells to be refined based on density threshold 
void PatchOrion::DensityTag(FArrayBox& denFab, FArrayBox& gmagFab, const Real densityThresh, const int m_level, const Box& bbox)
{
  int i, j, k, idim;
  int ioff, joff, koff, nxtot, nytot, nztot;
  int ibeg, iend, jbeg, jend, kbeg, kend;

  //A pointer array we'll use to access the data
  Real ***gradmag;
  Real ***gradden; //Used to access density data
  

  // Chombo command that gets the indices in the box of interest
  D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
	   iend = gmagFab.hiVect()[IDIR]; ,
	   jbeg = gmagFab.loVect()[JDIR];
	   jend = gmagFab.hiVect()[JDIR]; ,
	   kbeg = gmagFab.loVect()[KDIR];
	   kend = gmagFab.hiVect()[KDIR]; );

  // Using those indices, form an array pointing to the data
  gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

  // Chombo command that gets the indices in the box of interest
  D_EXPAND(ibeg = denFab.loVect()[IDIR];
           iend = denFab.hiVect()[IDIR]; ,
           jbeg = denFab.loVect()[JDIR];
           jend = denFab.hiVect()[JDIR]; ,
           kbeg = denFab.loVect()[KDIR];
           kend = denFab.hiVect()[KDIR]; );
  gradden = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,denFab.dataPtr(0));
  
  // Gets some information on the box size
  D_EXPAND(ibeg = bbox.loVect()[IDIR];
	   iend = bbox.hiVect()[IDIR]; ,
	   jbeg = bbox.loVect()[JDIR];
	   jend = bbox.hiVect()[JDIR]; ,
	   kbeg = bbox.loVect()[KDIR];
	   kend = bbox.hiVect()[KDIR]; );

  // Grid size
  nxtot = iend - ibeg + 1;
  nytot = jend - jbeg + 1;
  nztot = kend - kbeg + 1;
  
  //Loop over cells and check density for refinement
  for (k = kbeg; k <= kend; k++) {
    for (j = jbeg; j <= jend; j++) {
      for (i = ibeg; i <= iend; i++) {
	//Using the denFab which just hold a copy of the density state data, compare each cell to the threshhold value
	//Require densityThresh to be >0
	if (gradden[k][j][i] >= densityThresh && densityThresh > 0) gradmag[k][j][i] = 10.0;
	else gradmag[k][j][i] = -10.0;  // If outside nested region, prevent refinement from occuring
                                        // (requires the elseif statement in AMRLevelOrion.cpp to be uncommented

      }
    }
  } //End triple for loop

  // Let's free the data
  D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
	   iend = gmagFab.hiVect()[IDIR]; ,
	   jbeg = gmagFab.loVect()[JDIR];
	   jend = gmagFab.hiVect()[JDIR]; ,
	   kbeg = gmagFab.loVect()[KDIR];
	   kend = gmagFab.hiVect()[KDIR]; );
  free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);
  D_EXPAND(ibeg = denFab.loVect()[IDIR];
           iend = denFab.hiVect()[IDIR]; ,
           jbeg = denFab.loVect()[JDIR];
           jend = denFab.hiVect()[JDIR]; ,
	   kbeg = denFab.loVect()[KDIR];
	   kend = denFab.hiVect()[KDIR]; );
  free_chmatrix(gradden,kbeg,kend,jbeg,jend,ibeg,iend);

}

/*:PS Zoom-in refinement criterion */
void PatchOrion::ZoomREF(FArrayBox&         gmagFab,
                         const Box&         bbox)
{

 int i, j, k, idim;
 int ioff, joff, koff, nxtot, nytot, nztot;
 int ibeg, iend, jbeg, jend, kbeg, kend;
 
 double x1, x2, x3, dx;

 double ***gradmag;

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );

 dx = m_dx;
 nxtot = iend - ibeg + 1;
 nytot = jend - jbeg + 1;
 nztot = kend - kbeg + 1;
 /*
 cout << "jend = " << jend << endl;
 cout << "x2 = " << double(jend + 0.5)*dx << endl;
 cout << "ydom = " << DOM_XBEG[JDIR] << endl;
 cout << "x2+ = " << double(jend + 0.5)*dx+DOM_XBEG[JDIR] << endl;

 for (k=0 ; k < nztot ; k++) {
 for (j=0 ; j < nytot ; j++) {
 for (i=0 ; i < nxtot ; i++) {

   x1 = double(i + ibeg + 0.5)*dx;
   x2 = double(j + jbeg + 0.5)*dx;
   x3 = double(k + kbeg + 0.5)*dx;

   x1 += DOM_XBEG[IDIR];
   x2 += DOM_XBEG[JDIR];
   x3 += DOM_XBEG[KDIR];
   gradmag[k+kbeg][j+jbeg][i+ibeg] = 0.0;
 */

 /*PS: Nzoom is the number of regions to be zoomed in.  Currently the setting here is for my specific IRDC zoom-in simulation.  For general cases with many more zoom-in regions, Nzoom can be set as an input parameter in definitions.h and the zoom-in region coordinates zonbeg, ... etc. can be read from a text file. */

 // multiple zoom boxes from orion2.ini

 int l;

 int Nzoom;
 //double zoxbeg[Nzoom], zoxend[Nzoom];
 //double zoybeg[Nzoom], zoyend[Nzoom];
 //double zozbeg[Nzoom], zozend[Nzoom];

 ParmParse pp("refinement");
 pp.query("Nzoom",Nzoom);
 double zoxbeg[Nzoom], zoxend[Nzoom];
 double zoybeg[Nzoom], zoyend[Nzoom];
 double zozbeg[Nzoom], zozend[Nzoom];
 //double cx[Nzoom], cy[Nzoom], cz[Nzoom], width[Nzoom];
 vector<Real> cx(Nzoom,-1);
 vector<Real> cy(Nzoom,-1);
 vector<Real> cz(Nzoom,-1);
 vector<Real> width(Nzoom,-1);

 if(Nzoom == 1){
   pp.query("ZoomRegion_cx",cx[0]);
   pp.query("ZoomRegion_cy",cy[0]);
   pp.query("ZoomRegion_cz",cz[0]);
   pp.query("ZoomRegion_width",width[0]);
   zoxbeg[0]=cx[0] - width[0]/2.0; zoxend[0]=cx[0] + width[0]/2.0;
   zoybeg[0]=cy[0] - width[0]/2.0; zoyend[0]=cy[0] + width[0]/2.0;
   zozbeg[0]=cz[0] - width[0]/2.0; zozend[0]=cz[0] + width[0]/2.0;
 }
 else {
   pp.queryarr("ZoomRegion_cx",cx,0,Nzoom);
   pp.queryarr("ZoomRegion_cy",cy,0,Nzoom);
   pp.queryarr("ZoomRegion_cz",cz,0,Nzoom);
   pp.queryarr("ZoomRegion_width",width,0,Nzoom);
   for(int i=0; i<Nzoom; i++){
     zoxbeg[i]=cx[i] - width[i]/2.0; zoxend[i]=cx[i] + width[i]/2.0;
     zoybeg[i]=cy[i] - width[i]/2.0; zoyend[i]=cy[i] + width[i]/2.0;
     zozbeg[i]=cz[i] - width[i]/2.0; zozend[i]=cz[i] + width[i]/2.0;
   }

 }
 

 //pp.query("ZoomRegion_cx",cx);
 //pp.query("ZoomRegion_cy",cy);
 //pp.query("ZoomRegion_cz",cz);
 //pp.query("ZoomRegion_width",width);

 // box for 256^3, 256pc mhd SS simulation
 // centering at good size clump after 2.5 Myr no grav run
 //double cx = -25.0 * 3.08e18;
 //double cy = 15.0 * 3.08e18;
 //double cz = 2.5 * 3.08e18;
 //double width = 70.0 * 3.08e18;
 //zoxbeg[0]=cx - width/2.0; zoxend[0]=cx + width/2.0;
 //zoybeg[0]=cy - width/2.0; zoyend[0]=cy + width/2.0;
 //zozbeg[0]=cz - width/2.0; zozend[0]=cz + width/2.0;

 //zoxbeg[0]=-35.0*3.08e18; zoxend[0]=35.0*3.08e18;
 //zoybeg[0]=-35.0*3.08e18; zoyend[0]=35.0*3.08e18;
 //zozbeg[0]=-35.0*3.08e18; zozend[0]=35.0*3.08e18;

 // boxes for upsampled 64pc simulation
 //zoxbeg[0]=-3.08e19; zoxend[0]=3.08e19;
 //zoybeg[0]=-3.08e19; zoyend[0]=3.08e19;
 //zozbeg[0]=-3.08e19; zozend[0]=3.08e19;

 //zoxbeg[1]=36.0e18; zoxend[1]=140.0e18;
 //zoybeg[1]=100.0e18; zoyend[1]=180.0e18;
 //zozbeg[1]=-100.0e18; zozend[1]=100.0e18;

//> boxes for full GDZ simulation - originally from Shule Li
/* zoxbeg[0]=-62.0e18; zoxend[0]=48.0e18;
 zoybeg[0]=-60.0e18; zoyend[0]=160.0e18;
 zozbeg[0]=-100.0e18; zozend[0]=100.0e18;

 zoxbeg[1]=36.0e18; zoxend[1]=140.0e18;
 zoybeg[1]=100.0e18; zoyend[1]=180.0e18;
 zozbeg[1]=-100.0e18; zozend[1]=100.0e18;*/
 
 // zoom boxes for second extraction.
 // these should reeally be read in from orion2.ini
 
 //Nzoom = 1;
 //zoxbeg[0]=-9.24e18; zoxend[0]=9.24e18;
 //zoybeg[0]=-6.2e18; zoyend[0]=6.2e18;
 //zozbeg[0]=-6.2e18; zozend[0]=6.2e18;


 for (k = kbeg; k <= kend; k++) {
   for (j = jbeg; j <= jend; j++) {
     for (i = ibeg; i <= iend; i++) {
       x1 = double(i)*dx;
       x2 = double(j)*dx;
       x3 = double(k)*dx;
       
       x1 += DOM_XBEG[IDIR];
       x2 += DOM_XBEG[JDIR];
       x3 += DOM_XBEG[KDIR];
       gradmag[k][j][i] = 0.0;

       for (l = 0; l < Nzoom; l++) {
         if (x1 >= zoxbeg[l] && x1 <= zoxend[l] && x2 >= zoybeg[l] && x2 < zoyend[l] && x3 > zozbeg[l] && x3 <= zozend[l]) {
	   gradmag[k][j][i] = 10.0;
	 }
       }
     }
   }
 }

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

}

void PatchOrion::GETRELVELGRAD(FArrayBox&   gFab,
                               FArrayBox&   UFab,
			       const Real   abstol,
                               const int    dir,
			       const Box&   b)
{
  CH_assert(m_isDefined);

  int nx, ny, nz, nv;
  int i, j, k;
  int ip, im, jp, jm, kp, km;
  int ioff, joff, koff;
  int ibeg, iend, jbeg, jend, kbeg, kend;
 
  int nvar = UFab.nComp();
  double diff;
  double ***UU[nvar];
  double ***grad;
  double taup, taum, tau;
  double v1p, v2, v1m, v2p, v2m, c2;

#if EOS == IDEAL
  double m2, b2;
  double gmm1=gmm-1.0;
#endif

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;
  
  for (nv=0 ; nv<nvar ; nv ++)
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(dir));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  ioff = (dir == IDIR);
  joff = (dir == JDIR); 
  koff = (dir == KDIR);

  for (k = kbeg; k <= kend; k++) { kp = k + koff; km = k - koff;
  for (j = jbeg; j <= jend; j++) { jp = j + joff; jm = j - joff;
  for (i = ibeg; i <= iend; i++) { ip = i + ioff; im = i - ioff;

    tau = 1.0/UU[DN][k][j][i];
    taup = 1.0/UU[DN][kp][jp][ip];
    taum = 1.0/UU[DN][km][jm][im];

    D_EXPAND(v2  = UU[MX][k][j][i]*UU[MX][k][j][i]; ,
             v2 += UU[MY][k][j][i]*UU[MY][k][j][i]; ,
             v2 += UU[MZ][k][j][i]*UU[MZ][k][j][i];)

    v2 = v2/(UU[DN][km][jm][im]*UU[DN][km][jm][im]);

  #if EOS == IDEAL
    m2 = EXPAND(UU[MX][k][j][i]*UU[MX][k][j][i],
       + UU[MY][k][j][i]*UU[MY][k][j][i], + UU[MZ][k][j][i]*UU[MZ][k][j][i]);
    #ifdef STAGGERED_MHD
      b2 = EXPAND(UU[BX][k][j][i]*UU[BX][k][j][i],
       + UU[BY][k][j][i]*UU[BY][k][j][i],
       + UU[BZ][k][j][i]*UU[BZ][k][j][i]);
      c2 = max(gmm * gmm1 * (UU[EN][k][j][i] - 0.5*(m2*tau + b2)), SMALL_PR)
         * tau;
    #else /* STAGGERED_MHD */
      c2 = max(gmm * gmm1 * (UU[EN][k][j][i] - 0.5*m2*tau), SMALL_PR) * tau;
    #endif /* STAGGERED_MHD */

  #elif EOS == ISOTHERMAL
    c2 = C_ISO*C_ISO;
  #endif

    if (v2 > abstol*abstol*c2) {

      if (dir == KDIR) {
	v1p  = UU[MX][kp][jp][ip]*taup;
	v1m  = UU[MX][km][jm][im]*taum;
	v2p  = UU[MY][kp][jp][ip]*taup;
	v2m  = UU[MY][km][jm][im]*taum;
	diff = (v1p - v1m)*(v1p - v1m)+(v2p - v2m)*(v2p - v2m);
      }

      else if (dir == JDIR) {
	v1p   = UU[MX][kp][jp][ip]*taup;
	v1m   = UU[MX][km][jm][im]*taum;
	v2p   = UU[MZ][kp][jp][ip]*taup;
	v2m   = UU[MZ][km][jm][im]*taum;
	diff = (v1p - v1m)*(v1p - v1m)+(v2p - v2m)*(v2p - v2m);
      }

      else if (dir == IDIR) {
	v1p   = UU[MY][kp][jp][ip]*taup;
	v1m   = UU[MY][km][jm][im]*taum;
	v2p   = UU[MZ][kp][jp][ip]*taup;
	v2m   = UU[MZ][km][jm][im]*taum;
	diff = (v1p - v1m)*(v1p - v1m)+(v2p - v2m)*(v2p - v2m);
      }
      grad[k][j][i] = sqrt(diff)/v2;

    }
    else {
      grad[k][j][i] = 0.0;
    }
  }}}

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<nvar ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}


