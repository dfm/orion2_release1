/*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(05/26/09): Add magnetic pressure gradient as another variable
*                     for refinement.
*    2) PS(06/04/09): Add a geometrical refinement criterion.
*    3) PS(09/24/09): Modify refinement on density base on input 
*                     parameteraux[mg].
*/

#include <cstdio>
#include <string>
using std::string;

#include "PatchPluto.H"
#include "LoHiSide.H"

void PatchPluto::GETRELGRAD(FArrayBox&   gFab,
                            FArrayBox&   UFab,
                            const int    dir,
                            const Box&   b)
{
  CH_assert(m_isDefined);

  int ref_var;
  int nx, ny, nz, nv;
  int i, j, k;
  int ip, im, jp, jm, kp, km;
  int ioff, joff, koff;
  int ibeg, iend, jbeg, jend, kbeg, kend;
  int ilobeg, iloend, jlobeg, jloend, klobeg, kloend;
  int ihibeg, ihiend, jhibeg, jhiend, khibeg, khiend;
 
  double diff, aver;
  double dp, dm, den;
  double ***UU[NVAR];
  double ***q;
  double ***grad;
  double taup, tau, taum;
  double Kp, K, Km;
  double prp, pr, prm;
  double v1p, v1, v1m;
  double dv;
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
  double magpp=0.0,magpm=0.0;
// PS: aux[mg] = pi/(16G) = 7.32422e-2 for jden=3.0
  double jden=aux[mg]*0.01/(m_dx*m_dx);
#endif

  ref_var = DN;
  kbeg = kend = 0;

  D_EXPAND(ibeg = UFab.loVect()[IDIR]; 
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];   
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];   
           kend = UFab.hiVect()[KDIR]; );

  nx = iend - ibeg + 1;
  ny = jend - jbeg + 1;
  nz = kend - kbeg + 1;
  
  for (nv=0 ; nv<NVAR ; nv ++)
    UU[nv] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,UFab.dataPtr(nv));
//    UU[nv] = chmatrix3(nz,ny,nx,&UFab.dataPtr(0)[nv*nz*ny*nx]);


  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  grad = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(dir));

  D_EXPAND(ibeg  = b.loVect()[IDIR];
           iend  = b.hiVect()[IDIR]; ,
           jbeg  = b.loVect()[JDIR];
           jend  = b.hiVect()[JDIR]; ,
           kbeg  = b.loVect()[KDIR];
           kend  = b.hiVect()[KDIR]; );

  ioff = (dir == IDIR);
  joff = (dir == JDIR); 
  koff = (dir == KDIR);

  q = UU[ref_var];   

  for (k = kbeg; k <= kend; k++) { kp = k + koff; km = k - koff;
  for (j = jbeg; j <= jend; j++) { jp = j + joff; jm = j - joff;
  for (i = ibeg; i <= iend; i++) { ip = i + ioff; im = i - ioff;

/*  -- Shock Detection -- 

    taup = 1.0/UU[DN][kp][jp][ip];
    tau  = 1.0/UU[DN][k ][j ][i ];
    taum = 1.0/UU[DN][km][jm][im];

    v1p = UU[M1][kp][jp][ip]*taup;
    v1  = UU[M1][k ][j ][i ]*tau ;
    v1m = UU[M1][km][jm][im]*taum;

    D_EXPAND(Kp  = 0.5*UU[MX][kp][jp][ip]*UU[MX][kp][jp][ip]; ,
             Kp += 0.5*UU[MY][kp][jp][ip]*UU[MY][kp][jp][ip]; ,
             Kp += 0.5*UU[MZ][kp][jp][ip]*UU[MZ][kp][jp][ip];)

    D_EXPAND(K  = 0.5*UU[MX][k][j][i]*UU[MX][k][j][i]; ,
             K += 0.5*UU[MY][k][j][i]*UU[MY][k][j][i]; ,
             K += 0.5*UU[MZ][k][j][i]*UU[MZ][k][j][i];)

    D_EXPAND(Km  = 0.5*UU[MX][km][jm][im]*UU[MX][km][jm][im]; ,
             Km += 0.5*UU[MY][km][jm][im]*UU[MY][km][jm][im]; ,
             Km += 0.5*UU[MZ][km][jm][im]*UU[MZ][km][jm][im];)

    prp = (gmm - 1.0)*(UU[EN][kp][jp][ip] - Kp*taup);
    pr  = (gmm - 1.0)*(UU[EN][k ][j ][i ] - K *tau);
    prm = (gmm - 1.0)*(UU[EN][km][jm][im] - Km*taum);

    dp = fabs(prp - prm)/(dmin(pr,prm));
    dv = v1p - v1m;

    if (dp > 0.33 && dv < 0.1){
      grad[k][j][i] = 1.0/3.0;
    }else{
      grad[k][j][i] = 0.0;
    }    
*/
/*
    dp  = q[kp][jp][ip] - q[k][j][i];
    dm  = q[k][j][i] - q[km][jm][im];
   
    den  = fabs(q[kp][jp][ip]) + fabs(q[km][jm][im]) + fabs(q[k][j][i]);
    grad[k][j][i] = fabs(dp - dm)/(fabs(dp) + fabs(dm) + 0.01*den + 1.e-8);
*/    

   #if GEOMETRY == CYLINDRICAL
      diff = UU[ref_var][kp][jp][ip]/(ip+0.5) - UU[ref_var][km][jm][im]/(im+0.5);
      aver = UU[ref_var][kp][jp][ip]/(ip+0.5) + UU[ref_var][km][jm][im]/(im+0.5);
   #else
      diff = UU[ref_var][kp][jp][ip] - UU[ref_var][km][jm][im];
      aver = UU[ref_var][kp][jp][ip] + UU[ref_var][km][jm][im];
   #endif

   grad[k][j][i] = diff/aver;

  // PS: Flux-ct scheme

#ifdef STAGGERED_MHD
    D_EXPAND(magpp  = UU[BX][kp][jp][ip]*UU[BX][kp][jp][ip]; ,
             magpp += UU[BY][kp][jp][ip]*UU[BY][kp][jp][ip]; ,
             magpp += UU[BZ][kp][jp][ip]*UU[BZ][kp][jp][ip];)

    D_EXPAND(magpm  = UU[BX][km][jm][im]*UU[BX][km][jm][im]; ,
             magpm += UU[BY][km][jm][im]*UU[BY][km][jm][im]; ,
             magpm += UU[BZ][km][jm][im]*UU[BZ][km][jm][im];)
   diff = sqrt(magpp) - sqrt(magpm);
   aver = sqrt(magpp) + sqrt(magpm);
   if (abs(aver) > 1.0e-10) grad[k][j][i] += diff/aver;
#endif

#if 0
   //PS: Jeans criterion (assume G=0.04) 
   if (UU[ref_var][k][j][i] > jden) grad[k][j][i] = 10.0;
   //   grad[k][j][i] += UU[ref_var][k][j][i]*0.25;
#endif
  }}}

  D_EXPAND(ibeg = UFab.loVect()[IDIR];
           iend = UFab.hiVect()[IDIR]; ,
           jbeg = UFab.loVect()[JDIR];
           jend = UFab.hiVect()[JDIR]; ,
           kbeg = UFab.loVect()[KDIR];
           kend = UFab.hiVect()[KDIR]; );

  for (nv=0 ; nv<NVAR ; nv ++)
    free_chmatrix(UU[nv],kbeg,kend,jbeg,jend,ibeg,iend);

  D_EXPAND(ibeg = gFab.loVect()[IDIR];
           iend = gFab.hiVect()[IDIR]; ,
           jbeg = gFab.loVect()[JDIR];
           jend = gFab.hiVect()[JDIR]; ,
           kbeg = gFab.loVect()[KDIR];
           kend = gFab.hiVect()[KDIR]; );

  free_chmatrix(grad,kbeg,kend,jbeg,jend,ibeg,iend);
}

void PatchPluto::MAGNITUDE(FArrayBox&         gmagFab,
                           FArrayBox&         gFab,
                           const Box&         bbox)
{

 int i, j, k, idim;
 int ioff, joff, koff;
 int ibeg, iend, jbeg, jend, kbeg, kend;
 
 double sum,cur;

 double ***gradmag;
 double ***grad[DIMENSIONS];

 kbeg = kend = 0;

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );


 gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

 D_EXPAND(ibeg = gFab.loVect()[IDIR];
          iend = gFab.hiVect()[IDIR]; ,
          jbeg = gFab.loVect()[JDIR];
          jend = gFab.hiVect()[JDIR]; ,
          kbeg = gFab.loVect()[KDIR];
          kend = gFab.hiVect()[KDIR]; );

 for (idim=0; idim < DIMENSIONS; idim++)
  grad[idim] = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gFab.dataPtr(idim));  

 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );


 for (k=kbeg ; k <= kend ; k++) {
 for (j=jbeg ; j <= jend ; j++) {
 for (i=ibeg ; i <= iend ; i++) {

  sum = 0.0;
  for (idim=0; idim < DIMENSIONS; idim++) {
   cur = grad[idim][k][j][i];
   sum = sum + cur*cur; 
  }

  gradmag[k][j][i] = sqrt(sum);

 }}}

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

 D_EXPAND(ibeg = gFab.loVect()[IDIR];
          iend = gFab.hiVect()[IDIR]; ,
          jbeg = gFab.loVect()[JDIR];
          jend = gFab.hiVect()[JDIR]; ,
          kbeg = gFab.loVect()[KDIR];
          kend = gFab.hiVect()[KDIR]; );

 for (idim=0; idim < DIMENSIONS; idim++)
  free_chmatrix(grad[idim],kbeg,kend,jbeg,jend,ibeg,iend);

}

/*:PS Geometrical refinement criterion */
void PatchPluto::GEOMREF(FArrayBox&         gmagFab,
                         const Box&         bbox)
{

 int i, j, k, idim;
 int ioff, joff, koff, nxtot, nytot, nztot;
 int ibeg, iend, jbeg, jend, kbeg, kend;
 
 double x1, x2, x3, dx;

 double ***gradmag;

 kbeg = kend = 0;

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );


 gradmag = chmatrix(kbeg,kend,jbeg,jend,ibeg,iend,gmagFab.dataPtr(0));

 D_EXPAND(ibeg = bbox.loVect()[IDIR];
          iend = bbox.hiVect()[IDIR]; ,
          jbeg = bbox.loVect()[JDIR];
          jend = bbox.hiVect()[JDIR]; ,
          kbeg = bbox.loVect()[KDIR];
          kend = bbox.hiVect()[KDIR]; );

 dx = m_dx;
 nxtot = iend - ibeg + 1;
 nytot = jend - jbeg + 1;
 nztot = kend - kbeg + 1;

 for (k=0 ; k < nztot ; k++) {
 for (j=0 ; j < nytot ; j++) {
 for (i=0 ; i < nxtot ; i++) {

   x1 = double(i + ibeg + 0.5)*dx;
   x2 = double(j + jbeg + 0.5)*dx;
   x3 = double(k + kbeg + 0.5)*dx;

   x1 += DOM_XBEG[IDIR];
   x2 += DOM_XBEG[JDIR];
   x3 += DOM_XBEG[KDIR];
   gradmag[k+kbeg][j+jbeg][i+ibeg] = 0.0;

   //   if (x1 > 0.04 && x1 < 0.24 && x2 > -0.12 && x2 < 0.12 && x3 > -0.12 && x3 < 0.12) {
   //   if (x1 > -0.12 && x1 <= 0.12 && x2 > 0.24 && x2 <= 0.46 && x3 > -0.12 && x3 <= 0.12) {
   if (x1 > -0.48 && x1 <= 0.48 && x2 > -0.24 && x2 <= 0.24 && x3 > -0.24 && x3 <= 0.24) {
     gradmag[k+kbeg][j+jbeg][i+ibeg] = 1.0;
   }

 }}}

 D_EXPAND(ibeg = gmagFab.loVect()[IDIR];
          iend = gmagFab.hiVect()[IDIR]; ,
          jbeg = gmagFab.loVect()[JDIR];
          jend = gmagFab.hiVect()[JDIR]; ,
          kbeg = gmagFab.loVect()[KDIR];
          kend = gmagFab.hiVect()[KDIR]; );

 free_chmatrix(gradmag,kbeg,kend,jbeg,jend,ibeg,iend);

}




