#include"orion2.h"

/* ****************************************************************** */
void BODY_FORCE (double *v, 
                 double *g, double *phi, double *Omega,
                 double x1, double x2, double x3)
/*
 *
 *  PURPOSE
 *
 *   Include body force terms. These are:
 *
 *    - gravitational acceleration vector
 *    - gravitational potential
 *    - Coriolis vector
 *
 *  
 ******************************************************************* */
{

  g[IDIR] = 0.0;
  g[JDIR] = 0.0;
  g[KDIR] = 0.0;

  *phi = 0.0;

  Omega[IDIR] = 0.0;
  Omega[JDIR] = 0.0;
  Omega[KDIR] = 0.0;

}

