#include "orion2.h"

static void SHOW_DOMAIN_DECOMPOSITION (Grid *);

/* ***************************************************************** */
void INITIALIZE(int argc, char *argv[], Data *data, 
                Input *input, Grid *grid, real *dx_min_glob,
                Cmd_Line *cmd_line)
/*!
 *
 *
 *  Initialize several things before starting integration:
 *
 *   - parse command line options 
 *   - memory allocation 
 *   - grid generation
 *   - domain decomposition
 *  
 * \param input structure containing run-time parameters to be read;
 * 
 * \param data  a structure containing three-dimensional fields; 
 *
 * \param grid  array of grid structures;
 *
 * \param dx_min_glob minimum cell length.
 *
 * \param cmd_line pointer to a structure of cmd-line args.
 *
 * \author A. Mignone (mignone@to.astro.it)
 *
 *
 ******************************************************************** */
{
  real scrh;
  int  i, j, k, idim, nv;
  int  nx, ny, nz, nghost;
  int  gsize[DIMENSIONS], lsize[DIMENSIONS];
  int  beg[DIMENSIONS], end[DIMENSIONS];
  int  gbeg[DIMENSIONS], gend[DIMENSIONS];
  int  lbeg[DIMENSIONS], lend[DIMENSIONS];
  int  is_gbeg[DIMENSIONS], is_gend[DIMENSIONS];
  int  ghosts[DIMENSIONS];
  int  periods[DIMENSIONS];
  int  pardim[DIMENSIONS];
  int  procs[DIMENSIONS];
  char ini_file[128];
  real dx_min[3];
  Output *output;
  #ifdef PARALLEL
   MPI_Datatype rgb_type;
   MPI_Datatype Float_Vect_type;
  #endif

  sprintf (ini_file,"orion2.ini");

  #ifdef PARALLEL

 /* ----------------------------------------
     By default, parallelize all dimensions 
    ---------------------------------------- */

   cmd_line->parallel_dim[IDIR] = YES;
   cmd_line->parallel_dim[JDIR] = YES;
   cmd_line->parallel_dim[KDIR] = YES;

 /* ---------------------------------------
     By default, do not force periodicity
     along all boundaries
    --------------------------------------- */

   GET_OPT(argc, argv, ini_file, cmd_line);
   
   PRINT_CONFIG();

   if (prank == 0) SETUP (input, ini_file);

   MPI_Bcast (input,  sizeof (struct INPUT) , MPI_BYTE, 0, MPI_COMM_WORLD);

   nghost = GET_NGHOST(input);
   MPI_Allreduce (&nghost, &idim, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
   nghost = idim;
   
   for (idim = 0; idim < DIMENSIONS; idim++) {
     gsize[idim]   = input->npoint[idim];
     ghosts[idim]  = nghost;
     periods[idim] = input->lft_bound_side[idim] == PERIODIC;
     pardim[idim]  = cmd_line->parallel_dim[idim];
   }

   AL_Sz_init (MPI_COMM_WORLD, &SZ);
   AL_Set_type (MPI_ORION2_REAL, 1, SZ);
   AL_Set_dimensions (DIMENSIONS, SZ);
   AL_Set_global_dim (gsize, SZ);
   AL_Set_ghosts (ghosts, SZ);
   AL_Set_periodic_dim (periods, SZ);
   AL_Set_parallel_dim (pardim, SZ);

   AL_Decompose (SZ, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ, lsize);
   AL_Get_bounds (SZ, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ, is_gbeg, is_gend);

/* ---- float distributed array descriptor ---- */

   AL_Sz_init (MPI_COMM_WORLD, &SZ_float);
   AL_Set_type (MPI_FLOAT, 1, SZ_float);
   AL_Set_dimensions (DIMENSIONS, SZ_float);
   AL_Set_global_dim (gsize, SZ_float);
   AL_Set_ghosts (ghosts, SZ_float);
   AL_Set_periodic_dim (periods, SZ_float);
   AL_Set_parallel_dim (pardim, SZ_float);

   AL_Decompose (SZ_float, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ_float, lsize);
   AL_Get_bounds (SZ_float, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ_float, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ_float, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ_float, is_gbeg, is_gend);

/* ---- char distributed array descriptor ---- */

   AL_Sz_init (MPI_COMM_WORLD, &SZ_char);
   AL_Set_type (MPI_CHAR, 1, SZ_char);
   AL_Set_dimensions (DIMENSIONS, SZ_char);
   AL_Set_global_dim (gsize, SZ_char);
   AL_Set_ghosts (ghosts, SZ_char);
   AL_Set_periodic_dim (periods, SZ_char);
   AL_Set_parallel_dim (pardim, SZ_char);

   AL_Decompose (SZ_char, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ_char, lsize);
   AL_Get_bounds (SZ_char, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ_char, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ_char, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ_char, is_gbeg, is_gend);

/* ---- short int distributed array descriptor ---- */
/*
   AL_Sz_init (MPI_COMM_WORLD, &SZ_short);
   AL_Set_type (MPI_SHORT, 1, SZ_short);
   AL_Set_dimensions (DIMENSIONS, SZ_short);
   AL_Set_global_dim (gsize, SZ_short);
   AL_Set_ghosts (ghosts, SZ_short);
   AL_Set_periodic_dim (periods, SZ_short);
   AL_Set_parallel_dim (pardim, SZ_short);

   AL_Decompose (SZ_short, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ_short, lsize);
   AL_Get_bounds (SZ_short, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ_short, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ_short, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ_short, is_gbeg, is_gend);
*/
/* ---- RGB distributed array descriptor ---- */
/*
   MPI_Type_contiguous (3, MPI_UNSIGNED_CHAR, &rgb_type);
   MPI_Type_commit (&rgb_type);
 
   AL_Sz_init (MPI_COMM_WORLD, &SZ_rgb);
   AL_Set_type (MPI_UNSIGNED_CHAR, 3, SZ_rgb);
   AL_Set_dimensions (DIMENSIONS, SZ_rgb);
   AL_Set_global_dim (gsize, SZ_rgb);
   AL_Set_ghosts (ghosts, SZ_rgb);
   AL_Set_periodic_dim (periods, SZ_rgb);
   AL_Set_parallel_dim (pardim, SZ_rgb);

   AL_Decompose (SZ_rgb, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ_rgb, lsize);
   AL_Get_bounds (SZ_rgb, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ_rgb, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ_rgb, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ_rgb, is_gbeg, is_gend);
*/
/* ---- Float_Vec distributed array descriptor ---- */

   MPI_Type_contiguous (3, MPI_FLOAT, &Float_Vect_type);
   MPI_Type_commit (&Float_Vect_type);
 
   AL_Sz_init (MPI_COMM_WORLD, &SZ_Float_Vect);
   AL_Set_type (MPI_FLOAT, 3, SZ_Float_Vect);
   AL_Set_dimensions (DIMENSIONS, SZ_Float_Vect);
   AL_Set_global_dim (gsize, SZ_Float_Vect);
   AL_Set_ghosts (ghosts, SZ_Float_Vect);
   AL_Set_periodic_dim (periods, SZ_Float_Vect);
   AL_Set_parallel_dim (pardim, SZ_Float_Vect);

   AL_Decompose (SZ_Float_Vect, procs, AL_AUTO_DECOMP);
   AL_Get_local_dim (SZ_Float_Vect, lsize);
   AL_Get_bounds  (SZ_Float_Vect, beg, end, ghosts, AL_C_INDEXES);
   AL_Get_lbounds (SZ_Float_Vect, lbeg, lend, ghosts, AL_C_INDEXES);
   AL_Get_gbounds (SZ_Float_Vect, gbeg, gend, ghosts, AL_C_INDEXES);
   AL_Is_boundary (SZ_Float_Vect, is_gbeg, is_gend);

   for (idim = 0; idim < DIMENSIONS; idim++) {
     grid[idim].nghost      = nghost;
     grid[idim].np_tot      = lsize[idim] + 2*ghosts[idim];
     grid[idim].np_int      = lsize[idim];
     grid[idim].np_tot_glob = input->npoint[idim] + 2*ghosts[idim];
     grid[idim].np_int_glob = input->npoint[idim];
     grid[idim].beg         = beg[idim];
     grid[idim].end         = end[idim];
     grid[idim].gbeg        = gbeg[idim];
     grid[idim].gend        = gend[idim];
     grid[idim].lbeg        = lbeg[idim];
     grid[idim].lend        = lend[idim];
     grid[idim].lbound = input->lft_bound_side[idim]*is_gbeg[idim];
     grid[idim].rbound = input->rgt_bound_side[idim]*is_gend[idim];
     grid[idim].nproc  = procs[idim];
   }

   /*  ----  Find number of processors  ---- */

   AL_Get_size(SZ, &k);
   AL_Get_size(SZ_float, &k);

   NPROCESSORS = k;

  #else

/* -----------------------------------------------------
               Serial Initialization
   ----------------------------------------------------- */

   GET_OPT (argc, argv, ini_file, cmd_line);
   PRINT_CONFIG ();
   SETUP (input, ini_file);
   nghost = GET_NGHOST(input);

   for (idim = 0; idim < DIMENSIONS; idim++) {
     grid[idim].nghost  = nghost;
     grid[idim].np_int  = grid[idim].np_int_glob = input->npoint[idim];
     grid[idim].np_tot  = grid[idim].np_tot_glob = input->npoint[idim] + 2*nghost;
     grid[idim].beg     = grid[idim].gbeg = grid[idim].lbeg = nghost;
     grid[idim].end     = grid[idim].gend = grid[idim].lend = (grid[idim].lbeg - 1) + grid[idim].np_int;
     grid[idim].lbound  = input->lft_bound_side[idim];
     grid[idim].rbound  = input->rgt_bound_side[idim];
     grid[idim].nproc   = 1;
   }
   
   NPROCESSORS = 1;

  #endif

/* ---------------------------------------------------
                Grid Generation
   --------------------------------------------------- */

  SETGRID (input, grid);
  WHERE (-1, grid);     /* -- store grid inside the "WHERE" 
                              function for subsequent calls -- */
  if (cmd_line->makegrid == YES) {
    print1 ("\n> Done < \n");
    QUIT_ORION2(0);
  }

/* ------------------------------------------------
         initialize global variables
   ------------------------------------------------ */

  delta_t   = input->first_dt;
  glob_time = MAX_MACH_NUMBER = 0.0;
  CFL       = input->cfl;
  MAX_RIEM  = 0;
  USED_MEMORY = 0;

  IBEG = grid[IDIR].lbeg;
  JBEG = grid[JDIR].lbeg;
  KBEG = grid[KDIR].lbeg;
  IEND = grid[IDIR].lend;
  JEND = grid[JDIR].lend;
  KEND = grid[KDIR].lend;

  NX = grid[IDIR].np_int;
  NY = grid[JDIR].np_int;
  NZ = grid[KDIR].np_int;

  NX_TOT = grid[IDIR].np_tot; 
  NY_TOT = grid[JDIR].np_tot;
  NZ_TOT = grid[KDIR].np_tot;

/* ---------------------------------------
     get the maximum number of points 
     among all directions
   --------------------------------------- */

  NMAX_POINT = dmax(NX_TOT, NY_TOT);
  NMAX_POINT = dmax(NMAX_POINT, NZ_TOT);

/* --------------------------------------------------------------------
        FIND THE MINUM PHYSICAL CELL LENGTH IN EACH DIMENSIONS
   -------------------------------------------------------------------- */

  for (idim = 0; idim < DIMENSIONS; idim++)  dx_min[idim] = 1.e30;

  for (i = IBEG; i <= IEND; i++) {
  for (j = JBEG; j <= JEND; j++) {
  for (k = KBEG; k <= KEND; k++) {

    scrh = Length_1(i, j, k, grid);
    dx_min[0] = dmin (dx_min[0], scrh);

    scrh = Length_2(i, j, k, grid);
    dx_min[1] = dmin (dx_min[1], scrh);
 
    scrh = Length_3(i, j, k, grid); 
    dx_min[2] = dmin (dx_min[2], scrh);
     
  }}}

  #ifdef PARALLEL
   MPI_Allreduce (dx_min, dx_min_glob, 3, MPI_ORION2_REAL, 
                  MPI_MIN, MPI_COMM_WORLD);
  #else
   dx_min_glob[0] = dx_min[0];
   dx_min_glob[1] = dx_min[1];
   dx_min_glob[2] = dx_min[2];
  #endif

  grid[IDIR].dl_min = dx_min_glob[IDIR];
  grid[JDIR].dl_min = dx_min_glob[JDIR];
  grid[KDIR].dl_min = dx_min_glob[KDIR];

/* ------------------------------------------------------
    Copy user defined parameters into global array aux 
   ------------------------------------------------------ */

  for (nv = 0; nv < USER_DEF_PARAMETERS; nv++) aux[nv] = input->aux[nv];

/* ------------------------------------------------------------
          Allocate memory for 3D data arrays
   ------------------------------------------------------------ */

  print1 ("\n> memory allocation\n");
  data->Vc = Array_4D(NVAR, NZ_TOT, NY_TOT, NX_TOT, double);
 
  #ifdef STAGGERED_MHD
   data->Vs = Array_4D(DIMENSIONS, NZ_TOT, NY_TOT, NX_TOT, double);
  #endif  

  #if SAVE_VEC_POT == YES
   D_EXPAND(                                               ,
     data->A3 = Array_3D(NZ_TOT, NY_TOT, NX_TOT, double);  , 
     data->A1 = Array_3D(NZ_TOT, NY_TOT, NX_TOT, double);  
     data->A2 = Array_3D(NZ_TOT, NY_TOT, NX_TOT, double);  
   )
  #endif

  data->flag = Array_3D(NZ_TOT, NY_TOT, NX_TOT, char);

/* ------------------------------------------------------------
              Assign initial conditions
   ------------------------------------------------------------ */

  STARTUP (data, cmd_line->ext, grid);

/* ------------------------------------------------------------
      Initialize MAXC, required by GLM-Divergence Cleaning 
   ------------------------------------------------------------ */

  #ifdef PSI_GLM
   MAXC = input->cfl*grid[IDIR].dl_min/input->first_dt;
   #if PHYSICS == RMHD
    MAXC = 1.0;
   #endif
/*   MAXC = GLM_INIT(data, grid);   */  
  #endif

/* ------------------------------------------------------------ 
    Set output attributes (names, images, number of outputs...)
   ------------------------------------------------------------ */

  SET_OUTPUT (data, input);

  /* -----------------------------------
        print normalization units
     ----------------------------------- */

  #if INCLUDE_COOLING != NO
   print1 ("> Normalization Units:\n\n");
   print1 ("  -------------------------------------------------------- \n");
   print1 ("  * Density:    %8.3e (gr/cm^3), %8.3e (1/cm^3)\n",
           UNIT_DENSITY,UNIT_DENSITY/CONST_mp);
   print1 ("  * Pressure:   %8.3e (dyne/cm^2)\n",
           UNIT_DENSITY*UNIT_VELOCITY*UNIT_VELOCITY);
   print1 ("  * Velocity:   %8.3e (cm/s)\n",UNIT_VELOCITY);
   print1 ("  * Length:     %8.3e (cm)\n",UNIT_LENGTH);
   print1 ("  * Time:       %8.3e (sec), %8.3e (yrs) \n",
        UNIT_LENGTH/UNIT_VELOCITY, UNIT_LENGTH/UNIT_VELOCITY/86400./365.);

   #if PHYSICS == MHD || PHYSICS == RMHD
    print1 ("  * Mag Field:  %8.3e (Gauss)\n",
             UNIT_VELOCITY*sqrt(4.0*CONST_PI*UNIT_DENSITY));
   #endif

   print1 ("  -------------------------------------------------------- \n");
    
  #endif

  SHOW_DOMAIN_DECOMPOSITION(grid);
}


/* ************************************************************************ */
void SHOW_DOMAIN_DECOMPOSITION(struct GRID *GXYZ)
/* 
 *
 *
 *
 *
 ************************************************************************** */
#define MAX_NPROCESSORS 2048
{

  int i, j, k, ngh;
  int i0, i1, j0, j1, k0, k1;
  int nxp, nyp, nzp;
  int nghx, nghy, nghz;

  int    i0_proc[MAX_NPROCESSORS], i1_proc[MAX_NPROCESSORS];
  int    j0_proc[MAX_NPROCESSORS], j1_proc[MAX_NPROCESSORS];
  int    k0_proc[MAX_NPROCESSORS], k1_proc[MAX_NPROCESSORS];

  real x0, x1, y0, y1, z0, z1;

  real x0_proc[MAX_NPROCESSORS], x1_proc[MAX_NPROCESSORS];
  real y0_proc[MAX_NPROCESSORS], y1_proc[MAX_NPROCESSORS];
  real z0_proc[MAX_NPROCESSORS], z1_proc[MAX_NPROCESSORS];

  Grid *Gx, *Gy, *Gz;
  
  if (NPROCESSORS > MAX_NPROCESSORS) {
    print1 ("! Error: exceeded max number of processors\n");
    QUIT_ORION2(1);
  }

  Gx = GXYZ;
  Gy = GXYZ + 1;
  Gz = GXYZ + 2;
  
#ifdef PARALLEL  
  nxp = Gx->np_tot;
  nyp = Gy->np_tot;
  nzp = Gz->np_tot;

  i0 = nghx = Gx->nghost;
  j0 = nghy = Gy->nghost;
  k0 = nghz = Gz->nghost;

  i1 = i0 + Gx->np_int - 1;    
  j1 = j0 + Gy->np_int - 1;
  k1 = k0 + Gz->np_int - 1;

  x0 = Gx->xl[i0]; x1 = Gx->xr[i1];
  y0 = Gy->xl[j0]; y1 = Gy->xr[j1];
  z0 = Gz->xl[k0]; z1 = Gz->xr[k1];

  i0  = Gx->beg; i1 += Gx->beg - nghx;
  j0  = Gy->beg; j1 += Gy->beg - nghy;
  k0  = Gz->beg; k1 += Gz->beg - nghz;

  D_EXPAND(
    MPI_Gather (&x0, 1, MPI_ORION2_REAL, x0_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);
    MPI_Gather (&x1, 1, MPI_ORION2_REAL, x1_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);  ,
    
    MPI_Gather (&y0, 1, MPI_ORION2_REAL, y0_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);
    MPI_Gather (&y1, 1, MPI_ORION2_REAL, y1_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);  ,
    
    MPI_Gather (&z0, 1, MPI_ORION2_REAL, z0_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);
    MPI_Gather (&z1, 1, MPI_ORION2_REAL, z1_proc, 1, MPI_ORION2_REAL, 0, MPI_COMM_WORLD);
  )

  D_EXPAND(
    MPI_Gather (&i0, 1, MPI_INT, i0_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather (&i1, 1, MPI_INT, i1_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);  ,
    
    MPI_Gather (&j0, 1, MPI_INT, j0_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather (&j1, 1, MPI_INT, j1_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);  ,
    
    MPI_Gather (&k0, 1, MPI_INT, k0_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather (&k1, 1, MPI_INT, k1_proc, 1, MPI_INT, 0, MPI_COMM_WORLD);
  )

  print1 ("> Domain Decomposition (%d procs):\n\n", NPROCESSORS);

  for (k = 0; k < NPROCESSORS; k++){ 
    print1 ("  - Proc # %d, X1: [%f, %f], %d pt\n",k, x0_proc[k], x1_proc[k], Gx->np_int);
    print1 ("              X2: [%f, %f], %d pt\n",    y0_proc[k], y1_proc[k], Gy->np_int);
    #if DIMENSIONS == 3
     print1 ("              X3: [%f, %f], %d pt\n\n",  z0_proc[k], z1_proc[k], Gz->np_int);
    #endif

  }

  MPI_Barrier (MPI_COMM_WORLD);
#endif
  print1 ("\n");
}
