/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Pluto's root directory.
*
*   Modification: Orional code based on PLUTO3.0
*   1) PS(05/12/11): Add GRAVITY_FORCE function to include gravity 
*                    contribution computed in Chombo.
*
*/

#include"orion2.h"

static void GRADIENT_1D (double *, double *, int, int, Grid *);

/* ***************************************************************** */
Force *ACCELERATION (double **vv, int kmp, int beg, int end, 
                     Grid *grid)
/*
 *
 * Return the 'kmp' component of the acceleration vector
 * along a 1-D sweep in the DIR direction.
 *
 *
 *  ax = gx - dphi_dx + (wz*vy - wy*vz);
 *  ay = gy - dphi_dy + (wx*vz - wz*vx);
 *  az = gz - dphi_dz + (wy*vx - wx*vy);
 *
 ******************************************************************* */
{
  int    i;
  static int use_phi;
  static double *phi;
  double g[3], Om[3], fcor;
  double *x1, *x2, *x3;
  double *x1r, *x2r, *x3r, *v;
  double phimax, phimin;
  static Force F;

  x1 = grid[IDIR].x; x1r = grid[IDIR].xr;
  x2 = grid[JDIR].x; x2r = grid[JDIR].xr;
  x3 = grid[KDIR].x; x3r = grid[KDIR].xr;

/* --------------------------------------------
         Allocate memory areas
   -------------------------------------------- */

  if (F.a == NULL){
    F.a = Array_1D(NMAX_POINT, double);
    phi = Array_1D(NMAX_POINT, double);    
    F.d = Array_1D(NMAX_POINT, double);    
    F.g = Array_1D(NMAX_POINT, double);

  /* -- check if need to differentiate potential -- */

    phimax = 0.0; phimin = 1.e37;
    for (i = beg; i <= end; i++){
      BODY_FORCE(vv[i], g, phi + i, Om, 
                 x1[i], x2[(JBEG+JEND)/2], x3[(KBEG+KEND)/2]);
      phimax = dmax(fabs(phi[i]),phimax);
      phimin = dmin(fabs(phi[i]),phimin);
    }
    if (fabs(phimin - phimax) < 1.e-8) use_phi = NO;
    else use_phi = YES;

    for (i = 0; i < NMAX_POINT; i++) phi[i] = F.d[i] = 0.0;
  }

/* -----------------------------------------------------------------
     compute phi (if necessary) at cell _interfaces_  and dphi/dx  
   ----------------------------------------------------------------- */

  if (use_phi && kmp == DIR){
    if (DIR == IDIR){ 
      for (i = beg - 1; i <= end + 1; i++){
        BODY_FORCE (vv[i], g, phi + i, Om, x1r[i], x2[*NY_PT], x3[*NZ_PT]);
      }
    }else if (DIR == JDIR){
      for (i = beg - 1; i <= end + 1; i++){
        BODY_FORCE (vv[i], g, phi + i, Om, x1[*NX_PT], x2r[i], x3[*NZ_PT]);
      }
    }else if (DIR == KDIR){
      for (i = beg - 1; i <= end + 1; i++){
        BODY_FORCE (vv[i], g, phi + i, Om, x1[*NX_PT], x2[*NY_PT], x3r[i]);
      }
    }
    GRADIENT_1D (phi, F.d, beg, end, grid);
  }else{
    for (i = 0; i < NMAX_POINT; i++) phi[i] = F.d[i] = 0.0;
  }

/* -------------------------------------------------------------
        compute kmp-th component of acceleration vector 
   ------------------------------------------------------------- */

  if (kmp == IDIR){
    for (i = beg; i <= end; i++) { v = vv[i];
      BODY_FORCE (v, g, &phimax, Om, x1[i], x2[*NY_PT], x3[*NZ_PT]);  
      fcor = EXPAND(0.0, + Om[KDIR]*v[VY], - Om[JDIR]*v[VZ]);
      F.a[i] = (F.g[i] = g[kmp]) + 2.0*fcor - F.d[i];
    }
  }else if (kmp == JDIR){
    for (i = beg; i <= end; i++) { v = vv[i];
      BODY_FORCE (v, g, &phimax, Om, x1[*NX_PT], x2[i], x3[*NZ_PT]);
      fcor = EXPAND(- Om[KDIR]*v[VX],   , + Om[IDIR]*v[VZ]);  
      F.a[i] = (F.g[i] = g[kmp]) + 2.0*fcor - F.d[i];
    }
  }else if (kmp == KDIR){
    for (i = beg; i <= end; i++) { v = vv[i];
      BODY_FORCE (v, g, &phimax, Om, x1[*NX_PT], x2[*NY_PT], x3[i]);
      fcor = Om[JDIR]*v[VX] - Om[IDIR]*v[VY]; 
      F.a[i] = (F.g[i] = g[kmp]) + 2.0*fcor - F.d[i];
    }
  }

  return &F;
}

/* *********************************************************************  */
void ADD_BODY_FORCE (const State_1D *state, real dt, int beg, int end, 
                     Grid *grid)
/*! 
 * 
 * Add gravity contribution to the right hand side
 * of the conservative equations.
 * 
 *
 * \author A. Mignone (mignone@to.astro.it)
 * \date   Feb 26, 2008
 *
 *********************************************************************** */
{
  int    nv, i;
  real x1, x2, x3, scrh;
  real **v, **rhs;
  Force *f;

  rhs = state->rhs;
  v   = state->vh;
  f   = ACCELERATION (v, DIR, beg, end, grid);

  for (i = beg; i <= end; i++) {
    rhs[i][M1] += dt*v[i][DN]*f->a[i];
    #if EOS != ISOTHERMAL
     #if TIME_STEPPING == HANCOCK && PRIMITIVE_HANCOCK == NO
      rhs[i][EN] += dt*state->v[i][DN]*state->v[i][V1]*(f->g[i] - f->d[i]);   
     #else 
      rhs[i][EN] += dt*0.5*(state->flux[i][DN] + state->flux[i - 1][DN])*(f->g[i] - f->d[i]);   
     #endif
    #endif
  }

  #if DIMENSIONS == 2 && COMPONENTS == 3
   if (DIR == JDIR){
     f = ACCELERATION (v, KDIR, beg, end, grid);
     for (i = beg; i <= end; i++) {
       rhs[i][MZ] += dt*v[i][DN]*f->a[i];
       #if EOS != ISOTHERMAL
        rhs[i][EN] += dt*v[i][DN]*v[i][VZ]*f->g[i];
       #endif
     }
   }
  #endif
}

#if SELF_GRAVITY == YES || SINK_PARTICLES == YES
/* *********************************************************************  */
void GRAVITY_FORCE (const State_1D *state, real *gf1d,
                    real dt, int beg, int end, Grid *grid)
/*! 
 * 
 * Add gravity contribution, computed from Chombo, to the right hand side
 * of the conservative equations.
 * 
 *
 * \author PS Li
 * \date   Nov 24, 2010
 *
 *********************************************************************** */
{
  int    nv, i;
  real x1, x2, x3, scrh;
  real **v, **rhs;
  Force *f;

  rhs = state->rhs;
  v   = state->vh;

  for (i = beg; i <= end; i++) {
    rhs[i][M1] += dt*v[i][DN]*gf1d[i];
    #if EOS != ISOTHERMAL
     #if TIME_STEPPING == HANCOCK && PRIMITIVE_HANCOCK == NO
      rhs[i][EN] += dt*state->v[i][DN]*state->v[i][V1]*gf1d[i];   
     #else 
      rhs[i][EN] += dt*0.5*(state->flux[i][DN] + state->flux[i - 1][DN])*
                    gf1d[i];   
     #endif
    #endif
  }
}
#endif /* SELF_GRAVITY */

/* ****************************************************************** */
void GRADIENT_1D (double *phi, double *dphi, int beg, int end, Grid *grid)
/*
 *
 *
 * Using phi defined at cell interfaces, compute its gradient dphi 
 * at zone center.
 *
 *  Cartesian:    dphi/dx, dphi/dy, dphi/dz
 *  Cylindrical:  dphi/dr, dphi/dz
 *  Polar:        dphi/dr, 1/r * dphi/dphi, dphi/dz
 *  Spherical:    dphi/dr, 1/r * dphi/dtheta, 1/(r*sin(theta))*dphi/dphi
 * 
 ******************************************************************** */
{
  int    i;
  double r, th, r_1, *dx;

  dx = grid[DIR].dx;

  #if GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL
   for (i = beg; i <= end; i++){
     dphi[i] = (phi[i] - phi[i - 1])/dx[i];
   }
  #endif

  #if GEOMETRY == POLAR
   if (DIR == IDIR){
     for (i = beg; i <= end; i++){
       dphi[i] = (phi[i] - phi[i - 1])/dx[i];
     }
   }else if (DIR == JDIR){
     r_1 = 1.0/grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++){
       dphi[i] = r_1*(phi[i] - phi[i - 1])/dx[i];
     }
   }else if (DIR == KDIR){
     for (i = beg; i <= end; i++){
       dphi[i] = (phi[i] - phi[i - 1])/dx[i];
     }
   }
  #endif

  #if GEOMETRY == SPHERICAL
   if (DIR == IDIR){
     for (i = beg; i <= end; i++){
       dphi[i] = (phi[i] - phi[i - 1])/dx[i];
     }
   }else if (DIR == JDIR){
     r_1 = 1.0/grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++){
       dphi[i] = r_1*(phi[i] - phi[i - 1])/dx[i];
     }
   }else if (DIR == KDIR){
     r   = grid[IDIR].x[*NX_PT];
     th  = grid[JDIR].x[*NY_PT];
     r_1 = 1.0/(r*sin(th));
     for (i = beg; i <= end; i++){
       dphi[i] = r_1*(phi[i] - phi[i - 1])/dx[i];
     }
   }
  #endif

}
