/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Orion2's root directory.
*
*   Modification: Orional code based on PLUTO3.0
*   1) PS(08/06/10): Change log file name to orion2.
*
*/

#include "orion2.h"

/* ************************************************************ */
void ludcmp (real **a, int n, int *indx, real *d)
/*   LU decomposition routine
 *
 *
 *
 * 
 ************************************************************** */

#define TINY 1.0e-20;
{
  int i, imax, j, k;
  real big, dum, sum, temp;
  real *vv;

  vv = Array_1D (n, double);
  *d = 1.0;
  for (i = 0; i < n; i++) {
    big = 0.0;
    for (j = 0; j < n; j++)
      if ((temp = fabs (a[i][j])) > big)
        big = temp;
    if (big == 0.0) {
      print1 ("Singular matrix in routine LUDCMP - (i=%d, j=%d)",i,j);
      exit(1);
    }
    vv[i] = 1.0 / big;
  }
  for (j = 0; j < n; j++) {
    for (i = 0; i < j; i++) {
      sum = a[i][j];
      for (k = 0; k < i; k++)
        sum -= a[i][k] * a[k][j];
      a[i][j] = sum;
    }
    big = 0.0;
    for (i = j; i < n; i++) {
      sum = a[i][j];
      for (k = 0; k < j; k++)
        sum -= a[i][k] * a[k][j];
      a[i][j] = sum;
      if ((dum = vv[i] * fabs (sum)) >= big) {
        big = dum;
        imax = i;
      }
    }
    if (j != imax) {
      for (k = 0; k < n; k++) {
        dum = a[imax][k];
        a[imax][k] = a[j][k];
        a[j][k] = dum;
      }
      *d = -(*d);
      vv[imax] = vv[j];
    }
    indx[j] = imax;
    if (a[j][j] == 0.0)
      a[j][j] = TINY;
    if (j != n - 1) {
      dum = 1.0 / (a[j][j]);
      for (i = j + 1; i < n; i++)
        a[i][j] *= dum;
    }
  }
  free_array_1D (vv);

}
#undef TINY

/* **************************************************************** */
void lubksb (real **a, int n, int *indx, real b[])
/*
 *
 *
 *
 ****************************************************************** */
{
  int i, ii = 0, ip, j;
  real sum;

/*
  for (i = 0; i < n; i++) {
    if (b[i]!=b[i]) printf(" at input to lubksb: b[%d] = %12.6e\n",i,b[i]);
  }
*/  
  for (i = 0; i < n; i++) {
    ip = indx[i];
    sum = b[ip];
    b[ip] = b[i];
    if (ii)
      for (j = ii - 1; j <= i - 1; j++)
        sum -= a[i][j] * b[j];
    else if (sum)
      ii = i + 1;
    b[i] = sum;
  }
  for (i = n - 1; i >= 0; i--) {
    sum = b[i];
    for (j = i + 1; j < n; j++) {
      sum -= a[i][j] * b[j];
/* if (sum!=sum) printf(" a[%d][%d] = %12.6e    sum = %12.6e    n = %d\n",i,j,a[i][j],sum,n);*/
    }
    b[i] = sum / a[i][i];
  }
}


/* ################################################################# */
real ***chmatrix(int nrl,int nrh,int  ncl,int nch,int ndl,int ndh,real *uptr)
{
   int i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
   real ***t;

   /* allocate pointers to pointers to rows */
   t=(real ***) malloc((size_t)((nrow)*sizeof(real**)));
   if (!t) {
    printf ("Allocation Failure in matrix (1) \n");
    exit (1);
   }
   t -= nrl;

   /* allocate pointers to rows and set pointers to them */
   t[nrl]=(real **) malloc((size_t)((nrow*ncol)*sizeof(real*)));
   if (!t[nrl]) {
    printf ("Allocation Failure in matrix (2) \n");
    exit (1);
   }
   t[nrl] -= ncl;

   /* allocate rows and set pointers to them */
/*
   t[nrl][ncl]=(real *) malloc((size_t)((nrow*ncol*ndep)*sizeof(real)));
*/
   t[nrl][ncl] = uptr;
   if (!t[nrl][ncl]) {
    printf ("Allocation Failure in matrix (3) \n");
    exit (1);
   }
   t[nrl][ncl] -= ndl;

   for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
   for(i=nrl+1;i<=nrh;i++) {
      t[i]=t[i-1]+ncol;
      t[i][ncl]=t[i-1][ncl]+ncol*ndep;
      for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
   }

/* return pointer to array of pointers to rows */
return t;

}
/* ################################################################# */
real ***chmatrix3(int nx, int ny, int nz, real *uptr)
{
   int i,j;
   real ***t;

   /* allocate pointers to pointers to rows */
   t=(real ***) malloc((size_t)((nx)*sizeof(real**)));
   if (!t) {
     printf ("Allocation Failure in matrix (1) \n");
     exit (1);
           }

   /* allocate pointers to rows and set pointers to them */
   t[0]=(real **) malloc((size_t)((nx*ny)*sizeof(real*)));
   if (!t[0]) {
     printf ("Allocation Failure in matrix (2) \n");
     exit (1);
                }

   /* allocate rows and set pointers to them */
/*   t[0][0]=(real *) malloc((size_t)((nx*ny*nz)*sizeof(real)));
*/
   t[0][0] = uptr;
   if (!t[0][0]) {
     printf ("Allocation Failure in matrix (3) \n");
     exit (1);
                     }

   for(j = 1; j < ny; j++)
    t[0][j]=t[0][j-1]+nz;

   for(i = 1; i < nx; i++) {
    t[i]=t[i-1]+ny;
    t[i][0]=t[i-1][0]+ny*nz;
    for(j = 1; j < ny; j++) t[i][j]=t[i][j-1]+nz;
                            }
     
  /* return pointer to array of pointers to rows */
  return t;

}

/* ################################################################# */
void free_chmatrix(real ***t,int nrl,int nrh,int ncl,int nch,int ndl,int ndh)
{
free((char*) (t[nrl]+ncl));
free((char*) (t+nrl));
}

/* ################################################################# */
void free_chmatrix3(real ***t)
{
	free((char*) t[0]);
	free((char*) t);
}
    
/* ###############################################################
   
   FILE:

    tools.c

   DESCRIPTION:

    Contains various general-purpose routines:

     * general purpose function (integration)
     * vector operations,
     * memory allocations,
     * debugging routines,
     * abort codes.


   *************************************************************** */

/* ***************************************************************** */
void TRACE (real xx)
/* 
 *
 *  print a number xx and the number of times TRACE has been called
 *
 ******************************************************************* */
{
  static int ik;

  printf ("TRACE ------> %f ,  %d\n", xx, ++ik);
}
/* **************************************************************** */
void SHOW (real **a, int ip)
/* 
 *
 *  print the component of array a at point ip  
 *
 ****************************************************************** */
{
  int nv, ix, iy, iz;


  if (DIR == IDIR) {
    print ("X-sweep");
    ix = ip;
    iy = (NY_PT == NULL ? -1:*NY_PT);
    iz = (NZ_PT == NULL ? -1:*NZ_PT);
  } else if (DIR == JDIR) {
    print ("Y-sweep");
    ix = (NX_PT == NULL ? -1:*NX_PT);
    iy = ip;
    iz = (NZ_PT == NULL ? -1:*NZ_PT);
  } else if (DIR == KDIR) {
    print ("Z-sweep");
    ix = (NX_PT == NULL ? -1:*NX_PT);
    iy = (NY_PT == NULL ? -1:*NY_PT);
    iz = ip;
  }

  print (" (%d,%d,%d)> ", ix, iy, iz);

  for (nv = 0; nv < NVAR; nv++) {
    print ("%10.4e  ", a[ip][nv]);
  }
  print ("\n");
}

/* ************************************************* */
int CHECK_NAN (real **u, int is, int ie, int id)
/*
 *
 *  Check whether array  u contains any Not a Number
 *  (NaN)
 *
 *************************************************** */
{
  int ii, nv, i, j;

  for (ii = is; ii <= ie; ii++) {
  for (nv = 0; nv < NVAR; nv++) {
    if (u[ii][nv] != u[ii][nv]) {
      print (" > NaN found (%d), |", id);
      SHOW (u, ii);
      QUIT_ORION2(1);
    }
  }}
  return (0);
}

/* *************************************************************** */
void WHERE (int i, Grid *grid)
/* 
 *
 *
 ***************************************************************** */
{
  int    ii=0, jj=0, kk=0;
  double x1, x2, x3;
  static Grid *grid1, *grid2, *grid3;

/* --------------------------------------------------
    Keep a local copy of grid for subsequent calls
   -------------------------------------------------- */
 
  if (grid != NULL){
    grid1 = grid + IDIR;
    grid2 = grid + JDIR;
    grid3 = grid + KDIR;
    return;
  }

  if (DIR == IDIR){
    D_EXPAND(ii = i;, jj = *NY_PT;, kk = *NZ_PT;)
  }else if (DIR == JDIR){
    D_EXPAND(ii = *NX_PT;, jj = i;, kk = *NZ_PT;)
  }else if (DIR == KDIR){
    D_EXPAND(ii = *NX_PT;, jj = *NY_PT;, kk = i;)
  }

  D_EXPAND(
    x1 = grid1->x[ii];  ,
    x2 = grid2->x[jj];  ,
    x3 = grid3->x[kk];
  )

  int nx = grid1->np_tot;
  int ny = grid2->np_tot;
  int nz = grid3->np_tot;

  D_SELECT(
    print ("zone [x1(%d) = %f]",
            ii, grid1->x[ii]);  ,

    print ("zone [x1(%d) = %f, x2(%d) = %f]",
            ii, grid1->x[ii], 
            jj, grid2->x[jj]);  ,

    print ("zone [x1(%d) = %f, x2(%d) = %f, x3(%d) = %f]",
            ii, grid1->x[ii], 
            jj, grid2->x[jj],
            kk, grid3->x[kk]);
    /*PS: diagnostic only */
    if (ii > 4 && ii < nx-4 && jj > 4 && jj < ny-4 && kk > 4 && kk < nz-4)
    print ("inside!!!\n");

  )

  #ifdef PARALLEL
   print (", proc %d\n", prank);
  #else
   print ("\n");
  #endif  

}
/* **************************************************************** */
void ERROR (int condition, char *str)
/*
 *
 *
 *
 ****************************************************************** */
{
  char *str_err=" ! Error: ";

  if (condition) {
    print (str_err);
    print (str);
    print ("\n");
    QUIT_ORION2(1);
  }
}
/* **************************************************************** */
void print (char *fmt, ...)
/*
 *
 *   Define print function
 *
 ****************************************************************** */
{
  va_list args;

  va_start(args, fmt);

#if PRINT_TO_FILE == YES
  orion2_log_file = fopen("orion2.log","a"); 
  vfprintf(orion2_log_file, fmt, args);
  fclose(orion2_log_file);
#else
  vprintf(fmt, args);
#endif

  va_end(args);
}
/****************************************************************** */
void print1 (char *fmt, ...)
/*
 *
 *   Define print1 function
 *
 ****************************************************************** */
{
  va_list args;

  va_start(args, fmt);

  #if PRINT_TO_FILE == YES

   if (prank == 0){
     orion2_log_file = fopen("orion2.log","a"); 
     vfprintf(orion2_log_file,fmt, args);
     fclose(orion2_log_file);
   }

  #else

   if (prank == 0) vprintf(fmt, args);
 
  #endif

  va_end(args);
}
/* *********************************************************** */
void Make_State (State_1D *state)
/*
 *
 * Allocate memory areas for arrays inside the state
 * structure.
 *
 *
 ************************************************************* */
{
  state->u       = Array_2D(NMAX_POINT, NVAR, double);
  state->v       = Array_2D(NMAX_POINT, NVAR, double);
  state->vp      = Array_2D(NMAX_POINT, NVAR, double);
  state->vm      = Array_2D(NMAX_POINT, NVAR, double);
  state->up      = Array_2D(NMAX_POINT, NVAR, double);
  state->um      = Array_2D(NMAX_POINT, NVAR, double);
  state->flux    = Array_2D(NMAX_POINT, NVAR, double);
  state->pnt_flx = Array_2D(NMAX_POINT, NVAR, double);
  state->dff_flx = Array_2D(NMAX_POINT, NVAR, double);
  state->res_flx = Array_2D(NMAX_POINT, NVAR, double);
  state->vt      = Array_2D(NMAX_POINT, NVAR, double);
  state->src     = Array_2D(NMAX_POINT, NVAR, double);
  state->par_src = Array_2D(NMAX_POINT, NVAR, double);
  state->rhs     = Array_2D(NMAX_POINT, NVAR, double);
  state->press   = Array_1D(NMAX_POINT, double);
  state->bn      = Array_1D(NMAX_POINT, double);
  state->SL      = Array_1D(NMAX_POINT, double);
  state->SR      = Array_1D(NMAX_POINT, double);

  state->Lp      = Array_3D(NMAX_POINT, NVAR, NVAR, double);
  state->Rp      = Array_3D(NMAX_POINT, NVAR, NVAR, double);
/*  state->dwlim   = Array_2D(NMAX_POINT, NVAR, double);*/

  state->flag    = Array_1D(NMAX_POINT, char);

/* --------------------------------------
     define shortcut pointers for
     left and right values with respect
     to the cell center
   -------------------------------------- */
   
  state->vL = state->vp;
  state->vR = state->vm + 1;

  state->uL = state->up;
  state->uR = state->um + 1;

  #ifdef SINGLE_STEP
   state->vh = Array_2D(NMAX_POINT, NVAR, double);
   state->uh = Array_2D(NMAX_POINT, NVAR, double);
  #else
   state->vh = state->v;
   state->uh = state->u;
  #endif

}

/* ***************************************************** */
real VAN_DER_CORPUT(int n, int k1, int k2)
/*
 *
 * PURPOSE:
 *
 *  Compute a van der Corput Pseudo Random number
 *  based on the generator (k1,k2)
 *
 ******************************************************* */
{
  int  i;
  real N, K1, theta=0.0;
  real A;

  N  = (real) n;
  K1 = (real) k1;
  for(i = 0; i < (log(N)/log(K1) + 1); i++){
    A = k2*(n%k1)%k1;
    theta += A*pow(K1,-i-1);
    n /= k1;
  }

  return (theta);
}

/* **************************************************************** */
int Little_Endian (void) 
/*
 * PURPOSE
 *
 *  Return 1 if the current architecture has little endian order
 *
 ****************************************************************** */
{
  int TestEndian = 1;

  return *(char*)&TestEndian;
}

/* **************************************************************** */
void SWAP_ENDIAN (void *x, const int nbytes) 
/*
 * PURPOSE
 *
 *  Swap the byte order of x
 *
 ****************************************************************** */
{
  int k;
  static char Swapped[16];
  char *c;

  c = (char *) x;

  for (k = nbytes; k--; ) Swapped[k] = *(c + nbytes - 1 - k);
  for (k = nbytes; k--; ) c[k] = Swapped[k];

}
