#include "orion2.h"

/* ****************************************************************** */
void DEFAULT_VAR_NAMES(Output *output)
/*
 *
 *  PURPOSE
 *
 *    Set file names for I/O
 *
 *
 ******************************************************************** */
{
  int nv;

/* ----------------------------------------------
    Physics module file names; 
    these pertain to the physics module ONLY
   ---------------------------------------------- */

  output->var_name[DN] = "rho";
  EXPAND(output->var_name[VX] = "v1";  ,
         output->var_name[VY] = "v2";  ,
         output->var_name[VZ] = "v3";)
  #if EOS != ISOTHERMAL
   output->var_name[PR] = "pr";
  #endif

  #if PHYSICS == MHD || PHYSICS == RMHD
   EXPAND(output->var_name[BX] = "b1";  ,
          output->var_name[BY] = "b2";  ,
          output->var_name[BZ] = "b3";)
  #endif
  
  /* (staggered field names are set in SET_PUTPUT) */

  #ifdef PSI_GLM
   output->var_name[PSI_GLM] = "psi_glm";
  #endif
  
/* ------------------------------------------------
                   Tracers 
   ------------------------------------------------ */

  for (nv = TR; nv < TR + NTRACER; nv++){
    sprintf (output->var_name[nv],"tr%d",nv - TR + 1);
  } 

  #if ENTROPY_SWITCH == YES
   sprintf (output->var_name[ENTR],"entropy");
  #endif

/* ------------------------------------------------
               Cooling vars
   ------------------------------------------------ */

  #if INCLUDE_COOLING == NEQ

   output->var_name[HI]    = "fHI";
   output->var_name[HeI]   = "fHeI";
   output->var_name[HeII]  = "fHeII";
   output->var_name[CI]    = "fCI";
   output->var_name[CII]   = "fCII";
   output->var_name[CIII]  = "fCIII";
   output->var_name[CIV]   = "fCIV";
   output->var_name[CV]    = "fCV";
   output->var_name[NI]    = "fNI";
   output->var_name[NII]   = "fNII";
   output->var_name[NIII]  = "fNIII";
   output->var_name[NIV]   = "fNIV";
   output->var_name[NV]    = "fNV";
   output->var_name[OI]    = "fOI";
   output->var_name[OII]   = "fOII";
   output->var_name[OIII]  = "fOIII";
   output->var_name[OIV]   = "fOIV";
   output->var_name[OV]    = "fOV";
   output->var_name[NeI]   = "fNeI";
   output->var_name[NeII]  = "fNeII";
   output->var_name[NeIII] = "fNeIII";
   output->var_name[NeIV]  = "fNeIV";
   output->var_name[NeV]   = "fNeV";
   output->var_name[SI]    = "fSI";
   output->var_name[SII]   = "fSII";
   output->var_name[SIII]  = "fSIII";
   output->var_name[SIV]   = "fSIV";
   output->var_name[SV]    = "fSV";
   #if INCLUDE_Fe == YES
    output->var_name[FeI]   = "fFeI";
    output->var_name[FeII]  = "fFeII";
    output->var_name[FeIII] = "fFeIII";
   #endif 

  #elif INCLUDE_COOLING == RAYMOND

   output->var_name[FNEUT] = "fneut";

  #endif

}
