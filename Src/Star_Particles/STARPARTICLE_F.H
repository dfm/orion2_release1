#ifndef _STARPARTICLE_F_H_
#define _STARPARTICLE_F_H_  "%W% %G%"

#define ARLIM_P(x)  const int&,const int&,const int&
#define ARLIM(x)  x[0],x[1],x[2]

#ifdef CH_LANG_FORT
#    define FORT_STARRAD_EXTENDED starradiationextended
#    define FORT_STAR_WIND_INJECT starwindinject
#    define FORT_STAR_ISOWIND_INJECT starisowindinject
#ifdef MGRD
#    define FORT_MG_LUM_STAR mglumstar
#    define FORT_MG_LUM_DISK mglumdisk
#    define FORT_STARRAD_EXTENDED_MG    starradextendedmg
#    define FORT_STAR_ISOWIND_INJECT starwindisoinject
#endif
#    define FORT_COOL_GRID cool_grid
#endif

#ifdef CH_LANG_CC
#  ifdef CH_FORT_USE_UPPERCASE
#    define FORT_STARRAD_EXTENDED STARRADIATIONEXTENDED
#    define FORT_STAR_WIND_INJECT STARWINDINJECT
#    define FORT_STAR_ISOWIND_INJECT STARISOWINDINJECT
#ifdef MGRD
#    define FORT_MG_LUM_STAR MGLUMSTAR
#    define FORT_MG_LUM_DISK MGLUMDISK
#    define FORT_STARRAD_EXTENDED_MG    STARRADEXTENDEDMG
#endif
#    define FORT_COOL_GRID COOL_GRID
#  else
#  ifdef CH_AIX
#    define FORT_STARRAD_EXTENDED starradiationextended
#    define FORT_STAR_WIND_INJECT starwindinject
#    define FORT_STAR_ISOWIND_INJECT starisowindinject
#ifdef MGRD
#    define FORT_MG_LUM_STAR mglumstar
#    define FORT_MG_LUM_DISK mglumdisk
#    define FORT_STARRAD_EXTENDED_MG    starradextendedmg
#endif
#    define FORT_COOL_GRID cool_grid
#  else
#    define FORT_STARRAD_EXTENDED starradiationextended_
#    define FORT_STAR_WIND_INJECT starwindinject_
#    define FORT_STAR_ISOWIND_INJECT starisowindinject_
#ifdef MGRD
#    define FORT_MG_LUM_STAR mglumstar_
#    define FORT_MG_LUM_DISK mglumdisk_
#    define FORT_STARRAD_EXTENDED_MG    starradextendedmg_
#endif
#    define FORT_COOL_GRID cool_grid_
#endif
#endif

extern "C" {
  void FORT_SET_ZERO_OPACITY(ARLIM_P(rlo), ARLIM_P(rhi),
			     Real* kappar, const Real* xlo, const Real* dx,
			     Real* pos, int* ngrow);
  void FORT_STARRAD_EXTENDED(ARLIM_P(rlo), ARLIM_P(rhi),
			     Real* rad, const Real* xlo, const Real* dx,
			     Real *pos, Real* lum, int* ngrow);
  void FORT_STAR_WIND_INJECT(ARLIM_P(slo), ARLIM_P(shi), int *nstate,
                             Real* state, int* windrad, int* accrad, const Real* xlo,
                             const Real* dx, Real* pos, Real* mom,
			     Real* angmom, Real* vWind, Real* dm, int* tracer, int* wind_tracer, 
			     int*isowind_tracer, Real* wind_theta, Real* Twind); //Added Twind ALR 12/17
  void FORT_STAR_ISOWIND_INJECT(ARLIM_P(slo), ARLIM_P(shi), int *nstate,
                             Real* state, int* windrad, int* accrad, const Real* xlo,
                             const Real* dx, Real* pos, Real* mom,
			     Real* angmom, Real* vWind, Real* dm, int* tracer, int* wind_tracer,
			     int* isowind_tracer, Real* wind_theta);
#ifdef MGRD

  void FORT_MG_LUM_STAR(Real* energy, Real* estar, Real* lstar, Real* radius, 
		   int* ngroups, Real* xnu, Real* ergynu);

  void FORT_MG_LUM_DISK(Real* energy, Real* edisk, int* ngroups, Real* xnu, Real* ergynu);

  void FORT_STARRAD_EXTENDED_MG(ARLIM_P(rlo), ARLIM_P(rhi),
			     Real* rad, const Real* xlo, const Real* dx,
			     Real *pos, const int* idx, Real* lum, int* ngrow, 
			     int* ngroups, Real* enrgynu);
#endif
  void FORT_COOL_GRID (Real* state, ARLIM_P(slo), ARLIM_P(shi), 
		       Real* rad, ARLIM_P(rlo), ARLIM_P(rhi), 
		       int* nstate, int* tracer, Real* dt);
};
#endif
#endif
