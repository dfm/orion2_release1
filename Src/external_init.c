#include"orion2.h"

static void GET_EXTERNAL_GRID (double *x, int nx, char *fname);
static int  GET_EXTERNAL_DATA (double ***, char *,int,int,int);
static int GETFILESIZE(char *fname);

/* ******************************************************************* */
void EXTERNAL_GET_VAR (double ****V, Grid *grid)
/*
 *
 *
 *
 ********************************************************************* */
{
  static int first_call = 1;
  static int nx = 1, ny = 1, nz = 1;
  static int skip[NVAR];
  int i_u, j_u, k_u;
  int il, jl, kl;
  int ih, jh, kh;
  int i_u_mid, j_u_mid, k_u_mid;
  int i, j, k, nv;
  double c00,c01,c10,c11,c0,c1;
  double *x1, *x2, *x3;
  static double *x, *dx, *y, *dy, *z, *dz, ***Vin[NVAR];
  double ***V_u, ***Vpl;
  FILE *fp, *data;
  Output output;
  char filename[64];

/* -- Open once the files and get their size to have the tot indx -- */

  if (first_call) {

    print ("> Assigning initial conditions from external files:\n");
    nx = GETFILESIZE("gridx.out");
    x  = Array_1D(nx, double);
    GET_EXTERNAL_GRID (x, nx, "gridx.out");

    #if DIMENSIONS >= 2    
     ny = GETFILESIZE("gridy.out");
     y  = Array_1D(ny, double);
     GET_EXTERNAL_GRID (y, ny, "gridy.out");
    
     #if DIMENSIONS == 3
      nz = GETFILESIZE("gridz.out");
      z  = Array_1D(nz, double);
      GET_EXTERNAL_GRID (z, nz, "gridz.out");
     #endif
    #endif

/* ------------------------------------------------
     ALLOCATE memory for x, y, z, V_u     
   -----------------------------------------------  */

    for (nv = NVAR; nv--; ) Vin[nv] = Array_3D(nz,ny,nx, double);
   
/* ------------------------------------------------
     Get the values of the external grid & 
     The user defined vars  
   ------------------------------------------------ */
    
    output.var_name = Array_2D(64,128,char);
        
    DEFAULT_VAR_NAMES(&output);
    for (nv = 0; nv < NVAR; nv++){
      sprintf (filename,"%s.dbl", output.var_name[nv]);
      skip[nv] = GET_EXTERNAL_DATA(Vin[nv], filename, nx, ny, nz);
      if (!skip[nv]){
        print (" Reading %s...\n",filename);
      }
    }   
    first_call = 0;
  }

/*--------------------*/
/*    Interpolation   */
/*--------------------*/
/*
  find where x1 falls in x --> ilo
  find where x2 falls in y --> jlo
  find where x3 falls in z --> klo
*/

  
  x1 = grid[IDIR].x;
  x2 = grid[JDIR].x;
  x3 = grid[KDIR].x;

  for (nv = 0; nv < NVAR; nv++) { 
    if (skip[nv]) continue;
    V_u = Vin[nv];
    Vpl = V[nv];
    KTOT_LOOP(k){
    JTOT_LOOP(j){ 
    ITOT_LOOP(i){

/* ----------------------------------------------
        Table lookup by binary search  
   ---------------------------------------------- */
/*printf ("Searching  %d  %d...\n",i,j); */

      il = 0;
      ih = nx - 1;
/*
    if (x1[i] > x[ih]) print ("! X out of range\n");  
*/
      while (il != (ih-1)){
        i_u_mid = (il+ih)/2;
        if (x1[i] <= x[i_u_mid]){
          ih = i_u_mid;   
        }else if (x1[i] > x[i_u_mid]){
          il = i_u_mid;
        }
      }

      #if DIMENSIONS > 1
       jl = 0;
       jh = ny - 1;
/*
     if (x2[j] > y[jh]) print ("! Y out of range\n");  
*/
       while (jl != (jh-1)){
         j_u_mid = (jl+jh)/2;
         if (x2[j] <= y[j_u_mid]){
           jh = j_u_mid;   
         }else if (x2[j] > y[j_u_mid]){
           jl = j_u_mid;
         }
       }
      #endif

      #if DIMENSIONS == 3

       kl = 0;
       kh = nz - 1;
/*
     if (x3[k] > z[kh]) print ("! Z out of range\n");  
*/
       while (kl != (kh - 1)){
         k_u_mid = (kl+kh)/2;
         if (x3[k] <= z[k_u_mid]){
           kh = k_u_mid;   
         }else if (x3[k] > z[k_u_mid]){
           kl = k_u_mid;
         }
       }
      #else
       kl = 0;
       kh = nz - 1;
      #endif    
/*----------------------------*/

D_EXPAND( c00 = (V_u[kl][jl][il + 1] - V_u[kl][jl][il])*(x1[i] - x[il])/(x[il + 1]-x[il]) + V_u[kl][jl][il];                ,   
          c10 = (V_u[kl][jl + 1][il + 1]-V_u[kl][jl + 1][il])*(x1[i] - x[il])/(x[il + 1] - x[il]) + V_u[kl][jl + 1][il];
          c0  = (c10 - c00)*(x2[j] - y[jl])   + c00;                                                                        ,
          c01 = (V_u[kl+1][jl][il+1]-V_u[kl+1][jl][il])*(x1[i]-x[il])/(x[il + 1]-x[il]) + V_u[kl+1][jl][il];
          c11 = (V_u[kl+1][jl + 1][il + 1]-V_u[kl+1][jl + 1][il])*(x1[i]-x[il])/(x[il + 1]-x[il]) + V_u[kl+1][jl + 1][il];
          c1  = (c11 - c01)*(x2[j] - y[jl+1]) + c01;                                                                        )


D_EXPAND(Vpl[k][j][i] = c00;                           ,  
         Vpl[k][j][i] = c0;                            ,
         Vpl[k][j][i] = (c1 - c0)*(x3[k] - z[kl]) + c0;)

    }}}  /* -- end loop ijk -- */
  }
}

/* ******************************************************************************* */
void GET_EXTERNAL_GRID (double *x, int nx, char *fname)
/*
 *
 *
 *
 ********************************************************************************* */
{
  FILE *fp;

  fp = fopen(fname,"rb");
  fread (x, sizeof(double), nx, fp);
  fclose(fp);
}

/* ************************************************************************** */
int GET_EXTERNAL_DATA (double ***V_u, char *filename,int nx,int ny,int nz)
/*
 *
 *
 *
 **************************************************************************** */
{
  int i, j, k;
  double ur;
  size_t object_size;
  size_t object_count;
  size_t op_return;
  FILE *data; 

  data = fopen(filename,"rb");

  if (data == NULL){
    print("! Warning file %s does not exist. Using normal initialization instead\n",filename);
    return (1);
  }

object_size = sizeof(double);
object_count = 1;

  for (k = 0; k < nz; k++){
  for (j = 0; j < ny; j++){
  for (i = 0; i < nx; i++){
                            
     op_return = fread (&ur, object_size, object_count, data);
     V_u[k][j][i] = ur;

    if (op_return != object_count){
      printf ("Error reading data from file.\n");
    }

  }}}

  fclose(data);
  return(0);
}
/* ************************************************************** */
int GETFILESIZE(char *fname)
/*
 *
 * ************************************************************** */
{
  int pos, end;
  FILE *fp;  

  fp = fopen(fname,"rb");
  if (fp == NULL){
    print("! File %s does not exist\n!", fname);
    QUIT_ORION2(1);
  }

  pos = ftell (fp);
  fseek (fp, 0, SEEK_END);
  end = ftell (fp);
  fseek (fp, pos, SEEK_SET);
  fclose(fp);
  
  return (end/sizeof(double));
}
