#include "orion2.h"

/* ****************************************************************** */
void CTU_BOUNDARY_STATES (Data_Arr UPX, Data_Arr UMX, 
                          Data_Arr UPY, Data_Arr UMY, 
                          Data_Arr UPZ, Data_Arr UMZ, 
                          Grid *grid)
/* 
 *
 * PURPOSE:
 *
 *  Assign corner-coupled states in ghost boundary zones 
 *  when userdefined b.c. conditions are used.
 *  The only purpose of this routine is to get the electric
 *  fields in the boundaries.
 *
 *  Important:
 *
 *   at X-boundaries only UPY, UMY, UPZ, UMZ should be assigned                   
 *   at Y-boundaries only UPX, UMX, UPZ, UMZ should be assigned
 *   at Z-boundaries only UPX, UMX, UPY, UMY should be assigned
 *
 *  The array are ordered in different ways for
 *  efficiency considerations:
 *
 *   UPX and UMX are indexed  [k][j][i][nv];
 *   UPY and UMY are indexed  [k][i][j][nv];
 *   UPZ and UMZ are indexed  [j][i][k][nv];
 *
 * Thus, for example
 *
 *  at x-left, assign 
 *
 *    UPY,UMY[k][IBEG-1][j][nv],   JBEG - 1 <= j <= JEND + 1
 *                                 KBEG     <= k <= KEND
 *    UPZ,UMZ[j][IBEG-1][k][nv],   JBEG     <= j <= JEND 
 *                                 KBEG - 1 <= k <= KEND + 1
 *                    
 *
 *
 ******************************************************************** */
{
  int i,j,k,nv;

}
