#include "orion2.h"

/* ********************************************************************* */
void LF_SOLVER (const State_1D *state, int beg, int end, 
                real *cmax, Grid *grid)
/*
 *
 * NAME
 *
 *   LF_SOLVER
 *
 *
 * PURPOSE
 *
 *   - Solve Riemann problem using the Lax-Friedrichs 
 *     Rusanov solver:
 *
 *     F(i+1/2) = [FL + FR - c*(UR - UL)]*0.5
 *
 *     where c = max_speed[(UR + UL)*0.5]
 *
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 **************************************************************************** */

{
  int     nv, i;
  real    cRL;
  double  *uR, *uL;
  static  real **fL, **fR, **vRL;
  static  real *pR, *pL, *cmin_RL, *cmax_RL;
  static double **VL, **VR, **UL, **UR;

  real **bgf;

  if (fR == NULL){
    fR  = Array_2D(NMAX_POINT, NFLX, double);
    fL  = Array_2D(NMAX_POINT, NFLX, double);
    vRL = Array_2D(NMAX_POINT, NFLX, double);
    cmin_RL = Array_1D(NMAX_POINT, double);
    cmax_RL = Array_1D(NMAX_POINT, double);
    pR      = Array_1D(NMAX_POINT, double);
    pL      = Array_1D(NMAX_POINT, double);
    #ifdef GLM_MHD
     VL = Array_2D(NMAX_POINT, NVAR, double);
     VR = Array_2D(NMAX_POINT, NVAR, double);
     UL = Array_2D(NMAX_POINT, NVAR, double);
     UR = Array_2D(NMAX_POINT, NVAR, double);
    #endif
  }

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (beg, end, FACE_CENTER, grid);
   #ifdef GLM_MHD
    print1 ("! GLM MHD and BACKGROUND_FIELD incompatible\n");
    QUIT_ORION2(1);
   #endif
  #endif

  #ifdef GLM_MHD
   GLM_2X2_SOLVE (state, VL, VR, beg, end, grid);
   PRIMTOCON (VL, UL, beg, end);
   PRIMTOCON (VR, UR, beg, end);
  #else
   VL = state->vL; UL = state->uL;
   VR = state->vR; UR = state->uR;
  #endif

/* -------------------------------------------------------------------
                     i  -- >   i + 1/2
   ------------------------------------------------------------------- */

  FLUX (UL, VL, bgf, fL, pL, beg, end);
  FLUX (UR, VR, bgf, fR, pR, beg, end);

/* -------------------------------------------------------------------
             Compute max eigenvalue and fluxes
   ------------------------------------------------------------------- */

  for (i = beg; i <= end; i++) {
    for (nv = NFLX; nv--;      ) {
      vRL[i][nv] = 0.5*(VL[i][nv] + VR[i][nv]);
    }
    #if EOS == IDEAL
     MAX_MACH_NUMBER = dmax(MAX_MACH_NUMBER, fabs(vRL[i][V1])/sqrt(gmm*vRL[i][PR]/vRL[i][DN])); 
    #elif EOS == ISOTHERMAL
     MAX_MACH_NUMBER = dmax(MAX_MACH_NUMBER, fabs(vRL[i][V1])/C_ISO); 
    #endif
  }

  MAX_CH_SPEED (vRL, cmin_RL, cmax_RL, bgf, beg, end);

  for (i = beg; i <= end; i++) {
    cRL = dmax(fabs(cmin_RL[i]), fabs(cmax_RL[i]));
    state->SL[i] = -cRL, state->SR[i] = cRL;
    *cmax = dmax (cRL, *cmax);
    uL = UL[i];
    uR = UR[i];
    for (nv = NFLX; nv--;    ) {
      state->flux[i][nv] = 0.5*(fL[i][nv] + fR[i][nv] - cRL*(uR[nv] - uL[nv]));
    }
    state->press[i] = 0.5*(pL[i] + pR[i]);
  }

/* --------------------------------------------------------
              initialize source term
   -------------------------------------------------------- */

  #if MHD_FORMULATION == EIGHT_WAVES
   ROE_DIVB_SOURCE (state, beg + 1, end, grid);
  #endif

}

