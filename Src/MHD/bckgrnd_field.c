#include "orion2.h"

#if INCLUDE_BACKGROUND_FIELD == YES
/* ******************************************************************* */
real **GET_BACKGROUND_FIELD (int beg, int end, int where, Grid *grid)
/*
 *
 *
 *
 *
 *
 ********************************************************************* */
{
  int i;
  real *x1, *x2, *x3;
  static real **bck_fld;

/* ----------------------------------------------------
         Check for incompatibilities 
   ---------------------------------------------------- */
  
  #if TIME_STEPPING != RK2
   print1 ("! Background field splitting works with RK2 ONLY \n");
   QUIT_ORION2(1);
  #endif
  #if MHD_FORMULATION != FLUX_CT
   print1 ("! Background field splitting works with CT ONLY \n");
   QUIT_ORION2(1);
  #else
   #if CT_EMF_AVERAGE != ARITHMETIC
    print1 ("! Background field splitting works with ARITHMETIC average only\n");
    QUIT_ORION2(1);
   #endif
  #endif
  #if RESISTIVE_MHD != NO 
    print1 ("! Background field splitting works with Ideal MHD only\n");
    QUIT_ORION2(1);
  #endif  

  if (bck_fld == NULL) {
    bck_fld = Array_2D(NMAX_POINT, NVAR, double);
  }

/* ----------------------------------
     get pointers to coordinates 
   ---------------------------------- */
   
  x1 = grid[IDIR].x;
  x2 = grid[JDIR].x;
  x3 = grid[KDIR].x;

  if (DIR == IDIR){
    if (where == FACE_CENTER) x1 = grid[IDIR].xr;
    for (i = beg; i <= end; i++){
      BACKGROUND_FIELD (x1[i],x2[*NY_PT],x3[*NZ_PT], bck_fld[i] + BX);
    }
  }else if (DIR == JDIR){
    if (where == FACE_CENTER) x2 = grid[JDIR].xr;
    for (i = beg; i <= end; i++){
      BACKGROUND_FIELD (x1[*NX_PT],x2[i],x3[*NZ_PT], bck_fld[i] + BX);
    }
  }else{
    if (where == FACE_CENTER) x3 = grid[KDIR].xr;
    for (i = beg; i <= end; i++){
      BACKGROUND_FIELD (x1[*NX_PT],x2[*NY_PT],x3[i], bck_fld[i] + BX);
    }
  }    

  return(bck_fld);
}
#endif
