#include "orion2.h"


/* ********************************************************************* */
void PRIMTOCON (real **uprim, real **ucons, int ibeg, int iend)
/*
 *
 *
 *   Convert a vector of primitive quantities to conservative
 *
 *
 *********************************************************************** */
{
  int  i, nv;
  real *v, *u;
  real kin, gmm1, rho;
  real xpos, ypos, zpos;
  real gamma, temp, eth, pressure;
 
  #if EOS == IDEAL
   gmm1 = gmm - 1.0;
  #endif

  for (i = ibeg; i <= iend; i++) {
 
    v = uprim[i];
    u = ucons[i];

    u[DN] = rho = v[DN];


    EXPAND (u[MX] = v[DN]*v[VX];  ,
            u[MY] = v[DN]*v[VY];  ,
            u[MZ] = v[DN]*v[VZ];)

    EXPAND (u[BX] = v[BX];  ,
            u[BY] = v[BY];  ,
            u[BZ] = v[BZ];)




#if EOS == IDEAL
     kin   = EXPAND(v[VX]*v[VX], + v[VY]*v[VY], + v[VZ]*v[VZ]);
     u[EN] = v[DN]*kin + EXPAND(v[BX]*v[BX], + v[BY]*v[BY], + v[BZ]*v[BZ]);
     u[EN] = u[EN]*0.5 + v[PR]/gmm1;
#endif

    #ifdef GLM_MHD
     u[PSI_GLM] = v[PSI_GLM]; 
    #endif

    for (nv = NFLX; nv < (NFLX + NSCL); nv++) u[nv] = v[DN]*v[nv];
  }
}
/* ********************************************************************* */
int CONTOPRIM (real **ucons, real **uprim, int ibeg, int iend, char *flag)
/*
 *
 * PURPOSE
 *
 *   Convert a vector of conservative quantities to primitive values.
 *
 *
 * 
 *                  -----> use entropy ----> p = p(S)
 *    start with U {
 *                  -----> use energy -----> p = p(E)
 *
 *  if p is not physical fix it.
 *  
 *
 *********************************************************************** */
{
  int  i, nv, status=0, use_energy;
  real tau, rho, gmm1;
  real b2, m2, K, rhog1;
  real *u, *v;
  real vmax;
  real temp, eth, pressure;
  int j;

  #if EOS == IDEAL
   gmm1 = gmm - 1.0;
  #endif

  for (i = ibeg; i <= iend; i++) {

    flag[i] = 0;
    u = ucons[i];
    v = uprim[i];

  /* -------------------------------------------
           Check density positivity 
     ------------------------------------------- */
  
    if (u[DN] < 0.0) {
      print("! CONTOPRIM: negative density (%8.2e), ", u[DN]);
      WHERE (i, NULL);
      u[DN]    = SMALL_DN;
      flag[i] |= DN_FAIL;

      /* Note: flooring the density can cause unphysically large
      velocities, so here we check if the new velocity exceeds the
      ceiling. If it does, we set it to the ceiling. */

      vmax = abs(u[MX]/u[DN]);
      if (CH_SPACEDIM > 1)
	vmax = vmax > abs(u[MY]/u[DN]) ? vmax : abs(u[MY]/u[DN]);
      if (CH_SPACEDIM > 2)
	vmax = vmax > abs(u[MZ]/u[DN]) ? vmax : abs(u[MZ]/u[DN]);
      if (vmax > CEIL_VA) {
	u[MX] = CEIL_VA*u[DN]*(u[MX]/u[DN]/vmax);
	if (CH_SPACEDIM > 1) {
	  u[MY] = CEIL_VA*u[DN]*(u[MY]/u[DN]/vmax);
	  if (CH_SPACEDIM > 2) {
	    u[MZ] = CEIL_VA*u[DN]*(u[MZ]/u[DN]/vmax);
	  } 
	}
      }
    }

    v[DN] = rho = u[DN];

    tau = 1.0/u[DN];
    EXPAND(v[VX] = u[MX]*tau;  ,
           v[VY] = u[MY]*tau;  ,
           v[VZ] = u[MZ]*tau;)

    EXPAND(v[BX] = u[BX];  ,
           v[BY] = u[BY];  ,
           v[BZ] = u[BZ];)


  
     m2 = EXPAND(u[MX]*u[MX], + u[MY]*u[MY], + u[MZ]*u[MZ]);
     b2 = EXPAND(u[BX]*u[BX], + u[BY]*u[BY], + u[BZ]*u[BZ]);
     K  = 0.5*(m2*tau + b2);




  /* --------------------------------------------
       now try to recover pressure from 
       total energy or entropy
     -------------------------------------------- */

    #if EOS == IDEAL
   
     use_energy = 1;     
     #if ENTROPY_SWITCH == YES
      #ifdef CH_SPACEDIM
       if (ISTEP > 0)   /* -- HOT FIX used with Chombo: avoid calling 
                              CHECK_ZONE when writing file to disk          -- */
      #endif
       if (CHECK_ZONE(i,FLAG_ENTROPY)){
         use_energy = 0;
         rhog1 = pow(rho, gmm1);
         v[PR] = u[ENTR]*rhog1; 
         if (v[PR] < 0.0){
            WARNING(
              print("! CONTOPRIM: negative p(S) (%8.2e, %8.2e), ", v[PR], u[ENTR]);
              WHERE (i, NULL);
            )
          v[PR]    = SMALL_PR;
          flag[i] |= PR_FAIL;
         }
         u[EN] = v[PR]/gmm1 + K; /* -- redefine energy -- */
       }
     #endif  /* ENTROPY_SWITCH == YES */

     if (use_energy){
       if (u[EN] < 0.0) {
         WARNING(
           print("! CONTOPRIM: negative energy (%8.2e), ", u[EN]);
           WHERE (i, NULL);
         )
         v[PR]    = SMALL_PR;
         u[EN]    = v[PR]/gmm1 + K;
         flag[i] |= EN_FAIL;
       }else{
         v[PR] = gmm1*(u[EN] - K);
         if (v[PR] < 0.0){
           WARNING(
             print("! CONTOPRIM: negative p(E) (%8.2e), ", v[PR]);
             WHERE (i, NULL);
           )
           v[PR]    = SMALL_PR;
           flag[i] |= PR_FAIL;
           u[EN]    = v[PR]/gmm1 + K; /* -- redefine energy -- */
         }
       }
     }

    #endif  /* EOS == IDEAL */

  /* -- do remaning variables -- */
   
    for (nv = NFLX; nv < NVAR; nv++) v[nv] = u[nv]*tau;
    #ifdef GLM_MHD
     v[PSI_GLM] = u[PSI_GLM]; 
    #endif

    status |= flag[i];
  }

  return(status);
}

