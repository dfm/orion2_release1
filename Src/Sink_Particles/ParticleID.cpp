#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <ParticleID.H>

/* class ParticleID routines */
//inline
ParticleID::ParticleID(int proc, long n) {
  createProc = proc;
  num = n;
}

//inline
ParticleID::ParticleID() {
  createProc = -1;
  num = -1;
}

ParticleID::ParticleID(ParticleID& source) {
  createProc = source.createProc;
  num = source.num;
}

ParticleID::ParticleID(const ParticleID& source) {
  createProc = source.createProc;
  num = source.num;
}

//inline
ParticleID::~ParticleID() {}

//inline
ParticleID&
ParticleID::operator=(const ParticleID &rhs) {
  createProc = rhs.createProc;
  num = rhs.num;
  return(*this);
}

//inline
bool
ParticleID::operator==(const ParticleID &rhs) {
  return((createProc==rhs.createProc) && (num==rhs.num));
}

//inline
bool
ParticleID::operator>=(const ParticleID &rhs) {
  return( (createProc>rhs.createProc) ||
	  ((createProc==rhs.createProc) && (num>=rhs.num)) );
}

//inline
bool
ParticleID::operator<=(const ParticleID &rhs) {
  return( (createProc<rhs.createProc) ||
	  ((createProc==rhs.createProc) && (num<=rhs.num)) );
}

//inline
bool
ParticleID::operator>(const ParticleID &rhs) {
  return( (createProc>rhs.createProc) ||
	  ((createProc==rhs.createProc) && (num>rhs.num)) );
}

//inline
bool
ParticleID::operator<(const ParticleID &rhs) {
  return( (createProc<rhs.createProc) ||
	  ((createProc==rhs.createProc) && (num<rhs.num)) );
}

/* Return particle ID */
long 
ParticleID::partNum() { return(num); }

/* Return processor responsible for creating particle */
int 
ParticleID::partProc() { return(createProc); }

/* Set particle ID */
void 
ParticleID::setPartNum(long n) { num = n; }
