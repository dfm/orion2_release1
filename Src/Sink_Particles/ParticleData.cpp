#include <ParticleData.H>

ostream &operator<<(ostream &os, const ParticleData& partdata) {
  for (int i=0; i<CH_SPACEDIM-1; i++)
    os << partdata.pos[i] << " ";
  os << partdata.pos[CH_SPACEDIM-1];
  return(os);
}

istream &operator>>(istream &is, ParticleData& partdata) {
  for (int i=0; i<CH_SPACEDIM; i++) is >> partdata.pos[i];
  return(is);
}

ParticleData::ParticleData(Real *newpos) {
  for (int i=0; i<CH_SPACEDIM; i++) pos[i]=newpos[i];
}

ParticleData::~ParticleData() {}

Real *
ParticleData::position() {
  return(pos);
}

ParticleData&
ParticleData::operator=(const ParticleData& rhs) {
  for (int i=0; i<CH_SPACEDIM; i++) pos[i]=rhs.pos[i];
  return(*this);
}
