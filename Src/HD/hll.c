#include"orion2.h"

/* ********************************************************************* */
void HLL_SOLVER (const State_1D *state, int beg, int end, 
                 real *cmax, Grid *grid)
/*
 *
 * PURPOSE
 *
 *  - Solve Riemann problem for the Euler equations 
 *    using th HLLE solver;
 * 
 *     Reference:    "On Godunov-Type Method near Low Densities"
 *                    B. Einfeldt, C.D. Munz, P.L. Roe,
 *                    JCP 92, 273-295 (1991)
 *   
 *
 *  - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *  - Also, compute maximum wave propagation speed (cmax) 
 *    for  explicit time step computation
 *  
 * LAST_MODIFIED
 *
 *   April 11th 2008, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 **************************************************************************** */
{
  int    nv, i;
  real   scrh;
  static real *pL, *pR, *SL, *SR;
  static real **fL, **fR;
  double *uR, *uL;
  double bmax, bmin, *vL, *vR, aL, aR;

/* -- Allocate memory -- */

  if (fL == NULL){
    fL = Array_2D(NMAX_POINT, NFLX, double);
    fR = Array_2D(NMAX_POINT, NFLX, double);

    pR = Array_1D(NMAX_POINT, double);
    pL = Array_1D(NMAX_POINT, double);
    SR = Array_1D(NMAX_POINT, double);
    SL = Array_1D(NMAX_POINT, double);
  }

  FLUX  (state->uL, state->vL, fL, pL, beg, end);
  FLUX  (state->uR, state->vR, fR, pR, beg, end);

  HLL_SPEED (state->vL, state->vR, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {

    scrh = dmax(fabs(SL[i]), fabs(SR[i]));
    cmax[i]  = scrh;

/*    
    bmin = dmin(0.0, SL[i]);
    bmax = dmax(0.0, SR[i]);
    scrh = 1.0/(bmax - bmin);    
    for (nv = NFLX; nv--;   ){
      state->flux[i][nv]  = bmin*bmax*(uR[i][nv] - uL[i][nv]) 
                        +   bmax*fL[i][nv] - bmin*fR[i][nv];
      state->flux[i][nv] *= scrh;
    }
    state->press[i] = (bmax*pL[i] - bmin*pR[i])*scrh;
vL = state->vL[i];
vR = state->vR[i];
aL = sqrt(gmm*vL[PR]/vL[DN]);
aR = sqrt(gmm*vR[PR]/vR[DN]);
    *cmax  = dmax(*cmax, dmax(-bmin,bmax));
    scrh = (fabs(vL[V1]) + fabs(vR[V1]))/(aL + aR);
    MAX_MACH_NUMBER = dmax(scrh, MAX_MACH_NUMBER);
*/

    if (SL[i] > 0.0){
    
      for (nv = NFLX; nv--; ) state->flux[i][nv] = fL[i][nv];
      state->press[i] = pL[i];

    }else if (SR[i] < 0.0){

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fR[i][nv];
      state->press[i] = pR[i];

    }else{

      uR = state->uR[i];
      uL = state->uL[i];

      scrh = 1.0 / (SR[i] - SL[i]);
      for (nv = NFLX; nv--;  ) {
        state->flux[i][nv] = SL[i]*SR[i]*(uR[nv] - uL[nv]) +
                             SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
        state->flux[i][nv] *= scrh;
      }
      state->press[i] = (SR[i]*pL[i] - SL[i]*pR[i])*scrh;

    }

  } /* end loops on points */
}
