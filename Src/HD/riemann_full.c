#include "orion2.h"

#define MAX_ITER   20
#define small_p       1.e-9
#define small_rho     1.e-9

static void PFUN (real p, real *vL, real *vR, real *f, real *df);
static void FUN_LR (real p, real *v, real *fLR, real *dfLR);

/* ***************************************************************************** */
void TWO_SHOCK (const State_1D *state, real *cmax, Grid *grid)
/*
 *
 * NAME
 *
 *   RIEMANN
 *
 *
 * PURPOSE
 *
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ******************************************************************************* */
{
  int  nv, k, i, beg, end;
  real pstar, ustar, dp, fp, dfp;
  real th, q, scrh, gmmp, gp1, gm1;
  real *vL, *vR;
  real fL, dfL, SL, STL, csL;
  real fR, dfR, SR, STR, csR;
  static real *s, **vs, **us, *cmax_loc;
  static int *shock;

  if (vs == NULL){
    vs       = array_2D(NMAX_POINT, NVAR);
    us       = array_2D(NMAX_POINT, NVAR);
    s        = array_1D(NMAX_POINT);
    cmax_loc = array_1D(NMAX_POINT);
    shock    = int_array_1D(NMAX_POINT);
  }
 
  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend;

  gm1  = gmm - 1.0;
  gp1  = gmm + 1.0;
  gmmp = gm1/gp1;

  for (i = beg; i <= end; i++){

    vL = state->vL[i];
    vR = state->vR[i];
    s[i] = 0.0;

  /* -- guess here -- */

    pstar = 0.5*(vL[PR] + vR[PR]);
    
    for (k = 0; k < MAX_ITER; k++){

      PFUN (pstar, vL, vR, &fp, &dfp);
      dp     = fp/dfp;
      pstar -= dp;

      if (fabs(dp) < 1.e-7*pstar) break;
      if (k == (MAX_ITER-5)){
        print ("! Too many iterations in Rieman\n");
        SHOW(state->vL,i);
        SHOW(state->vR,i);

        QUIT_ORION2(1);
      }
    }

    FUN_LR (pstar, vL, &fL, &dfL);
    FUN_LR (pstar, vR, &fR, &dfR);

    ustar = 0.5*(vL[V1] + vR[V1] + fR - fL);

  /* -- sample solution -- */

    if (s[i] <= ustar){  /* -- left of CD -- */
      q   = pstar/vL[PR];
      csL = sqrt(gmm*vL[PR]/vL[DN]);

      if (q > 1.0) { /* -- left wave is a shock -- */

        scrh = gp1*q + gm1;          
        SL   = vL[V1] - csL*sqrt(0.5/gmm*scrh);
        if (s[i] < SL){
          for (nv = NVAR; nv--; ) vs[i][nv] = vL[nv];
        }else { 
          vs[i][DN] = vL[DN]*(q + gmmp)/(gmmp*q + 1.0);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
        }

      }else{  /* -- left wave is a rarefaction -- */

        SL = vL[V1] - csL;
 
        if (s[i] < SL) {
          for (nv = NVAR; nv--; ) vs[i][nv] = vL[nv];
        }else { 
          vs[i][DN] = vL[DN]*pow(q, 1.0/gmm);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
          STL = ustar - sqrt(gmm*pstar/vs[i][DN]);
          if (s[i] < STL){ /* -- sol inside rarefaction -- */
            scrh = 2.0 + gm1/csL*(vL[V1] - s[i]);
            vs[i][DN] = vL[DN]*pow(scrh/gp1, 2.0/gm1);
            vs[i][PR] = vL[PR]*pow(scrh/gp1, 2.0*gmm/gm1);
            vs[i][V1] = 2.0/gp1*(csL + 0.5*gm1*vL[V1] + s[i]);
          }
        }
      } 

    }else{  /* -- right of CD -- */

      q   = pstar/vR[PR];
      csR = sqrt(gmm*vR[PR]/vR[DN]);

      if (q > 1.0) { /* -- right wave is a shock -- */

        scrh = gp1*q + gm1;          
        SR   = vR[V1] + csR*sqrt(0.5/gmm*scrh);
        if (s[i] > SR){
          for (nv = NVAR; nv--; ) vs[i][nv] = vR[nv];
        }else { 
          vs[i][DN] = vR[DN]*(q + gmmp)/(gmmp*q + 1.0);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
        }

      }else{  /* -- right wave is a rarefaction -- */

        SR = vR[V1] + csR;
 
        if (s[i] > SR) {
          for (nv = NVAR; nv--; ) vs[i][nv] = vR[nv];
        }else { 
          vs[i][DN] = vR[DN]*pow(q, 1.0/gmm);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
          STR = ustar + sqrt(gmm*pstar/vs[i][DN]);
          if (s[i] > STR){ /* -- sol inside rarefaction -- */
            scrh = 2.0 - gm1/csR*(vR[V1] - s[i]);
            vs[i][DN] = vR[DN]*pow(scrh/gp1, 2.0/gm1);
            vs[i][PR] = vR[PR]*pow(scrh/gp1, 2.0*gmm/gm1);
            vs[i][V1] = 2.0/gp1*(-csR + 0.5*gm1*vR[V1] + s[i]);
          }
        }
      } 
    }   

    if (ustar > 0.0) {
      EXPAND(                     ,
             vs[i][V2] = vL[V2];  ,
             vs[i][V3] = vL[V3];)
    }else{
      EXPAND(                     ,
             vs[i][V2] = vR[V2];  ,
             vs[i][V3] = vR[V3];)
    }
      
  }

/* -- compute fluxes -- */

  MAX_CH_SPEED (vs, cmax_loc, grid, beg, end);
  for (i = beg; i <= end; i++) {
    *cmax = dmax(cmax_loc[i], *cmax);
  }
  FLUX(us, vs, state->flux, state->press, beg, end);

}

/* ************************************************ */
void PFUN (real p, real *vL, real *vR, real *f, real *df)
/*
 *
 *
 *
 *
 ************************************************** */
{
  real fL , fR; 
  real dfL, dfR;
  
  FUN_LR (p, vL, &fL, &dfL);
  FUN_LR (p, vR, &fR, &dfR);

  *f  = fL  + fR + vR[V1] - vL[V1];
  *df = dfL + dfR; 
}

/* ************************************************ */
void FUN_LR (real p, real *v, real *fLR, real *dfLR)
/*
 *
 *
 *
 *
 ************************************************** */
{
  real A, B, scrh, cs;
  real q;

  if (p > v[PR]) {  /* -- (shock) -- */

    A = 2.0/(gmm + 1.0)/v[DN];
    B = (gmm - 1.0)/(gmm + 1.0)*v[PR];
    scrh  = A/(p + B);
    *fLR  = (p - v[PR])*sqrt(scrh);
    *dfLR = sqrt(scrh)*(1.0 - 0.5*(p - v[PR])/(B + p));

  }else{   /* -- (rarefaction) -- */

    cs = sqrt(gmm*v[PR]/v[DN]);
    q  = p/v[PR];
    scrh = pow(q, 0.5*(gmm - 1.0)/gmm) - 1.0;
    *fLR  = 2.0*cs/(gmm - 1.0)*scrh;
    scrh  = pow(q, -0.5*(gmm + 1.0)/gmm);
    *dfLR = 1.0/(v[DN]*cs)*scrh;
  }

}


