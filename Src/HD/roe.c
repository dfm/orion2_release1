#include"orion2.h"

#define ROE_AVERAGE       YES

/* ********************************************************************** */
void ROE_SOLVER (const State_1D *state, int beg, int end, 
                 real *cmax, Grid *grid)
/*
 *
 * NAME
 *
 *   ROE_SOLVER 
 *
 *
 * PURPOSE
 *
 *   - Solve riemann problem for the Euler equations 
 *     using the standard Roe method with local characteristic 
 *     approach; it is identical to the Roe method.
 *     Eigenvectors are identical to the ones given by Toro, 
 *     and can also be derived from the maple script
 *     "eigenv.maple", in Src/HD/.
 *
 *     when ROE_AVERAGE == YES use Roe average   
 *     when ROE_AVERAGE == NO  use arithmetic average 
 * 
 *     Reference:    "Riemann Solver and Numerical Methods 
 *                    for Fluid Dynamics"
 *                    E.F. Toro
 *                    --> see page 359-367
 *   
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   June 5th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ************************************************************************ */
{
  int   nv, i, j, k, nn;
  real  scrh;
  real  um[NFLX], vel2;
  real  a2, a, h;
  real  dv[NFLX], eta[NFLX];
  real  Rc[NFLX][NFLX], alambda[NFLX], lambda[NFLX];
  real  delta, delta_inv, gmm1, gmm1_inv;
  real  a2R, a2L;
#if ROE_AVERAGE == YES
  real s, c, hl, hr;
#endif
  real *ql, *qr, *uL, *uR;
  static real  **fL, **fR, *pL, *pR;

  real bmin, bmax, scrh1;
  real Us[NFLX];

  delta     = 1.e-7;
  delta_inv = 1.0/delta;
  #if EOS == IDEAL
   gmm1      = gmm - 1.0;
   gmm1_inv  = 1.0/gmm1;
  #endif

  if (fL == NULL){
    fL = Array_2D(NMAX_POINT, NFLX, double);
    fR = Array_2D(NMAX_POINT, NFLX, double);
    pR = Array_1D(NMAX_POINT, double);
    pL = Array_1D(NMAX_POINT, double);
  }

  for (i = NFLX; i--;  ) {
  for (j = NFLX; j--;  ) {
    Rc[i][j] = 0.0;
  }}

  /*  ----  initialize fluxes  ----  */

  FLUX  (state->uL, state->vL, fL, pL, beg, end);
  FLUX  (state->uR, state->vR, fR, pR, beg, end);

  for (i = beg; i <= end; i++)  {

    uR = state->uR[i];
    uL = state->uL[i];

    #if SHOCK_FLATTENING == MULTID   

    /* ---------------------------------------------
       HLL switching function as in Quirk (1994).
       Since the problem is related to multidimensional 
       pathologies, it works in more than 1-D only.	 
       Use the HLL flux function if the interface 
       lies within a strong shock.
       The effect of this switch is visible
       in the Mach reflection test.
      --------------------------------------------- */

     if (CHECK_ZONE(i, FLAG_HLL) || CHECK_ZONE(i+1, FLAG_HLL)){
       HLL_SPEED (state->vL, state->vR, &a2L - i, &a2R - i, i, i);
       a     = dmax(fabs(a2L), fabs(a2R));
      /*PS: replace using old style cmax
       cmax[i] = a;
      */
       *cmax  = dmax(*cmax, a);
       a2L   = dmin(0.0, a2L);
       a2R   = dmax(0.0, a2R);
       scrh  = 1.0/(a2R - a2L);
       for (nv = NFLX; nv--; ){
         state->flux[i][nv]  = a2L*a2R*(uR[nv] - uL[nv])
                            +  a2R*fL[i][nv] - a2L*fR[i][nv];
         state->flux[i][nv] *= scrh;
       }
       state->press[i] = (a2R*pL[i] - a2L*pR[i])*scrh;
       continue;
     }
    #endif

    ql = state->vL[i];
    qr = state->vR[i];

  /*  ----  prevent round off in velocity  ----  */
/*
    if ( fabs(ql[V1]) < 1.e-12 ) ql[V1] = 0.0;
    if ( fabs(qr[V1]) < 1.e-12 ) qr[V1] = 0.0;
*/

  /*  ----  Define Wave Jumps  ----  */

    for (nv = NFLX; nv--;   ) {
      dv[nv] = qr[nv] - ql[nv];

      /* --------------------------------------
           prevent round off from growing.
           This pathology is particularly 
           when solving a Riemann problem 
           with a tangential discontinuity 
           in density and transverse velocity. 
         -------------------------------------- */
/*
      if (fabs(dv[nv]) < 1.e-12) dv[nv] = 0.0;
*/

    }

    #if ROE_AVERAGE == YES    
     s       = sqrt(qr[DN]/ql[DN]);
     um[DN]  = ql[DN]*s;
     s       = 1.0/(1.0 + s); 
     c       = 1.0 - s;
  
     EXPAND(um[VX] = s*ql[VX] + c*qr[VX];  ,
            um[VY] = s*ql[VY] + c*qr[VY];  ,
            um[VZ] = s*ql[VZ] + c*qr[VZ];)

     #if EOS == IDEAL
      a2L = gmm*ql[PR]/ql[DN];
      a2R = gmm*qr[PR]/qr[DN];
      vel2 = EXPAND(um[VX]*um[VX], + um[VY]*um[VY], + um[VZ]*um[VZ]);

      hl  = 0.5*(EXPAND(ql[VX]*ql[VX], + ql[VY]*ql[VY], + ql[VZ]*ql[VZ]));    
      hl += a2L*gmm1_inv;
     
      hr = 0.5*(EXPAND(qr[VX]*qr[VX], + qr[VY]*qr[VY], + qr[VZ]*qr[VZ]));    
      hr += a2R*gmm1_inv;

      h = s*hl + c*hr;

  /* -------------------------------------------------
       the following should be  equivalent to 
    
       scrh = EXPAND(   dv[VX]*dv[VX],
                      + dv[VY]*dv[VY],
                      + dv[VZ]*dv[VZ]);

       a2 = s*a2L + c*a2R + 0.5*gmm1*s*c*scrh;

       and therefore always positive.
       just work out the coefficiendnts...

     -------------------------------------------------- */
     
      a2 = gmm1*(h - 0.5*vel2);
      a  = sqrt(a2);
     #endif
    #else
     for (nv = NFLX; nv--;   ) um[nv] = 0.5*(ql[nv] + qr[nv]);  
     #if EOS == IDEAL
      a2   = gmm*um[PR]/um[DN];
      a    = sqrt(a2);
     
      vel2 = EXPAND(um[VX]*um[VX], + um[VY]*um[VY], + um[VZ]*um[VZ]);
      h    = 0.5*vel2 + a2/gmm1;
     #endif /* EOS == IDEAL */
    #endif /* ROE_AVERAGE == YES */
  
    #if EOS == ISOTHERMAL
     a2 = C_ISO2;
     a  = C_ISO;
    #endif

  /* ----------------------------------------------------------------
      define non-zero components of conservative eigenvectors Rc, 
      eigenvalues (lambda) and wave strenght eta = L.du     
     ----------------------------------------------------------------  */

  /*  ---- (u - c_s)  ----  */ 

    nn         = 0;
    lambda[nn] = um[V1] - a;
    #if EOS == IDEAL
     eta[nn] = 0.5/a2*(dv[PR] - dv[V1]*um[DN]*a);
    #elif EOS == ISOTHERMAL
     eta[nn] = 0.5*(dv[DN] - um[DN]*dv[V1]/C_ISO);
    #endif

    Rc[DN][nn]        = 1.0;
    EXPAND(Rc[M1][nn] = um[V1] - a;   ,
           Rc[M2][nn] = um[V2];       ,
           Rc[M3][nn] = um[V3];)
    #if EOS == IDEAL
     Rc[EN][nn] = h - um[V1]*a;
    #endif

  /*  ---- (u + c_s)  ----  */ 

    nn         = 1;
    lambda[nn] = um[V1] + a;
    #if EOS == IDEAL
     eta[nn]    = 0.5/a2*(dv[PR] + dv[V1]*um[DN]*a);
    #elif EOS == ISOTHERMAL
     eta[nn] = 0.5*(dv[DN] + um[DN]*dv[V1]/C_ISO);
    #endif

    Rc[DN][nn]        = 1.0;
    EXPAND(Rc[M1][nn] = um[V1] + a;   ,
           Rc[M2][nn] = um[V2];       ,
           Rc[M3][nn] = um[V3];)
    #if EOS == IDEAL
     Rc[EN][nn] = h + um[V1]*a;
    #endif

  /*  ----  (u)  ----  */ 
     
    #if EOS == IDEAL
     nn         = 2;
     lambda[nn] = um[V1];
     eta[nn]    = dv[DN] - dv[PR]/a2;
     Rc[DN][nn]        = 1.0;
     EXPAND(Rc[MX][nn] = um[VX];   ,
            Rc[MY][nn] = um[VY];   ,
            Rc[MZ][nn] = um[VZ];)
     Rc[EN][nn]        = 0.5*vel2;
    #endif
    
    #if COMPONENTS > 1

  /*  ----  (u)  ----  */ 

     nn++;
     lambda[nn] = um[V1];
     eta[nn]    = um[DN]*dv[V2];
     Rc[M2][nn] = 1.0;
     #if EOS == IDEAL
      Rc[EN][nn] = um[V2];  
     #endif
    #endif

    #if COMPONENTS > 2

  /*  ----  (u)  ----  */ 

     nn++;
     lambda[nn] = um[V1];
     eta[nn]    = um[DN]*dv[V3];
     Rc[M3][nn] = 1.0;
     #if EOS == IDEAL
      Rc[EN][nn] = um[V3];  
     #endif
    #endif

  /*  ----  get max eigenvalue  ----  */

      /*PS: replace using old style cmax
	cmax[i] = fabs(um[V1]) + a;
      */
    *cmax  = dmax(*cmax, fabs(um[V1]) + a);
    MAX_MACH_NUMBER = dmax(fabs(um[V1]/a), MAX_MACH_NUMBER);

    #if DIMENSIONS > 1
   
    /* ---------------------------------------------
         use the HLL flux function if the interface 
         lies within a strong shock.
         The effect of this switch is visible
         in the Mach reflection test.
      --------------------------------------------- */

     #if EOS == IDEAL
      scrh  = fabs(ql[PR] - qr[PR]);
      scrh /= dmin(ql[PR],qr[PR]);
     #elif EOS == ISOTHERMAL
      scrh  = fabs(ql[DN] - qr[DN]);
      scrh /= dmin(ql[DN],qr[DN]);
      scrh *= C_ISO2;
     #endif
     if (scrh > 0.5 && (qr[V1] < ql[V1])){   /* -- tunable parameter -- */
       bmin = dmin(0.0, lambda[0]);
       bmax = dmax(0.0, lambda[1]);
       scrh1 = 1.0/(bmax - bmin);
       for (nv = NFLX; nv--;   ){
        state->flux[i][nv]  = bmin*bmax*(uR[nv] - uL[nv])
                          +   bmax*fL[i][nv] - bmin*fR[i][nv];
        state->flux[i][nv] *= scrh1;
       }
       state->press[i] = (bmax*pL[i] - bmin*pR[i])*scrh1;
       continue;
     } 
    #endif

    #if CHECK_ROE_MATRIX == YES
     for (nv = 0; nv < NFLX; nv++){
       um[nv] = 0.0;
       for (k = 0; k < NFLX; k++){
       for (j = 0; j < NFLX; j++){
         um[nv] += Rc[nv][k]*(k==j)*lambda[k]*eta[j];
       }}
     }
     for (nv = 0; nv < NFLX; nv++){
       scrh = fR[i][nv] - fL[i][nv] - um[nv];
       if (nv == M1) scrh += pR[i] - pL[i];
       if (fabs(scrh) > 1.e-6){
         print ("! Matrix condition not satisfied %d, %12.6e\n", nv, scrh);
         SHOW(state->vL, i);
         SHOW(state->vR, i);
         exit(1);
       }
     }
    #endif

  /* -----------------------------------------------------------
                      compute Roe flux 
     ----------------------------------------------------------- */
      
    for (nv = NFLX; nv--;   ) alambda[nv]  = fabs(lambda[nv]);

  /*  ----  entropy fix  ----  */

    if (alambda[0] <= delta) {
      alambda[0] = 0.5*lambda[0]*lambda[0]/delta + 0.5*delta;
    }
    if (alambda[1] <= delta) {
      alambda[1] = 0.5*lambda[1]*lambda[1]/delta + 0.5*delta;
    }

    for (nv = NFLX; nv--;   ) {
      state->flux[i][nv] = fL[i][nv] + fR[i][nv];
      for (k  = NFLX; k-- ;   ) {
        state->flux[i][nv] -= alambda[k]*eta[k]*Rc[nv][k];
      }
      state->flux[i][nv] *= 0.5;
    }
    state->press[i] = 0.5*(pL[i] + pR[i]);
  }
}
#undef ROE_AVERAGE        
