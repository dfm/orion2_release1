#include "orion2.h"

/* ******************************************************************** */
void FLUX (real *u[], real *w[], real *fx[], real *p, 
           int beg, int end)
/* 
 *          
 *
 *      Flux vector in terms of primitive quantities.
 *
 *      V1 (M1)        = NORMAL VEL. COMPONENT;
 *      V2 (M2), V3(M3) = TRANSVERSE VEL. COMP;
 *
 *
 *********************************************************************** */
{
  int   nv, ii;

  for (ii = beg; ii <= end; ii++) {
    fx[ii][DN] = u[ii][M1];
    EXPAND(fx[ii][MX] = u[ii][MX]*w[ii][V1]; ,
           fx[ii][MY] = u[ii][MY]*w[ii][V1]; ,
           fx[ii][MZ] = u[ii][MZ]*w[ii][V1];)
    #if EOS == IDEAL     
     p[ii] = w[ii][PR];
     fx[ii][EN] = (u[ii][EN] + w[ii][PR])*w[ii][V1];
    #elif EOS == ISOTHERMAL
     p[ii] = C_ISO2*w[ii][DN];
    #endif
  }
}
