#include"orion2.h"



/* ********************************************************************** */
void AUSMp (const State_1D *state, real *cmax, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   - Solve riemann problem for the Euler equations using the AUSM+ 
 *     scheme given in 
 *
 *  "A Sequel to AUSM: AUSM+"
 *  Liou, M.S., JCP (1996), 129, 364
 *
 * LAST_MODIFIED
 *
 *   July 17th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ************************************************************************ */
{
  int  i, nv, beg, end;
  real aL, asL2, asL, atL,  phiL[NFLX];
  real aR, asR2, asR, atR,  phiR[NFLX];
  real ML, Mp1L, Mp2L, Mm2L, Mp4L, Pp5L;
  real MR, Mm1R, Mp2R, Mm2R, Mm4R, Pm5R;
  real a, m;
  real Ku, Kp, sigma, fa, Mm2, Mo2, Mo, M, scrh;
  real *vL, *vR, *uL, *uR;
  real alpha = 3.0/16.0, beta = 0.125;
  static real  **fl, **fr, **ul, **ur;


  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend;

  if (fl == NULL){
    fl = array_2D(NMAX_POINT, NFLX);
    fr = array_2D(NMAX_POINT, NFLX);
    ul = array_2D(NMAX_POINT, NVAR);
    ur = array_2D(NMAX_POINT, NVAR);
  }

  PRIMTOCON (state->vL, ul, beg, end);
  PRIMTOCON (state->vR, ur, beg, end);

  Ku = 0.75; Kp = 0.25; sigma = 1.0;

  for (i = beg; i <= end; i++)  {

    vL = state->vL[i];
    vR = state->vR[i];

    uL = ul[i];
    uR = ur[i];

    phiL[DN] = 1.0;
    EXPAND(phiL[MX] = vL[VX];  ,
           phiL[MY] = vL[VY];  ,
           phiL[MZ] = vL[VZ];)
    phiL[EN] = (uL[EN] - vL[PR])/vL[DN];

    phiR[DN] = 1.0;
    EXPAND(phiR[MX] = vR[VX];  ,
           phiR[MY] = vR[VY];  ,
           phiR[MZ] = vR[VZ];)
    phiR[EN] = (uR[EN] - vR[PR])/vR[DN];


    aL = sqrt(gmm*vL[PR]/vL[DN]);
    aR = sqrt(gmm*vR[PR]/vR[DN]);

    asL2  = EXPAND(vL[VX]*vL[VX], + vL[VY]*vL[VY], + vL[VZ]*vL[VZ]);
    asL2  = aL*aL/(gmm - 1.0) + 0.5*asL2;
    asL2 *= 2.0*(gmm - 1.0)/(gmm + 1.0);

    asR2  = EXPAND(vR[VX]*vR[VX], + vR[VY]*vR[VY], + vR[VZ]*vR[VZ);
    asR2  = aR*aR/(gmm - 1.0) + 0.5*asR2;
    asR2 *= 2.0*(gmm - 1.0)/(gmm + 1.0);

    asL = sqrt(asL2);
    asR = sqrt(asR2);

    atL = asL2/dmax(asL, fabs(vL[V1]));
    atR = asR2/dmax(asR, fabs(vR[V1]));

    a = dmin(atL, atR);
/*
    a = 0.5*(aL + aR);
*/


  /* --------------------------------------------
          Compute alpha and beta coeff
     -------------------------------------------- */

    Mm2 = 0.5*(vL[V1]*vL[V1] + vR[V1]*vR[V1])/a/a;
    Mo2 = dmax(Mm2, 0.4);
    Mo2 = dmin(1.0, Mo2);
    Mo  = sqrt(Mo2);
    fa  = Mo*(2.0 - Mo);

    alpha = 3.0/16.0*(-4.0 + 5.0*fa*fa);
    beta  = 1.0/8.0;

  /* --------------------------------------------
          define split Mach numbers  
          define pressure terms
     -------------------------------------------- */

    ML = vL[V1]/a;
    MR = vR[V1]/a;

    Mp1L = 0.5*(ML + fabs(ML));
    Mm1R = 0.5*(MR - fabs(MR));

    Mp2L =   0.25*(ML + 1.0)*(ML + 1.0);
    Mm2L = - 0.25*(ML - 1.0)*(ML - 1.0);

    Mp2R =   0.25*(MR + 1.0)*(MR + 1.0);
    Mm2R = - 0.25*(MR - 1.0)*(MR - 1.0);

    if (fabs(ML) >= 1.0){
      Mp4L = Mp1L;
      Pp5L = Mp1L/ML;
    }else{
      Mp4L = Mp2L*(1.0 - 16.0*beta*Mm2L);
      Pp5L = Mp2L*((2.0 - ML) - 16.0*alpha*ML*Mm2L);
    }

    if (fabs(MR) >= 1.0){
      Mm4R = Mm1R;
      Pm5R = Mm1R/MR;
    }else{
      Mm4R = Mm2R*(1.0 + 16.0*beta*Mp2R);
      Pm5R = Mm2R*(( - 2.0 - MR) + 16.0*alpha*MR*Mp2R);
    }

    scrh  = dmax(1.0 - sigma*Mm2, 0.0);

    M  = Mp4L + Mm4R - Kp/fa*scrh*(vR[PR] - vL[PR])/(vL[DN] + vR[DN])/a/a*2.0;

    m  = a*M;
    m *= M > 0.0 ? vL[DN]: vR[DN];

  /* -------------------------------------------------------------
                     Compute fluxes 
     ------------------------------------------------------------- */

    state->press[i]  = Pp5L*vL[PR] + Pm5R*vR[PR];
    state->press[i] -= Ku*Pp5L*Pm5R*(vL[DN] + vR[DN])*fa*a*(vR[V1] - vL[V1]);

    if (m > 0.0){
      for (nv = NFLX; nv--;   ) state->flux[i][nv] = m*phiL[nv];
    }else{
      for (nv = NFLX; nv--;   ) state->flux[i][nv] = m*phiR[nv];
    }
 
  /*  ----  get max eigenvalue  ----  */

    *cmax = dmax(*cmax, fabs(vL[V1]) + aL);
    *cmax = dmax(*cmax, fabs(vR[V1]) + aR);

    MAX_MACH_NUMBER = dmax(fabs(ML), MAX_MACH_NUMBER);
    MAX_MACH_NUMBER = dmax(fabs(MR), MAX_MACH_NUMBER);

  }
}
