#include"orion2.h"

/*  Define  cell i,j,k boundary points  */

#define Ipm      (DIMENSIONS >= 1)
#define Jpm      (DIMENSIONS >= 2)
#define Kpm      (DIMENSIONS >= 3)
#define EPS      1.e-12

static double ****psi;

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/*
 *
 *
 *
 ************************************************************** */
{
  int i,j,k,nv;
  int beg, end;
  double **v, **vp, **vm;
  double dv;
  
  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

  if (DIR == IDIR){
    j = *NY_PT; k = *NZ_PT;
    
    for (i = beg; i <= end; i++) {
    for (nv = NVAR; nv--; ){
      dv = 0.25*psi[nv][k][j][i]*(v[i + 1][nv] - v[i - 1][nv]);
      vp[i][nv] = v[i][nv] + dv;
      vm[i][nv] = v[i][nv] - dv;
    }}

  }else if (DIR == JDIR){

    i = *NX_PT; k = *NZ_PT;
    
    for (j = beg; j <= end; j++) {
    for (nv = NVAR; nv--; ){
      dv = 0.25*psi[nv][k][j][i]*(v[j + 1][nv] - v[j - 1][nv]);
      vp[j][nv] = v[j][nv] + dv;
      vm[j][nv] = v[j][nv] - dv;
    }}

  }else if (DIR == KDIR){

    i = *NX_PT; j = *NY_PT;
    
    for (k = beg; k <= end; k++) {
    for (nv = NVAR; nv--; ){
      dv = 0.25*psi[nv][k][j][i]*(v[k + 1][nv] - v[k - 1][nv]);
      vp[k][nv] = v[k][nv] + dv;
      vm[k][nv] = v[k][nv] - dv;
    }}

  }

  #if SHOCK_FLATTENING != NO
   FLATTEN (state, beg, end, grid);
  #endif

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}

/* ******************************************************************** */
void MULTID_LIMITER (const Data *d, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   Compute a multidimensional limiter psi(i,j,k) so that
 *
 *
 *    q(x,y,z) = q + psi*(dq_x/dx*x + dq_y/dy*y + dq_z/dz*z)
 *
 *  lies within the bounds provided by neighbor cells.
 * 
 *                                / q1max - q  q - q1min \
 *   psi(i,j,k) = min(1, kappa*min| ---------, --------- |  )
 *                                \ qmax  - q  q - qmin  /
 *
 *  where q1max and q1min are the max and min values of q in 
 *  the neighbor cells. qmax and qmin are the max and min values
 *  in the reconstruction.
 *
 ********************************************************************** */
{
  int  i, j, k, nv; 
  int  i1, j1, k1;
  int  ibeg, jbeg, kbeg;
  int  iend, jend, kend;
  real scrh1, scrh2;
  real vmax, v1max, vc;
  real vmin, v1min, dv;
  real dvx, dvy, dvz, dvmax;
  real kappa[NVAR];
  real ***V;

  if (psi == NULL) psi = Array_4D(NVAR, NZ_TOT, NY_TOT, NX_TOT,double);


/*   here  0.5 <= kappa <= 1.
 
     for kappa = 0.5 we have a minmod limiter
     for kappa = 1.0 we have a (MC) limiter   */

  kappa[DN]   = 1.0;
  EXPAND(kappa[VX] = 0.5;  ,
         kappa[VY] = 0.5;  ,
         kappa[VZ] = 0.5;)

  #if PHYSICS == MHD || PHYSICS == RMHD
   EXPAND(kappa[BX] = 1.0;  ,
          kappa[BY] = 1.0;  ,
          kappa[BZ] = 1.0;)
  #endif

  #if EOS != ISOTHERMAL
   kappa[PR] = 0.75;
  #endif

  for (nv = NFLX; nv < NVAR; nv++) kappa[nv] = 1.0;

/* -- get the maximum allowable stencil for a linear interpolants -- */

  jbeg = jend = kbeg = kend = 0;

  D_EXPAND(ibeg = IBEG - 1; iend = IEND + 1; ,   
           jbeg = JBEG - 1; jend = JEND + 1; ,   
           kbeg = KBEG - 1; kend = KEND + 1;)

  for (nv = NVAR; nv--;  ){
    V = d->Vc[nv];
    for (k = kbeg; k <= kend; k++){
    for (j = jbeg; j <= jend; j++){
    for (i = ibeg; i <= iend; i++){
 
  /* -- get the max and min value in neighbor cells -- */
 
      v1max = v1min = vc = V[k][j][i];
      for (k1 = k - Kpm; k1 <= k + Kpm; k1++){
      for (j1 = j - Jpm; j1 <= j + Jpm; j1++){
      for (i1 = i - Ipm; i1 <= i + Ipm; i1++){
        v1max = dmax(v1max, V[k1][j1][i1]);
        v1min = dmin(v1min, V[k1][j1][i1]);
      }}}

      D_EXPAND(dvx = 0.5*(V[k][j][i + 1] - V[k][j][i - 1]);  ,
               dvy = 0.5*(V[k][j + 1][i] - V[k][j - 1][i]);  ,
               dvz = 0.5*(V[k + 1][j][i] - V[k - 1][j][i]);)

      dvmax  = D_EXPAND(fabs(dvx), + fabs(dvy), + fabs(daz));
      dvmax *= 0.5;
      vmax   = vc + dvmax; 
      vmin   = vc - dvmax;
    
      scrh1 =  (v1max - vc)/(dvmax + EPS);
      scrh2 =  (vc - v1min)/(dvmax + EPS);

      psi[nv][k][j][i] = kappa[nv]*dmin(scrh1, scrh2); 
      psi[nv][k][j][i] = dmin(1.0, psi[nv][k][j][i]);

    }}}
  }

}
    
#undef Ipm
#undef Jpm
#undef Kpm
#undef EPS
