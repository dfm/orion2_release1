#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    provide piece-wise linear reconstruction inside each 
 *    cell. Notice that vL and vR are left and right states
 *    with respect to the INTERFACE, while vm and vp (later
 *    in this function) refer to the cell center, that is:
 *
 *                    vL-> <-vR
 *      |--------*--------|--------*--------|
 *       <-vm   (i)   vp->       (i+1)
 *    
 *
 *
 *  Limiters are set in the function set_limiters(). 
 *  A default setup is used when LIMITER is set to DEFAULT in
 *  in your definitions.h. 
 *  Characteristic limiting is enabled when CHAR_LIMITING is set
 *  to YES in your definitions.h
 *
 *  REFERENCE
 *
 *
 *  "Adaptive Limiters for Improving the Accuracy
 *   of the MUSCL Approach for Unsteady Flows"
 *
 *   G. Billet and O. Louedin, JCP, 170, 161 (2001)
 *
 *  LAST MODIFIED
 *
 *   July 8, 2006 by A. mignone (mignone@to.astro.it).
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  real **v, **vp, **vm;
  real scrhp, scrhm;
  real A, B, i2, i4, om;
  static real *d, *s, *k, *Dp, *Dm;

  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend + 1;

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (d == NULL) {
    d  = Array_1D(NMAX_POINT, double);
    s  = Array_1D(NMAX_POINT, double);
    Dp = Array_1D(NMAX_POINT, double);
    Dm = Array_1D(NMAX_POINT, double);
    k  = Array_1D(NMAX_POINT, double);
  }

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

   for (nv = 0; nv < NVAR; nv++) {
     for (i = beg - 3; i <= end + 2; i++) {
       d[i] = v[i + 1][nv] - v[i][nv];
       s[i] = 0.0;
       if (d[i] > 0.0) s[i] =  1.0;
       if (d[i] < 0.0) s[i] = -1.0;
     }

     for (i = beg; i <= end; i++) {
     
       i4 = 0.25*fabs(s[i-2] + s[i-1] + s[i] + s[i+1]);
       i2 = 0.5 *fabs(s[i-1] + s[i]);

       k[i] = i4/3.0 + (1.0 - i4)*i2;
       om   = (3.0 - k[i])/(1.0 - k[i] + 1.e-9)*i2 + (1.0 - i2);
              
       A = d[i]  *s[i-1];
       B = d[i-1]*s[i];
       Dp[i] = dmin(A, om*B);
       Dp[i] = dmax(0.0, Dp[i])*s[i];

       Dm[i] = dmin(B, om*A);
       Dm[i] = dmax(0.0, Dm[i])*s[i-1];

     }

     for (i = beg; i <= end; i++) {
       scrhp = (1.0 - k[i])*Dm[i] + (1.0 + k[i])*Dp[i];
       scrhm = (1.0 - k[i])*Dp[i] + (1.0 + k[i])*Dm[i];
       
       vp[i][nv] = v[i][nv] + 0.25*scrhp;
       vm[i][nv] = v[i][nv] - 0.25*scrhm;
     }       
       
   }

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == YES 
   FLATTEN_1D (state, beg, end);
  #endif

}
