#include "orion2.h"
/*
http://chimeracfd.com/programming/gryphon/muscl.html
*/
static double PSI_FUNC (double);
static double LIM_FUNC (double dvp, double dvm);

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    provide piece-wise linear reconstruction inside each 
 *    cell. Notice that vL and vR are left and right states
 *    with respect to the INTERFACE, while vm and vp (later
 *    in this function) refer to the cell center, that is:
 *
 *                    vL-> <-vR
 *      |--------*--------|--------*--------|
 *       <-vm   (i)   vp->       (i+1)
 *    
 *
 *
 *  Limiters are set in the function set_limiters(). 
 *  A default setup is used when LIMITER is set to DEFAULT in
 *  in your definitions.h. 
 *  Characteristic limiting is enabled when CHAR_LIMITING is set
 *  to YES in your definitions.h
 *
 *
 *  A stencil of 3 zones is required for all limiters 
 *  except for the fourth_order_lim which requires 
 *  5 zones. 
 *
 *  LAST MODIFIED
 *
 *   May 2, 2008 by A. mignone (mignone@to.astro.it).
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  static int s = 1;
  double k = 1.0, dvl, r, eta, eps = 1.e-9;
  double ap, am, dap, dam;
  real **v, **vp, **vm, *dvp, *dvm;
  static real **maxv, **minv, **dv;

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (dv == NULL) {
    dv   = Array_2D(NMAX_POINT, NVAR, double);
    maxv = Array_2D(NMAX_POINT, NVAR, double);
    minv = Array_2D(NMAX_POINT, NVAR, double);
  }

/* ---- get stencil ---- */

  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

  #if CHAR_LIMITING == NO   /* ---- primitive variable limiting ---- */

   for (i = beg-1; i <= end; i++){
   for (nv = NVAR; nv--; ){
     dv[i][nv] = v[i+1][nv] - v[i][nv];
     if (v[i][nv] > v[i+1][nv]) {
       maxv[i][nv] = v[i][nv];
       minv[i][nv] = v[i+1][nv];
     }else{
       maxv[i][nv] = v[i+1][nv];
       minv[i][nv] = v[i][nv];
     }      
   }}
   for (i = beg; i <= end; i++){
     dvp = dv[i];
     dvm = dv[i-1];
     
     for (nv = NVAR; nv--; ){
/*
       r = dvm[nv]/(dvp[nv] + 1.e-6);
       ap = PSI_FUNC(r);
       am = PSI_FUNC(1.0/r);

       vp[i][nv] = v[i][nv] + 0.5*ap*dvp[nv];
       vm[i][nv] = v[i][nv] - 0.5*am*dvm[nv];
*/
       vp[i][nv] = v[i][nv] + 0.5*LIM_FUNC(dvp[nv], dvm[nv]);
       vm[i][nv] = v[i][nv] - 0.5*LIM_FUNC(dvm[nv], dvp[nv]);

/*
dvl = (2.0*dvp[nv]*dvm[nv] + 1.e-6)/(dvp[nv]*dvp[nv] + dvm[nv]*dvm[nv]+1.e-6);     
dvl = 2.0*r/(r*r + 1.0);             
dvl = (r + fabs(r))/(1.0 + fabs(r)); 
dvl = dmax(0.0, dmin(1.0 ,r));       
dap = + 0.25*dvl*((1.0 - k)*dvm[nv] + (1.0 + k)*dvp[nv]);
dam = - 0.25*dvl*((1.0 - k)*dvp[nv] + (1.0 + k)*dvm[nv]);
*/

/*
eta = grid[DIR].dx[i]*0.1;
eta = (dvp[nv]*dvp[nv] + dvm[nv]*dvm[nv])/(eta*eta);
if (eta <= 1.0 - eps) {
  ap = (2.0+r)/3.0;
  am = (2.0+1.0/r)/3.0;
}else if (eta >= 1.0 + eps){
}else{
ap = 0.5*(  (1.0 - (eta-1.0)/eps)*(2.0 + r)/3.0 
          + (1.0 + (eta-1.0)/eps)*ap);
am = 0.5*(  (1.0 - (eta-1.0)/eps)*(2.0 + 1./r)/3.0 
          + (1.0 + (eta-1.0)/eps)*am);
}
*/


/*
ap = am = MC(dv[i][nv], dv[i-1][nv]); 
vp[i][nv] = v[i][nv] + 0.5*ap;
vm[i][nv] = v[i][nv] - 0.5*am;
*/
     }
   }        

  #else                     /* ---- characteristic variable limiting ---- */

  /* ------------ first version ------------------ */
  
   for (i = 0; i < grid[DIR].np_tot - 1; i++) {
   for (nv = NVAR; nv--; ){
     dv[i][nv]   = v[i + 1][nv] - v[i][nv];
   }}
   CHAR_SLOPES (v, dv, dv_lim, limiter, beg, end, grid);
   for (i = beg; i <= end; i++) {
   for (nv = NVAR; nv--; ){
     vm[i][nv] = v[i][nv] - 0.5*dv_lim[i][nv];
     vp[i][nv] = v[i][nv] + 0.5*dv_lim[i][nv];
   }}

  #endif
   
/*  -------------------------------------------
               Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == MULTID
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == ONED 
   FLATTEN (state, beg, end, grid);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg - 1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}


double PSI_FUNC (double r)
{
  double a,b,c,q;
/*  
  a = (2.0 + r)/3.0;
  b = 2.0*fabs(r)/(0.6+fabs(r));
  c = 5.0/fabs(r);
  a = dmin(a,b);
  a = dmin(a,c);
  return dmax(-3.0/fabs(r),a);
*/
  q = (2.0 + r)/3.0;
  a = dmin(1.5,2.0*r);
  a = dmin(q,a);
  b = dmax(-0.5*r,a);
  c = dmin(q,b);
  return dmax(0.0, c);
 
}

double LIM_FUNC (double dvp, double dvm)
{
  double sp_dvp, sp_dvm, q, scrh;
  
  sp_dvp = fabs(dvp);
  sp_dvm = (dvp >= 0.0 ? dvm:-dvm);
  
  q = (2.0*sp_dvp + sp_dvm)/3.0;
  scrh = dmin( 2.0*sp_dvm, 1.5*sp_dvp);
  scrh = dmax(-0.5*sp_dvm, scrh);
  scrh = dmax(0.0,dmin(q,scrh));
  return (dvp >= 0.0 ? scrh:-scrh);  
}



