#include"orion2.h"

/* ----------------------------------------------
         PPM interpolation parameters
   ---------------------------------------------- */

#if PHYSICS == HD
 #define   K0      0.1
 #define   ETA1    20.0
 #define   ETA2    0.05
 #define   EPS1    0.01

 #define CONTACT_STEEPENING  YES

#endif

#if (PHYSICS == MHD || PHYSICS == RMHD)

 #define   K0      0.1
 #define   ETA1    20.0
 #define   ETA2    0.05
 #define   EPS1    0.01

 #define CONTACT_STEEPENING NO

#endif

#if PHYSICS == RHD

 #define   K0      1.0
 #define   ETA1    5.0
 #define   ETA2    0.05
 #define   EPS1    0.1

 #if DIMENSIONS == 1
  #define CONTACT_STEEPENING  YES
 #else
  #define CONTACT_STEEPENING  NO
 #endif 

#endif

void CHECK_BOUND (real ***vv[], real **a, real **vm, real **ap,
                  int *in, int *nxp, int *nyp, int *nzp, int ibeg, int iend,
                  struct GRID *GXYZ);
void CHECK_LIM (real **vm, real **ap, int ibeg, int iend);

static void STEEPEN (real **, real **, real **,  
                     real **, Grid *, int, int);

static void PPM_COEFF (Grid *, real **, real **, real **, real **, real **);

/* ********************************************************************** */
void PPM_COEFF (Grid *grid, real **a, real **b, real **c, 
                real **d, real **e)
/* 
 *
 * PURPOSE
 *
 *   Compute PPM interpolation coefficients for arbitrary 
 *   mesh size. Coefficients are stored in the 2D arrays 
 *   a, b, c, d, e. The first index refers to the dimension,
 *   the second to the grid. 
 *   Coefficients for the steepening are computed in the 
 *   STEEPEN function.
 *
 *
 *
 ************************************************************************ */
{
  int    dim, i, beg, end;
  real scrh, *dx;

  for (dim = 0; dim < DIMENSIONS; dim++){
    beg = grid[dim].lbeg - grid[dim].nghost + 1;
    end = grid[dim].lend + grid[dim].nghost - 2;
    dx  = grid[dim].dx;
    for (i = beg; i <= end; i++) {

      scrh = 1.0/(dx[i - 1] + dx[i] + dx[i + 1] + dx[i + 2]);

      b[dim][i] = dx[i + 1]*scrh*(dx[i + 1] + dx[i + 2])/(dx[i] + 2.0*dx[i + 1]);
      c[dim][i] = dx[i]*scrh*(dx[i - 1] + dx[i])/(2.0*dx[i] + dx[i + 1]);

      a[dim][i] = dx[i]/(dx[i] + dx[i + 1]) + 
             + 2.0*(dx[i + 1]*c[dim][i] - dx[i]*b[dim][i])/(dx[i] + dx[i + 1]);

      scrh = dx[i]/(dx[i - 1] + dx[i] + dx[i + 1]);
      d[dim][i] = scrh*(2.0*dx[i - 1] + dx[i])/(dx[i + 1] + dx[i]);
      e[dim][i] = scrh*(2.0*dx[i + 1] + dx[i])/(dx[i - 1] + dx[i]);


    /*  When the grid is uniform you should recover */

/*
       a[dim][i] = 0.5;
       b[dim][i] = c[dim][i] = 1.0/6.0;
       d[dim][i] = e[dim][i] = 0.5;
  */  
    }
  }
}

/* ********************************************************************** */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/*
 *
 *
 *  PURPOSE
 *    
 *    provide piece-wise parabolic reconstruction inside each 
 *    cell. Notice that wl and wr are left and right states
 *    with respect to the INTERFACE, while am and ap (later
 *    in this function) refer to the cell center, that is:
 *
 *                    vL-> <-vR
 *      |--------*--------|--------*--------|
 *       <-vm   (i)   vp->       (i+1)
 *
 *
 ************************************************************************ */
{
  int     beg, end, i, nv, s, k;
  real  scrh1, scrh2, scrh3, scrh4;
  real  steepness[NVAR];
  real  **vm, **vp, **vc, *a, *b, *c, *d, *e;
  static real  **dv, **dv_lim;
  static real **aa, **bb, **cc, **dd, **ee;
  static Limiter *limiter[NVAR];

/* --------------------------------------------------- 
     PPM stencil is +- 2 zones:

     |---*---|---*---|---*---|---*---|---*---|
        i-2     i-1      i      i+1     i+2

     Thus, only 3 ghost zones are necessary.
     However, if FLATTENING or STEEPENING are added, 
     one more zone is required and the number of 
     ghost zones becomes 4.

     We define the leftmost and rightmost indexes  
     in the domain as 

      beg - s = 0
      end + s = grid[DIR].np_tot - 1

     where s is the required stencil.
   ---------------------------------------------------- */

  s = 3;
  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

  /* PS: Flux-ct scheme, before upgrade
#ifdef STAGGERED_MHD
  beg = grid[DIR].lbeg - 2;
  end = grid[DIR].lend + 2;
#else
  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend + 1;
#endif
  */

  if (dv == NULL){
    dv     = Array_2D(NMAX_POINT, NVAR, double);
    dv_lim = Array_2D(NMAX_POINT, NVAR, double);
    aa     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    bb     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    cc     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    dd     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    ee     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    PPM_COEFF (grid, aa, bb, cc, dd, ee);
    SET_LIMITER (limiter);
  }

  for (nv = NVAR; nv--; ) steepness[nv] = 2.0;

  vm = state->vm;
  vp = state->vp;
  vc = state->v;

  a = aa[DIR];
  b = bb[DIR];
  c = cc[DIR];
  d = dd[DIR];
  e = ee[DIR];

/* -------------------------------------------------------
         Compute undivided differences
   ------------------------------------------------------- */

  for (i = beg - s; i <= end + s - 1; i++) {
  for (nv = 0; nv < NVAR; nv++) {
    dv[i][nv] = vc[i + 1][nv] - vc[i][nv];
  }}

/* ---------------------------------------------------------
       STEP #1:   single value edge interpolation
   --------------------------------------------------------- */

  #if CHAR_LIMITING == NO

   for (i = beg - s + 1; i <= end + s - 1; i++) {
   for (nv = 0; nv < NVAR; nv++){
     scrh1 = dv[i][nv]*dv[i - 1][nv];
     if (scrh1 > 0.0) {
       scrh2 = d[i]*dv[i][nv] + e[i]*dv[i - 1][nv];
       scrh3 = steepness[nv]*dmin(fabs(dv[i][nv]), fabs(dv[i - 1][nv]));
       scrh3 = dmin(fabs(scrh2), scrh3);
       dv_lim[i][nv] = scrh3*dsign(scrh2);
     }else{
       dv_lim[i][nv] = 0.0;
     }
   }}

  #else 
   CHAR_SLOPES (state, dv_lim, beg - s + 1, end + s - 1, grid);
  #endif

  for (i = beg - s + 1; i <= end + s - 2; i++) {
  for (nv = 0; nv < NVAR; nv++){
    vp[i][nv] = vc[i][nv] + a[i]*dv[i][nv]  
                + b[i]*dv_lim[i][nv] - c[i]*dv_lim[i + 1][nv];
    vm[i + 1][nv] = vp[i][nv];
  }}

  #if CONTACT_STEEPENING == YES
   STEEPEN (vc, vm, vp, dv_lim, grid, beg, end);
  #endif

/* -------------------------------------------------------
        S T E P     # 3:  Parabolic Limiter
   ------------------------------------------------------- */

  for (i = beg - s + 2; i <= end + s - 2; i++) {
  for (nv = 0; nv < NVAR; nv++){
    scrh1 = (vp[i][nv] - vc[i][nv])*(vc[i][nv] - vm[i][nv]);
    if (scrh1 <= 0.0) {
      vm[i][nv] = vp[i][nv] = vc[i][nv];
    }
  }}

  for (i = beg - s + 2; i <= end + s - 2; i++) {
  for (nv = 0; nv < NVAR; nv++){
    scrh2 = vp[i][nv] - vm[i][nv];
    scrh3 = scrh2*(vc[i][nv] - 0.5*(vp[i][nv] + vm[i][nv]));
    scrh4 = scrh2*scrh2 / 6.0;

    if (scrh3 > scrh4) {
      vm[i][nv] = 3.0*vc[i][nv] - 2.0*vp[i][nv];
    }
      
    if ((-scrh4) > scrh3) {
      vp[i][nv] = 3.0*vc[i][nv] - 2.0*vm[i][nv];
    }
  }}

/* --------------------------------------------------------
            S T E P   # 4 : Flattening
   -------------------------------------------------------- */

  #if SHOCK_FLATTENING != NO
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------------
         S T E P   # 5 : Additional limiting for 
                         Relativistic flows 
   -------------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif
}

#if 1==0
/* ********************************************************************** */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/*
 *
 *  Same as the old one, except:
 *
 *  - linear slopes and parabolic limiters can be applied 
 *    on either characteristic or primitive variables;
 *
 *  - MULTID flattening consists of 
 *     a) use minmod lim to compute slopes
 *     b) use a linear interpolant inside the cell 
 *
 *
 ************************************************************************ */
{
  int    beg, end, i, nv, s, k;
  real   **vm, **vp, **vc, *a, *b, *c, *d, *e;
  double scrh1, scrh2, scrh3;
  double *dvp, *dvm, dwp[NVAR], dwm[NVAR], **L, **R;
  double steep, dvmax, dvpc[NVAR], dvmc[NVAR];
  static real  **dv, **dv_lim;
  static real **aa, **bb, **cc, **dd, **ee;
  static double **vmax, **vmin;

/* --------------------------------------------------- 
     PPM stencil is +- 2 zones:

     |---*---|---*---|---*---|---*---|---*---|
        i-2     i-1      i      i+1     i+2

     Thus, only 3 ghost zones are necessary.
     However, if FLATTENING or STEEPENING are added, 
     one more zone is required and the number of 
     ghost zones becomes 4.

     We define the leftmost and rightmost indexes  
     in the domain as 

      beg - s = 0
      end + s = grid[DIR].np_tot - 1

     where s is the required stencil.
   ---------------------------------------------------- */

  s = 3;
  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

  if (dv == NULL){
    dv     = Array_2D(NMAX_POINT, NVAR, double);
    dv_lim = Array_2D(NMAX_POINT, NVAR, double);
    vmax   = Array_2D(NMAX_POINT, NVAR, double);
    vmin   = Array_2D(NMAX_POINT, NVAR, double);
    aa     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    bb     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    cc     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    dd     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    ee     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    PPM_COEFF (grid, aa, bb, cc, dd, ee);
  }

  vm = state->vm;
  vp = state->vp;
  vc = state->v;

  a = aa[DIR];
  b = bb[DIR];
  c = cc[DIR];
  d = dd[DIR];
  e = ee[DIR];

/* -------------------------------------------------------
         Compute undivided differences
   ------------------------------------------------------- */

  for (i = beg - s; i <= end + s - 1; i++) {
  for (nv = 0; nv < NVAR; nv++) {
    dv[i][nv] = vc[i + 1][nv] - vc[i][nv];
  }}

/* ---------------------------------------------------------
     Compute limited slopes in characteristic or 
     primitive variables.
   --------------------------------------------------------- */

  #if CHAR_LIMITING == YES

   CHAR_SLOPES_NEW (state, dv_lim, beg - s + 1, end + s - 1, grid);

  #else 

   i = beg - s;
   for (nv = NVAR; nv--;    ) {
     dv[i][nv] = vc[i + 1][nv] - vc[i][nv];
   }
   for (i = beg - s + 1; i <= end + s - 1; i++) {
     steep = 2.0;
     #if SHOCK_FLATTENING == MULTID    
      if (CHECK_ZONE (i, FLAG_MINMOD)) steep = 1.0; 
     #endif
     for (nv = NVAR; nv--;    ) {
       dv[i][nv] = vc[i + 1][nv] - vc[i][nv];
     }

     dvp = dv[i]; dvm = dv[i-1];
     for (nv = NVAR; nv--;     ){
       scrh1 = dvp[nv]*dvm[nv];
       if (scrh1 > 0.0) {
         scrh2 = d[i]*dvp[nv] + e[i]*dvm[nv];
         scrh3 = steep*dmin(fabs(dvp[nv]), fabs(dvm[nv]));
         scrh3 = dmin(fabs(scrh2), scrh3);
         dv_lim[i][nv] = scrh3*dsign(scrh2);
       }else{
         dv_lim[i][nv] = 0.0;
       }
     }
   }

  #endif

/* ---------------------------------------------------------
                  Interface values
   --------------------------------------------------------- */

  for (i = beg - s + 1; i <= end + s - 2; i++) {
    for (nv = NVAR; nv--;     ){
      vmax[i][nv] = dmax(vc[i][nv], vc[i+1][nv]);
      vmin[i][nv] = dmin(vc[i][nv], vc[i+1][nv]);

      vp[i][nv] = vc[i][nv] + a[i]*(vc[i + 1][nv] - vc[i][nv])  
                  + b[i]*dv_lim[i][nv] - c[i]*dv_lim[i + 1][nv];
      vm[i + 1][nv] = vp[i][nv];
    }
    #if SHOCK_FLATTENING == MULTID    
     if (CHECK_ZONE (i, FLAG_MINMOD)) {
       for (nv = NVAR; nv--;     ){
         vp[i][nv] = vc[i][nv] + a[i]*dv_lim[i][nv];
         vm[i][nv] = vc[i][nv] - a[i]*dv_lim[i][nv];
       }
     }
    #endif
  }

/* -------------------------------------------------------
                 Parabolic Limiter
   ------------------------------------------------------- */

  for (i = beg; i <= end; i++){

    dvp = dv[i]; dvm = dv[i-1];

    for (nv = NVAR; nv--;     ){
      dvpc[nv] = vp[i][nv] - vc[i][nv];
      dvmc[nv] = vm[i][nv] - vc[i][nv];
    }
 
    #if CHAR_LIMITING == NO

     L = state->Lp[i];
     R = state->Rp[i];
     LpPROJECT(L, dvpc, dwp);
     LpPROJECT(L, dvmc, dwm);
     for (k = NFLX; k--;  ){
       if (dwp[k]*dwm[k] > 0.0) {
         dwp[k] = dwm[k] = 0.0;
       }else {
         if      (fabs(dwp[k]) > 2.0*fabs(dwm[k])) dwp[k] = -2.0*dwm[k];
         else if (fabs(dwm[k]) > 2.0*fabs(dwp[k])) dwm[k] = -2.0*dwp[k];
       }
     }

     for (nv = NFLX; nv--; ){
       vp[i][nv] = vm[i][nv] = vc[i][nv];
       for (k = NFLX; k--;      ){
         vp[i][nv] += dwp[k]*R[nv][k];
         vm[i][nv] += dwm[k]*R[nv][k];
       }

       if      (vp[i][nv] > vmax[i][nv]) vp[i][nv] = vmax[i][nv];
       else if (vp[i][nv] < vmin[i][nv]) vp[i][nv] = vmin[i][nv];

       if      (vm[i][nv] > vmax[i-1][nv]) vm[i][nv] = vmax[i-1][nv];
       else if (vm[i][nv] < vmin[i-1][nv]) vm[i][nv] = vmin[i-1][nv];

     }
/*
     for (nv = NFLX; nv--; ){
       vp[i][nv] = vm[i][nv] = vc[i][nv];
       scrh1 = scrh2 = 0.0;
       for (k = NFLX; k--;      ){
         scrh1 += dwp[k]*R[nv][k];
         scrh2 -= dwm[k]*R[nv][k];
       }
       if (dvp[nv]*dvm[nv] > 0.0)
         dvmax = (fabs(dvp[nv]) < fabs(dvm[nv]) ? dvp[nv]:dvm[nv]);
       else
         dvmax = 0.0;
  
       if (dvmax*scrh1 >= 0.0) {
         vp[i][nv] += (fabs(dvmax) < fabs(scrh1) ? dvmax:scrh1);
       }
       if (dvmax*scrh2 >= 0.0) {
         vm[i][nv] -= (fabs(dvmax) < fabs(scrh2) ? dvmax:scrh2);
       }  
     }
*/
    /* -- passive scalars -- */

     #if NFLX != NVAR
      for (nv = NFLX; nv < NVAR; nv++){
        if (dvpc[nv]*dvmc[nv] > 0.0) {
          dvpc[nv] = dvmc[nv] = 0.0;
        }else {
          if      (fabs(dvpc[nv]) > 2.0*fabs(dvmc[nv])) dvpc[nv] = -2.0*dvmc[nv];
          else if (fabs(dvmc[nv]) > 2.0*fabs(dvpc[nv])) dvmc[nv] = -2.0*dvpc[nv];
        }
        vp[i][nv] = vc[i][nv] + dvpc[nv];
        vm[i][nv] = vc[i][nv] + dvmc[nv];
      }
     #endif

    #else

     for (nv = 0; nv < NVAR; nv++){
       if (dvpc[nv]*dvmc[nv] >= 0.0) {
         dvpc[nv] = dvmc[nv] = 0.0;
       }else {
         if      (fabs(dvpc[nv]) > 2.0*fabs(dvmc[nv])) dvpc[nv] = -2.0*dvmc[nv];
         else if (fabs(dvmc[nv]) > 2.0*fabs(dvpc[nv])) dvmc[nv] = -2.0*dvpc[nv];
       }
       vp[i][nv] = vc[i][nv] + dvpc[nv];
       vm[i][nv] = vc[i][nv] + dvmc[nv];
     }

    #endif
  }

/* --------------------------------------------------------
            S T E P   # 4 : Flattening
   -------------------------------------------------------- */

  #if SHOCK_FLATTENING == ONED
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------------
         S T E P   # 5 : Additional limiting for 
                         Relativistic flows 
   -------------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif
}

/* ********************************************************************** */
void RECONSTRUCT_NEW (const State_1D *state, Grid *grid)
/*
 *
 *  Provide PPM in Rider's way. It works in characteristic variables 
 *  only and requires more eigenvectors projections and limiters.
 *  In other words, the following differences are projected on 
 *  the *SAME* eigenvector:
 *
 *   dwm2 = L(i)*(v[i-1] - v[i-2])
 *   dwm1 = L(i)*(v[i  ] - v[i-1])
 *   dwp1 = L(i)*(v[i+1] - v[i  ])
 *   dwp2 = L(i)*(v[i+2] - v[i+1])
 *
 *
 *
 ************************************************************************ */
{
  int     beg, end, i, nv, s, k;
  real   **vm, **vp, **vc, *a, *b, *c, *d, *e;
  double **L, **R, dwm2[NVAR], dwm1[NVAR], dwp1[NVAR], dwp2[NVAR];
  double dwm[NVAR],dwc[NVAR],dwp[NVAR], lambda[NVAR];
  static real  **dv;
  static real **aa, **bb, **cc, **dd, **ee;
  static Limiter *limiter[NVAR];
  static double *a2, *h, **vmax, **vmin;
  
/* --------------------------------------------------- 
     PPM stencil is +- 2 zones:

     |---*---|---*---|---*---|---*---|---*---|
        i-2     i-1      i      i+1     i+2

     Thus, only 3 ghost zones are necessary.
     However, if FLATTENING or STEEPENING are added, 
     one more zone is required and the number of 
     ghost zones becomes 4.

     We define the leftmost and rightmost indexes  
     in the domain as 

      beg - s = 0
      end + s = grid[DIR].np_tot - 1

     where s is the required stencil.
   ---------------------------------------------------- */

  s = 3;
  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

  if (dv == NULL){
    dv   = Array_2D(NMAX_POINT, NVAR, double);
    aa   = Array_2D(DIMENSIONS, NMAX_POINT, double);
    bb   = Array_2D(DIMENSIONS, NMAX_POINT, double);
    cc   = Array_2D(DIMENSIONS, NMAX_POINT, double);
    dd   = Array_2D(DIMENSIONS, NMAX_POINT, double);
    ee   = Array_2D(DIMENSIONS, NMAX_POINT, double);
    vmax = Array_2D(NMAX_POINT, NVAR, double);
    vmin = Array_2D(NMAX_POINT, NVAR, double);
    a2   = Array_1D(NMAX_POINT, double);
    h    = Array_1D(NMAX_POINT, double);
    PPM_COEFF (grid, aa, bb, cc, dd, ee);
    SET_LIMITER (limiter);
  }

  vm = state->vm;
  vp = state->vp;
  vc = state->v;

  a = aa[DIR];
  b = bb[DIR];
  c = cc[DIR];
  d = dd[DIR];
  e = ee[DIR];

/* -------------------------------------------------------
         Compute undivided differences
   ------------------------------------------------------- */

  for (i = beg - s; i <= end + s - 1; i++) {
  for (nv = 0; nv < NVAR; nv++) {
    dv[i][nv] = vc[i + 1][nv] - vc[i][nv];
    if (vc[i][nv] > vc[i+1][nv]) {
      vmax[i][nv] = vc[i][nv]; vmin[i][nv] = vc[i+1][nv];
    }else{
      vmin[i][nv] = vc[i][nv]; vmax[i][nv] = vc[i+1][nv];
    }        
  }}

/* ---------------------------------------------------------
       STEP #1:   single value edge interpolation
   --------------------------------------------------------- */

  #if CHAR_LIMITING == YES

   SOUND_SPEED2 (vc, a2, h, beg, end);
   for (i = beg; i <= end; i++) {
     L = state->Lp[i];
     R = state->Rp[i];
     EIGENV (vc[i], a2[i], h[i], lambda, L, R);

     LpPROJECT(L, dv[i-2], dwm2);
     LpPROJECT(L, dv[i-1], dwm1);
     LpPROJECT(L, dv[i  ], dwp1);
     LpPROJECT(L, dv[i+1], dwp2);
     
     #if SHOCK_FLATTENING == MULTID    
      if (CHECK_ZONE (i, FLAG_MINMOD)){ 
        for (k = NFLX; k--;    ){   
          minmod_lim (dwm1 + k, dwp1 + k, dwc + k, 0, 0, grid + DIR);
          dwp[k] = 0.5*dwc[k];
          dwm[k] = -dwp[k];
        }         
      }else{
     #endif
        for (k = NFLX; k--;    ){
          (limiter[k]) (dwm2 + k, dwm1 + k, dwm + k, 0, 0, grid + DIR);
          (limiter[k]) (dwm1 + k, dwp1 + k, dwc + k, 0, 0, grid + DIR);
          (limiter[k]) (dwp1 + k, dwp2 + k, dwp + k, 0, 0, grid + DIR);
          dwp[k] = dwp1[k]*a[i]         + dwc[k]*b[i]   - dwp[k]*c[i];
          dwm[k] = dwm1[k]*(a[i-1]-1.0) + dwm[k]*b[i-1] - dwc[k]*c[i-1];
          if (dwp[k]*dwm[k] > 0.0) {
            dwp[k] = dwm[k] = 0.0;
          }else {
            if      (fabs(dwp[k]) > 2.0*fabs(dwm[k])) dwp[k] = -2.0*dwm[k];
            else if (fabs(dwm[k]) > 2.0*fabs(dwp[k])) dwm[k] = -2.0*dwp[k];
          }
        }   
     #if SHOCK_FLATTENING == MULTID
      }
     #endif                            

     for (nv = 0; nv < NVAR; nv++){
       #ifdef STAGGERED_MHD 
        if (nv == B1) continue;
       #endif	       
       vp[i][nv] = vm[i][nv] = vc[i][nv];
       for (k = NVAR; k--; ){       
         vp[i][nv] += dwp[k]*R[nv][k];
         vm[i][nv] += dwm[k]*R[nv][k];
       }           

       if      (vp[i][nv] > vmax[i][nv]) vp[i][nv] = vmax[i][nv];
       else if (vp[i][nv] < vmin[i][nv]) vp[i][nv] = vmin[i][nv];

       if      (vm[i][nv] > vmax[i-1][nv]) vm[i][nv] = vmax[i-1][nv];
       else if (vm[i][nv] < vmin[i-1][nv]) vm[i][nv] = vmin[i-1][nv];

     } 
   }         
  #endif

/* --------------------------------------------------------
            S T E P   # 4 : Flattening
   -------------------------------------------------------- */

  #if SHOCK_FLATTENING == ONED
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------------
         S T E P   # 5 : Additional limiting for 
                         Relativistic flows 
   -------------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}
#endif /* 1==0 */

/* ************************************************************ */
void STEEPEN (real **vv, real **vm, real **vp, 
              real **dv_lim, Grid *grid, int beg, int end)
/*
 *
 *  Apply contact steepener to density 
 *
 ************************************************************** */
{
  int    i, dim;
  real amd, apd;
  real eta_t, etai, gammak0;
  real scrh1, scrh2, scrh3, scrh4;
  static real  *drho, *delta2, *dp, *min_p, *min_rho;
  static real  **Sc1, **Sc2, **Sc3;
  real *sc1, *sc2, *sc3, *dx;
  #if EOS == ISOTHERMAL
   int PR = DN;
  #endif

  #if EOS != ISOTHERMAL
   gammak0 = gmm*K0;
  #else
   gammak0 = K0;
  #endif

  if (drho == NULL){
    dp      = Array_1D(NMAX_POINT, double);
    min_p   = Array_1D(NMAX_POINT, double);
    drho    = Array_1D(NMAX_POINT, double);
    min_rho = Array_1D(NMAX_POINT, double);
    delta2  = Array_1D(NMAX_POINT, double);
    Sc1     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    Sc2     = Array_2D(DIMENSIONS, NMAX_POINT, double);
    Sc3     = Array_2D(DIMENSIONS, NMAX_POINT, double);

    for (dim = 0; dim < DIMENSIONS; dim++){
      dx = grid[dim].dx;
      for (i = grid[dim].lbeg - 3; i <= grid[dim].lend + 3; i++) {
        scrh1 = 0.5*(dx[i - 1] + dx[i]);
        scrh2 = 0.5*(dx[i + 1] + dx[i]);
        Sc1[dim][i]  = (scrh1*scrh1*scrh1 + scrh2*scrh2*scrh2) / 
                       (scrh1 + scrh2);
        Sc2[dim][i]  = 1.0/(dx[i - 1] + dx[i] + dx[i + 1]);
        Sc3[dim][i]  = Sc2[dim][i]/(dx[i] + dx[i - 1]);
        Sc2[dim][i] /= (dx[i] + dx[i + 1]);
      }
    }
  }

  sc1 = Sc1[DIR];
  sc2 = Sc2[DIR];
  sc3 = Sc3[DIR];

  for (i = beg - 2; i <= end + 2; i++) {

    dp[i]    = vv[i + 1][PR] - vv[i - 1][PR];
    min_p[i] = dmin(vv[i + 1][PR], vv[i - 1][PR]);

    delta2[i]  =   sc2[i]*(vv[i+1][DN] - vv[i][DN]) 
                 + sc3[i]*(vv[i-1][DN] - vv[i][DN]);
    drho[i]    = vv[i + 1][DN] - vv[i - 1][DN];
    min_rho[i] = dmin(vv[i + 1][DN], vv[i - 1][DN]);
  }

  for (i = beg-1; i <= end+1; i++) {
    scrh1 = gammak0*fabs(drho[i])/min_rho[i];
    scrh2 = fabs(dp[i])/min_p[i];

    if (scrh1 > scrh2) {  /* -- zone i is inside a c.d. -- */
      scrh3 = -delta2[i + 1]*delta2[i - 1];
      scrh4 = fabs(drho[i]) - EPS1*min_rho[i];

          /* --  get ETA_TILDE -- */

      eta_t = 0.0;
      if ((scrh3 > 0.0) && (scrh4 > 0.0)) {
        eta_t = (delta2[i + 1] - delta2[i - 1])/drho[i];
        eta_t *= -sc1[i];
      }

     /* --  get ETA  -- */

      scrh3 = dmin (ETA1*(eta_t - ETA2), 1.0);
      etai  = dmax (0.0, scrh3);

      amd = vv[i - 1][DN] + 0.5*dv_lim[i - 1][DN];
      apd = vv[i + 1][DN] - 0.5*dv_lim[i + 1][DN];

      scrh3 = 1.0 - etai;
      vm[i][DN] = vm[i][DN]*scrh3 + amd*etai;
      vp[i][DN] = vp[i][DN]*scrh3 + apd*etai;
    }
  }
}

/* #################################################################### */
void CHECK_BOUND (real ***vv[], real **a, real **am, real **ap, 
                  int *in, int *nxp, int *nyp, int *nzp, int ibeg, int iend,
                  struct GRID *GXYZ)
/*
 #
 #
 #
 #
 ##################################################################### */
{
  int  i1, j1, k1, nv;
  real xmax, xmin;

  for (*in = ibeg; *in <= iend; (*in)++) {
    for (nv = 0; nv < NVAR; nv++){
      xmax = xmin = vv[nv][*nzp][*nyp][*nxp];
#if DIMENSIONS == 2
      for (j1 = *nyp - 1; j1 <= *nyp + 1; j1++){
      for (i1 = *nxp - 1; i1 <= *nxp + 1; i1++){
        xmax = dmax(xmax, vv[nv][0][j1][i1]);
        xmin = dmin(xmin, vv[nv][0][j1][i1]);
      }}
#elif DIMENSIONS == 3
      for (k1 = *nzp - 1; k1 <= *nzp + 1; k1++){
      for (j1 = *nyp - 1; j1 <= *nyp + 1; j1++){
      for (i1 = *nxp - 1; i1 <= *nxp + 1; i1++){
        xmax = dmax(xmax, vv[nv][k1][j1][i1]);
        xmin = dmin(xmin, vv[nv][k1][j1][i1]);
      }}}
#endif

      am[*in][nv] = dmin(am[*in][nv],xmax);
      ap[*in][nv] = dmin(ap[*in][nv],xmax);
      am[*in][nv] = dmax(am[*in][nv],xmin);
      ap[*in][nv] = dmax(ap[*in][nv],xmin);
    }
  }
}

/* ########################################################### */
void CHECK_LIM (real **am, real **ap, int ibeg, int iend)
/* 
 #
 #
 #
 ############################################################ */
{
  int   j;
  real  scrh1, scrh2;

/*   CHECK VALUES       */

  for (j = ibeg; j <= iend; j++) {
    #if EOS != ISOTHERMAL
     if (am[j][PR] < 0.0) {
       printf ("pL < 0.0 in reconstruct\n");
       exit(1);
     }
     if (ap[j][PR] < 0.0) {
       printf ("pR < 0.0 in reconstruct at %d\n",j);
       exit(1);
     }
    #endif
    if (am[j][DN] < 0.0) {
      printf ("rhoL < 0.0 in reconstruct\n");
      exit(1);
    }
    if (ap[j][DN] < 0.0) {
      printf ("rhoR < 0.0 in reconstruct\n");
      exit(1);
    }

#if PHYSICS == RHD && USE_FOUR_VELOCITY == NO
    
    scrh1 = EXPAND(  ap[j][VX]*ap[j][VX], 
                    + ap[j][VY]*ap[j][VY],
                    + ap[j][VZ]*ap[j][VZ]);
    scrh2 = EXPAND(  am[j][VX]*am[j][VX],
                    + am[j][VY]*am[j][VY],
                    + am[j][VZ]*am[j][VZ]);
    if (scrh1 >= 1.0){
      printf ("VR > 1 in slopes_ppm %12.6e\n",scrh1);
      exit(1);
    }
    if (scrh2 >= 1.0){
      printf ("VL > 1 in slopes_ppm %12.6e\n",scrh2);
      exit(1);
    }

#endif
  }
}

#undef   K0
#undef   ETA1
#undef   ETA2
#undef   EPS1

#undef  CONTACT_STEEPENING 
