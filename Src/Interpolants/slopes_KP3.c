#include "orion2.h"

/*#define IMPROVED*/
/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  "A third-order semi-discrete genuinely
 *   multidimensional central scheme for hyperbolic
 *   conservation laws and related problems"
 *
 *   Kurganov & Petrova, Numer. Math (2001) 88:683 
 *
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  real M, m, scrhp, scrhm, theta;
  real **v, **vp, **vm;
  static real *dvp, *dv0, *dv2, *Mp, *Mm, *mp, *mm;
  static real *qp, *qm;
  static real *Lp, *Lm, *dvlim;

  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend + 1;

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (dvp == NULL) {
    dvp = array_1D(NMAX_POINT);
    dv0 = array_1D(NMAX_POINT);
    dv2 = array_1D(NMAX_POINT);
    Mp  = array_1D(NMAX_POINT);
    Mm  = array_1D(NMAX_POINT);
    mp  = array_1D(NMAX_POINT);
    mm  = array_1D(NMAX_POINT);
    qp  = array_1D(NMAX_POINT);
    qm  = array_1D(NMAX_POINT);

    Lp  = array_1D(NMAX_POINT);
    Lm  = array_1D(NMAX_POINT);
    
    dvlim = array_1D(NMAX_POINT);
  }

  for (nv = 0; nv < NVAR; nv++){
    for (i = beg - 1; i <= end + 1; i++){
      dvp[i] = v[i + 1][nv] - v[i][nv];
      dv0[i] = 0.5*(v[i + 1][nv] - v[i - 1][nv]);
      dv2[i] = v[i + 1][nv] - 2.0*v[i][nv] + v[i - 1][nv];
    }

    #ifdef IMPROVED 
     minmod_lim (dvp, dvp-1, dvlim, beg, end, grid);
    #endif
    for (i = beg - 1; i <= end + 1; i++){
      qp[i] = v[i][nv] - dv2[i]/24.0 + 0.5*dv0[i] + 0.125*dv2[i];
      qm[i] = v[i][nv] - dv2[i]/24.0 - 0.5*dv0[i] + 0.125*dv2[i];

      #ifdef IMPROVED 
       Lp[i] = v[i][nv] + 0.5*dvlim[i];
       Lm[i] = v[i][nv] - 0.5*dvlim[i];
      #endif      
    }

    
    for (i = beg; i <= end; i++){
      #ifdef IMPROVED
       scrhp = 0.5*(Lm[i+1] + Lp[i]);
       scrhm = 0.5*(Lp[i-1] + Lm[i]);

       Mp[i] = dmax(scrhp, qm[i + 1]);
       Mm[i] = dmax(scrhm, qp[i - 1]);

       mp[i] = dmin(scrhp, qm[i + 1]);
       mm[i] = dmin(scrhm, qp[i - 1]);
      #else
       scrhp = 0.5*(v[i][nv] + v[i + 1][nv]);
       scrhm = 0.5*(v[i][nv] + v[i - 1][nv]);

       Mp[i] = dmax(scrhp, qm[i + 1]);
       Mm[i] = dmax(scrhm, qp[i - 1]);

       mp[i] = dmin(scrhp, qm[i + 1]);
       mm[i] = dmin(scrhm, qp[i - 1]);
      #endif
    }

    for (i = beg; i <= end; i++){
      M = dmax(qp[i], qm[i]);
      m = dmin(qp[i], qm[i]);

      #ifdef IMPROVED

       theta = 1.0;
       if (v[i-1][nv] < v[i][nv] && v[i][nv] < v[i+1][nv]){ 
         scrhp = (Mp[i] - Lp[i])/(M - Lp[i] + 1.e-9);
         scrhm = (mm[i] - Lm[i])/(m - Lm[i] + 1.e-9);
         theta = dmin(scrhp, scrhm);
       }else if (v[i-1][nv] > v[i][nv] && v[i][nv] > v[i+1][nv]){ 
         scrhp = (Mm[i] - Lm[i])/(M - Lm[i] + 1.e-9);
         scrhm = (mp[i] - Lp[i])/(m - Lp[i] + 1.e-9);
         theta = dmin(scrhp, scrhm);
       }
       theta = dmin(theta, 1.0);

      #else

       theta = 1.0;
       if (v[i-1][nv] < v[i][nv] && v[i][nv] < v[i+1][nv]){ 
         scrhp = (Mp[i] - v[i][nv])/(M - v[i][nv] + 1.e-9);
         scrhm = (mm[i] - v[i][nv])/(m - v[i][nv] + 1.e-9);
         theta = dmin(scrhp, scrhm);
       }else if (v[i-1][nv] > v[i][nv] && v[i][nv] > v[i+1][nv]){ 
         scrhp = (Mm[i] - v[i][nv])/(M - v[i][nv] + 1.e-9);
         scrhm = (mp[i] - v[i][nv])/(m - v[i][nv] + 1.e-9);
         theta = dmin(scrhp, scrhm);
       }
       theta = dmin(theta, 1.0);
      #endif

      #ifdef IMPROVED
       vp[i][nv] = (1.0 - theta)*Lp[i] + theta*qp[i];
       vm[i][nv] = (1.0 - theta)*Lm[i] + theta*qm[i];
      #else
       vp[i][nv] = (1.0 - theta)*v[i][nv] + theta*qp[i];
       vm[i][nv] = (1.0 - theta)*v[i][nv] + theta*qm[i];
      #endif
    }
  }

   

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == YES 
   FLATTEN_1D (state, beg, end);
  #endif

}
