#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{
  int    i, j, k, nv, S=2;
  int    beg, end;
  double *vp, *vm;
  double wp[NVAR], wm[NVAR];
  double lambda[NVAR], w[NVAR];
  static double **L, **R;
  static double **wsp, **wsm;
  static double *a2, *h;

  if (wsp == NULL){
    wsp = Array_2D(NVAR, NMAX_POINT, double);
    wsm = Array_2D(NVAR, NMAX_POINT, double);
    a2  = Array_1D(NMAX_POINT, double);
    h   = Array_1D(NMAX_POINT, double);
    L   = Array_2D(NFLX, NFLX, double);
    R   = Array_2D(NFLX, NFLX, double);
  } 

 /* ---- Reset eigenvectors ---- */
                                                                                                                                        
  for (nv = NFLX; nv--; ){
  for (k  = NFLX; k--;  ){
    L[nv][k] = R[nv][k] = 0.0;
  }}
 
/* ----------------------------------------------------
   ---------------------------------------------------- */

  beg = grid[DIR].lbeg - grid[DIR].nghost + S+1;
  end = grid[DIR].lend + grid[DIR].nghost - S-1;

  SOUND_SPEED2 (state->v, a2, h, beg, end);

  for (i = beg; i <= end; i++){    
    vp = state->vp[i];
    vm = state->vm[i];

    EIGENV (state->v[i], a2[i], h[i], lambda, L, R);

    for (j = i - S; j <= i + S; j++){
      LpPROJECT(L, state->v[j], w);
      for (k = NVAR; k--;   ){  
        wsp[k][j]     = w[k];
        wsm[k][2*i-j] = w[k];
      }
    }

    for (k = NVAR; k--;   )
      PPM_RECONSTRUCT (wsp[k], wm + k, wp + k, i);


/*
    for (k = NVAR; k--;   ){  
      wp[k] = MP5_RECONSTRUCT (wsp[k], i);
      wm[k] = MP5_RECONSTRUCT (wsm[k], i);
    }
*/
    for (nv = NFLX; nv--;   ){
      vp[nv] = vm[nv] = 0.0;
      for (k = NFLX; k--;   ){
        vp[nv] += wp[k]*R[nv][k];
        vm[nv] += wm[k]*R[nv][k];
      }
    }

  /* -- check unphysical values -- */

    if (vp[PR] < 0.0 || vm[PR] < 0.0) PRIM_LIMIT (state, i, PR);
    if (vp[DN] < 0.0 || vm[DN] < 0.0) PRIM_LIMIT (state, i, DN);

  /* -- constrain density to lie 
        between neighbour max and min -- */


    {
      double **v, vmin, vmax, vmin2, vmax2;
      v = state->v;
      nv = DN;
      vmax  = dmax(v[i+1][nv],v[i-1][nv]);
      vmax2 = dmax(v[i+2][nv],v[i-2][nv]);

      vmax = dmax(vmax,vmax2);
      vmax = dmax(vmax,v[i][nv]);

      vmin  = dmin(v[i+1][nv],v[i-1][nv]);
      vmin2 = dmin(v[i+2][nv],v[i-2][nv]);
      vmin = dmin(vmin,vmin2);
      vmin = dmin(vmin,v[i][nv]);
  
      vp[nv] = dmax(vp[nv],vmin);
      vp[nv] = dmin(vp[nv],vmax);
      vm[nv] = dmax(vm[nv],vmin);
      vm[nv] = dmin(vm[nv],vmax);
    }

  } /* -- end zone loop -- */

  #if SHOCK_FLATTENING == MULTID
   FLATTEN(state,beg,end,grid);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg - 1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}


/* ************************************************************* */
void RECONSTRUCT2 (const State_1D *state, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{
  int    i, j, nv, S=2;
  int    beg, end;
  double vp_med, vm_med, scrh, dvc;
  double dvp_med, dvm_med;
  double vp[NVAR], wm[NVAR];
  static double one_six = 1.0/6.0;
  static double *v, *dv, *vi, *dvlim, *dv6;

  if (v == NULL){
    v     = Array_1D(NMAX_POINT, double);
    vi    = Array_1D(NMAX_POINT, double);
    dv    = Array_1D(NMAX_POINT, double);
    dv6   = Array_1D(NMAX_POINT, double);
    dvlim = Array_1D(NMAX_POINT, double);
  } 

  beg = grid[DIR].lbeg - grid[DIR].nghost + S;
  end = grid[DIR].lend + grid[DIR].nghost - S;

  #if INTERPOLATION == MP5
   for (nv = NVAR; nv--;  ){
     for (i = beg-S; i <= end+S; i++){
       v[i]  = state->v[i][nv];
     }
     for (i = beg; i <= end; i++){
       state->vp[i][nv] = MP5_RECONSTRUCT (v, i);
       for (j = i - S; j <= i + S; j++) vi[j] = v[2*i-j];
       state->vm[i][nv] = MP5_RECONSTRUCT (vi, i);
     } 
   }

  /* -- check unphysical values -- */

   for (i = beg; i <= end; i++){
    if (state->vp[i][DN] < 0.0 || state->vm[i][DN] < 0.0) PRIM_LIMIT (state, i, DN);
    if (state->vp[i][PR] < 0.0 || state->vm[i][PR] < 0.0) PRIM_LIMIT (state, i, PR);
  }

  return;
#endif
 



  for (nv = NVAR; nv--;  ){
    for (i = beg-S; i <= end+S-1; i++){
      dv[i] = state->v[i+1][nv] - state->v[i][nv];    
      vi[i] = 0.5*(state->v[i+1][nv] + state->v[i][nv]);
      v[i]  = state->v[i][nv];
    }
    for (i = beg-S+1; i <= end+S-1; i++){
      dvc      = 0.5*(dv[i] + dv[i-1]);
      scrh     = 2.0*MinMod(dv[i], dv[i-1]);
      dvlim[i] = MinMod(dvc, scrh);
    }
    for (i = beg-S+1; i <= end; i++){
      vi[i]  -= (dvlim[i+1] - dvlim[i])*one_six;
      dv6[i]  = (dvlim[i+1] - dvlim[i])*one_six;
    }
 
    for (i = beg; i <= end; i++){

      dvp_med = MinMod( 0.5*dv[i]   - dv6[i]  ,  dv[i]);
      dvm_med = MinMod(-0.5*dv[i-1] - dv6[i-1], -dv[i-1]);
/*
      dvp_med = MinMod(vi[i]   - v[i],  dv[i]);
      dvm_med = MinMod(vi[i-1] - v[i], -dv[i-1]);
*/
      state->vp[i][nv] = v[i] + MinMod(dvp_med, - 2.0*dvm_med);
      state->vm[i][nv] = v[i] + MinMod(dvm_med, - 2.0*dvp_med);
/*
      vp_med = Median(v[i], vi[i]  , v[i+1]);
      vm_med = Median(v[i], vi[i-1], v[i-1]);
      state->vp[i][nv] = Median(v[i], vp_med, 3.0*v[i] - 2.0*vm_med);
      state->vm[i][nv] = Median(v[i], vm_med, 3.0*v[i] - 2.0*vp_med);
*/
    }
  }
}

/* ************************************************************* */
void RECONSTRUCT3 (const State_1D *state, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{
  int    i, j, k, nv, S=2;
  int    beg, end;
  double wp_med, wm_med, *vp, *vm;
  double dwmax, dwmin;
  double wp[NVAR], wm[NVAR];
  double lambda[NVAR];
  static double ***w, **wip, **wim, **L, ***R;
  static double *ws, *a2, *h;

  if (w == NULL){
    w   = Array_3D(NMAX_POINT, NMAX_POINT, NVAR, double);
    wip = Array_2D(NMAX_POINT, NVAR, double);
    wim = Array_2D(NMAX_POINT, NVAR, double);
    L  = Array_2D(NVAR, NVAR, double);
    R  = Array_3D(NMAX_POINT, NVAR, NVAR, double);
    a2 = Array_1D(NMAX_POINT, double);
    h  = Array_1D(NMAX_POINT, double);
    ws = Array_1D(NMAX_POINT, double);
  } 

 /* ---- Reset left eigenvectors ---- */
                                                                                                                                        
  for (nv = NFLX; nv--; ){
  for (k  = NFLX; k--;  ){
    L[nv][k] = 0.0;
  }}
 
/* ---- reset right eigenvectors ---- */
 
  for (i = NMAX_POINT; i--; ){
  for (nv = NFLX; nv--; ){
  for (k  = NFLX; k--;  ){
    R[i][nv][k] = 0.0;
  }}}
 
/* ----------------------------------------------------
    Obtain:

     1. characteristic variables  w[k] = L[k].v
     2. interface values
   ---------------------------------------------------- */

  beg = grid[DIR].lbeg - grid[DIR].nghost + S;
  end = grid[DIR].lend + grid[DIR].nghost - S;

  SOUND_SPEED2 (state->v, a2, h, beg-S, end+S);

  for (i = beg; i <= end; i++){
    
    EIGENV (state->v[i], a2[i], h[i], lambda, L, R[i]);
/*
for (nv = NFLX; nv--; ){
for (k  = NFLX; k--;  ){
  L[nv][k] = R[i][nv][k] = (nv == k ? 1.0:0.0);
}}
*/


    for (k = 0; k < NVAR; k++){  
      for (j = i - S; j <= i + S; j++){
        ws[j] = 0.0;
        for (nv = 0; nv < NVAR; nv++){
          ws[j] += L[k][nv]*state->v[j][nv];
        }
        w[i][j][k] = ws[j];
      }

/*#if INTERPOLATION == MP5 */
      wip[i][k] = MP5_RECONSTRUCT (ws, i);
      for (j = i - S; j <= i + S; j++) ws[j] = w[i][2*i-j][k];
      wim[i][k] = MP5_RECONSTRUCT (ws, i);
/*
#else
      wip[i][k] = PPM_RECONSTRUCT (ws, i);
      wim[i][k] = PPM_RECONSTRUCT (ws, i-1);
#endif
*/
    }

  }

/* ----------------------------------------------------
                Monotonicity constraints
   ---------------------------------------------------- */

  for (i = beg; i <= end; i++){
    vp = state->vp[i];
    vm = state->vm[i];

    for (k = NVAR; k--;  ){

/* #if INTERPOLATION == MP5 */
wp[k] = wip[i][k];
wm[k] = wim[i][k];
/*
#else
      wp_med = Median(w[i][i][k], wip[i][k], w[i][i+1][k]);
      wm_med = Median(w[i][i][k], wim[i][k], w[i][i-1][k]);
      wp[k]  = Median(w[i][i][k], wp_med, 3.0*w[i][i][k] - 2.0*wm_med);
      wm[k]  = Median(w[i][i][k], wm_med, 3.0*w[i][i][k] - 2.0*wp_med);
#endif
*/

    }     
    for (nv = NVAR; nv--;   ){
      vp[nv] = vm[nv] = 0.0;
      for (k = NVAR; k--;   ){
        vp[nv] += wp[k]*R[i][nv][k];
        vm[nv] += wm[k]*R[i][nv][k];
      }
    }

  /* -- check unphysical values -- */

    if (vp[DN] < 0.0 || vm[DN] < 0.0) PRIM_LIMIT (state, i, DN);
    if (vp[PR] < 0.0 || vm[PR] < 0.0) PRIM_LIMIT (state, i, PR);

  }
}


/* ************************************************************* */
void RECONSTRUCT_LIN (const State_1D *state, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{
  int    i, j, k, nv, S=2;
  int    beg, end;
  double *vp, *vm;
  double wp[NVAR], wm[NVAR];
  double lambda[NVAR];
  static double **L, **R, **v;
  static double *a2, *h;
double w[NVAR];
double dw, dwp[NFLX], dwm[NFLX];
double dvp[NVAR], dvm[NVAR];

  if (a2 == NULL){
    a2  = Array_1D(NMAX_POINT, double);
    h   = Array_1D(NMAX_POINT, double);
    L   = Array_2D(NFLX, NFLX, double);
    R   = Array_2D(NFLX, NFLX, double);
  } 

 /* ---- Reset eigenvectors ---- */
                                                                                                                                        
  for (nv = NFLX; nv--; ){
  for (k  = NFLX; k--;  ){
    L[nv][k] = R[nv][k] = 0.0;
  }}
 
/* ----------------------------------------------------
   ---------------------------------------------------- */

  beg = grid[DIR].lbeg - grid[DIR].nghost + S;
  end = grid[DIR].lend + grid[DIR].nghost - S;

  SOUND_SPEED2 (state->v, a2, h, beg, end);

  for (i = beg; i <= end; i++){    
    vp = state->vp[i];
    vm = state->vm[i];
    v  = state->v;

    EIGENV (state->v[i], a2[i], h[i], lambda, L, R);

    for (nv = NVAR; nv--;   ){  
      dvp[nv] = v[i+1][nv] - v[i][nv];
      dvm[nv] = v[i][nv]   - v[i-1][nv];
    }

    for (k  = NVAR; k--;   ) {
      w[k] = dwp[k] = dwm[k] = 0.0;
      for (nv = NVAR; nv--;  ) {
        dwp[k] += L[k][nv]*dvp[nv];
        dwm[k] += L[k][nv]*dvm[nv];
        w[k]   += L[k][nv]*v[i][nv];
      }
    }

    for (k = 0; k < NVAR; k++){  
      dw = 0.0;
      if (dwp[k]*dwm[k] > 0.0){
        dw = (fabs(dwp[k]) < fabs(dwm[k]) ? dwp[k]:dwm[k]);
      }
      wp[k] = w[k] + 0.*dw;
      wm[k] = w[k] - 0.*dw;
    }

    for (nv = NFLX; nv--;   ){
      vp[nv] = vm[nv] = 0.;
      for (k = 0; k < NVAR; k++){
        vp[nv] += wp[k]*R[nv][k];
        vm[nv] += wm[k]*R[nv][k];
      }
if (nv != B1 && fabs(vp[nv] - v[i][nv]) > 1.e-8){
  printf (" State not correct\n");
  exit(1);
}
  
    }


{
  int ii,jj;

  for (ii = 0; ii < NVAR; ii++){
  for (jj = 0; jj < NVAR; jj++){
    dw = 0.0;
    for (k = 0; k < NVAR; k++){
      dw += L[k][jj]*R[ii][k];
    }
    if (ii == jj && ii == B1) continue;
    if (ii == jj && fabs(dw-1.0) > 1.e-8) {
      printf ("!!! in diagonal  %12.6e at %d %d\n", dw,ii,jj);
      exit(1);
    }
    if (ii != jj && fabs(dw) > 1.e-8){
      printf ("!!! out of diagonal\n");
      exit(1);
    }

  }}
}



  } /* -- end zone loop -- */

}

