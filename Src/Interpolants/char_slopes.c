#include "orion2.h"

#if CHAR_LIMITING == YES
/* ********************************************************************** */
void CHAR_SLOPES (const State_1D *state, double **dv_lim, 
                  int beg, int end, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    obtain primitive slopes by using characteristic limiting.
 *
 *   - obtain slopes in characteritstic vars:
 *
 *      dw(k)      = w(k).dv
 *      dw_plim(k) = w(k).dv_lim
 *
 *   - limit dw(k) --> dw_lim(k)
 *     constrain dw_lim(k) = min(|dw_lim(k)|,|dw_plim(k)|)
 *
 *   - project dw_lim(k) back on primitive space:
 *     
 *     dv_lim = \sum dw_lim(k)*r(k)
 *   
 *   - enforce monotonicity in primitive vars as well:
 *
 *      dv_lim = minmod(dv_lim, 2*dv+, 2*dv-)
 *
 *  ARGUMENTS
 *
 *    vv      (IN)   cell centered array of primitive quantities
 *    dv      (IN)   forward differences of primitive quantities
 *    dv_lim  (OUT)  limited slopes in primitive variables
 *    limiter (IN)   an array of pointer to limiter function.
 *    beg, end (IN)  leftmost and rightmost where slopes are needed.
 *                   (3 stencil required, so beg must be no less than
 *                    1, end must be less than ntot - 1)
 *
 *
 *  LAST MODIFIED
 *
 *    June 05, 2006 by A. Mignone (mignone@to.astro.it)
 *
 **************************************************************** */
{
  int i, nv, k;
  double dwp[NVAR], dwm[NVAR];
  double dvc, dvmax, lambda[NVAR];
  double *vc, *vp, *vm, *dvp, *dvm;
  double **L, **R, dwlim[NVAR];
  static double **dv, *a2, *h;
  static Limiter *limiter[NVAR];

  if (dv == NULL){
    dv = Array_2D(NMAX_POINT, NVAR, double);
    a2 = Array_1D(NMAX_POINT, double);
    h  = Array_1D(NMAX_POINT, double);
    SET_LIMITER (limiter);
  }

  vp = state->v[beg];
  vc = state->v[beg-1];
  for (nv = NVAR; nv--;   ){
    dv[beg - 1][nv] = vp[nv] - vc[nv];
  }

/* ----------------------------------------- 
             compute slopes
   ----------------------------------------- */

  SOUND_SPEED2 (state->v, a2, h, beg, end);
  for (i = beg; i <= end; i++){

    vp = state->v[i+ 1];
    vc = state->v[i];
    for (nv = NVAR; nv--;   ){
      dv[i][nv] = vp[nv] - vc[nv];
    }

    vc  = state->v[i];
    vp  = state->vp[i];
    vm  = state->vm[i];
    L   = state->Lp[i];
    R   = state->Rp[i];

    dvp = dv[i];
    dvm = dv[i - 1];
    
  /* -------------------------------
      project undivided differences 
      onto characteristic space
     ------------------------------- */
     
    EIGENV (vc, a2[i], h[i], lambda, L, R);
    LpPROJECT(L, dvp, dwp);
    LpPROJECT(L, dvm, dwm);

    #if SHOCK_FLATTENING == MULTID    
     if (CHECK_ZONE (i, FLAG_MINMOD)) 
       for (k = NFLX; k--;    )   
         minmod_lim (dwp + k, dwm + k, dwlim + k, 0, 0, grid + DIR);
     else 
    #endif    
     for (k = NFLX; k--;    )
       (limiter[k]) (dwp + k, dwm + k, dwlim + k, 0, 0, grid + DIR);

  /* -----------------------------------------
      obtain slopes in primitive vars by 
      projecting back onto right eignvectors  
     ----------------------------------------- */

    for (nv = NFLX; nv--; ){
      #ifdef STAGGERED_MHD
       if (nv == B1) continue;
      #endif
      dvc = dwlim[0]*R[nv][0];
      for (k = 1; k < NFLX; k++) dvc += dwlim[k]*R[nv][k]; 
   
    /* -- enforce monotonicity in primitive vars -- */

      dv_lim[i][nv] = 0.0;
      if (dvp[nv]*dvm[nv] > 0.0){
        dvmax = 2.0*(fabs(dvp[nv]) < fabs(dvm[nv]) ? dvp[nv]:dvm[nv]);
        if (dvmax*dvc > 0.0) dv_lim[i][nv] = (fabs(dvmax) < fabs(dvc) ? dvmax:dvc);
      }
    }

  /* ----------------------------------------------- 
       Compute limited slopes for tracers;
       Notice that the characteristic tracer 
       variable is equal to the primitive one, 
       since  l = r = (0,..., 1 , 0 ,....)
     ----------------------------------------------- */            

    #if NFLX != NVAR
     for (nv = NFLX; nv < NVAR; nv++ ){
       if (dvp[nv]*dvm[nv] > 0.0){
         dvc   = 0.5*(dvp[nv] + dvm[nv]);
         dvmax = 2.0*(fabs(dvp[nv])  < fabs(dvm[nv]) ? dvp[nv]:dvm[nv]);
         dv_lim[i][nv]   = fabs(dvmax) < fabs(dvc) ? dvmax: dvc;
       }else{
         dv_lim[i][nv] = 0.0;
       }
     }
    #endif
    
  }
}
#endif /* CHAR_LIMITING == YES */

