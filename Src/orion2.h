#define ORION2_VERSION  "1.0"

/*  ********************************************************************
        orion2.h, ORION2  main header file based on pluto.h version 3.01
    ********************************************************************  */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define dmax(a,b)  ( (a) >= (b) ? (a) : (b) )
#define dmin(a,b)  ( (a) <= (b) ? (a) : (b) )
#define dsign(x)   ( (x) >= 0.0 ? (1.0) : (-1.0))
#define square(x)  ( (x)*(x) )
#define MinMod(a,b)  ((a)*(b) > 0.0 ? (fabs(a) < fabs(b) ? (a):(b)):0.0)
#define VanLeer(a,b) ((a)*(b) > 0.0 ? 2.0*(a)*(b)/((a)+(b)):0.0)
#define MC(a,b) (MinMod(0.5*((a)+(b)), 2.0*MinMod((a),(b))))
#define Median(a,b,c) (a - MinMod((b)-(a),(c)-(a)))
#define SWAP_VAR(x) SWAP_ENDIAN(&x, sizeof(x));

#define YES      1
#define NO       0
#define DEFAULT -1

/* --- TURBULENCE DRIVING LABELS --- */
#define HDF5         2
#define INITIAL_ONLY 3


/* --- CHEMISTRY LABELS --- */
#define CONTEMPORARY   2
#define PRIMORDIAL     3
#define SIMPLE_COOL    4



 /*  ----  GEOMETRY LABELS  ----  */

#define CARTESIAN    1
#define CYLINDRICAL  2
#define POLAR        3
#define SPHERICAL    4

#define UNIFORM_GRID             1
#define STRETCHED_GRID           2
#define LOGARITHMIC_INC_GRID     3
#define LOGARITHMIC_DEC_GRID     4

    /*  ----  SET LABELS FOR EQUATION OF STATE (EOS)  ----  */

#define IDEAL         1
#define TAUB          3
#define ISOTHERMAL    4

         /*  ----  TIME_STEPPING LABELS  ----  */

#define EULER                      1
#define HANCOCK                    2
#define CHARACTERISTIC_TRACING     3
#define RK2                        4
#define RK3                        5

#define EXPLICIT             12  /* -- just a number different from 0 !!!  -- */
#define SUPER_TIME_STEPPING  44  /* -- just a number different from EXPLICIT --   */ 

     /*  ----   Output labels:  ---- */

#define DBL_OUTPUT   1
#define FLT_OUTPUT   2
#define VTK_OUTPUT   3
#define TAB_OUTPUT   4
#define PPM_OUTPUT   5
#define PNG_OUTPUT   6

#define VTK_VECTOR  5  /* -- any number but NOT 1  -- */

#define MAX_OUTPUT_TYPES 16 


     /*  ----  SET FLAGS FOR SPLIT/UNSPLIT INCLUSION OF SOURCE TERMS  ----   */

#define SOURCE_STEP      11
#define HYDRO_SWEEP      12

   /*  ----  SET LABELS FOR COOLING  ----  */

#define POWER_LAW    3
#define NEQ          4
#define RAYMOND      5
#define TABULATED    6
#define H2_COOL      7

  /*  ----  SET LABELS FOR PHYSICS MODULE  ----  */

#define HD     1
#define RHD    2
#define MHD    3
#define RMHD   5

    /*  ----  SET LABELS FOR DIV.B REMOVAL  ----  
        If you move them to the MHD header, 
        definitions.h (which is included before)
        cannot correctly use them                */
        
#define NONE           0
#define EIGHT_WAVES    1
#define DIV_CLEANING   2
#define FLUX_CT        3

   /*  ----  SET LABELS FOR BOUNDARIES  ----  */

#define OUTFLOW          1  /* any number except 0 !! */
#define REFLECTIVE       2 
#define AXISYMMETRIC     3
#define EQTSYMMETRIC     4
#define PERIODIC         5 
#define USERDEF          6

#define X1_BEG           101
#define X1_END           102
#define X2_BEG           103
#define X2_END           104
#define X3_BEG           105
#define X3_END           106

    /* ---- LABELS FOR IMAGE SLICING ---- */

#define X12_PLANE       3  
#define X13_PLANE       5
#define X23_PLANE       6

   /* ---- FLAG LABELS ---- */

#define FLAG_MINMOD      1
#define FLAG_HLL         2
#define FLAG_ENTROPY     4
#define FLAG_SPLIT_CELL  8
#define FLAG_BIT5       16
#define FLAG_BIT6       32
#define FLAG_BIT7       64
#define FLAG_BIT8      128  /* used for the sign ?? */

#define PR_FAIL  1
#define EN_FAIL  2
#define DN_FAIL  4

#define IDIR    0
#define JDIR    1
#define KDIR    2
#define ALLDIR -1

#define CELL_CENTER    50
#define FACE_CENTER    51
#define EDGE_CENTER    52

        /*  ----  SET LABELS FOR INTERPOLATION  ----   */

#define FLAT              1
#define LINEAR            2
#define CENO3             3
#define PARABOLIC         4
#define WENO5             5
#define WENO7             6
#define WENO9             7
#define LINEAR_MULTID     8
#define mpPPM             9
#define MP5              10
#define LIM3             11


#define TRIAD_LIM         21  /* ** used with LINEAR interpolants only ** */

#define SINGLE 1
#define DOUBLE 2

#define ONED   1
#define MULTID 3

/* ###############################################################

                 PROBLEM-DEPENDENT DECLARATIONS

   ############################################################### */

#include "definitions.h"

#ifdef PARALLEL
 #include <al.h>
#endif

/* ###################################################################

        SWITCHES USED FOR DEBUGGING PURPOSES ONLY

   ################################################################### */
 
/* -- CHECK_EIGENVECTORS: used in eigenv.c in HD/, MHD/, RHD/
      to check orthogonality and the correctness through 
      the relation the A = L*\Lambda*R  -- */

#define CHECK_EIGENVECTORS      NO

/* -- CHECK_ROE_MATRIX: used in HD/roe.c, MHD/roe.c to 
      check that the characteristic decomp. reproduces
      Roe matrix -- */

#define CHECK_ROE_MATRIX        NO

/* -- CHECK_CONSERVATIVE_VAR: used in RHD/mappers.c to 
      check that conservative vars are physical -- */

#define CHECK_CONSERVATIVE_VAR  NO

/* -- CHECK_DIVB_CONDITION: used in MHD/FCT/fct.c 
      to check if div.B = 0 -- */

#ifndef CHECK_DIVB_CONDITION
 #define CHECK_DIVB_CONDITION    NO
#endif

/* ###############################################################

                    Define precision

   ############################################################### */

#ifndef PRECISION 
 #define PRECISION  DOUBLE
#endif

#if PRECISION == DOUBLE
 typedef double real;
 #ifdef PARALLEL 
  #define MPI_ORION2_REAL  MPI_DOUBLE
 #endif
#elif PRECISION == SINGLE
 typedef float real;
 #ifdef PARALLEL 
  #define MPI_ORION2_REAL   MPI_FLOAT 
 #endif
#endif

/* ###############################################################
    
                        EXIT MACRO

   ############################################################### */

#ifdef PARALLEL 
/* #define QUIT_ORION2(e_code)   {MPI_Finalize(); exit(e_code);}  */
 #define QUIT_ORION2(e_code)   {MPI_Abort(MPI_COMM_WORLD, e_code);MPI_Finalize(); exit(e_code);} 
#else
 #define QUIT_ORION2(e_code)   exit(e_code);
#endif
#define QUIT_ORION2(e_code)   exit(e_code);
        
/* ##################################################################

           MACROS TO EXPAND DIMENSION-DEPENDENT LINES

   #################################################################  */

#if COMPONENTS == 1
 #define EXPAND(a,b,c) a
 #define SELECT(a,b,c) a
#endif

#if COMPONENTS == 2
 #define EXPAND(a,b,c) a b
 #define SELECT(a,b,c) b
#endif

#if COMPONENTS == 3
 #define EXPAND(a,b,c) a b c
 #define SELECT(a,b,c) c
#endif

#if DIMENSIONS == 1
 #define D_EXPAND(a,b,c)  a
 #define D_SELECT(a,b,c)  a
 #define IOFFSET 1   /* ** so we know when to loop in boundary zones ** */
 #define JOFFSET 0
 #define KOFFSET 0
#endif

#if DIMENSIONS == 2
 #define D_EXPAND(a,b,c) a b
 #define D_SELECT(a,b,c) b
 #define IOFFSET 1   /* ** so we know when to loop in boundary zones ** */
 #define JOFFSET 1
 #define KOFFSET 0
#endif

#if DIMENSIONS == 3
 #define D_EXPAND(a,b,c) a b c
 #define D_SELECT(a,b,c) c
 #define IOFFSET 1   /* ** so we know when to loop in boundary zones ** */
 #define JOFFSET 1
 #define KOFFSET 1
#endif

#if WARNING_MESSAGES == YES
 #define WARNING(a)  a
#else
 #define WARNING(a)
#endif

/* ################################################################# 

                       SPATIAL LOOP MACROS        

   ################################################################# */

#define IBEG_LOOP(i)  for ((i) = IBEG; (i)--;    )
#define JBEG_LOOP(j)  for ((j) = JBEG; (j)--;    )
#define KBEG_LOOP(k)  for ((k) = KBEG; (k)--;    )

#define IEND_LOOP(i)  for ((i) = IEND + 1; (i) < NX_TOT; (i)++)
#define JEND_LOOP(j)  for ((j) = JEND + 1; (j) < NY_TOT; (j)++)
#define KEND_LOOP(k)  for ((k) = KEND + 1; (k) < NZ_TOT; (k)++)

#define IDOM_LOOP(i)  for ((i) = IBEG; (i) <= IEND; (i)++)
#define JDOM_LOOP(j)  for ((j) = JBEG; (j) <= JEND; (j)++)
#define KDOM_LOOP(k)  for ((k) = KBEG; (k) <= KEND; (k)++)

#define ITOT_LOOP(i)  for ((i) = 0; (i) < NX_TOT; (i)++)
#define JTOT_LOOP(j)  for ((j) = 0; (j) < NY_TOT; (j)++)
#define KTOT_LOOP(k)  for ((k) = 0; (k) < NZ_TOT; (k)++)

#define DOM_LOOP(k,j,i) KDOM_LOOP(k) JDOM_LOOP(j) IDOM_LOOP(i)

#define X1_BEG_LOOP(k,j,i) KTOT_LOOP(k) JTOT_LOOP(j) IBEG_LOOP(i)
#define X2_BEG_LOOP(k,j,i) KTOT_LOOP(k) JBEG_LOOP(j) ITOT_LOOP(i)
#define X3_BEG_LOOP(k,j,i) KBEG_LOOP(k) JTOT_LOOP(j) ITOT_LOOP(i)

#define X1_END_LOOP(k,j,i) KTOT_LOOP(k) JTOT_LOOP(j) IEND_LOOP(i)
#define X2_END_LOOP(k,j,i) KTOT_LOOP(k) JEND_LOOP(j) ITOT_LOOP(i)
#define X3_END_LOOP(k,j,i) KEND_LOOP(k) JTOT_LOOP(j) ITOT_LOOP(i)


#define TRANSVERSE_LOOP(indx, in, i,j,k) \
 if (DIR == IDIR) {i = &in; j = &indx.t1; k = &indx.t2;} \
 if (DIR == JDIR) {j = &in; i = &indx.t1; k = &indx.t2;} \
 if (DIR == KDIR) {k = &in; i = &indx.t1; j = &indx.t2;} \
 NX_PT = i; NY_PT = j; NZ_PT = k;\
 for (indx.t2 = indx.t2_beg; indx.t2 <= indx.t2_end; indx.t2++) \
 for (indx.t1 = indx.t1_beg; indx.t1 <= indx.t1_end; indx.t1++)

/*
#define DIR_LOOP(grid,in,k,j,i) \
    if (DIR == IDIR) {in = &i; j = *NY_PT; k = *NZ_PT;} \
    if (DIR == JDIR) {in = &j; i = *NX_PT; k = *NZ_PT;} \
    if (DIR == KDIR) {in = &k; i = *NX_PT; j = *NY_PT;} \
    for (*in = grid[DIR].lbeg - 2; *in <= grid[DIR].lend + 2; (*in)++)
*/

/*  not used for now... will serve in EMF_BOUNDARY...
#define ISTAG_LOOP(i)  for ((i) = IBEG - 1; (i) <= IEND; (i)++)
#define JSTAG_LOOP(j)  for ((j) = JBEG - 1; (j) <= JEND; (j)++)
#define KSTAG_LOOP(k)  for ((k) = KBEG - 1; (k) <= KEND; (k)++)
*/


/* ################################################################# 

                   VARIABLE LOOP MACROS        

   ################################################################# */

 /* ------------------------------------------------------------
     The LRVAR_LOOP and LRFLX_LOOP will speed up computation 
     of the left/right states at a given interface by excluding 
     the normal component of magnetic field, which is usually  
     assigned at the end of the reconstruction step.
    ------------------------------------------------------------ */
/*      
#ifdef STAGGERED_MHD
 #define LR_VAR_LOOP(i) for (i = NVAR; i--; i -= (i == B1+1)) 
 #define LR_FLX_LOOP(i) for (i = NFLX; i--; i -= (i == B1+1)) 
#else
 #define LR_VAR_LOOP(i) for (i = NVAR; i--; ) 
 #define LR_FLX_LOOP(i) for (i = NFLX; i--; ) 
#endif
*/
/* ################################################################# 

                  TIME INTEGRATOR DEFINITIONS

   ################################################################# */

#if (TIME_STEPPING == HANCOCK) || (TIME_STEPPING == CHARACTERISTIC_TRACING)
 #define SINGLE_STEP   1
#endif

/* ############################################################### 
  
          Check whether a seprate source step has to be done

   ############################################################### */

#if (INCLUDE_COOLING != NO) 

 #define INCLUDE_SPLIT_SOURCE   YES

#else

 #define INCLUDE_SPLIT_SOURCE   NO

#endif

/* ###############################################################

      set to NO all undefined names that may appear in the code 

   ############################################################### */

#ifndef RESISTIVE_MHD 
 #define RESISTIVE_MHD   NO
#endif

#ifndef THERMAL_CONDUCTION
 #define THERMAL_CONDUCTION NO
#endif

#ifndef VISCOSITY
 #define VISCOSITY NO
#endif

#if (RESISTIVE_MHD == EXPLICIT) || \
    (THERMAL_CONDUCTION == EXPLICIT) || \
    (VISCOSITY == EXPLICIT)  || \
    (ARTIFICIAL_VISCOSITY == YES) 

 #define INCLUDE_PARABOLIC_FLUX  YES   /* -- explicit parabolic fluxes 
                                             are added to upwind fluxes -- */
#else
 #define INCLUDE_PARABOLIC_FLUX  NO
#endif

/* ################################################################# 

                       USEFUL CONSTANTS        

   ################################################################# */

#define ACCURACY    1.e-14          /* ZERO ACCURACY */

/* The following physical constants are taken 
   from http://physic.nist.gov/cuu/Constants/index.html  
   They are given in c.g.s, 
      [erg], [cm] and [second] :                               */

#define CONST_PI      3.14159265358979   /*  pi                        */
#define CONST_amu     1.66053886e-24     /*  atomic mass unit          */
#define CONST_mp      1.67262171e-24     /*  proton mass               */
#define CONST_mn      1.67492728e-24     /*  neutron mass              */
#define CONST_me      9.1093826e-28      /*  electron mass             */
#define CONST_mH      1.6733e-24         /*  Hydrogen atom mass        */
#define CONST_kB      1.3806505e-16      /*  Boltzmann constant        */
#define CONST_sigma   5.67051e-5         /*  Stephan Boltmann constant */
#define CONST_sigmaT  6.6524e-25         /*  Thomson Cross section  */
#define CONST_NA      6.0221367e23       /*  Avogadro Contant          */
#define CONST_c       2.99792458e10      /*  Speed of Light            */
#define CONST_Msun    1.98892e33         /*  Solar Mass                */
#define CONST_Rsun    6.96e10            /*  Solar Radius              */
#define CONST_Mearth  5.9736e27          /*  Earth Mass                */
#define CONST_Rearth  6.378136e8         /*  Earth Radius              */
#define CONST_G       6.6726e-8          /*  Gravitational Constant    */
#define CONST_h       6.62606876e-27     /*  Planck Constant           */
#define CONST_pc      3.0856775807e18    /*  parsec                    */
#define CONST_ly      0.9461e18          /*  light year                */
#define CONST_au      1.49597892e13      /*  astronomical unit         */
#define CONST_eV      1.602176463158e-12 /*  electron Volt in erg      */

/* ############################################################### 
 
                     Define Structures here     

   ############################################################### */

typedef struct CMD_LINE {
  short int restart;       /* ------------------------------------- */
  short int makegrid;      /*  structure of command line options    */    
  short int write;         /* ------------------------------------- */
  short int maxsteps;
  #ifdef AMR_1D
   short int amr1d_levels;     /* ------------------------------------- */
   short int amr1d_buffer;     /*  options use by the AMR_1D module     */
   short int amr1d_freq;       /*                                       */
   short int amr1d_ref_var;    /* ------------------------------------- */
  #endif
  short int ext;  /* -- start from external files ? -- */
  short int jet;  /* -- follow jet evolution -- */
  short int parallel_dim[3];
} Cmd_Line;

   
typedef struct DATA{
  real ****Vc;   /* -- "c"ell centered data --*/
  real ****Vs;   /* -- "s"taggered data -- */
  real ****Vuser;
  real ***A1;   /* -- vector potential -- */
  real ***A2;
  real ***A3;
  char ***flag;
} Data;
   
typedef struct STATE_1D{
  real **v;    /* cell-centered primitive    vars at t=tn */
  real **u;    /* cell-centered conservative vars at t=tn   */
  real **vL;   /* prim vars left  of the interface, vL[i] = vL(i+1/2) */
  real **vR;   /* prim vars right of the interface, vR[i] = vR(i+1/2) */
  real **vm;   /* prim vars at i-1/2 edge, vm[i] = vR(i-1/2)     */
  real **vp;   /* prim vars at i+1/2 edge, vp[i] = vL(i+1/2)     */

  real **uL;   /* same as vL, in conservative vars */
  real **uR;   /* same as vR, in conservative vars */
  real **um;   /* same as vm, in conservative vars */
  real **up;   /* same as vp, in conservative vars */

  real **flux;      /* upwind flux computed with the Riemann solver */
  real **pnt_flx;
  real **dff_flx;
  real **res_flx;
  double ***Lp, ***Rp; /* left and right primitive eigenvectors */
/*  double **dwlim; */     /* limited slopes in characteristic vars */      
  real **vt;
  real **src;     
  real **par_src; /* geometrical source terms for parabolic operators in curvilinear coords */
  real **vh;      /* Primitive    state at n+1/2 (only for one step method) */
  real **uh;      /* Conservative state at n+1/2 (only for one step method) */
  real **rhs;     /* conservative right hand side */
  real *press;    /* upwind pressure term computed with the Riemann solver */
  real *bn;       /* face magentic field, bn = bx(i+1/2) */
  real *SL;       /* leftmost  velocity in the Riemann fan at i+1/2 */
  real *SR;       /* rightmost velocity in the Riemann fan at i+1/2 */
  double q1,q2; 
  char *flag;
} State_1D;

typedef struct GRID{
  real xi, xf;              /* initial (leftmost) and final (rightmost) point in the (local) domain  */
  real *x, *x_glob;         /* cell geometrical central points */
  real *xr, *xr_glob;       /* cell right interface */
  real *xl, *xl_glob;       /* cell left interface */
  real *dx, *dx_glob;       /* cell size  */ 
  real *xvc;                /* cell volumetric centroid (!= x when geometry != CARTESIAN)  */
  real *dV;                 /* cell volume  */
  real *A;                  /* right interface area */
  real *r_1;                /* geometrical factor 1/r  */
  real *ct;                 /* geometrical factor cot(theta)  */
  real (*dl)(), (*h)();
  real *qq[26];             /* useless, just to make the structure size a power of 2 */
  real dl_min;              /* minimum cell length (e.g. min[dr, r*dth, r*sin(th)*dphi] 
                               (GLOBAL DOMAIN  !!! )                                   */
  int np_tot_glob;          /* TOTAL NUMBER OF POINTS  (Boundaries included) */
  int np_int_glob;          /* TOTAL NUMBER OF POINTS  (Boundaries excluded) */
  int np_tot;               /* NUMBER OF POINTS PER PROC (Boundaries included) */
  int np_int;               /* NUMBER OF POINTS PER PROC (Boundaries excluded) */
  int nghost;               /* GHOST CELLS */
  int lbound;               /*  left physical boundary when != 0. lbound = 0 possible in PARALLEL only  */ 
  int rbound;               /* right physical boundary when != 0. rbound = 0 possible in PARALLEL only  */
  int gbeg;                 /* global start index for the global array */
  int gend;                 /* global end   index for the global array */
  int beg;                  /* global start index for the local array */
  int end;                  /* global end   index for the local array */
  int lbeg;                 /* local start  index for the local array */
  int lend;                 /* local end    index for the local array */
  int uniform;              /* = 1 when the grid is cartesian AND uniform everywhere  */
  int nproc;                /* number of processor for this grid */
} Grid;

typedef struct TIME_STEP{
  real cmax[3];   /* maximum signal velocity for hyperbolic eqns */
  real eta_max;   /* maximum resistivity (MHD only) */
  real kappa_max; /* maximum thermal conductivity */
  real nu_max;    /* maximum viscosity coefficient */
  real dt_cool;  /* cooling time step */
  real dt_adv;
} Time_Step;

typedef struct OUTPUT{
  int    type;       /* output format (DBL, FLT, ...)       - one per output */
  int    nvar;       /* tot. # of vars that can be written  - same for all   */
  int    user_outs;  
  int    nfile;      /* current number being saved          - one per output  */
  int    dn;         /* step increment between outputs      - one per output  */
  int    *stag_var;  /* centered or staggered variable      - same for all   */
  int    *dump_var;  /* select vars being written           - one per output */
  char   mode[32];   /* single or multiple files            - one per output */
  char   **var_name; /* variable names                      - same for all   */
  char   ext[8];     /* output extension                                     */
  double dt;         /* time increment between outputs      - one per output */
  double ***V[64];   /* pointer to arrays being written     - same for all  */
} Output;

typedef struct INPUT{
  int    npoint[3];
  int    lft_bound_side[3];       /* left  boundary type */
  int    rgt_bound_side[3];      /* right boundary type */
  int    grid_is_uniform[3];    /* = 1 when grid is uniform, 0 otherwise */
  int    npatch[5];               /* number of grid patches  */
  int    patch_npoint[5][16];     /* number of points per patch */
  int    patch_type[5][16];             
  int    log_freq;              /* log frequency */
  int    user_var;              /* number of additional user-variables being
                                   held in memory and written to disk */
  int    anl_dn;                /*  number of step increment for ANALYSIS */
  char   solv_type[64];
  char   user_var_name[128][128];
  Output output[MAX_OUTPUT_TYPES];  
  real   patch_left_node[5][16];  /*  self-expl. */
  real   cfl;
  real   cfl_max_var;
  real   tstop;
  real   first_dt;
  real   anl_dt;               /* time step increment for ANALYSIS */
  real   aux[32];             /* we keep aux inside this structure, 
                                  since in parallel execution it has
                                  to be comunicated to all processors  */
} Input;

typedef struct RUNTIME{
  int nstep;
  int nfile[MAX_OUTPUT_TYPES];
  double t;
  double dt;
} Runtime;

typedef struct RGB{
  unsigned char r, g, b;
} RGB;


typedef struct IMAGE{
  int    nrow, ncol;    /* -- image rows and columns -- */
  int    slice_plane;   /* -- one of X12_PLANE, X13_PLANE, X23_PLANE -- */
  int    logscale;      /* -- YES/NO for log scale -- */
  char   *colormap;     /* -- colormap name -- */
  char   basename[32];  /* -- image base name (no extensions) -- */
  unsigned char r[256], g[256], b[256]; /* -- colortable saved here -- */
  RGB    **rgb;         /* -- rgb array containing image values -- */
  double max;           /* -- max image value -- */
  double min;           /* -- min image value -- */
  double slice_coord;   /* -- slice coord orthogonal to slice_plane -- */
} Image;

typedef struct FLOAT_VECT{
  float v1, v2, v3;
} Float_Vect;

typedef struct INDEX{
  int ntot, beg, end;
  int t1, t1_beg, t1_end;
  int t2, t2_beg, t2_end;
} Index;

typedef struct FORCE{
  double *g;  /* -- gravity --  */
  double *d;  /* -- grad phi -- */
  double *c;  /* -- coriolis -- */
  double *a;  /* -- total acceleration -- */
} Force;

/* ####################################################### 

            Recurrent function types 
 
   ####################################################### */

typedef void Riemann_Solver (const State_1D *, int, int, real *, Grid *);
typedef void Limiter     (real *, real *, real *, int, int, Grid *);
typedef void Limiter_3rd (real *, real *, real *, real *, int, int, Grid *);
typedef real ****Data_Arr;


/* ----------------------------------------------------------
      Include label definitions and module prototyping
   ---------------------------------------------------------- */

#if   PHYSICS == HD

 #include "HD/mod_defs.h"

#elif PHYSICS == RHD

 #include "RHD/mod_defs.h"

#elif PHYSICS == MHD

 #include "MHD/mod_defs.h"

#elif PHYSICS == RMHD

 #include "RMHD/mod_defs.h"

#endif

#if INCLUDE_COOLING == NEQ
 #include "Cooling/NEQ/cooling.h"
#elif INCLUDE_COOLING == RAYMOND 
 #include "Cooling/Raymond/cooling.h"
#elif INCLUDE_COOLING == POWER_LAW
 #include "Cooling/Power_Law/cooling.h"
#elif INCLUDE_COOLING == TABULATED
 #include "Cooling/Tab/cooling.h"
#elif INCLUDE_COOLING == H2_COOL
 #include "Cooling/H2_COOL/cooling.h"
#endif

#if THERMAL_CONDUCTION != NO
 #include "Thermal_Conduction/tc.h"
#endif

#if VISCOSITY != NO
 #include "Viscosity/viscosity.h"
#endif

#if INCLUDE_PARTICLES == YES
 #include "Particles/particles.h"
#endif



/* *****************************************************
   *****************************************************

                  GLOBAL VARIABLES

   *****************************************************
   ***************************************************** */

#ifdef PARALLEL
 extern int SZ;
 extern int SZ_char;
 extern int SZ_float;
 extern int SZ_Float_Vect;
 extern int SZ_rgb;
 extern int SZ_short;
 extern int prank;
#else 
extern int prank;
/*
 #ifdef CH_SPACEDIM
  #define prank  (rank)
 #else
  #define prank  0 
 #endif
*/
#endif

extern int NPROCESSORS;
extern int MAX_RIEM;

extern long int USED_MEMORY, NMAX_POINT, NSTEP;
extern long int IBEG, IEND, JBEG, JEND, KBEG, KEND;
extern long int NX, NY, NZ;
extern long int NX_TOT, NY_TOT, NZ_TOT;

extern int ISTEP;

extern int V1, V2, V3;
extern int M1, M2, M3;
extern int B1, B2, B3;

extern int DIR;
extern int *NX_PT, *NY_PT, *NZ_PT;

extern real SMALL_DN, SMALL_PR, MAX_COOLING_RATE;

extern real UNIT_DENSITY, UNIT_LENGTH, UNIT_VELOCITY;
extern real T_CUT_COOL, FLOOR_TRAD, FLOOR_TGAS, CEIL_VA, CEIL_TGAS;

extern real glob_time, delta_t, CFL;
extern real MAX_MACH_NUMBER;

extern real DOM_XBEG[3], DOM_XEND[3];

extern real aux[32];
#if EOS != ISOTHERMAL
 extern real gmm;
#elif EOS == ISOTHERMAL
 extern real C_ISO;
#endif
#ifdef PSI_GLM
 extern double MAXC;
#endif

extern FILE *orion2_log_file;

#define KELVIN (UNIT_VELOCITY*UNIT_VELOCITY*CONST_amu/CONST_kB)

/* ----------------------------------------------------------
    define NX_MAX, NY_MAX, NZ_MAX as the maximum 
    size on a patch.
    For the static grid of ORION2, they simply coincide
    with NX_TOT, NY_TOT, NZ_TOT. Otherwise
    the grid is taken to be the maxium, according
    to maxGridSize. 
    This avoids allocating and freeing memory all the times.
   ---------------------------------------------------------- */

#ifdef CH_SPACEDIM
 #define NX_MAX NMAX_POINT
 #define NY_MAX NMAX_POINT
 #define NZ_MAX NMAX_POINT
#else
 #define NX_MAX NX_TOT
 #define NY_MAX NY_TOT
 #define NZ_MAX NZ_TOT
#endif 

/* ------------------------------------------------------------------
    Define total number of variables to be integrated;
    this includes:

     NFLX     = Number of equations defining the system of 
                conservation laws. For example, for the 
                HD module, it consists of density, momentum and energy.
                It is defined in the physics module header file mod_defs.h.

     NTRACER  = Number of user-defined tracers; defined in the problem
                directory header file definitions.h

     NIONS = Number of chemical species; it is defined in 
                the cooling modules cooling.h, if present.

  nv = 0...NFLX - 1; NFLX...NFLX+NIONS-1; TR...TR + NTRACER-1; ENTR; 
       <----------> <------------------> <------------------>  
          NFLX              NIONS               NTRACER              

                    <---------------------------------------------->
                                    NSCL

   ------------------------------------------------------------------ */

#ifndef ENTROPY_SWITCH
 #define ENTROPY_SWITCH  NO 
#endif 

#ifndef NIONS
 #define NIONS 0
#endif

#define TR   (NFLX + NIONS)
#define NSCL (NTRACER + NIONS + ENTROPY_SWITCH)

#if ENTROPY_SWITCH == YES
 #define ENTR  (TR + NTRACER)
#else
 #if EOS != ISOTHERMAL
  #define ENTR (EN)
 #endif
#endif

#define NVAR (NFLX + NSCL)


/* ------------------------------------------------------------------
                     P R O T O T Y P I N G
   ------------------------------------------------------------------ */

Limiter     fourth_order_lim, flat_lim, minmod_lim, gminmod_lim, 
            umist_lim, vanleer_lim, vanalbada_lim,  mc_lim;
Limiter_3rd triad_lim;

Force *ACCELERATION (double **, int, int, int, Grid *);

void ADD_GEOM_SOURCE (const State_1D *, int, int, double, Grid *);

void ADVECT_FLUX (const State_1D *, int, int, Grid *);

void ANALYSIS (const Data *, Grid *);

void BOUNDARY         (const Data *, int, Grid *);
real ***BOUND_VAR     (const Data *, int, int);

void OUTFLOW_BOUND    (double ***, int);
void PERIODIC_BOUND   (double ***, int);
void REFLECTIVE_BOUND (double ***, int, int);

void CHAR_SLOPES (const State_1D *, double **, int, int, Grid *);

void CHECK_CONS_STATE  (real **, real **, real **, int, int);
void CHECK_PRIM_STATES (real **, real **, real **, int, int);
char CHECK_ZONE (int z, int bit);

void Entropy_Switch (const Data *, Grid *);

void FLATTEN (const State_1D *, int, int, Grid *);

int GET_NGHOST (Input *);

void GET_OPT (int, char *argv[], char *, Cmd_Line *);

#if SELF_GRAVITY == YES || SINK_PARTICLES == YES
 void GET_RHS (const State_1D *, int, int, real, real *, Grid *);
 void GRAVITY_FORCE (const State_1D *, real *, real, int, int, Grid *);
#else
 void GET_RHS (const State_1D *, int, int, real, Grid *);
#endif

#if INCLUDE_BODY_FORCE != NO
 void ADD_BODY_FORCE (const State_1D *, real, int, int, Grid *);
 void BODY_FORCE (double *v, double *, double *, double *,
                  double x1, double x2, double x3);

#endif

void INIT (real *us, real x, real y, real z,
           int i, int j, int k, int *prim_var);

void INITIALIZE(int argc, char *argv[], Data *,
                Input *, Grid *, real *, Cmd_Line *);

void MAKE_GEOMETRY (Grid *);

void FIND_SHOCK (const Data *, Grid *);
void MULTID_LIMITER (const Data *, Grid *);

void PARABOLIC_FLUX (Data_Arr, const State_1D *, int, int, 
                     Time_Step *, Grid *);

void PRINT_CONFIG();

void RECONSTRUCT (const State_1D *, Grid *);

Riemann_Solver *SET_SOLVER (const char *);

void SET_INDEXES (Index *indx, const State_1D *, Grid *grid);

void SET_LIMITER (Limiter *limiter[]);

int  SETUP (Input *, char *);

void SETGRID (struct INPUT *INI, Grid *);

void SPLIT_SOURCE (const Data *, double, Time_Step *, Grid *);

void STARTUP (Data *, int, Grid *);

void STATES (const State_1D *, int beg, int end, real, Grid *);

void SWEEP (const Data *, Riemann_Solver *, Time_Step *, Grid *);

void UNSPLIT (const Data *, Riemann_Solver *, Time_Step *, Grid *);

void USERDEF_BOUNDARY (const Data *, int, Grid *); 
void VISC_FLUX (const State_1D *, int beg, int end, Grid *);

void WRITE_DATA (const Data *, Output *, Grid *);

#if INCLUDE_COOLING != NO
 void   Numerical_Jacobian (real *v, real **J);
 void   Jacobian (real *v, real *rhs, real **dfdy);
 void COOLING_SOURCE (const Data *, real, Time_Step *, Grid *);
 #if INCLUDE_COOLING == POWER_LAW
  void  POWER_LAW_COOLING (Data_Arr, real, Time_Step *, Grid *);
 #endif
 /* move the following elsewhere ?  */
 real COOL_SOLVER_CK45  (real *, real *, real *, real, real);
 real COOL_SOLVER_RKF23 (real *, real *, real *, real, real);
 real COOL_SOLVER_RKF12 (real *, real *, real *, real, real);
 real COOL_SOLVER_ROS34 (real *v0, real *k1, real *v4th, real dt, real tol);
 real COOL_SOLVER_RK4   (real *v0, real *k1, real *v5th, real dt);
 real COOL_SOLVER_RK2   (real *v0, real *k1, real *v5th, real dt);
 real COOL_SOLVER_EULER (real *v0, real *k1, real *v5th, real dt);
 real COOL_SOLVER_HS (real *v0, real *k1, real *v5th, real dt);
#endif

/* --------------------------------------------------------- 
        General utility functions contained in tools.c
   --------------------------------------------------------- */


char    *ARRAY_1D (int, size_t);
char   **ARRAY_2D (int, int, size_t);
char  ***ARRAY_3D (int, int, int, size_t);
char ****ARRAY_4D (int, int, int, int, size_t);

#define Array_1D(nx,type)          (type    *)ARRAY_1D(nx,sizeof(type))
#define Array_2D(nx,ny,type)       (type   **)ARRAY_2D(nx,ny,sizeof(type))
#define Array_3D(nx,ny,nz,type)    (type  ***)ARRAY_3D(nx,ny,nz,sizeof(type))
#define Array_4D(nx,ny,nz,nv,type) (type ****)ARRAY_4D(nx,ny,nz,nv,sizeof(type))

void free_array_1D (real *);
void free_array_2D (real **);
void free_array_3D (real ***);
void free_array_4D (real ****);

void free_int_array_2D (int **);
void free_int_array_1D (int *);

void Make_State (State_1D *);
void ERROR (int, char *);
void TRACE (real);
void SHOW (real **, int);
void WHERE (int, Grid *);
int  CHECK_NAN (real **, int, int, int);
void print  (char *fmt, ...);
void print1 (char *fmt, ...);

void ludcmp (real **a, int n, int *indx, real *d);
void lubksb (real **a, int n, int *indx, real b[]);

real AREA_1 (int i, int j, int k, Grid *);
real AREA_2 (int i, int j, int k, Grid *);
real AREA_3 (int i, int j, int k, Grid *);
real Length_1 (int i, int j, int k, Grid *);
real Length_2 (int i, int j, int k, Grid *);
real Length_3 (int i, int j, int k, Grid *);
real h_scale_1 (int i, int j, int k, Grid *);
real h_scale_2 (int i, int j, int k, Grid *);
real h_scale_3 (int i, int j, int k, Grid *);

/* ---------------------------------------------------
            Chombo stuff 
   --------------------------------------------------- */

real ***chmatrix(int nrl,int nrh,int ncl,int nch,int ndl,int ndh,real *uptr);
real ***chmatrix3(int nx, int ny, int nz,real *uptr);
void free_chmatrix(real ***t,int nrl,int nrh,int ncl,int nch,int ndl,int ndh);
void free_chmatrix3(real ***t);

/* -- I/O -- */

FILE *FILE_APPEND (char *, size_t, size_t);
FILE *FILE_OPEN (char *, size_t, char *);
int FILE_CLOSE  (FILE *, size_t);

void WRITE_BIN_ARRAY (void *, size_t, FILE *, int);
void WRITE_VTK_HEADER (FILE *fvtk, Grid *grid);
void WRITE_VTK_VECTOR (FILE *, Data_Arr, char *, Grid *);
void WRITE_VTK_SCALAR (FILE *, double ***, char *, Grid *);


void WRITE_TAB_ARRAY (Output *, char *, Grid *);
void WRITE_PPM (double ***, char *, char *, Grid *);
void WRITE_PNG (double ***, char *, char *, Grid *);
void READ_BIN_ARRAY  (real ***, FILE *, int);
int  Little_Endian (void);
void RESTART_DUMP (Input *, int);
void RESTART_GET  (Input *, int);
void RESTART (Input *, int);

int SET_DUMP_VAR (char *, int, int);
void CHANGE_DUMP_VAR ();

void SET_COLORMAP (unsigned char *, unsigned char *, unsigned char *,
                   char *map_name);

void COMPUTE_USERVAR (const Data *, Grid *);


int  PAR_OPEN  (char *);
char *PAR_GET  (char *, int );
int  PAR_QUERY (char *);

real VAN_DER_CORPUT(int n, int k1, int k2);
void SET_JET_DOMAIN (const Data *d, Grid *grid);
void UNSET_JET_DOMAIN (const Data *d, Grid *grid);
void CREATE_IMAGE (char *);
Image *GET_IMAGE (char *);
void SWAP_ENDIAN (void *, const int); 

float ***CONVERT_TO_FLOAT (double ***, int);
void SET_OUTPUT (Data *d, Input *input);
double ***GET_USERVAR (char *var_name);
void EXTERNAL_GET_VAR (double ****, Grid *grid);
void DEFAULT_VAR_NAMES(Output *);
void FORCE_PERIODIC_BOUND (real ***V, int dir, Grid *grid);
void FLAG_RESET (const Data *d);

#if SAVE_VEC_POT == YES
 void VEC_POT (const Data *d, const void *vp, const State_1D *state,
               const Grid *grid);
#endif


double MP5_RECONSTRUCT(double *F, int j);
void PPM_RECONSTRUCT (double *v, double *vm, double *vp, int i);
double WENO_RECONSTRUCT(double *f, int j);

double MINMOD (double a, double b);
double MEDIAN (double a, double b, double c);
void PRIM_LIMIT (const State_1D *state, int i, int nv);
void LpPROJECT (double **, double *, double *);
void RpPROJECT (double **, double *, double *);
void LcWAVE (double **Lc, double **uc, double *w);




double Lflat_lim (const double dp, const double dm);
double Lminmod_lim (const double dp, const double dm);
double Lvanleer_lim (const double dp, const double dm);
double Lmc_lim (const double dp, const double dm);
double Lvanalbada_lim (const double dp, const double dm);

