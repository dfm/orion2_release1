#include "orion2.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 *
 **************************************************************** */
{
  *prim_var = 1;

  gmm = aux[GAMMA];

  if (x1 < 0.5){

    us[DN] = aux[RHO_LEFT];
    us[VX] = aux[VX_LEFT];
    us[VY] = aux[VY_LEFT];
    us[VZ] = aux[VZ_LEFT];
    us[BX] = aux[BX_CONST];
    us[BY] = aux[BY_LEFT];
    us[BZ] = aux[BZ_LEFT];
    us[PR] = aux[PR_LEFT];

  }else{

    us[DN] = aux[RHO_RIGHT];
    us[VX] = aux[VX_RIGHT];
    us[VY] = aux[VY_RIGHT];
    us[VZ] = aux[VZ_RIGHT];
    us[BX] = aux[BX_CONST];
    us[BY] = aux[BY_RIGHT];
    us[BZ] = aux[BZ_RIGHT];
    us[PR] = aux[PR_RIGHT];
 
  }

  if (aux[DIVIDE_BY_4PI] > 0.5){
    us[BX] /= sqrt(4.0*CONST_PI);
    us[BY] /= sqrt(4.0*CONST_PI);
    us[BZ] /= sqrt(4.0*CONST_PI);
  }
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 **************************************************************** */
{
}

/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid) 
/* 
 *
 * 
 **************************************************************** */
{
}
