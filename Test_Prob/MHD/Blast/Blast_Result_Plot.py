import yt
import matplotlib.colorbar as cb
import sys

fn = 'data.0020.3d.hdf5'
field = 'density' 
dir = "z"

ds = yt.load(fn)
c = (ds.domain_left_edge + ds.domain_right_edge) / 2
p = yt.SlicePlot(ds, dir, field, center = c)
p.annotate_grids()
p.save('Blast_Result')

