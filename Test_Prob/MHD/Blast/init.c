#include "orion2.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 *
 **************************************************************** */
{
  real r, theta, phi, B0;
  real dx1, dx2, dx3;
  *prim_var = 1;

/* ------------------------------------------------------------
    Set initial condition for the 2 or 3-D blast wave
    problem. 
    The 2-D versions are taken from 

     -test #1:  Balsara & Spicer (1999), JCP 149, 270 
  
    The 3-D version is taken from 

     -test #2, #3 (in axisymmetry):  Ziegler (2004), JCP 196,393 
            
    and from 

     -test #4,5: Gardiner & Stone (2008), JCP, 227

   ------------------------------------------------------------ */

  gmm = aux[GAMMA];
  dx1=x1-aux[cx1];
  if (dx1 >1.0-aux[RADIUS]) dx1=1.0-dx1;
  dx2=x2-aux[cx2];
  if (dx2 >1.0-aux[RADIUS]) dx2=1.0-dx2;
  dx3=x3-aux[cx3];
  if (dx3 >1.0-aux[RADIUS]) dx3=1.0-dx3;

  r = D_EXPAND(pow(dx1,2), + pow(dx2,2), + pow(dx3,2));
  r = sqrt(r);

  us[DN] = 1.0;
  us[VX] = 0.0;
  us[VY] = 0.0;
  us[VZ] = 0.0;
  us[PR] = aux[P_OUT];
  
  theta = aux[THETA]*CONST_PI/180.0;
  phi   =   aux[PHI]*CONST_PI/180.0;
  B0 = aux[BMAG];
 
  us[BX] = B0*sin(theta)*cos(phi);
  us[BY] = B0*sin(theta)*sin(phi);
  us[BZ] = B0*cos(theta);
  
  if (r <= aux[RADIUS]) us[PR] = aux[P_IN];
  
  #if GEOMETRY == CARTESIAN
   us[AX] = 0.0;
   us[AY] =  us[BZ]*x1;
   us[AZ] = -us[BY]*x1 + us[BX]*x2;
  #elif GEOMETRY == CYLINDRICAL
   us[AX] = us[AY] = 0.0;
   us[AZ] = 0.5*us[BY]*x1;
  #endif


  #if INCLUDE_BACKGROUND_FIELD == YES
   us[BX] = us[BY] = us[BZ] =
   us[AX] = us[AY] = us[AZ] = 0.0;
  #endif

}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{

}
/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid) 
/* 
 *
 * 
 *
 *
 **************************************************************** */
{
  int   i, j, k, nv;
  real  x1, x2, x3;

  if (side == X1_BEG){

    X1_BEG_LOOP(k,j,i){
      for (nv = 0; nv < NVAR; nv++){
        d->Vc[nv][k][j][i] = d->Vc[nv][k][j][2*IBEG - i - 1];
      }
      d->Vc[VX][k][j][i] *= -1.0;
      d->Vc[BX][k][j][i] *= -1.0;

      #ifdef STAGGERED_MHD
       d->Vs[BYs][k][j][i] = d->Vs[BYs][k][j][2*IBEG - i - 1];
       d->Vs[BZs][k][j][i] = d->Vs[BZs][k][j][2*IBEG - i - 1];
      #endif
    }

  }

  if (side == X1_END){

    X1_END_LOOP(k,j,i){}

  }

  if (side == X2_BEG){

    X2_BEG_LOOP(k,j,i){
         
      for (nv = 0; nv < NVAR; nv++){
        d->Vc[nv][k][j][i] = d->Vc[nv][k][2*JBEG - j - 1][i];
      }
      EXPAND(d->Vc[BX][k][j][i] *= -1.0; ,
             d->Vc[VY][k][j][i] *= -1.0; ,
             d->Vc[BZ][k][j][i] *= -1.0;)
      #ifdef STAGGERED_MHD
       d->Vs[BXs][k][j][i] = - d->Vs[BXs][k][2*JBEG - j - 1][i];
       d->Vs[BZs][k][j][i] = - d->Vs[BZs][k][2*JBEG - j - 1][i];
      #endif

    }
  }

  if (side == X2_END) {

    X2_END_LOOP(k,j,i){}

  }

  if (side == X3_BEG){
  
    X3_BEG_LOOP (k,j,i){
  
      for (nv = 0; nv < NVAR; nv++){
        d->Vc[nv][k][j][i] = d->Vc[nv][2*KBEG - k - 1][j][i];
      }
      d->Vc[VZ][k][j][i] *= -1.0;
      d->Vc[BZ][k][j][i] *= -1.0;

      #ifdef STAGGERED_MHD
       d->Vs[BXs][k][j][i] = d->Vs[BXs][2*KBEG - k - 1][j][i];
       d->Vs[BYs][k][j][i] = d->Vs[BYs][2*KBEG - k - 1][j][i];
      #endif

    }

  }
 
  if (side == X3_END) {
  
   X3_END_LOOP(k,j,i){}

  }
}


