#!/bin/sh

## ensure we are in the source directory
cd $ORION2_DIR/Test_Prob/MHD/Blast

## run simulation
./orion2

## check results
python3 ./Blast_Result_Eval.py
