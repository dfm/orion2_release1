#define    PHYSICS   MHD
#define    DIMENSIONS   2
#define    COMPONENTS   2
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   1

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO
#define    VISCOSITY   NO

/* -- pointers to user-def parameters -- */

#define  SCRH   0

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         NO
#define  SHOCK_FLATTENING      NO
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         NO
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE        UCT_CONTACT
#define  CT_EN_CORRECTION      NO
#define  CT_VEC_POT_INIT       YES
#define  SAVE_VEC_POT          NO
