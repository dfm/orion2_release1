#include "orion2.h"
#include "math.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 *
 **************************************************************** */
{
  *prim_var = 1;

  // The initial diaphragm can be at an angle, given in the orion2.ini file 
  // The units of DANGLE should be degrees and limited to some value between 0 and 90. 
  double theta = aux[DANGLE];

  double slope = 0;
  if(theta <0 || theta >=90) slope = 0; // error checks
  else slope = -tan(theta*3.14159/180.0); // quantity is always negative or 0

  // The diaphragm was originally perpendicular to the x-axis. When theta is > 0,
  // the location rotates in the x-y plane such that it always goes through the 
  // point (x,y) = (1/2,1/2). Therefore, it is ideal to keep the domain dimensions 
  // as [0,1] for these coordinates. 
  double xLine = 0.5 + slope*(x2-0.5); // to the left of this line, use rho,P = 1

  gmm = 1.4; // gmm is a global variable. This sets its value. 
  if (x1 < xLine) {
    us[DN] = 1.0;
    us[PR] = 1.0;
  }else{
    us[DN] = 0.125;
    us[PR] = 0.1;
  }

  // Gas is initially stationary.
  us[VX] = 0.0;
  us[VY] = 0.0;
  us[VZ] = 0.0;
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 **************************************************************** */
{

}

/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *GXYZ) 
/* 
 *
 * 
 *
 **************************************************************** */
{
}

