#include "orion2.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * PURPOSE
 *
 *  Set the initial condition for a Sedov-Taylor blast 
 *  wave.
 *  The input parameters are (in orion2.ini):
 *
 *   - DNST0: initial density
 *   - ENRG0: initial energy (NOT energy density)
 *   - GAMMA: specific heat ratio.
 *
 *   
 *  The numerical solution can be checked against the 
 *  analytical one available, for instance, at
 * 
 *     http://flash.uchicago.edu/~ahaque/sedov.html
 *
 *  [run with ./sedov.exe sedov.param -r -auto]
 *
 * 
 * LAST MODIFIED
 *
 *   May 28, 2010 by A. Mignone (mignone@ph.unito.it)
 *
 **************************************************************** */
{
  double dr, vol, r;
  *prim_var = 1;

  gmm = aux[GAMMA];

/* --------------------------------------------------
    dr is the size of the initial energy deposition 
    region: 2 ghost zones.
   -------------------------------------------------- */

  #if DIMENSIONS == 1
   dr = 2.0*(DOM_XEND[IDIR]-DOM_XBEG[IDIR])/(double)NX_TOT;
  #else
   dr = 3.5*(DOM_XEND[IDIR]-DOM_XBEG[IDIR])/(double)NX_TOT;
  #endif

/* ---------------------------------------
     compute region volume 
   --------------------------------------- */

  #if (GEOMETRY == CARTESIAN) && (DIMENSIONS == 1)
   vol = 2.0*dr;
  #elif (GEOMETRY == CYLINDRICAL && DIMENSIONS == 1)|| \
        (GEOMETRY == CARTESIAN   && DIMENSIONS == 2)
   vol = CONST_PI*dr*dr;
  #elif (GEOMETRY == SPHERICAL   && DIMENSIONS == 1)|| \
        (GEOMETRY == CYLINDRICAL && DIMENSIONS == 2)|| \
        (GEOMETRY == CARTESIAN   && DIMENSIONS == 3)
   vol = 4.0/3.0*CONST_PI*dr*dr*dr;
  #else
   print1 ("! geometrical configuration not allowed\n");
   QUIT_ORION2(1);
  #endif

  r = EXPAND(x1*x1, + x2*x2, +x3*x3);
  r = sqrt(r);

  us[DN] = aux[DNST0];
  us[VX] = 0.0;
  us[VY] = 0.0;
  us[VZ] = 0.0;

  if (r <= dr) {
    us[PR] =  (gmm - 1.0)*aux[ENRG0]/vol;
  }else{
    us[PR] = 1.e-5;
  }

  us[TR]  = 0.0;

  #if PHYSICS == MHD || PHYSICS == RMHD

   us[BX] = 0.0;
   us[BY] = 0.0;
   us[BZ] = 0.0;

   #ifdef STAGGERED_MHD

    us[AX] = 0.0;
    us[AY] = 0.0;
    us[AZ] = 0.0;

   #endif

  #endif
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * ARGUMENTS
 *
 *   d:      the ORION2 Data structure.
 *   grid:   pointer to array of GRID structures  
 *
 **************************************************************** */
{

}
#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    vector component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid) 
/* 
 *
 * PURPOSE
 *
 *   Define user-defined boundary conditions.
 *
 *
 * ARGUMENTS
 * 
 *   uu (IN/OUT)       a three-dimensional vector uu[nv][k][j][i], 
 *                     containing the solution and boundary values 
 *                     to be assigned.
 *
 *   side (IN)         tells on which side boundary conditions need 
 *                     to be assigned. side can assume the following 
 *                     pre-definite values: X1_BEG, X1_END,
 *                                          X2_BEG, X2_END, 
 *                                          X3_BEG, X3_END
 *
 *                     When 
 *
 *                      side = Xn_BEG  -->  b.c. are assigned at the 
 *                                          beginning of the xn
 *                                          direction.
 *                      side = Xn_BEG  -->  b.c. are assigned at the 
 *                                          beginning of the xn
 *                                          direction.
 *
 **************************************************************** */
{
  int   i, j, k, nv;
  real  *x1, *x2, *x3;

  x1 = grid[IDIR].x;
  x2 = grid[JDIR].x;
  x3 = grid[KDIR].x;

  if (side == 0) {    /* -- check solution inside domain -- */
    DOM_LOOP(k,j,i){};
  }

  if (side == X1_BEG){  /* -- X1_BEG boundary -- */
    X1_BEG_LOOP(k,j,i){}
  }

  if (side == X1_END){  /* -- X1_END boundary -- */
    X1_END_LOOP(k,j,i){}
  }

  if (side == X2_BEG){  /* -- X2_BEG boundary -- */
    X2_BEG_LOOP(k,j,i){}
  }

  if (side == X2_END){  /* -- X2_END boundary -- */
    X2_END_LOOP(k,j,i){}
  }

  if (side == X3_BEG){  /* -- X3_BEG boundary -- */
    X3_BEG_LOOP(k,j,i){}
  }
 
  if (side == X3_END) {  /* -- X3_END boundary -- */
    X3_END_LOOP(k,j,i){}
  }
}
