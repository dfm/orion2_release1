#define  PHYSICS                 HD
#define  DIMENSIONS              1
#define  COMPONENTS              1
#define  GEOMETRY                CYLINDRICAL
#define  INCLUDE_BODY_FORCE      NO
#define  INCLUDE_COOLING         NO
#define  INTERPOLATION           LINEAR
#define  TIME_STEPPING           HANCOCK
#define  DIMENSIONAL_SPLITTING   YES
#define  NTRACER                 0
#define  USER_DEF_PARAMETERS     3

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    THERMAL_CONDUCTION   NO
#define    VISCOSITY   NO

/* -- pointers to user-def parameters -- */

#define  ENRG0              0
#define  DNST0              1
#define  GAMMA              2

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      YES
#define  PRINT_TO_FILE         NO
#define  SHOCK_FLATTENING      NO
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         NO
#define  LIMITER               fourth_order_lim
#define  PRIMITIVE_HANCOCK     YES
