import numpy as np
import numpy.linalg as LA
import sys

au = 14959787014953.414

AfterFile=open("data.0000.3d.sink")
AfterFile.readline()
MergedStar = AfterFile.readline().split(' ')
L_m = np.array([float(MergedStar[7]), float(MergedStar[8]), float(MergedStar[9])])
L_mag = LA.norm(L_m)

Rz = np.array([[np.cos(np.pi/6.), -1*np.sin(np.pi/6.), 0.],[np.sin(np.pi/6.),np.cos(np.pi/6.), 0.],[0.,0.,1,]])
Ry = np.array([[np.cos(np.pi/6.), 0. , -1*np.sin(np.pi/6.)],[0.,1., 0,],[np.sin(np.pi/6.), 0. , np.cos(np.pi/6.)]])
Rx = np.array([[1.,0., 0.],[0., np.cos(np.pi/6.), -1*np.sin(np.pi/6.)],[0., np.sin(np.pi/6.),    np.cos(np.pi/6.)]])
L_pmag=1.65e51
L_p = np.dot(Rz, np.dot(Ry, np.dot(Rx, np.array([[1.],[0.],[0.]])))) * 1.65e51
L_p = np.transpose(L_p)[0]

print( 'predicted\t', L_pmag)
print( 'actual \t\t', L_mag, '\n')

if L_mag > 0.95*L_pmag and L_mag < 1.05*L_pmag:
    print( 'Pass!')
    print( 'The predicted ang. mom. was   :', L_pmag)
    print( 'and you got the ang.mom. to be:', L_mag)
    print( 'which is only off by          :', (L_mag - L_pmag)/L_pmag * 100.,'%')
    if np.dot(L_p/LA.norm(L_p) , L_m/LA.norm(L_m) ) ==0: print( 'And the angle difference was  : 0.000')
    else: print( 'And the angle difference was  :', (180./np.pi) * np.arccos(np.dot(L_p/LA.norm(L_p) , L_m/LA.norm(L_m) )))
    writer=open('test_pass', 'w')
    writer.close()
else:
    print( 'Fail!')
    print( 'should have gotten', L_p, 'but instead got', L_mag)
