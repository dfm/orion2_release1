import numpy as np
import math

import zams

MSUN = 1.989e33
RSUN = 6.94e10
LSUN = 3.84e33
Z=1.0

def get_windprop(msol = []):
    r"""
    Function to calculate the zero age main sequence (ZAMS) isotropic mass-loss and 
    wind velocity from Vink+2001 given a list of stellar masses

    Parameters
    ----------

    msol : list of stellar masses in solar units.
    
    Example
    --------

        >>> from get_windprop import *
        >>> get_windprop(msol = [10,100])
        
    Prints out:
    --------
    Masses (Msun) =[ 10 100]
    Luminosity (Lsun) =[   5560.56075138 1272650.45418463]
    Radii (Rsun) =[ 3.95315198 17.27526241]
    Teff (K) =[25119.1876211  46737.19624702]
    Mass loss rates (Msun/yr) =[2.52475577e-10 8.19719142e-06]
    Wind velocity (km/s) =[597.92982013 618.51974945]
    vrat =[2.6 2.6]
    """

    #cutoff Temperatures for stellar class
    Teff_af = 12.5e3
    Teff_o = 21.0e3


    if len(msol) == 0:
        msol = np.array([1, 2.5, 5, 10, 20, 25, 30])
    else:
        msol = np.asarray(msol)
    #set up arrays
    rsol = np.zeros(len(msol))
    lsol = np.zeros(len(msol))
    Teff = np.zeros(len(msol))
    
    log10mdot = np.zeros(len(msol))
    logvout = np.zeros(len(msol))
    vrat = np.zeros(len(msol))+1.3
    
    for i,m in enumerate(msol):
        rsol[i] = zams.rzams(m)/RSUN
        lsol[i] = zams.lzams(m)/LSUN
        Teff[i] = zams.tzams(m)
        
    ind  = np.where(Teff < Teff_af)[0]
    if len(ind > 0):
        vrat[ind] = 0.7 #A-F stars
    ind  = np.where(Teff > Teff_o)[0]
    if len(ind > 0):
        vrat[ind] = 2.6#O stars


    #Get mdots
    ind = np.where(Teff < 2.25e4)[0]
    if len(ind) > 0:
        log10mdot[ind] = -6.688 + 2.210*np.log10(lsol[ind]/1.0e5) - \
                         1.339*np.log10(msol[ind]/30.) - 1.601*np.log10(vrat[ind]/2.0) + \
                         1.07*np.log10(Teff[ind]/2.0e4) + 0.85*np.log10(Z/1.0)
    
    ind = np.where(Teff >= 2.5e4)[0]
    if len(ind) > 0:
        log10mdot[ind] = -6.697+2.194*np.log10(lsol[ind]/1.0e5) - \
                         1.313*np.log10(msol[ind]/30.)-1.226*np.log10(vrat[ind]/2.0) + \
                         0.933*np.log10(Teff[ind]/4.0e4) - 10.92*(np.log10(Teff[ind]/4.0e4))**2.0 + \
                         0.85*np.log10(Z/1.0)

    #Units Msun/yr
    mdot =  10**log10mdot
    #Get wind velocities
    logvout = 1.23 - 0.3*np.log(lsol) + 0.55*np.log(msol) + \
              0.64*np.log(Teff) + 0.13*np.log(Z)
    vout = np.exp(logvout)


    print('Masses (Msun) =' + str(msol) )
    print('Luminosity (Lsun) =' + str(lsol))
    print('Radii (Rsun) =' + str(rsol))
    print('Teff (K) =' + str(Teff))
    print('Mass loss rates (Msun/yr) =' + str(mdot))
    print('Wind velocity (km/s) =' + str(vout))
    print('vrat =' + str(vrat))

