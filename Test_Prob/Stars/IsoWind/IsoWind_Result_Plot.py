import yt
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
import glob
import math
from yt.units.yt_array import YTArray
from yt.units import mass_hydrogen, kb, pc, mass_sun
from get_windprop import *
SECYR = 3600.*24*365.25
PCCM = 3.08567758e18
MP =1.67e-24 #proton mass
MU = 2.33 #mean molecular weight forH_2 with cosmic abundances
MUH = 2.34e-24 #g mean molecular weight for hydrogen
def _Rshock(field, data):
    R=(data['x']**2+data['y']**2+data['z']**2)**0.5
    fac = 1.25
    rho0 = data.get_field_parameter("rho0")
    if rho0 is None:
        # use a default if unset
        rho0 = 2.04e-21
    rho = data['density']
    R[rho>fac*rho0] = 1
    R[rho<=fac*rho0] = 0
    return(R)
yt.add_field(("gas", "Rshock"), function=_Rshock, units="cm")

def _IsoTemp(field,data):
    MUM = 2.3
    MUI = 1.27
    rho = data['density']
    P= data['rho']#data['pressure']
    Tm=MUM*mass_hydrogen/kb*(P/rho)
    Ti= MUI*mass_hydrogen/kb*(P/rho)
    return(Tm)
#yt.add_field(("gas", "iso_temperature"), function=_IsoTemp, units="K")

if __name__=="__main__":
    yt.enable_parallelism()
    #For plotting
    colors = ['Teal', 'DeepPink', 'MediumOrchid']
    ls = ['-.','--', '-']
    labs = [r'$R_{\rm true}$', r'$R_{\mathrm{sim}, \rho}$']

    #Directory where data is in, leave blank if this directory.
    fdir = '/nobackupp8/arosen2/ORION2/TestProblems/IsoWind/'
    
    fstr = ['']
    fdir2 = fdir+fstr[0]
    #Parameters for analytical comparison
    vw = 200 ##km/s (weak wind test problem)
    Mdotw = 2.92533047e-08 #Msun/yr 20 Msun star from init.sink
    Tlim = 500
    #Create figure
    fig, (ax1, ax2) =plt.subplots(2,1, sharex=True)
    fig.subplots_adjust(hspace=0.075,wspace=0.05, left = 0.15, right=0.98, top=0.98, bottom=0.125)

    fpath = fdir2 + 'data.[0-9][0-9][0-9][0-9].3d.hdf5'
    filenames = glob.glob(fpath)
    filenames.sort()
    nfiles = len(filenames)
    
    #Get ambient density from initial data file
    ds = yt.load(filenames[0])
    rho0 = np.mean(ds.r['density']).v
    nH =rho0/MUH
    
    #Analytical formula is from Appendix A of Offner & Arce 2015, derived from Koo & McKee 1992 (i.e, the slow winds case).
    storage = {}
    ts = yt.DatasetSeries.from_filenames(filenames)
    for store, ds in ts.piter(storage=storage):
    #for i, ds in enumerate(ts.piter()):
        ad =ds.all_data()
        ad.set_field_parameter("rho0", rho0)
        time =  ds.current_time.v
        tsim = (time/(SECYR*1e6))
        #Analytical solution from Offner & Arce 2015
        Ra =0.24* (Mdotw/1e-7)**0.25 * (vw/200)**(0.25) * (nH/872)**(-0.25) * (tsim/0.1)**0.5 #(output units pc)
                            
        print('Getting Rshell for ds = %s' %ds)
        #Get density weighted average
        num=sum(ad["Rshock"].v*ad["density"].v*(ad["x"].v**2.+ ad["y"].v**2.+ad["z"].v**2.)**0.5)
        denom=sum(ad["Rshock"].v*ad["density"].v)
        if denom == 0:
            Rsh_rho = np.nan
        else:
            Rsh_rho= (num/denom)/PCCM
        store.result = (tsim, Ra, Rsh_rho)
        

    #Get results
    results = np.array(list(storage.values()))
    tsim = results[:,0]
    Ra = results[:,1]
    Rsh = results[:,2]
    
    #Plot the analytical solution and simulations radii
    ax1.plot(tsim, Ra, color = colors[0], ls = ls[0], label = labs[0])
    Resid = (Ra-Rsh)/Ra
    ax1.plot(tsim, Rsh, color = colors[1], ls = ls[1], label = labs[1])
    ax2.plot(tsim, Resid, color = colors[1], ls = ls[1], label = labs[1])
    ax1.set_xlim(0.0,0.1)
    ax2.set_xlim(0.0,0.1)

    fontP = FontProperties()
    fontP.set_size('medium')
    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend(handles, labels, loc='lower right', ncol=1, prop=fontP)
    
    ax1.set_ylabel(r'$R_{\rm sh}$ [pc]')
    ax2.set_ylabel(r'$(R_{\rm true}-R_{\rm sh})/R_{\rm true}$')
    ax2.set_xlabel('Time [Myr]')
    fig.savefig(fdir2 + 'IsoWind_Result.png')
    plt.clf()
