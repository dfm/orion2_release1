import numpy as np
import numpy.linalg as LA
import sys
import os
import pylab as pl

G = 6.6726e-8
c_iso = 1.8e4
msol = 1.98892e33
au = 14959787014953.414

pfiles=os.popen("ls data.*.sink").read().strip(' \n').split("\n")

mass = []
AngMomX = []
AngMomY = []
AngMomZ = []

for file in pfiles:
    f=open(file)
    f.readline()
    all = f.readline().split(' ')
    mass.append(float(all[0])/msol)
    AngMomX.append(float(all[7]))
    AngMomY.append(float(all[8]))
    AngMomZ.append(float(all[9]))


nout = len(mass) 

Measured_deltaL_deltaM = np.zeros(nout-1)
DeltaM = np.zeros(nout-1)
for i in range(nout-1):
	DeltaM[i] = mass[i+1]*msol - mass[i]*msol
	DeltaL = np.array([AngMomX[i+1]-AngMomX[i],AngMomY[i+1]-AngMomY[i],AngMomZ[i+1]-AngMomZ[i]])
	Measured_deltaL_deltaM[i] = np.sqrt(np.sum(np.square(DeltaL)))/DeltaM[i]
	
# Angular Momentum Direction Accuracy
ANGLE = np.arccos(np.dot(DeltaL/LA.norm(DeltaL), np.array([1.,1.,1.])/LA.norm(np.array([1.,1.,1.]))))*(180./np.pi)

# The Angular Momentum per unit mass is supposed to be 1.35e15
#Measured_deltaL_deltaM = np.sqrt(np.sum(np.square(DeltaL)))/DeltaM
Analytic_deltaL_deltaM = 2.0e15

print( 'The measured specific angular momentum is      :', Measured_deltaL_deltaM[0])
print( 'The analytic specific angular momentum is      :', Analytic_deltaL_deltaM)
print( 'The angle between the measured and analytic is :', ANGLE)

Error = (Measured_deltaL_deltaM[0]-Analytic_deltaL_deltaM)/Analytic_deltaL_deltaM

print( 'The Ang Mom per unit mass is off by '+str(Error*100.)+' %')
if np.abs(Error) < 0.05 and os.path.exists("data.0001.3d.sink") and ANGLE < 2.0:
    print('Pass!')
    writer=open('test_pass', 'w')
    writer.close()
else:
    print('FAIL!')


lw = 3.0
fig = pl.figure()
ax = fig.gca()

ax.plot(np.abs(Measured_deltaL_deltaM-np.ones(nout-1)*Analytic_deltaL_deltaM)/Analytic_deltaL_deltaM*100.0, lw=3, marker='o', ls='-', markersize=5)
pl.xlabel('Data Output')
pl.ylabel('% Error')
pl.ylim(0, 10)

pl.savefig("AngMom_Result")
