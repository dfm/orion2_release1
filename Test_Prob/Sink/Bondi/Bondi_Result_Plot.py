"""
Script to evaluate results for Bondi accretion test
"""

# Imports
import numpy as np
import matplotlib.pyplot as plt
import yt
import yt.units as u
from yt.units import physical_constants as const
from glob import glob

# Extract background density and sound speed from orion2.ini file
try:
    lines = open('orion2.ini').readlines()
except IOError:
    print('Error: orion2.ini not found; run this script in the directory '
          'that contains the orion2.ini file used to run the problem')
    raise IOError # Re-raise to exit
for line in lines:
    sp = line.split()
    if len(sp) < 2:
        continue
    param = sp[0].strip()
    if param == 'den0':
        rho0 = float(sp[1].strip()) * u.g/u.cm**3
    elif param == 'cs0':
        cs0 = float(sp[1].strip()) * u.cm/u.s

# Read the analytic solution from the table
try:
    lines = open('tabular.h').readlines()
except IOError:
    print('Error: tabular.h  not found; run this script in the directory '
          'that contains the tabular.h file used to compile the problem')
    raise IOError # Re-raise to exit
for line in lines:
    if line[:14] == '#define DELTA ':
        delta = float(line[14:-1])
    if line[:16] == '#define N_TABLE ':
        N = float(line[16:-1])
    if line[:20] == 'float v[N_TABLE] = {':
        us = np.array([float(val) for val in line[20:-3].split(',')])
    if line[:20] == 'float a[N_TABLE] = {':
        alphas = np.array([float(val) for val in line[20:-3].split(',')])
xs = np.arange(delta, (N+1)*delta, delta)

# Get list of plot files
files = glob('data.*.3d.hdf5')
files.sort()

# Read all files using yt
ds = []
for f in files:
    ds.append(yt.load(f))

# Extract the initial particle mass and compute the Bondi radius, time, and accretion rate
m = ds[0].all_data()['particle_mass'][0]
r_bondi = const.G * m / cs0**2
t_bondi = r_bondi / cs0
dmdt_bondi = 4.0 * np.pi * rho0 * (const.G * m)**2 / cs0**3

# Compute simulation mass accretion rate
t = np.array([ float(d.current_time) * u.s for d in ds ]) * u.s
msink = np.array([ d.all_data()['particle_mass'][0] for d in ds ]) * u.g
dmdt = (msink[1:] - m) / t[1:]

# Extract density and velocity profiles from last time
dx = float((ds[0].domain_right_edge[0] - ds[0].domain_left_edge[0]) /
           ds[0].domain_dimensions[0]) * u.cm
r_sink = 4*dx
r_domain = float(ds[0].domain_right_edge[0]) * u.cm
x_domain = np.ceil(r_domain/r_bondi)
source = ds[-1].sphere("c", 4*r_bondi)
prof = source.profile(["radius"],
                      ["density", "radial_velocity"],
                      extrema={"radius" : (0.5*r_sink, x_domain*r_bondi)},
                      weight_field="cell_volume",
                      logs={"radius": False},
                      n_bins=32)

# Plot numerical results on top of analytic solution
plt.clf()

# Density
ax = plt.subplot(3,1,1)
plt.plot(xs, alphas, 'k', label='Analytic')
plt.plot(prof.x/r_bondi, prof['density']/rho0, 'o', mec='k',
         label=r'Numerical, $t/t_{{\mathrm{{Bondi}}}} = {:3.1f}$'.format(t[-1]/t_bondi))
plt.plot(xs, np.ones(xs.size), 'k:', alpha=0.5)
plt.xlim([0,x_domain])
ymax = np.ceil(np.amax(prof['density']/rho0))
plt.ylim([0,ymax])
plt.fill_between([0, r_sink/r_bondi], [ymax, ymax], color='k', alpha=0.3,
                 label=r'$r < r_{\mathrm{sink}}$')
plt.legend()
plt.ylabel(r'$\rho/\rho_\infty$')
plt.xlabel(r'$r / r_{\mathrm{Bondi}}$')

# Velocity
ax = plt.subplot(3,1,2)
plt.plot(xs, us, 'k', label='Analytic')
plt.plot(prof.x/r_bondi, prof['radial_velocity']/cs0, 'o', mec='k')
plt.xlim([0,x_domain])
ymax = np.ceil(np.amax(prof['radial_velocity']/cs0))
plt.ylim([0,ymax])
plt.fill_between([0, r_sink/r_bondi], [0, 0], [ymax, ymax], color='k', alpha=0.3)
plt.ylabel(r'$v_r / c_{s,0}$')
plt.xlabel(r'$r / r_{\mathrm{Bondi}}$')

# Accretion rate
ax = plt.subplot(3,1,3)
plt.plot(t[1:] / t_bondi, dmdt / dmdt_bondi, 'o', mec='k')
plt.plot([0, t[-1]/t_bondi], [1,1], 'k')
plt.ylim([0,2])
plt.ylabel(r'$\dot{M} / \dot{M}_{\mathrm{Bondi}}$')
plt.xlabel(r'$t/t_{\mathrm{Bondi}}$')

# Adjust spacing
plt.subplots_adjust(top=0.95, right=0.95, hspace=0.5)

# Save
plt.savefig('Bondi_Result.png')

# Print quantitative error
print("Final error in dm/dt: {:f}".format(dmdt[-1]/dmdt_bondi - 1))
