.. highlight:: rest
  
.. _sec-quickstart:

Quick Start
===========

Once the libraries are ready, you can compile a problem in the “$ORION2_DIR/Test_Prob” directory. Right now, there are some example test problems in there for the MHD physics module at “$ORION2_DIR/Test_Prob/MHD”, as well as a few radiation tests at “$ORION2_DIR/Test_Prob/MHD/Radiation”. ORION 2 uses the same build system as PLUTO, which creates the makefile for you using python scripts. To generate a makefile, go into a test problem directory. You should see the following files::

    init.c
    definitions.h
    orion2.ini

There may additionally be a “makefile” or “make.vars” in there, but these will be overwritten with files appropriate for your machine. To do so, type::
 
    python $ORION2_DIR/setup.py --with-orion2

An interactive menu will pop up. Select “Setup Problem” and list of choices will appear. The default values will be based on the contents of the “definitions.h” file. You can accept the defaults given in the definitions.h file by using::

    python $ORION2_DIR/setup.py --with-orion2 --no-gui

The last step is::

    make -j8

which should create the executable orion2. Put orion2 and the orion2.ini in your scratch directory for your simulation. Do this for “init.sink” as well if your problem setup requires this file. In your batch script, what you need is “mpirun -np 8 ./orion2 [options]” to run the executable. You don't need to specify the name of the parameter file. See PLUTO3 documentation to find out what “options” that you can use, such as “-restart” for restarting the run (e.g., mpirun -np 8 ./orion2 -restart=3), “-maxsteps” to set the maximum number of steps to run, etc.

On different platforms, compilation of Chombo and ORION2 could be quite different because of different compilers, optimization flags, parallel libraries, etc. Also the MPI configuration on these massively parallel platforms can be quite different. Chombo uses very aggressive MPI asynchronous messaging. Nodes have to run “unexpected” buffers. For example on TACC Ranger, one needs to set the environment variable MV2_SRQ_SIZE to 1000 or even 2000 in order for runs that require large number of cpus. If you have any particular settings for any specific platform for ORION2 to work, please post it here.



