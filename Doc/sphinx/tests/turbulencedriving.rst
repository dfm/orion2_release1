.. highlight:: drive

.. _ssec-tests-turbulencedriving:


Turbulence Driving Demonstration
=================================

This section shows a demonstration of setting up a box that will be turbulently driving with periodic boundary conditions.

The test in this section is located in the ``Test_Prop/Drive`` directory.

Driving Turbulence with periodic boundary conditions
-----------------------------------------------------

Example description
^^^^^^^^^^^^^^^^^^^^

A 4 parsec box is initially filled with uniform magnetized self-gravitating gas. The initial conditions are such that the gas condenses from self-gravity rapidly. More appropriate initial conditions can be generated using the scaling relations found in `McKee et al. (2010, ApJ, 720, 1612) <https://ui.adsabs.harvard.edu/abs/2010ApJ...720.1612M/abstract>`_. Turbulence is driven over length scales comparable to the size of the box. ORION2 is run with MHD, self-gravity, and allowing for star particles to form while turbulence is driven. The code runs for one sound-crossing time of the domain. To keep the example simple, a coarse 128 grid is used in each dimension.

How to run the example
^^^^^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Drive`` subdirectory. You must first create the turbulence file by copying perturbation.py into the running directory and running::

  python perturbation.py --kmin=1 --kmax=2 --alpha=0 --size=128 --seed=2020 --f_solenoidal=0.6667

This will generate a zdrv.hdf5 file that you should keep in the same directory as the Orion2 file. Compile the test as described in the :ref:`sec-quickstart` instructions with::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of 10 output files, containing the solution at times from 0 to 2 crossing times.


What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Even with fixing the seed number for the random number generator, it is possible your exact numerical solution will not look exact the same as what is shown below. Bulk motions on the largest scales cascade down to form filamentary structures that collapse. It is unlikely that a sink particle might form this early in the collapse process.

A script that uses `yt <https://yt-project.org/>` to produce a kinetic energy power spectrum at half and one crossing time is included in the directory as ``Driving_Plot.py``; if you have ``yt`` installed, you can run the script via::

  python Driving_Plot.py

and it will produce a file ``Driving_Result.png``. An example of a successful test is shown below. Here the power spectrum rapidly approaches a ~ -5/3 powerlaw with a decrease in power at smaller scales. Continuing the simulation beyond this would eventually produce sink particles along the collapsing filaments:

.. image:: results/Driving_Result.png
   :width: 400
.. image:: results/Driving_Result2.png
   :width: 400
