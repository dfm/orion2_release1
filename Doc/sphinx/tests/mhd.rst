.. highlight:: rest

.. _ssec-tests-mhd:

Magnetohydrodynamics Tests
==========================

The tests in this section apply to the magnetohydrodynamics (MHD) module.  Three classic MHD tests are located in the ``Test_Prob/MHD`` directory and are briefly described below:

* :ref:`sssec-tests-mhd-blast`
* :ref:`sssec-tests-mhd-briowu`
* :ref:`sssec-tests-mhd-fieldloop`

.. _sssec-tests-mhd-blast:

MHD blast wave test
-------------------

Test description
^^^^^^^^^^^^^^^^

The detailed description and the results of this test can be found in Gardiner & Stone (2005) <https://ui.adsabs.harvard.edu/abs/2005JCoPh.205..509G/abstract>`_ and will not be repeated here.  In Gardiner & Stone (2005), the test was run on a unigrid.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup is in the ``Test_Prob/MHD/Blast`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions.  Specifically, ``cd`` into the test problem directory and do::

	python $ORION2_DIR/setup.py --with-orion2 --no-gui
	make -j8


This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

	mpirun -np X ./orion2

When the code finishes running, it will have produced a series of output files, ``data.0000.3d.hdf5`` through ``data.0020.3d.hdf5``.

Users can impose refinement grid in this test (e.g. 2 levels of refinement are used in this test, see orion2.ini) to see how AMR computation can improve the the resolution locally.



What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As set in orion2.ini, data files are dumped out every 0.001 time init. Users can see the time evolution of the blastwave in these files. Without magnetic fields, this test is a simple blastwave test.  In this test a uniform magnetic field is imposed diagonally.  With the magnetic pressure restraining the movement of the blastwave in the perpendicular direction, the shape of the wave becomes elongated.  The density, density with refinement meshes, magnetic field strength and total energy density are plotted in the following at 0.02 time unit using the visulization software VisIt (<https://wci.llnl.gov/simulation/computer-codes/visit>). 
Additionally, the test problem directory also contains two python scripts, Blast_Result_Eval.py and Blast_Result_Plot.py, that use `yt <https://yt-project.org/>` to analyze and vizualize the data. If you have ``yt`` installed, you can run the scripts via::

	python Blast_Result_Eval.py
	python Blast_Result_Plot.py

These scripts check the blast energy and produce a slice plot of the gas density at the final time. 

 

.. image:: results/mhdBlast.png

.. _sssec-tests-mhd-briowu:

Brio-Wu shock test
-------------------

Test description
^^^^^^^^^^^^^^^^

The Brio-Wu shock test is one of the typical MHD shock tests used for testing MHD algorithms and visualizing the compound wave.  A detailed description of the test and the results can be found in the original paper by Brio & Wu (1988) <https://ui.adsabs.harvard.edu/abs/1988JCoPh..75..400B/abstract>`_ or in Mignone et al. (2010) <https://ui.adsabs.harvard.edu/abs/2010JCoPh.229.5896M/abstract>`_.

How to run the test
^^^^^^^^^^^^^^^^^^^
The test setup is in the ``Test_Prob/MHD/Brio_Wu`` subdirectory. The compilation and running of the test are the same as in the above blast wave test.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The test results are dumped out after 0.1 time unit and are shown in the following figure produced by VisIt.  The right panel shows the cross section of the shock.  The width of the spikelike compound wave will become narrower with higher resolution. Users can vary the resolution (currently 800 in the test) defined in definitions.h to see this effect.
Additionally, the test problem directory also contains python scripts, BrioWu_Result_Eval.py and BrioWu_Result_Plot.py, that use yt to analyze and vizualize the data. These files compare the simulated result with the data contained in BrioWu_High_Res.dat.

.. image:: results/brio_wu.png
	   
.. _sssec-tests-mhd-fieldloop:

Field loop test
---------------

Test description
^^^^^^^^^^^^^^^^

This is a test created by Gardiner & Stone (2005) <https://ui.adsabs.harvard.edu/abs/2005JCoPh.205..509G/abstract>`_ to demonstrate a clear difference between a split and an unsplit MHD schemes.  A detailed description can be found in Gardiner & Stone (2005) and will not be repeated here.  In this test, users can choose different EMF averaging schemes in definitions.h.  The best result is found using the UCT_HLL scheme.  This scheme is not as fast as other averaging schemes, such as simple mathematical averaging, but the quality of the results are better and the scheme is recommended for ideal MHD simulations.

How to run the test
^^^^^^^^^^^^^^^^^^^
The test setup is in the ``Test_Prob/MHD/Field_Loop`` subdirectory. The compilation and running of the test are the same as in the above blast wave test.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the following figure, the initial and the final field loop after 2 repeated crossing of the computational domain (with periodic boudaries) diagonally are shown.  These figures are produced in VisIt. The loop remains stable.  More test reuslts on field loop using ORION2 can be found in Li et al. (2010) <https://ui.adsabs.harvard.edu/abs/2012ApJ...745..139L/abstract>`_.
Additionally, the test problem directory also contains the python script, FieldLoop_Result_Plot.py, that incorporates yt to vizualize the data. 

.. image:: results/fieldloop.png
	   
.. _sssec-tests-mhd-sod:


Users are encourage to try out differet EMF averaging schemes and Riemann solvers to compare the quality of results with or without AMR of all the above tests.  Users can even change the equation of state to isothermal, as some test examples in the subdirectory /isothermal.  In addition to the above tests, users can try out different MHD shock tests in Ryu & Jones (1995a) <https://ui.adsabs.harvard.edu/abs/1995ApJ...442..228R/abstract>`_ and (1995b) <https://ui.adsabs.harvard.edu/abs/1995ApJ...452..785R/abstract>`_.  One of the tests in Ryu & Jones is shown in the Figure 1 in Li et al. (2012) using ORION2.  The description and test results can be found in this paper, and users are encourage to use this problem as an exercise inn setting up a MHD problem with ORION2.

 

