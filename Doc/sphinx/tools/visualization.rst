.. highlight:: rest

.. _ssec-tools:

Visualization and Data Analysis
===============================

The ORION2 outputs, which are based on the Chombo data format, can be visualized and analyzed using the `yt <https://yt-project.org/>`_ python package. See the yt project website for extensive documentation. The python tools released with ORION2 are compatible with yt version 3.

Currently, yt only reads the hdf5 data output files. However, it is possible to also read the checkpoint files by adding the following to your yt script ::

  import yt.frontends.chombo.io
  yt.frontends.chombo.IOHandlerChomboHDF5._offset_string = 'cell_data:offsets=0'
  yt.frontends.chombo.IOHandlerChomboHDF5._data_string = 'cell_data:datatype=0'
