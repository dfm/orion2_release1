.. highlight:: rest

.. _sec-contributing:
 
Contributing 
======================

This is a guide for users and developers working with the orion2 radiation-magnetohydrodynamics code. Orion2 is built into the `chombo <https://commons.lbl.gov/display/chombo/Chombo+-+Software+for+Adaptive+Solutions+of+Partial+Differential+Equations>`_ adaptive mesh refinement (AMR) framework Different pieces of the code have been described in a variety of publications. The primary references are:

  * Magnetohydrodynamics (MHD): `Li et al. (2012, ApJ, 745, 139) <http://adsabs.harvard.edu/abs/2012ApJ...745..139L>`_ 
  * Gravity: `Miniati & Colella (2007), JCP, 227, 400) <https://ui.adsabs.harvard.edu/abs/2007JCoPh.227..400M/abstract>`_ and `Martin et al. (2008, JCP, 227, 1863) <https://ui.adsabs.harvard.edu/abs/2008JCoPh.227.1863M/abstract>`_
  * Flux-limited diffusion radiation: original implicit solution method by `Howell & Greenough (2003, JCP, 184, 53) <https://ui.adsabs.harvard.edu/abs/2003JCoPh.184...53H/abstract>`_, extension to radiation pressure by `Krumholz et al. (2007, ApJ, 667, 626) <http://adsabs.harvard.edu/abs/2007ApJ...667..626K>`_, improved solution method using pseudo-transient continuation by `Shestakov & Offner (2008, JCP, 227, 2154) <https://ui.adsabs.harvard.edu/abs/2008JCoPh.227.2154S/abstract>`_
  * Sink/star particles: original sink particle method by `Krumholz, Klein, & McKee (2004, ApJ, 611, 399) <http://adsabs.harvard.edu/abs/2004ApJ...611..399K>`_, star particle model described in `Offner et al. (2009, ApJ, 703, 131) <http://adsabs.harvard.edu/abs/2009ApJ...703..131O>`_, stellar wind implementation from `Cunningham et al. (2011, ApJ, 740, 104) <http://adsabs.harvard.edu/abs/2011ApJ...740..107C>`_ extension from HD to MHD described in `Lee et al. (2014, ApJ, 783, 50) <http://adsabs.harvard.edu/abs/2014ApJ...783...50L>`_
  * Hybrid ray-moment method for radiation by `Rosen et al. (2017, JCP, 330, 924) <http://adsabs.harvard.edu/abs/2017JCoPh.330..924R>`_

More details on each of these parts of the code and how to use them are provided in :ref:`sec-physics-modules`.

Orion is managed by the following team of active users: Richard Klein (UC Berkeley/LLNL), Christopher McKee (UC Berkeley), Pak Shing Li (UC Berkeley), Mark Krumholz (ANU), Stella Offner (UT Austin), Andrew Cunningham (LLNL), Aaron Lee (St. Mary's College), Anna Rosen (Harvard/CfA), Brandt Gaches (U of Cologne) and Aaron Skinner (UC Berkeley).

A complete list of developers and contributors: Richard Klein, Christopher McKee, Kelly Truelove, Robert Fisher, Mark Krumholz, Rob Crockett, Pak Shing Li, Stella Offner, Andrew Cunningham, Charles Hansen, Andrew Myers, Aaron Lee, Drummond Fielding, Anna Rosen, Brandt Gaches and Aaron Skinner.

To join the Orion users group email list subscribe to: [orionusers@emaillist.com]

