.. highlight:: rest

.. _ssec-sinks:

Sink/Star Particles
=====================


Sink particles represent point masses that interact with the gas via
gravity and by accreting from it, but that do not feel or respond to
pressure forces. They may also have an attached subgrid model for
stellar evolution that calculates the luminosity and wind output of
stars and injects these back into the computational grid.

Equations solved
^^^^^^^^^^^^^^^^

The equations describing sink particle evolution are:

.. math:: \frac{d}{dt} M_i & = \dot{M}_i \\
          \frac{d}{dt} \mathbf{x}_i & = \frac{\mathbf{p}_i}{M_i} \\
          \frac{d}{dt} \mathbf{p}_i & = -M_i \nabla \phi + \dot{\mathbf{p}}_i \\

where :math:`M_i`, :math:`\mathbf{x}_i`, and :math:`\mathbf{p}_i` are
the mass, position, and momentum of sink particle `i`, and the source
terms :math:`\dot{M}_i` and :math:`\dot{\mathbf{p}}_i` represent the
rates of transfer from gas to sink particles; equal and opposite terms
appear as sources in the gas equations. The transfer rates are
determined as described in  `Krumholz, Klein, & McKee (2004, ApJ,
611, 399) <http://adsabs.harvard.edu/abs/2004ApJ...611..399K>`_ (for
the hydrodynamic case) and `Lee et al. (2014, ApJ, 783, 50)
<http://adsabs.harvard.edu/abs/2014ApJ...783...50L>`_ (when magnetic
fields are present).

Star particles and inject radiation and winds into the gas also
introduce additional source terms. The wind source terms are as
described in `Cunningham et al. (2011, ApJ, 740, 104)
<http://adsabs.harvard.edu/abs/2011ApJ...740..107C>`_. The functional
form used for the radiation source terms depends on the radiative
transfer method being used -- see :ref:`ssec-fld` and
:ref:`ssec-harm2`.

A new angular momentum treatment for star particles was developed as described in `Fielding et al. (2015, MNRAS, 450, 3306) 
<https://ui.adsabs.harvard.edu/abs/2013MNRAS.450.3306F/abstract>`_. This model, rather than simply add the 
angular momentum of the accreting gas to the sink particle, adopts a sub-grid model to represent the transport
of angular momentum from the outer accretion radius (4 :math:`\Delta x`) to the stellar radius. This method only applies when star
particles are in use, since sink particles do not track the effective stellar radius. 


Compile-time options
^^^^^^^^^^^^^^^^^^^^

Sink particles can be enabled or disabled at compile-time by including
the option::

  #define SINK_PARTICLES   YES

or::

  #define SINK_PARTICLES   NO

in ``definitions.h``; see :ref:`sec-compiling`. Star particles can be
enabled or disabled at compile-time via the options::

  #define STAR_PARTICLES   YES

or::

  #define STAR_PARTICLES   NO

Note that you must enable sink particles in order to enable star
particles; attempting to enable star particles without sink particles
will result in a compilation error.


Run-time options
^^^^^^^^^^^^^^^^

The following parameters in ``orion2.ini`` affect the behavior of sink
particles:

* ``sink.radius``: when sink particles accrete, they remove mass and
  momentum from a control zone around themselves. The parameter
  ``sink.radius`` specifies the size of the control zone in units of
  cell spacing on the finest level. Recommended value: 8.
* ``sink.ntag``: regions within a certain distance of sink particles
  will be tagged for refinement to higher levels (see
  :ref:`sec-amr`). The parameter ``sink.ntag`` specifies the size of
  the tagged region in units of cell spacings on each level. This can
  be either a single number, in which case the same tagging radius
  is used on all levels, or a list of numbers with as many entries as
  AMR levels in the calculation. This `must` be larger than
  ``sink.radius`` on the finest level. Recommended value: 8.
* ``sink.soften``: sink particle gravitational forces are softened so
  that the graviational acceleration at a distance :math:`r` from a
  sink of mass :math:`M` is :math:`GM / [r^2 + (s\,dx)^2]`, where
  :math:`dx` is the finest level cell spacing, and ``sink.soften``
  gives the value of `s`. Recommended value: 1.0.
* ``sink.use_sink_timestep``: if set to 1, the time step is reduced if
  necessary to ensure that sink particles move no more than `C` cell
  lengths per time step, where `C` is the CFL number.

The following parameters affect the behavior of star particles:

* ``star.use_wind``: if set to 1, protostellar outflows are enabled;
  if set to 0 they are turned off
* ``star.vw_fkep``: the wind velocity is set to a fraction of the
  Kepler velocity at the stellar surface; this parameter specifies the
  ratio of wind velocity to Kepler velocity.
* ``star.vw_max``: the wind velocity will be capped to the specified
  value
* ``sink.angmom_method``: if set to 1, stars will use the sub-grid angular momentum method described in `Fielding et al. (2015, MNRAS, 450, 3306) <https://ui.adsabs.harvard.edu/abs/2013MNRAS.450.3306F/abstract>`_.

  
Sink particle gravity
^^^^^^^^^^^^^^^^^^^^^

Gravitational interaction between different sink particles, and between sink particles and the gas, can be computed using two different schemes: direct force computation, and particle-mesh. Direct force is the default option.

Direct force
------------

In direct force computation, the gravitational accelerations between each pair of particles, and between every particle and cell, are computed directly as :math:`\vec{a}=G m \hat{r}/r^2`, where :math:`m` is the mass of the particle or cell providing the acceleration and :math:`r` is the distance to this particle or cell. For cells close to sink particles, the acceleration is (crudely) integrated over the cell volume. Particle trajectories are integrated using a Runge-Kutta scheme. Full details of how the force computation and orbit integration are carried out can be found in `Krumholz, Klein, & McKee (2004, ApJ, 611, 399) <http://adsabs.harvard.edu/abs/2004ApJ...611..399K>`_.

The direct force scheme has the advantage that it maintains particle orbits very accurately, even for particles that are separated by much less hydrodynamic cell. For this reason, it is the preferred method in simulations where the expected number of sink particles is no more than a few hundred. However, it becomes impractically-slow for larger numbers of particles.


Particle-mesh
-------------

The alternative choice is particle-mesh gravity, which can be enabled by setting the compile-time option::

  #define CIC              YES

When this option is selected, particle masses are added to the same grid used to compute the gravitational acceleration of the gas using a cloud-in-cell (CIC) scheme, and thus the potential and accelerations due to particles are computed simultaneously with those due to gas -- see :ref:`ssec-gravity` for details. Particle accelerations are similarly computed directly from the potential retuend by the gravity solution. This is less accurate than the direct force scheme for particles in tight orbits, but the computational cost is generally negligible in comparison to the overall gravity solve for any plausible number of sink particles. The CIC scheme is described in detail in `Feng & Krumholz (2014, Nature, 513, 523) <https://ui.adsabs.harvard.edu/abs/2014Natur.513..523F/abstract>`_.

  
Test Problems
^^^^^^^^^^^^^^^^^^^^

The following test problems are available in Test_Prob/Sink:

* AngularMomentumMerger: Verifies that two merging sink particles have the correct final angular momentum.

* BinaryOrbit: Verifies the orbital integration for two binary stars.

* BinaryOrbitCIC: Verifies the orbital intergration for two binary stars using CIC gravity (not updated).

* Bondi: Compares the sink particle accretion to the classic Bondi solution.

* BondiSubgrid: Compares the sink particle accretion to the Bondi-Hoyle solution for different resolutions.

* IsothermalSphere: Compares the sink properties and gas  solution to the analytical prediction for the  Shu 1977 isothermal sphere model.

* UniformCloud: Setup to follow the collapse of a cloud of uniform density.

The following test problems are available in Test_Prob/Star:

* AngularMomentumAccretion: Compares the angular momentum accreted for a star in a stationary, sub-sonic and supersonic flow. 

* AngularMomentumMerger: Verifies that two merging star particles have the correct final angular momentum. Identical setup to AngularMomentumMerger test problem for sink particles.

* Bondi: Compares the sink particle accretion to the classic Bondi solution. Identical setup to Bondi test problem for sink particles.

