.. highlight:: rest

.. _sec-infrastructure:
	       
Infrastructure
==================================

This section describes the AMR structure, the various run-time
parameters that control it, as well as the data output format and content.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   infrastructure/amr
   infrastructure/output

