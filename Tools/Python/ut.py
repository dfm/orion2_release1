import os
import shutil
import string
import sys
import time
import make
import menu
import screen1

#######################################################
def no_gui(orion2_dir):
#
#
#
#
#
#
#######################################################

  work_dir = os.getcwd()
  additional_files  = ['']
  additional_flags  = ['']
  additional_cppflags  = ['']
  orion2_path        = ['']
  makefile_template = []

  if (not os.path.exists(work_dir+'/init.c')):
    shutil.copy(orion2_dir+'/Src/Templates/init.c',work_dir+'/init.c')
    
  if (not os.path.exists(work_dir+'/orion2.ini') and (WITH_CHOMBO == 0)):
    shutil.copy(orion2_dir+'/Src/Templates/orion2.ini',work_dir+'/orion2.ini')
    
  make.problem(work_dir, orion2_path, orion2_dir, additional_files, 
               additional_flags, additional_cppflags, makefile_template, 1)
  make.makefile(work_dir, orion2_path, orion2_dir, additional_files, 
                additional_flags, additional_cppflags, makefile_template, 1)


#######################################################
def main_menu(orion2_dir):
#
#
#
#
#
#
#######################################################

# check if setup has been invoked with --with-chombo

  WITH_CHOMBO = 0
  for x in sys.argv:
    if (x == "--with-chombo"):
      WITH_CHOMBO = 1

  what   = 0

#  work_dir = SELECT_WORKING_DIR(orion2_dir)
  work_dir = os.getcwd()
  options = ['Setup problem', 'Change makefile', 
             'Auto-update','Save Setup','Change working dir','Quit']

  additional_files  = ['']
  additional_flags  = ['']
  additional_cppflags  = ['']
  orion2_path        = ['']
  makefile_template = []
  what              = ''
  
  while (what != options[5]):
  
#    what = menu.select0(options,">> Python ORION2 setup (08/2006)\n\n )
    what = menu.select0(options,"Python setup (08/2006)", "Working dir: "+work_dir+"\nORION2 dir  : "+orion2_dir)

    print('what = ', what)

    if   (what == options[0]):   # build problem 

      if (not os.path.exists(work_dir+'/init.c')):
        shutil.copy(orion2_dir+'/Src/Templates/init.c',work_dir+'/init.c')
        
      if (not os.path.exists(work_dir+'/orion2.ini') and (WITH_CHOMBO == 0)):
        shutil.copy(orion2_dir+'/Src/Templates/orion2.ini',work_dir+'/orion2.ini')

      make.problem(work_dir, orion2_path, orion2_dir, additional_files, 
                   additional_flags, additional_cppflags, makefile_template, 0)
      make.makefile(work_dir, orion2_path, orion2_dir, additional_files, 
                    additional_flags, additional_cppflags, makefile_template, 1)

    elif (what == options[1]):   # makefile
    
      # --------------------------------------------------------
      #   Read problem definitions again;
      #   Useful when one wants to change the makefile 
      #   without necessarily going to build/change problem.
      # --------------------------------------------------------
      
      make.problem(work_dir, orion2_path, orion2_dir, additional_files,  
                   additional_flags, additional_cppflags, makefile_template, 1)
      make.makefile(work_dir, orion2_path, orion2_dir, additional_files, 
                    additional_flags, additional_cppflags, makefile_template, 0)
      screen1.dumpclear (" > makefile has been changed\n")
      time.sleep (1)

    elif (what == options[2]):    # automatic update

      menu.PROMPT_MESSAGE('Press Enter to Update '+work_dir)
      make.problem (work_dir, orion2_path, orion2_dir, additional_files, 
                    additional_flags, additional_cppflags, makefile_template, 1)
      make.makefile(work_dir, orion2_path, orion2_dir, additional_files, 
                    additional_flags, additional_cppflags, makefile_template, 1)
      time.sleep(1)
      
    elif (what == options[3]):   # save setup
  
      # ------------------------------------
      #  get the path basename and make 
      #  a tar archive 
      # ------------------------------------

      os.chdir(work_dir)
      default_name = os.path.basename(work_dir)
      os.system("clear")
      setup_name = input(" > Archive name ("+default_name+"): ")
      complete_backup = 0
      scrh       = input(" > Backup source files ? (n): ")
      if (setup_name == ''): 
        setup_name = default_name
 
      if (scrh == "y"): 
        complete_backup = 1

      setup_name = setup_name+".tar"
      screen1.dump (" > Creating setup ...\n")
      screen1.dump ("-----------------------------------------------\n")
      screen1.dump ("Archive "+setup_name+".gz contains: \n\n")
 
      ifail = os.system ("tar cvf "+setup_name+" *.c *.ini *.h")

      # -------------------------------
      #  back up source tree
      # -------------------------------

      if (complete_backup):
        ifail = ifail or os.system("tar rvf "+setup_name+" --directory="+orion2_dir+"/   Src/ ")
        ifail = ifail or os.system("tar rvf "+setup_name+" --directory="+orion2_dir+"/   Config/")
        ifail = ifail or os.system("tar rvf "+setup_name+" makefile")
          
      screen1.dump ("-----------------------------------------------\n")
      ifail = ifail or os.system ("gzip -f "+setup_name)
      if (ifail == 0):
        screen1.dump (" > "+setup_name+".gz successfully created\n")
      else:
        screen1.dump (" ! Error creating "+setup_name+".gz\n")
 
      screen1.dump(" > Press enter to continue...\n")
      scrh = input()
      
    elif (what == options[4]):    # change work dir
      work_dir = SELECT_WORKING_DIR(orion2_dir)
      os.chdir(work_dir)

  print ("\n\n Bye.")
  sys.exit()
    
###############################################################
def SELECT_WORKING_DIR(orion2_dir):
#
#
#
###############################################################

 what        = 0
 current_dir = os.getcwd();
 current_dir = os.path.abspath(current_dir)
 while (what != 'q'):

#  ut.clear('INITIALIZING MENU')

#        WORKING DIR SELECTION:
#       Get current directories
#

   dir_list =[f for f in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir, f))]
  

#
# Remove ORION2 directories from the previous list,
# so no harm can be done
#
   try:
     dir_list.remove('Src')
     dir_list.remove('Config')
     dir_list.remove('Doc')
     dir_list.remove('Lib')
     dir_list.remove('Tools')
   except ValueError:
     pass

#
# Remove directories beginning with a '.'
#

   scrh = []
   for x in dir_list:
     if x[0] == ".": scrh.append(x)

   for x in scrh:
     dir_list.remove(x)

#  if nlevel==2:
#    print dir_list
#    sys.exit()  
#
# Now replace the relative path with the absolute path
#

   for x in dir_list:
     i = dir_list.index(x)
     dir_list.remove(x)
     x = os.path.abspath(x)
     dir_list.insert(i,x)

#
#  Add the option 'NEW_DIR'
#

   dir_list.insert(0,os.getcwd())
   dir_list.append('--> CREATE NEW DIR <--')

   what = menu.dir_browse(dir_list,"Choose working directory:")

   if what[0] == 'SELECT_THIS_DIR':
     work_dir = dir_list[what[1]]
     if  work_dir == dir_list[-1]:
       work_dir = input("Enter a valid working directory > ")
#
#   want to exit ?
#
       if (work_dir == 'q' or work_dir == 'exit' or work_dir == 'quit'):
         sys.exit()
        
       elif (os.path.exists(work_dir)):
#
#   typed an already existing path ?
#
         print ("Directory '"+work_dir+"' already exists")
         sys.exit()

       else:
#
#  Ok, now you can create a new directory
#
         os.makedirs(work_dir)
         shutil.copy(orion2_dir+'/Src/Templates/init.c',work_dir+'/init.c')
         shutil.copy(orion2_dir+'/Src/Templates/orion2.ini',work_dir+'/orion2.ini')

     return work_dir
             
   elif what[0] == 'DOWN_ONE_LEVEL':

     new_dir = dir_list[what[1]]
     os.chdir(new_dir)
     current_dir = os.getcwd()
    
   elif what[0] == 'UP_ONE_LEVEL' or what==dir_list[0]:

     os.chdir('../')
     current_dir = os.getcwd()

 sys.exit()    

