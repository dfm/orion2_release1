import os
import os.path
import sys
import file
import time
import file
import string
import urllib

def check(orion2_dir):

 work_dir = os.getcwd() 
 print ("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
 print ("\n >> Inspecting system configuration << \n")

# log_file     = orion2_dir+'/system.conf'
 log_file     = work_dir+'/sysconf.out'
 config_dir   = orion2_dir+'/Config/'
 python_dir   = orion2_dir+'/Tools/Python'
 bintools_dir = orion2_dir+'/Tools/bin_tools'
 param_file   = orion2_dir+'/Src/orion2.h'

 os.chdir (python_dir)
 log = []

 #  ---------------------
 #    get user name
 #  ---------------------

 try:
   user = os.getlogin()
 except OSError:
   user = 'unknown'

 #  ---------------------
 #    get system spec.
 #  ---------------------

 PLATFORM = os.uname()
 
 #  ---------------------
 #   find current 
 #   version of ORION2
 #  ---------------------

 scrh = file.word_find (param_file,"ORION2_VERSION")
 ipos = scrh[0]
 scrh = file.read_lines (param_file, ipos, ipos + 1)
 vers = scrh[0].split()
 vers = vers[2]

 #  ----------------------------
 #    check for online updates
 #  ----------------------------
 
# print " > Checking for updates...\n"
# try:
#   urllib.urlretrieve("http://plutocode.to.astro.it/updates.txt","updates.txt")
#   scrh = file.word_find ("updates.txt","release")
#   ipos = scrh[0]
#   scrh = file.read_lines ("updates.txt", ipos, ipos + 1)
#   rels = string.split(scrh[0])
#   if (rels[1] != vers):
#     print "   ******************************************************* "
#     print "    A new version of PLUTO ("+rels[1]+") is available at"
#     print "    http://plutocode.to.astro.it."
#     print "   *******************************************************\n"
#     scrh = raw_input("  > Press enter to continue.")
#     os.chdir(work_dir)   
# except:
#   print " ! Connection not available\n"
 
# print "User.......................",user
# print "System name................",PLATFORM[0]
# print "Node name..................",PLATFORM[1]
# print "Release....................",PLATFORM[2]
# print "Arch.......................",PLATFORM[4]
# print "Byteorder..................",sys.byteorder
# print "Version....................",PLATFORM[3]
# print "Working_dir................",work_dir
# print "PLUTO main dir.............",orion2_dir
# print "PLUTO version..............",vers

# --------------------------------------
#  Check for gcc or another c compiler
# --------------------------------------

 compiler_list = ['gcc','cc','gcc2']
 COMPILER_NAME = ''
 
 for x in compiler_list:
   if (CHECK_FOR(x) == 'YES'):
     print("C Compiler................. "+x)
     COMPILER_NAME = x
     break

 if (COMPILER_NAME == ''):
   print(" ! Can not find a C compiler       !")

# -----------------
#  check for mpi 
# -----------------

 mpi_compiler_list = ['mpicc','mpiCC','mpcc_r','hcc','mpcc']
 MPI_COMPILER_NAME = ''

 for x in mpi_compiler_list:
   if (CHECK_FOR(x) == 'YES'):
     print("MPI Compiler .............. "+x)
     MPI_COMPILER_NAME = x
     break
   
 if (MPI_COMPILER_NAME == ''):
   print("MPI Compiler............... NOT FOUND")

 
# ------------------
#  check for libpng
# ------------------

 if (os.system ("gcc -lm -lpng -v test.c -o test 2> /dev/null") == 0):
   HAVE_LIBPNG = 'YES'
   print("Check for libpng........... OK")
 else:
   HAVE_LIBPNG = 'NO'
   print("Check for libpng........... NOT FOUND")

# ---------------------------------------------------
#  Build log list, that will be compared to the 
#  sysconf.out file.
# ---------------------------------------------------

 log.append("USER           = "+user+'\n')
 log.append("WORKING_DIR    = "+work_dir+"\n")
 log.append("SYSTEM_NAME    = "+PLATFORM[0]+"\n")
 log.append("NODE_NAME      = "+PLATFORM[1]+"\n")
 log.append("RELEASE        = "+PLATFORM[2]+"\n")
 log.append("ARCH           = "+PLATFORM[4]+"\n")
 log.append("BYTE_ORDER     = "+sys.byteorder+"\n")
 log.append("VERSION        = "+PLATFORM[3]+"\n")
 log.append("ORION2_DIR     = "+orion2_dir+'\n')
 log.append("ORION2_VERSION  = "+vers+'\n')
 log.append("C_COMPILER     = "+COMPILER_NAME+'\n')
 log.append("MPI_C_COMPILER = "+MPI_COMPILER_NAME+'\n')
 log.append("LIBPNG         = "+HAVE_LIBPNG+'\n')

# ------------------------------------------------
#  Compare the list 'log' with the file log_file;
#  
#   - if they match, no update is necessary, 
#               ==> return to main menu
#
#   - if they do not match or if log_file does not
#     exists, create a new one
#
# ------------------------------------------------

 if (os.path.exists(log_file)):  
   scrh = file.read_lines(log_file,0,128)
   if (scrh[0:] == log[0:]):
     print("\n >> ok")
     os.chdir (work_dir)
     return
   else:
     print("\n > System configuration file is not up to date. Updating...")
 else:
   print("\n > System configuration file not found, creating one...")

 file.create_file(log_file,log)

# ------------
#  Make Tools
# ------------

# print (" > Making binary tools in "+bintools_dir+"...") 
# os.chdir(bintools_dir)
# os.system('gmake -s clean')
# os.system('gmake -s dbl2flt')
# os.system('gmake -s bin2ascii')
# if (HAVE_LIBPNG == 'YES'):
#   os.system('gmake -s bin2png')


# ---------------------------
#   Add important info here
# ---------------------------

# scrh = raw_input(" > Press enter to continue.")
 os.chdir(work_dir)
 return


######################################################
def CHECK_FOR (file):

#
#  find whether file can be found in 
#  the user's path
#
#
#######################################################

 # 
 # determine user's path
 #
 
 scrh = os.getenv('PATH')
 path = scrh.split(':')

 #
 # search file 
 #
 
 have_file = "NO"
 for x in path:
   if (os.path.exists(x+"/"+file)):
     have_file = "YES"
     
 return have_file
  
