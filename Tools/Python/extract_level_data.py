import numpy as np
import h5py

## A number of routines for extracting a region from a orion2 checkpoint file 
## and converting it into hdf5 format which can be read with orion2 using
## READHDF5ICS definitions.h option
##
## author: Chalence Safranek-Shrader

## extract: main routine for extracting data
## 
## fname = orion2 checkpoint file name
## level = which level to extract from (generally the highest one)
## dims = extraction dimensions
## indices = centerpoint indices of extraction in units of the full
##           dimensions of level
## 
## returns numpy arrays representing data: 
##         dens,velx,vely,velz,pressure,Bxc,Byc,Bzc
def extract(fname,level,dims,indices,checkpoint=True,faceBfields=True):

    verbose = True
    # indices of center of zoom region
    ix = indices[0]
    iy = indices[1]
    iz = indices[2]

    # what level are we extracting?
    level = '/level_'+str(level)

    print " "
    print "extract: reading in ", fname
    print "level string = ", level
    print "dims = ", dims
    print "indices = ", indices

    fin = h5py.File(fname,'r')

    key = '/'
    

    #for i in range(0,10):
    #    print fin[key].attrs['component_'+str(i)]

    dens = np.zeros([dims]*3)
    velx = np.zeros([dims]*3)
    vely = np.zeros([dims]*3)
    velz = np.zeros([dims]*3)
    etot = np.zeros([dims]*3)
    if not checkpoint:
        Bxc = np.zeros([dims]*3)
        Byc = np.zeros([dims]*3)
        Bzc = np.zeros([dims]*3)
        gpot = np.zeros([dims]*3)
    
    dname = 'cell_data'
    if not checkpoint:
        dname = 'data'
    

    boxes = fin[level]['boxes'].value
    offsets = fin[level][dname+':offsets=0'].value

    nvar = 8
    if not checkpoint:
        nvar = 9
    
    for ibox in range(0,len(offsets)-1):
        box = boxes[ibox]
        nd = (offsets[ibox+1] -  offsets[ibox])/nvar

        xl = box[0] + dims/2 - ix
        yl = box[1] + dims/2 - iy
        zl = box[2] + dims/2 - iz
        xh = box[3] + dims/2 - ix
        yh = box[4] + dims/2 - iy 
        zh = box[5] + dims/2 - iz
        nz = zh-zl+1
        ny = yh-yl+1
        nx = xh-xl+1


        azl = 0; azh = nz
        ayl = 0; ayh = ny
        axl = 0; axh = nx

        if zl < 0:
            azl -= zl
            zl -= zl

        if yl < 0:
            ayl -= yl
            yl -= yl

        if xl < 0:
            axl -= xl
            xl -= xl

        if zh+1 > dims:
            azh -= zh + 1 - dims
            zh = dims-1

        if yh+1 > dims:
            ayh -= yh + 1 - dims
            yh = dims-1

        if xh+1 > dims:
            axh -= xh + 1 - dims
            xh = dims-1

        if zh < 0: continue
        if xh < 0: continue
        if yh < 0: continue
        if zl+1 >= dims: continue
        if xl+1 >= dims: continue
        if yl+1 >= dims: continue

        bd = fin[level][dname+':datatype=0'][offsets[ibox]:offsets[ibox]+nd] 
        #print "bd shape", bd.shape, nz,ny, nx
        #print "nd = ", nd
        a = bd.reshape((nz,ny,nx))
        dens[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh]

        bd = fin[level][dname+':datatype=0'][offsets[ibox]+nd:offsets[ibox]+2*nd] 
        a = bd.reshape((nz,ny,nx))
        velx[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] / dens[zl:zh+1,yl:yh+1,xl:xh+1]

        bd = fin[level][dname+':datatype=0'][offsets[ibox]+2*nd:offsets[ibox]+3*nd] 
        a = bd.reshape((nz,ny,nx))
        vely[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] / dens[zl:zh+1,yl:yh+1,xl:xh+1]

        bd = fin[level][dname+':datatype=0'][offsets[ibox]+3*nd:offsets[ibox]+4*nd] 
        a = bd.reshape((nz,ny,nx))
        velz[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] / dens[zl:zh+1,yl:yh+1,xl:xh+1]

        if checkpoint:
            bd = fin[level][dname+':datatype=0'][offsets[ibox]+7*nd:offsets[ibox]+8*nd] 
            a = bd.reshape((nz,ny,nx))
            etot[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 
        else:
            bd = fin[level][dname+':datatype=0'][offsets[ibox]+4*nd:offsets[ibox]+5*nd] 
            a = bd.reshape((nz,ny,nx))
            Bxc[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 

            bd = fin[level][dname+':datatype=0'][offsets[ibox]+5*nd:offsets[ibox]+6*nd] 
            a = bd.reshape((nz,ny,nx))
            Byc[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 

            bd = fin[level][dname+':datatype=0'][offsets[ibox]+6*nd:offsets[ibox]+7*nd] 
            a = bd.reshape((nz,ny,nx))
            Bzc[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 


            bd = fin[level][dname+':datatype=0'][offsets[ibox]+7*nd:offsets[ibox]+8*nd] 
            a = bd.reshape((nz,ny,nx))
            etot[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 


            bd = fin[level][dname+':datatype=0'][offsets[ibox]+8*nd:offsets[ibox]+9*nd] 
            a = bd.reshape((nz,ny,nx))
            gpot[zl:zh+1,yl:yh+1,xl:xh+1] = a[azl:azh,ayl:ayh,axl:axh] 



    if verbose: 
        print "min of density = ", np.min(dens)
        print "max of density = ", np.max(dens)
    assert np.min(dens) > 0.0 

    if checkpoint:
        boxes = fin[level]['boxes'].value
        offsets = fin[level]['face_data:offsets=0'].value
        size = len(fin[level]['face_data:datatype=0'])
        bxf = np.zeros([dims,dims,dims+1])
        byf = np.zeros([dims,dims+1,dims])
        bzf = np.zeros([dims+1,dims,dims])
        bfields = [bxf,byf,bzf]

        ndim = 3
        for ibox in range(0,len(offsets)-1):
            box = boxes[ibox]

            xl = box[0] + dims/2 - ix
            yl = box[1] + dims/2 - iy
            zl = box[2] + dims/2 - iz
            xh = box[3] + dims/2 - ix
            yh = box[4] + dims/2 - iy 
            zh = box[5] + dims/2 - iz

            #print xl, yl, zl, xh, yh, zh
            if zh < 0: continue
            if xh < 0: continue
            if yh < 0: continue
            if zl+1 >= dims: continue
            if xl+1 >= dims: continue
            if yl+1 >= dims: continue
            #if zl < 0: continue
            #if yl < 0: continue
            #if xl < 0: continue

            nd = (offsets[ibox+1] - offsets[ibox])/3
            ndx = (zh-zl+1)*(yh-yl+1)*(xh-xl+2)
            ndy = (zh-zl+1)*(yh-yl+2)*(xh-xl+1)
            ndz = (zh-zl+2)*(yh-yl+1)*(xh-xl+1)
            bdx = fin[level]['face_data:datatype=0'][offsets[ibox]:offsets[ibox]+ndx] 
            bdy = fin[level]['face_data:datatype=0'][offsets[ibox]+ndx:offsets[ibox]+ndx+ndy] 
            bdz = fin[level]['face_data:datatype=0'][offsets[ibox]+ndy+ndx:offsets[ibox]+ndx+ndy+ndz] 

            # bx:
            try:
                nz = zh-zl+1; ny = yh-yl+1; nx = xh-xl+2
                nzl = 0; nyl = 0; nxl = 0
                shape = (zh-zl+1,yh-yl+1,xh-xl+2)
                a = bdx.reshape(shape)

                # keep original limits in tact
                zll = zl
                zhh = zh+1
                yll = yl
                yhh = yh+1
                xll = xl
                xhh = xh+2


                if zl < 0:
                    zll = 0
                    nzl -= zl

                if yl < 0:
                    yll = 0
                    nyl -= yl

                if xl < 0:
                    xll = 0
                    nxl -= xl

                if zhh > bxf.shape[0]:
                    nz -= zhh - bxf.shape[0]
                    zhh = bxf.shape[0]

                if yhh > bxf.shape[1]:
                    ny -= yhh - bxf.shape[1]
                    yhh = bxf.shape[1]

                if xhh > bxf.shape[2]:
                    nx -= xhh - bxf.shape[2]
                    xhh = bxf.shape[2]

                bxf[zll:zhh,yll:yhh,xll:xhh] = a[nzl:nz,nyl:ny,nxl:nx]
            except: 
                print "error in x"
                pass


            # by:
            try:
                nz = zh-zl+1; ny = yh-yl+2; nx = xh-xl+1
                nzl = 0; nyl = 0; nxl = 0
                shape = (zh-zl+1,yh-yl+2,xh-xl+1)
                a = bdy.reshape(shape)

                # keep original limits in tact
                zll = zl
                zhh = zh+1
                yll = yl
                yhh = yh+2
                xll = xl
                xhh = xh+1


                if zl < 0:
                    zll = 0
                    nzl -= zl

                if yl < 0:
                    yll = 0
                    nyl -= yl

                if xl < 0:
                    xll = 0
                    nxl -= xl

                if zhh > byf.shape[0]:
                    nz -= zhh - byf.shape[0]
                    zhh = byf.shape[0]

                if yhh > byf.shape[1]:
                    ny -= yhh - byf.shape[1]
                    yhh = byf.shape[1]

                if xhh > byf.shape[2]:
                    nx -= xhh - byf.shape[2]
                    xhh = byf.shape[2]

                byf[zll:zhh,yll:yhh,xll:xhh] = a[nzl:nz,nyl:ny,nxl:nx]
            except: 
                print "error in y"
                pass


            # bz:
            try:
                nz = zh-zl+2; ny = yh-yl+1; nx = xh-xl+1
                nzl = 0; nyl = 0; nxl = 0
                shape = (zh-zl+2,yh-yl+1,xh-xl+1)
                a = bdz.reshape(shape)

                # keep original limits in tact
                zll = zl
                zhh = zh+2
                yll = yl
                yhh = yh+1
                xll = xl
                xhh = xh+1


                if zl < 0:
                    zll = 0
                    nzl -= zl

                if yl < 0:
                    yll = 0
                    nyl -= yl

                if xl < 0:
                    xll = 0
                    nxl -= xl

                if zhh > bzf.shape[0]:
                    nz -= zhh - bzf.shape[0]
                    zhh = bzf.shape[0]

                if yhh > bzf.shape[1]:
                    ny -= yhh - bzf.shape[1]
                    yhh = bzf.shape[1]

                if xhh > bzf.shape[2]:
                    nx -= xhh - bzf.shape[2]
                    xhh = bzf.shape[2]

                bzf[zll:zhh,yll:yhh,xll:xhh] = a[nzl:nz,nyl:ny,nxl:nx]
            except: 
                print "error in z"
                pass 


        Bxc=0.5*(bxf[:,:,:-1]+bxf[:,:,1:])
        Byc=0.5*(byf[:,:-1,:]+byf[:,1:,:])
        Bzc=0.5*(bzf[:-1,:,:]+bzf[1:,:,:])

        checkDivB = False
        if checkDivB:
            print "min of Bxc", np.min(abs(Bxc))
            print "min of Byc", np.min(abs(Byc))
            print "min of Bzc", np.min(abs(Bzc))
            divb = np.zeros(dens.shape)
            for i in range(0,dims-1):
                for j in range(0,dims-1):
                    for k in range(0,dims-1):
                        divb[k,j,i] = bxf[k,j,i+1] - bxf[k,j,i] + \
                                byf[k,j+1,i] - byf[k,j,i] + \
                                bzf[k+1,j,i] - bzf[k,j,i]  

            print "bxc average", np.average(abs(Bxc))
            print "max divb", np.max(abs(divb))
            del divb


    EK = 0.5 * dens * (velx**2 + vely**2 + velz**2) 
    EB = 0.5 * (Bxc**2 + Byc**2 + Bzc**2)
    eth = etot - EB - EK
    if verbose: print "min of eth and etot", np.min(eth), np.min(etot)

    gamma = 5.0/3.0
    mh = 1.67e-24
    kb = 1.38e-16
    yhe = 0.08
    fac = (1.0 + yhe) / (1.0 + 4.0 * yhe)
    pressure = (gamma-1.0) * eth 
    temperature = pressure * mh / dens / kb / fac
    if verbose:   print "min of temperature", np.min(temperature)

    vxcom = np.sum(velx*dens)/np.sum(dens)
    vycom = np.sum(vely*dens)/np.sum(dens)
    vzcom = np.sum(velz*dens)/np.sum(dens)
    if verbose:  print "com vels = ", vxcom, vycom, vzcom
    velx -= vxcom
    vely -= vycom
    velz -= vzcom
    if verbose:   print "dens shape = ", dens.shape
    if verbose:   print "mean square velocity", np.sqrt(np.average(velx*velx+vely*vely+velz*velz))
    
    if faceBfields:
        return dens,velx,vely,velz,pressure,bxf,byf,bzf
    else:
        if checkpoint:
            return dens,velx,vely,velz,pressure,Bxc,Byc,Bzc
        else:
            return dens,velx,vely,velz,pressure,Bxc,Byc,Bzc,gpot


## toh5: reads in output from extract and writes hdf5 file
## with filename fileout
def toh5(fileout,dens,velx,vely,velz,pres,magx,magy,magz):

    with h5py.File(fileout) as data_file:
        dset = data_file.create_dataset("dens", data=dens)
        dset = data_file.create_dataset("velx", data=velx)
        dset = data_file.create_dataset("vely", data=vely)  
        dset = data_file.create_dataset("velz", data=velz)
        dset = data_file.create_dataset("pres", data=pres)
        dset = data_file.create_dataset("magx", data=magx)
        dset = data_file.create_dataset("magy", data=magy)  
        dset = data_file.create_dataset("magz", data=magz)
    print "wrote ", fileout

## returns velocity dispersion given density and velocity
def get_vel_dispersion(dens,velx,vely,velz):
    v2 = velx*velx+vely*vely+velz*velz
    sigma = np.sqrt(np.sum(dens*v2)/np.sum(dens))
    return sigma

## returns checkpoint file time
def get_time(fname):
    fin = h5py.File(fname,'r')
    return fin['/'].attrs['time']


## returns bounds of level=level in checkpoint file fname.
## can be useful for selecting/checking indices for extract
def check_extract(fname,level,dims,indices,checkpoint=True):

    # indices of center of zoom region
    ix = indices[0]
    iy = indices[1]
    iz = indices[2]


    # what level are we extracting?
    level = '/level_'+str(level)

    
    print "check_extract: reading in ", fname
    print "level string = ", level
    print "dims = ", dims
    print "indices = ", indices

    fin = h5py.File(fname,'r')

    dname = 'cell_data'
    if not checkpoint:
        dname = 'data'

    boxes = fin[level]['boxes'].value
    offsets = fin[level][dname+':offsets=0'].value

    xmax = -9999999
    ymax = -9999999
    zmax = -9999999
    xmin = 9999999
    ymin = 9999999
    zmin = 9999999
    for ibox in range(0,len(offsets)-1):
        box = boxes[ibox]

        xl = box[0] #+ dims/2 # - ix
        yl = box[1] #+ dims/2 #- iy
        zl = box[2] #+ dims/2 #- iz
        xh = box[3] #+ dims/2 #- ix
        yh = box[4] #+ dims/2 #- iy 
        zh = box[5] #+ dims/2 #- iz

        xmin = min(xl,xmin)
        ymin = min(yl,ymin)
        zmin = min(zl,zmin)

        xmax = max(xh,xmax)
        ymax = max(yh,ymax)
        zmax = max(zh,zmax)
        
    print "check_extract:"
    print "min/max indices", xmin, xmax
    print ymin, ymax
    print zmin, zmax
    print "xyz sizes = ", xmax-xmin, ymax-ymin,zmax-zmin
    print "averaged region center indices", (xmin+xmax)/2.0, (ymin+ymax)/2.0, (zmin+zmax)/2.0
