''''
Visualize the ORION2 grid-processor map in axis slices.
Author: Brandt Gaches
Purpose: Draw the orion2 grid boxes for each of the processors that intersect some axis slice. The code provides for 6 different colors and hatches to distinguish between levels. It is not recommended to use this for many processors - although the code will certainly do so (The image may just be huge and cluttered!). It has been tested with all the different refinement criteria for grids up to 256^3 and 24 processors.
Use: Example call - python draw_boxes --fname=data.00001.3d.hdf5 --slice_dir=2 --slice_idx=0
This will draw the boxes that contain the z index 0. Note: ORION2 boxes can have negative indices. I.e. a 64^3 box will have indices that go from -32 to 31.
'''


import h5py
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import sys
from optparse import OptionParser

parser = OptionParser()
parser.add_option('--fname', dest='fname', help="Input file name - assumed to be a data.___.hdf5 file")
parser.add_option('--slice_dir', dest='slice_dir', help="Which axis to perform the slice on - 0 = x, 1 = y, 2 = z")
parser.add_option('--slice_idx', dest='slice_idx', help="Coarsest grid index to perform the slice on")


(options, args) = parser.parse_args()
sdir = int(options.slice_dir)
indx = int(options.slice_idx)

if options.fname == None:
	print("ERROR: No file name provided!")
	sys.exit(1)

print("Drawing boxes for: %s"%(options.fname))

f = h5py.File(options.fname, 'r')
NLEV = len(f.keys()) - 2

#Run through the levels quickly to find the number of processors
NPROC = 0
for i in range(NLEV+1):
	proc_str = 'level_%d/Processors'%i
	procs = f[proc_str].value
	NPROC = max(NPROC, max(procs))
NPROC += 1

print("Number of AMR levels: %d"%(NLEV))
print("Number of processors: %d"%(NPROC))

PATCHES = []
for i in range(NPROC):
	PATCHES.append([])
colors = ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c']
hatches = ['', 'x', 'O', 'o', '.', '*']
PROB_DOMAIN = f['level_0'].attrs['prob_domain']
XDOM = np.fabs(PROB_DOMAIN[0])
YDOM = np.fabs(PROB_DOMAIN[1])
ZDOM = np.fabs(PROB_DOMAIN[2])
for i in range(NLEV+1):
	proc_str = 'level_%d/Processors'%i
	box_str = 'level_%d/boxes'%i
	lev_str = 'level_%d'%i

	ref_ratio = f[lev_str].attrs['ref_ratio']

	boxes = f[box_str].value
	procs = f[proc_str].value

	NBOXES = len(boxes)
	print("Level: %d\tNumber of boxes: %d\n"%(i, NBOXES))

	for j in range(NBOXES):
		xl = boxes[j][0]; yl = boxes[j][1]; zl = boxes[j][2]; xh = boxes[j][3]; yh = boxes[j][4]; zh = boxes[j][5]
		sCheck = False
		if sdir == 0:
			sCheck = (xl/ref_ratio**i <= indx) and (xl/ref_ratio**i >= indx)
		elif sdir == 1:
			sCheck = (yl/ref_ratio**i <= indx) and (yl/ref_ratio**i >= indx)
		elif sdir == 2:
			sCheck = (zl/ref_ratio**i <= indx) and (zl/ref_ratio**i >= indx)
		else:
			print("ERROR: BAD AXIS SLCE")
			sys.exit(1)
		if sCheck:
			#Now add patch
			W = (xh - xl)/ref_ratio**i
			H = (yh - yl)/ref_ratio**i
			lc = (xl/ref_ratio**i, yl/ref_ratio**i)
			PATCHES[procs[j]].append(Rectangle(lc, W, H, facecolor=colors[i%5], hatch=hatches[i%5]))

NROWS = np.sqrt(NPROC)
if (NROWS - int(NROWS)) > 0.5:
	NROWS += 1
NROWS = int(NROWS)
NCOL = float(NPROC)/float(NROWS)
if (NCOL - int(NCOL)) > 0.5:
	NCOL += 1
NCOL = int(NCOL)

print("(%d, %d)"%(NROWS, NCOL))

f, axarr = plt.subplots(NROWS, NCOL, figsize=(NROWS+6, NCOL+6), facecolor='white', sharex=True, sharey = True)
iplot = 0
for i in range(NROWS):
	for j in range(NCOL):
		if iplot < NPROC:
			for p in PATCHES[iplot]:
				axarr[i,j].add_patch(p)
				axarr[i,j].set_xlim(-XDOM, XDOM)
				axarr[i,j].set_ylim(-XDOM, XDOM)
				axarr[i,j].annotate("Proc %d"%(iplot+1), xy=(XDOM/2-4, YDOM-8), xycoords='data', xytext=(XDOM/2-4, YDOM-8))
		else:
			f.delaxes(axarr[i,j])
		iplot += 1
		

plt.show()
	
