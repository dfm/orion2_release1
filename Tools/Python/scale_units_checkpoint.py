import h5py
import numpy as np
from optparse import OptionParser
import sys

# set these to whatever you want
unit_density  = 8.85744e-21
unit_velocity = 265872.149726
unit_length   = 3.08568e18

# these can be derived from the above:
unit_time = unit_length / unit_velocity
unit_mass = unit_density * unit_length**3
unit_bfld = np.sqrt(unit_density)*unit_velocity

# recursively copy the group structure
def copy_groups(key):
    for attr in fin[key].attrs.keys():
        try:
            fout.create_group(key)
        except:
            pass # the group is already created
    #
    next_keys = fin[key].keys()
    for next_key in next_keys:
        next = next_key
        if key != '/': next = key+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_groups(next)

# recursively copy all hdf5 attributes in the file hierarchy
def copy_attrs(key):
    for attr in fin[key].attrs.keys():
        dtype = h5py.h5a.open(fin[key].id,attr).dtype
        if dtype.isbuiltin: 
            fout[key].attrs[attr] = dtype.type(fin[key].attrs[attr])
        else:
            fout[key].attrs[attr] = np.array([fin[key].attrs[attr]], dtype=dtype)
    #
    next_keys = fin[key].keys()
    for next_key in next_keys:
        next = next_key
        if key != '/': next = key+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_attrs(next)

# coarsen the boxes, *:datatype=0, and *:offsets=0 datasets 
# on each level and write to fout.
def copy_datasets():
    box_idt = np.dtype([('lo_i', np.int32), ('lo_j', np.int32), ('lo_k', np.int32), 
                        ('hi_i', np.int32), ('hi_j', np.int32), ('hi_k', np.int32)])
    i = -1
    while(True):
        i += 1
        level = 'level_%d'%i
        try:
            keys = fin[level].keys()
        except:
            # we've run out of levels
            return
        fout[level].attrs['dx'] = fin[level].attrs['dx'] / unit_length
        fout[level].attrs['dt'] = fin[level].attrs['dt'] / unit_time
        fout[level].attrs['CeilVA_mass'] = fin[level].attrs['CeilVA_mass'] / unit_mass
        # copy over the problem domain
        dom = fin[level].attrs['prob_domain']
        ndim = len(dom)/2
        fout[level].attrs['prob_domain'] = np.array([tuple(dom)], dtype=box_idt)[0]
        # copy over the processor assignments
        fout[level]['Processors'] = fin[level]['Processors'].value
        # copy the boxes
        boxes = fin[level]['boxes'].value
        ncells = 0
        ncellsFlx = 0
        for ibox in range(0,len(boxes)):
            box = np.array(list(boxes[ibox]),dtype=np.int)
            boxes[ibox] = tuple(box)
            # count the number of coarse cells and flux box enteries in this box
            ncells += np.product(box[ndim:2*ndim] - box[0:ndim] + 1)
            for ii in range(0,ndim): 
                stagger = np.array([0,0,0],dtype=np.int)
                stagger[ii] += 1
                ncellsFlx += np.product(box[ndim:2*ndim] - box[0:ndim] + 1 + stagger)
        fout[level]['boxes'] = boxes
        # loop over the various datas on this level
        for key in fin[level]:
            if not ':datatype=0' in key: continue
            if type(fin[level][key]) != h5py.highlevel.Dataset: continue
            dset_name = key.split(':')[0]
            if dset_name == 'face_data': # is there a more general way to distinguish flux boxes from cell-center data?
                # this is a flux box
                # create a dataset and offset big enough for the data
                fout[level].create_dataset(dset_name+':datatype=0',[ncellsFlx],dtype=np.float)
                fout[level].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
                # write the begining data offset
                offsets = fin[level][dset_name+':offsets=0'].value
                coarse_offset = 0
                for ibox in range(0,len(offsets)-1):
                    # read and reshape the current box data
                    box = np.array(list(boxes[ibox]),dtype=np.int)
                    # read the data
                    count = 0
                    fout[level][dset_name+':offsets=0'][ibox] = coarse_offset
                    for ii in range(0,ndim):
                        # read and reshape the current data box
                        stagger = np.array([0,0,0],dtype=np.int)
                        stagger[ii] += 1
                        box = np.array(list(boxes[ibox]),dtype=np.int)
                        shape = box[ndim:2*ndim] - box[0:ndim] + 1 + stagger
                        n = np.product(shape)
                        data = fin[level][dset_name+':datatype=0'][offsets[ibox]+count:offsets[ibox]+count+n].reshape(shape,order='F')
                        # if the data is face-centered, it's a magnetic field
                        data /= unit_bfld
                        count += n

                        # flatten and write the data
                        fout[level][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                        coarse_offset += data.size
                    # write the end data offset
                    fout[level][dset_name+':offsets=0'][ibox+1] = coarse_offset
            else:
                # this is a regular dataset
                # create a dataset and offset big enough for the data
                ncomps = fin[level+'/'+dset_name+'_attributes'].attrs['comps']
                fout[level].create_dataset(dset_name+':datatype=0',[ncells*(ncomps+1)],dtype=np.float)
                fout[level].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
                # read the offsets into the 1D array
                offsets = fin[level][dset_name+':offsets=0'].value
                coarse_offset = 0
                for ibox in range(0,len(offsets)-1):
                    # read and reshape the current box data
                    box = np.array(list(boxes[ibox]),dtype=np.int)
                    shape = box[ndim:2*ndim] - box[0:ndim] + 1
                    shape = list(shape) + [ncomps]
                    data = fin[level][dset_name+':datatype=0'][offsets[ibox]:offsets[ibox+1]].reshape(shape,order='F')

                    if dset_name == 'cell_data':
                        data[:,:,:,0]   /= unit_density
                        data[:,:,:,1:4] /= (unit_density*unit_velocity)
                        data[:,:,:,4:7] /= unit_bfld
                        data[:,:,:,7]   /= unit_density
                        
                    # flatten and write the data
                    fout[level][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                    # write the data offsets
                    fout[level][dset_name+':offsets=0'][ibox] = coarse_offset
                    fout[level][dset_name+':offsets=0'][ibox+1] = coarse_offset+data.size
                    coarse_offset += data.size

###################
# input parameters, read from command line
###################
parser = OptionParser()
parser.add_option('--infile', dest='infile', 
                  help='input file name')
parser.add_option('--outfile', dest='outfile', 
                  help='output file name')
(options, args) = parser.parse_args()
error = False
if options.infile == None: 
    error = True
    print "Error, no --infile option specified"
if options.outfile == None: 
    error = True
    print "Error, no --outfile option specified"
if error: sys.exit(-1)
###################
# begin computation
###################

fin=h5py.File(options.infile,'r')
fout=h5py.File(options.outfile,'w')
copy_groups('/')
copy_attrs('/')
fout['/'].attrs['time'] = fin['/'].attrs['time'] / unit_time
fout.attrs['CeilVA_mass'] = fin.attrs['CeilVA_mass'] / unit_mass
copy_datasets()
fin.close()
fout.close()
