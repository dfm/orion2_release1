import h5py
import numpy as np
import numpy.fft
from math import *
from optparse import OptionParser
import sys
import scipy as sp
import scipy.interpolate
from scipy import ndimage


def init_perturbations(n, kmin, kmax, dtype):
    kx = np.zeros(n, dtype=dtype)
    ky = np.zeros(n, dtype=dtype)
    kz = np.zeros(n, dtype=dtype)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
            
    kx = np.array(kx, dtype=dtype)
    ky = np.array(ky, dtype=dtype)
    kz = np.array(kz, dtype=dtype)
    k = np.sqrt(np.array(kx**2+ky**2+kz**2, dtype=dtype))

    # only use the positive frequencies
    inds = np.where(np.logical_and(k**2 >= kmin**2, k**2 < (kmax+1)**2))
    nr = len(inds[0])

    phasex = np.zeros(n, dtype=dtype)
    phasex[inds] = 2.*pi*np.random.uniform(size=nr)
    fx = np.zeros(n, dtype=dtype)
    fx[inds] = np.random.normal(size=nr)
    
    phasey = np.zeros(n, dtype=dtype)
    phasey[inds] = 2.*pi*np.random.uniform(size=nr)
    fy = np.zeros(n, dtype=dtype)
    fy[inds] = np.random.normal(size=nr)
    
    phasez = np.zeros(n, dtype=dtype)
    phasez[inds] = 2.*pi*np.random.uniform(size=nr)
    fz = np.zeros(n, dtype=dtype)
    fz[inds] = np.random.normal(size=nr)

    # rescale perturbation amplitude so that low number statistics
    # at low k do not throw off the desired power law scaling.
    for i in range(kmin, kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        rescale = sqrt(np.sum(np.abs(fx[slice_inds])**2 + np.abs(fy[slice_inds])**2 + np.abs(fz[slice_inds])**2))
        fx[slice_inds] = fx[slice_inds]/rescale
        fy[slice_inds] = fy[slice_inds]/rescale
        fz[slice_inds] = fz[slice_inds]/rescale

    # set the power law behavior
    # wave number bins
    fx[inds] = fx[inds]*k[inds]**-(0.5*alpha)
    fy[inds] = fy[inds]*k[inds]**-(0.5*alpha)
    fz[inds] = fz[inds]*k[inds]**-(0.5*alpha)

    # add in phases
    fx = np.cos(phasex)*fx + 1j*np.sin(phasex)*fx
    fy = np.cos(phasey)*fy + 1j*np.sin(phasey)*fy
    fz = np.cos(phasez)*fz + 1j*np.sin(phasez)*fz

    return fx, fy, fz, kx, ky, kz


def normalize(fx, fy, fz):
    norm = np.sqrt(np.sum(fx**2 + fy**2 + fz**2)/np.product(n))
    fx = fx/norm
    fy = fy/norm
    fz = fz/norm
    return fx, fy, fz


def make_perturbations(n, kmin, kmax, f_solenoidal):
    fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, dtype)
    if f_solenoidal != None:
        k2 = kx**2+ky**2+kz**2
        # solenoidal part
        fxs = 0.; fys =0.; fzs = 0.
        if f_solenoidal != 0.0:
            fxs = fx - kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fys = fy - ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fzs = fz - kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            ind = np.where(k2 == 0)
            fxs[ind] = 0.; fys[ind] = 0.; fzs[ind] = 0.
        # compressive part
        # get a different random cube for the compressive part
        # so that we can target the RMS solenoidal fraction,
        # instead of setting a constant solenoidal fraction everywhere.
        fx, fy, fz, kx, ky, kz = init_perturbations(n, kmin, kmax, dtype)
        fxc = 0.; fyc =0.; fzc = 0.
        if f_solenoidal != 1.0:
            fxc = kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fyc = ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            fzc = kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
            ind = np.where(k2 == 0)
            fxc[ind] = 0.; fyc[ind] = 0.; fzc[ind] = 0.
    
        # back to real space
        pertx = np.real(np.fft.ifftn(f_solenoidal*fxs + (1.-f_solenoidal)*fxc))
        perty = np.real(np.fft.ifftn(f_solenoidal*fys + (1.-f_solenoidal)*fyc))
        pertz = np.real(np.fft.ifftn(f_solenoidal*fzs + (1.-f_solenoidal)*fzc))
    else:
        # just convert to real space
        pertx = np.real(np.fft.ifftn(fx))
        perty = np.real(np.fft.ifftn(fy))
        pertz = np.real(np.fft.ifftn(fz))
    
    # subtract off COM (assuming uniform density)
    pertx = pertx-np.average(pertx)
    perty = perty-np.average(perty)
    pertz = pertz-np.average(pertz)
    # scale RMS of perturbation cube to unity
    pertx, perty, pertz = normalize(pertx, perty, pertz)
    return pertx, perty, pertz


def window(pertx,perty,pertz):
    n = pertx.shape
    
    x,y,z = np.mgrid[0:n[0],0:n[1],0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    
    rc = np.zeros((n[0],n[1],n[2]),dtype=np.float64)
    rc[:,:,:] = (n[0]-1)/2.
    rad = np.sqrt(x*x+ y*y + z*z)
    
    zzs = np.zeros((n[0],n[1],n[2]),dtype=np.float64)
    
    rr = rad/rc
    
    px = np.where(rr > 1.0e0, zzs, pertx)
    py = np.where(rr > 1.0e0, zzs, perty)
    pz = np.where(rr > 1.0e0, zzs, pertz)
    
    pertx = px
    perty = py
    pertz = pz
    return pertx,perty,pertz

def get_erot_ke_ratio(pertx, perty, pertz):
    n = pertx.shape
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r2 = x**2+y**2+z**2
    
    rc = np.zeros((n[0],n[1],n[2]))
    rc[:,:,:] = (n[0]-1)/2.
    rad = np.sqrt(r2)/rc
    
    mass = np.where(rad>1.0, 0.0e0, rad**(-1.5))
    mt = np.sum(mass)
    mass = mass/mt
    
    wx = np.sum(y*pertz-z*perty) #/(np.sum(r2)*np.product(n))
    wy = np.sum(z*pertx-x*pertz) #/(np.sum(r2)*np.product(n))
    wz = np.sum(x*perty-y*pertx) #/(np.sum(r2)*np.product(n))
    
    wm = np.sqrt(wx*wx+wy*wy+wz*wz)
    
    wx2 = np.sum((y*pertz-z*perty)*mass)
    wy2 = np.sum((z*pertx-x*pertz)*mass)
    wz2 = np.sum((x*perty-y*pertx)*mass)
    
    wm2 = np.sqrt(wx2*wx2+wy2*wy2+wz2*wz2)
    

    erot_ke_ratio = (np.sum(y*pertz-z*perty)**2 +
                     np.sum(z*pertx-x*pertz)**2 +
                     np.sum(x*perty-y*pertx)**2)/(np.sum(r2)*np.product(n)) 
    return erot_ke_ratio


def get_erot_ke_ratio_pad(pertx, perty, pertz):
    n = pertx.shape
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r2 = x**2+y**2+z**2
    
    rc = np.zeros((n[0],n[1],n[2]))
    rc[:,:,:] = (n[0]-1)/2.
    rad = np.sqrt(r2)
    rr = rad/rc
    wx = 0.0e0
    wy = 0.0e0
    wz = 0.0e0
    nn = 0.0e0
    rr2 = 0.0e0
    
    wx  = np.sum( np.where(rr > 1.0, 0.0, (y*pertz-z*perty)))
    wy  = np.sum( np.where(rr > 1.0, 0.0, (z*pertx-x*pertz)))
    wz  = np.sum( np.where(rr > 1.0, 0.0, (x*perty-y*pertx)))
    rr2 = np.sum( np.where(rr > 1.0, 0.0, r2))
    nn  = np.sum( np.where(rr > 1.0, 0.0, 1.0))

    wm = np.sqrt(wx*wx + wy*wy + wz*wz)
    print 'wx: ', wx/wm
    print 'wy: ', wy/wm
    print 'wz: ', wz/wm
    
    erot_ke_ratio = (wx*wx + wy*wy + wz*wz) / (rr2 * nn)
    return erot_ke_ratio

def rotation_matrix(axis,theta):
    axis = axis/np.sqrt(np.dot(axis,axis))
    a = np.cos(theta/2)
    b,c,d = -axis*np.sin(theta/2)
    return np.array([[a*a+b*b-c*c-d*d , 2*(b*c-a*d)     , 2*(b*d+a*c)        ],
                     [2*(b*c+a*d)     , a*a+c*c-b*b-d*d , 2*(c*d-a*b)        ],
                     [2*(b*d-a*c)     , 2*(c*d+a*b)     , a*a+d*d-b*b-c*c    ]])


def get_omega(pertx,perty,pertz,angle):
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r2 = x**2+y**2+z**2
    
    rc = np.zeros((n[0],n[1],n[2]))
    rc[:,:,:] = (n[0]-1)/2.
    rad = np.sqrt(r2)/rc
    
    mass = np.where(rad>1.0, 0.0e0, rad**(-1.5))
    mt = np.sum(mass)
    mass = mass/mt
    
    wx = np.sum(y*pertz-z*perty) 
    wy = np.sum(z*pertx-x*pertz) 
    wz = np.sum(x*perty-y*pertx) 
    
    wm = np.sqrt(wx*wx+wy*wy+wz*wz)
    
    wx2 = np.sum((y*pertz-z*perty)*mass)
    wy2 = np.sum((z*pertx-x*pertz)*mass)
    wz2 = np.sum((x*perty-y*pertx)*mass)
    wm2 = np.sqrt(wx2*wx2+wy2*wy2+wz2*wz2)
    wx2 = wx2/wm2
    wy2 = wy2/wm2
    wz2 = wz2/wm2
    
    wxc = np.sum( np.where(rad > 0.05, 0.0e0,(y*pertz-z*perty)*mass))
    wyc = np.sum( np.where(rad > 0.05, 0.0e0,(z*pertx-x*pertz)*mass))
    wzc = np.sum( np.where(rad > 0.05, 0.0e0,(x*perty-y*pertx)*mass))
    wmc = np.sqrt(wxc*wxc+wyc*wyc+wzc*wzc)
    wxc = wxc/wmc
    wyc = wyc/wmc
    wzc = wzc/wmc
    
    print 'wx2: ', wx2
    print 'wy2: ', wy2
    print 'wz2: ', wz2

    #Determine the axis that I want to align to.
    zaxis = [0.0,0.0,1.0]
    taxis = [0.0,0.0,0.0]
    aang = angle * np.pi / 180.0 
    print 'angle: ', angle, ' aang: ', aang
    taxis[0] =  zaxis[0] * np.cos(aang) + zaxis[1] * 0.0e0 + zaxis[2] * np.sin(aang)
    taxis[1] =  zaxis[0] * 0.0e0        + zaxis[1] * 1.0e0 + zaxis[2] * 0.0e0
    taxis[2] = -zaxis[0] * np.sin(aang) + zaxis[1] * 0.0e0 + zaxis[2] * np.cos(aang)

    zaxis = taxis
    axis = np.cross(zaxis,[wx2,wy2,wz2])
    ang = np.arccos(np.dot(zaxis,[wx2,wy2,wz2]))

    print 'CLOUD: axis: ', axis
    print 'CLOUD: ANG: ', ang, ang*(180.)/np.pi

    axis2 = np.cross(zaxis,[wxc,wyc,wzc])
    ang2 = np.arccos(np.dot(zaxis,[wxc,wyc,wzc]))

    print 'CORE: axis: ',axis2
    print 'CORE: ANG: ',ang2, ang2*(180.)/np.pi

    print 'FROM ZAXIS CLOUD: ', np.arccos(np.dot([0.0,0.0,1.0],[wx2,wy2,wz2]))*180.0/np.pi
    print 'FROM ZAXIS CORE: ', np.arccos(np.dot([0.0,0.0,1.0],[wxc,wyc,wzc]))*180.0/np.pi

    return axis,ang
   
def do_rot(pertx,perty,pertz,axis,ang):
    n = pertx.shape
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    
    nx = np.zeros((n[0],n[1],n[2]))
    ny = np.zeros((n[0],n[1],n[2]))
    nz = np.zeros((n[0],n[1],n[2]))
    npx = np.zeros((n[0],n[1],n[2]))
    npy = np.zeros((n[0],n[1],n[2]))
    npz = np.zeros((n[0],n[1],n[2]))
    rpx = np.zeros((n[0],n[1],n[2]))
    rpy = np.zeros((n[0],n[1],n[2]))
    rpz = np.zeros((n[0],n[1],n[2]))

    rmatrix = rotation_matrix(axis,ang)
    print rmatrix
    r00 = rmatrix[0,0]
    r01 = rmatrix[0,1]
    r02 = rmatrix[0,2]
    r10 = rmatrix[1,0]
    r11 = rmatrix[1,1]
    r12 = rmatrix[1,2]
    r20 = rmatrix[2,0]
    r21 = rmatrix[2,1]
    r22 = rmatrix[2,2]

#OK, do rot
    nx  = r00*x + r01*y + r02*z    
    ny  = r10*x + r11*y + r12*z  
    nz  = r20*x + r21*y + r22*z    
       
    npx = r00*pertx + r01*perty + r02*pertz
    npy = r10*pertx + r11*perty + r12*pertz
    npz = r20*pertx + r21*perty + r22*pertz

#OK, now here is the trick. I need to remap back to old coords

    points = np.zeros((n[0]*n[1]*n[2],3))
    valuesx = np.zeros((n[0]*n[1]*n[2]))
    valuesy = np.zeros((n[0]*n[1]*n[2]))
    valuesz = np.zeros((n[0]*n[1]*n[2]))
    points[:,0] = nx.reshape(-1)
    points[:,1] = ny.reshape(-1)
    points[:,2] = nz.reshape(-1)
    valuesx[:] = npx.reshape(-1)
    valuesy[:] = npy.reshape(-1)
    valuesz[:] = npz.reshape(-1)

    print 'DOING X'
    rpx = scipy.interpolate.griddata(points,valuesx,(x,y,z),method='nearest')
    print 'DOING Y'
    rpy = scipy.interpolate.griddata(points,valuesy,(x,y,z),method='nearest')
    print 'DOING Z'
    rpz = scipy.interpolate.griddata(points,valuesz,(x,y,z),method='nearest')

    print 'DONE'
    
    return rpx,rpy,rpz

def padit(pertx,perty,pertz,pad):
    n = pertx.shape
    n2 = [pad*n[0], pad*n[1], pad*n[2]]
    nc = np.zeros(3)
    nc[0] = n2[0]/2
    nc[1] = n2[1]/2
    nc[2] = n2[2]/2
    
    nx1 = nc[0]-n[0]/2 
    nx2 = nc[0]+n[0]/2
    ny1 = nc[1]-n[1]/2
    ny2 = nc[1]+n[1]/2 
    nz1 = nc[2]-n[2]/2 
    nz2 = nc[2]+n[2]/2 

    npx = np.zeros(n2)
    npy = np.zeros(n2)
    npz = np.zeros(n2)

    npx[nx1:nx2,ny1:ny2,nz1:nz2] = pertx
    npy[nx1:nx2,ny1:ny2,nz1:nz2] = perty
    npz[nx1:nx2,ny1:ny2,nz1:nz2] = pertz
    return npx,npy,npz

#this accepts the original, windowed perts!
def loop_rot(pertx,perty,pertz,angle):
    n = pertx.shape
    x, y, z = np.mgrid[0:n[0], 0:n[1], 0:n[2]]
    x = x - (n[0]-1)/2.
    y = y - (n[1]-1)/2.
    z = z - (n[2]-1)/2.
    r = np.sqrt(x**2+y**2+z**2)
    
    num = 20
    radial = np.linspace(0,(n[0]-1)/2,num)

    npx = np.zeros(n) #These will become my new rotated perts
    npy = np.zeros(n) 
    npz = np.zeros(n)

    for i in range(num-1):
        print 'working on: ', i
        tpx = np.zeros(n)
        tpy = np.zeros(n)
        tpz = np.zeros(n)
        tpx = np.where( r <= radial[i+1] , pertx, 0.0e0)
        tpx = np.where( r >= radial[i], tpx , 0.0e0)
        tpy = np.where( r <= radial[i+1] , perty, 0.0e0)
        tpy = np.where( r >= radial[i], tpy, 0.0e0)
        tpz = np.where( r <= radial[i+1] , pertz, 0.0e0)
        tpz = np.where( r >= radial[i], tpz, 0.0e0)

        axis,ang = get_omega(tpx,tpy,tpz,angle)
        tpx,tpy,tpz = do_rot(tpx,tpy,tpz,axis,ang)
        
        tpx = np.where( r <= radial[i+1] , tpx, 0.0e0)
        tpx = np.where( r >= radial[i], tpx, 0.0e0)
        tpy = np.where( r <= radial[i+1] , tpy, 0.0e0)
        tpy = np.where( r >= radial[i], tpy, 0.0e0)
        tpz = np.where( r <= radial[i+1] , tpz, 0.0e0)
        tpz = np.where( r >= radial[i], tpz, 0.0e0)

        npx = npx + tpx
        npy = npy + tpy
        npz = npz + tpz
    
    return npx,npy,npz       

def plot_spectrum1D(pertx, perty, pertz):
    # plot the 1D power to check the scaling.
    fx = np.abs(np.fft.fftn(pertx))
    fy = np.abs(np.fft.fftn(perty))
    fz = np.abs(np.fft.fftn(pertz))
    fx = np.abs(fx)
    fy = np.abs(fy)
    fz = np.abs(fz)
    kx = np.zeros(n, dtype=dtype)
    ky = np.zeros(n, dtype=dtype)
    kz = np.zeros(n, dtype=dtype)
    # perform fft k-ordering convention shifts
    for j in range(0,n[1]):
        for k in range(0,n[2]):
            kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
    for i in range(0,n[0]):
        for k in range(0,n[2]):
            ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
    for i in range(0,n[0]):
        for j in range(0,n[1]):
            kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
    k = np.sqrt(np.array(kx**2+ky**2+kz**2,dtype=dtype))
    k1d = []
    power = []
    for i in range(kmin,kmax+1):
        slice_inds = np.where(np.logical_and(k >= i, k < i+1))
        k1d.append(i+0.5)
        power.append(np.sum(fx[slice_inds]**2 + fy[slice_inds]**2 + fz[slice_inds]**2))
        print i,power[-1]
    import matplotlib.pyplot as plt
    plt.loglog(k1d, power)
    plt.show()


###################
# input parameters, read from command line
###################
parser = OptionParser()
parser.add_option('--kmin', dest='kmin', 
                  help='minimum wavenumber.  default=1', 
                  default=2)
parser.add_option('--kmax', dest='kmax', 
                  help='maximum wavenumber.  default=2', 
                  default=4)
parser.add_option('--size', dest='size', 
                  help='size of each direction of data cube.  default=256', 
                  default=256)
parser.add_option('--alpha', dest='alpha', 
                  help='negative of power law slope.  default=0', 
                  default = 0)
parser.add_option('--seed', dest='seed', 
                  help='seed for random # generation.  default=0', 
                  default = 0)
parser.add_option('--f_solenoidal', dest='f_solenoidal', 
                  help='volume RMS fraction of solenoidal componet of the perturbations.  ' + 
                       'If --f_solenoidal=None, the motions are purely random.  default=1.0', 
                  default = 1.0)
parser.add_option('--angle',dest='angle',
                  help='angle in degrees that the cube will be rotated about the y-axis. default=0 i.e. aligned with z-axis',default = 0.0)

parser.add_option('--dopad',dest='dopad',
                  help='Do you want to pad the cube? If =1 then --pad must also be set. default = 0 i.e. no padding',default =0)

parser.add_option('--pad',dest='pad',
                  help='controls wether or not the cube is centered within a larger cube. value determines how large the padding is. default=, i.e. no padding is added. 2 means the cube is twice the size in each direction and the perturubations are centered', default = 0)



(options, args) = parser.parse_args()

# size of the data domain
n = [int(options.size), int(options.size), int(options.size)]
# range of perturbation length scale in units of the smallest side of the domain
kmin = int(options.kmin)
kmax = int(options.kmax)
if kmin > kmax or kmin < 0 or kmax < 0:
    print "kmin must be < kmax, with kmin > 0, kmax > 0"
    sys.exit(0)
if kmax > floor(np.min(n))/2:
    print "kmax must be <= floor(size/2)"
    sys.exit(0)
# 1D power law index: magnitude(k)^2 ~ |k|^-alpha
alpha = int(options.alpha)
seed = int(options.seed)
# data precision
dtype = np.float64
# ratio of solenoidal to compressive components
if options.f_solenoidal=="None" or options.f_solenoidal==None:
    f_solenoidal = None
else:
    f_solenoidal = min(max(float(options.f_solenoidal), 0.), 1.)
angle = float(options.angle)
dopad = int(options.dopad)
pad = int(options.pad)
###################
# begin computation
###################

np.random.seed(seed=seed)
pertx, perty, pertz = make_perturbations(n, kmin, kmax, f_solenoidal)
erot_ke_ratio = get_erot_ke_ratio(pertx, perty, pertz)
pertx, perty, pertz = window(pertx,perty,pertz)
print "erot_ke_ratio = ", erot_ke_ratio

#axis,ang = get_omega(pertx,perty,pertz,angle)
#rpx,rpy,rpz = do_rot(pertx,perty,pertz,axis,ang)
rpx,rpy,rpz = loop_rot(pertx,perty,pertz,angle)
rpx,rpy,rpz = window(rpx,rpy,rpz)
axis2,ang2 = get_omega(rpx,rpy,rpz,angle)
erot2 = get_erot_ke_ratio_pad(rpx, rpy, rpz)
print "erot_ke_ratio2 = ", erot2

if(dopad == 1):
    print 'PADDING'
    erot2=get_erot_ke_ratio_pad(rpx,rpy,rpz)
    rpx,rpy,rpz = padit(rpx,rpy,rpz,pad)
    n[0] = n[0]*pad
    n[1] = n[1]*pad
    n[2] = n[2]*pad
    
#plot_spectrum1D(pertx,perty,pertz)
#plot_spectrum1D(rpx,rpy,rpz)

#divV_rms = 0
#for i in range(1,n[0]-1):
#    for j in range(1,n[1]-1):
#        for k in range(1,n[2]-1):
#            divV_rms += ((pertx[i+1,j,k]-pertx[i-1,j,k])+(perty[i,j+1,k]-perty[i,j-1,k])+(pertz[i,j,k+1]-pertz[i,j,k-1]))**2
#divV_rms = sqrt(divV_rms/np.product(n))
#print divV_rms

# hdf5 output
f = h5py.File('zdrv.hdf5', 'w')
ds = f['/'].create_dataset('pertx', n, dtype=np.float)
ds[:] = rpx
ds = f['/'].create_dataset('perty', n, dtype=np.float)
ds[:] = rpy
ds = f['/'].create_dataset('pertz', n, dtype=np.float)
ds[:] = rpz
f['/'].attrs['kmin'] = kmin
f['/'].attrs['kmax'] = kmax
f['/'].attrs['alpha'] = alpha
if f_solenoidal!=None: f['/'].attrs['f_solenoidal'] = f_solenoidal
f['/'].attrs['erot_ke_ratio'] = erot2
f['/'].attrs['seed'] = seed
f.close()

