import h5py
import numpy as np
from optparse import OptionParser
import sys

# recursively copy the group structure
def copy_groups(key):
    for attr in fin[key].attrs.keys():
        try:
            fout.create_group(key)
        except:
            pass # the group is already created
    #
    next_keys = fin[key].keys()
    for next_key in next_keys:
        next = next_key
        if key != '/': next = key+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_groups(next)

# recursively copy all hdf5 attributes in the file hierarchy
def copy_attrs(key, index):
    print "in copy attrs", key
    for attr in fin[key].attrs.keys():
        print "attr = ", attr
        dtype = h5py.h5a.open(fin[key].id,attr).dtype
        # make an opening for the tracer name in the component_N attributes
        if key == '/' and 'component_' in attr:
            if int(attr[10:])>= index:
                fout[key].attrs['component_'+str(int(attr[10:])+1)] = dtype.type(fin[key].attrs[attr]).strip('\' []')
                continue
        if dtype.isbuiltin: 
            fout[key].attrs[attr] = dtype.type(fin[key].attrs[attr])
        else:
            fout[key].attrs[attr] = np.array([fin[key].attrs[attr]], dtype=dtype)
    #
    next_keys = fin[key].keys()
    for next_key in next_keys:
        next = next_key
        if key != '/': next = key+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_attrs(next, index)

# coarsen the boxes, *:datatype=0, and *:offsets=0 datasets 
# on each level and write to fout.
def copy_datasets(index):
    box_idt = np.dtype([('lo_i', np.int32), ('lo_j', np.int32), ('lo_k', np.int32), 
                        ('hi_i', np.int32), ('hi_j', np.int32), ('hi_k', np.int32)])
    i = -1
    while(True):
        i += 1
        print "copying datasets. level = ", i
        level = 'level_%d'%i
        try:
            keys = fin[level].keys()
        except:
            # we've run out of levels
            return
        fout[level].attrs['dx'] = fin[level].attrs['dx']
        # coarsen the prob domain
        dom = fin[level].attrs['prob_domain']
        ndim = len(dom)/2
        fout[level].attrs['prob_domain'] = np.array([tuple(dom)], dtype=box_idt)[0]
        # copy over the processor assignments
        fout[level]['Processors'] = fin[level]['Processors'].value
        # coarsen the boxes
        boxes = fin[level]['boxes'].value
        ncells = 0
        ncellsFlx = 0
        for ibox in range(0,len(boxes)):
            box = np.array(list(boxes[ibox]),dtype=np.int)
            boxes[ibox] = tuple(box)
            # count the number of coarse cells and flux box enteries in this box
            ncells += np.product(box[ndim:2*ndim] - box[0:ndim] + 1)
            for ii in range(0,ndim): 
                stagger = np.array([0,0,0],dtype=np.int)
                stagger[ii] += 1
                ncellsFlx += np.product(box[ndim:2*ndim] - box[0:ndim] + 1 + stagger)
        fout[level]['boxes'] = boxes
        # loop over the various datas on this level
        for key in fin[level]:
            if not ':datatype=0' in key: continue
            if type(fin[level][key]) != h5py.highlevel.Dataset: continue
            dset_name = key.split(':')[0]
            if dset_name == 'face_data': # is there a more general way to distinguish flux boxes from cell-center data?
                # this is a flux box
                # create a dataset and offset big enough for the data
                fout[level].create_dataset(dset_name+':datatype=0',[ncellsFlx],dtype=np.float)
                fout[level].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
                # write the begining data offset
                offsets = fin[level][dset_name+':offsets=0'].value
                coarse_offset = 0
                for ibox in range(0,len(offsets)-1):
                    # read and reshape the current box data
                    box = np.array(list(boxes[ibox]),dtype=np.int)
                    # read the data
                    count = 0
                    fout[level][dset_name+':offsets=0'][ibox] = coarse_offset
                    for ii in range(0,ndim):
                        # read and reshape the current data box
                        stagger = np.array([0,0,0],dtype=np.int)
                        stagger[ii] += 1
                        box = np.array(list(boxes[ibox]),dtype=np.int)
                        shape = box[ndim:2*ndim] - box[0:ndim] + 1 + stagger
                        n = np.product(shape)
                        data = fin[level][dset_name+':datatype=0'][offsets[ibox]+count:offsets[ibox]+count+n].reshape(shape,order='F')
                        count += n

                        # flatten and write the data
                        fout[level][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                        coarse_offset += data.size
                    # write the end data offset
                    fout[level][dset_name+':offsets=0'][ibox+1] = coarse_offset
            else:
                # this is a regular dataset
                # create a dataset and offset big enough for the data
                ncomps = fin[level+'/'+dset_name+'_attributes'].attrs['comps']
                fout[level].create_dataset(dset_name+':datatype=0',[ncells*(ncomps+1)],dtype=np.float)
                fout[level].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
                # read the offsets into the 1D array
                offsets = fin[level][dset_name+':offsets=0'].value
                coarse_offset = 0
                for ibox in range(0,len(offsets)-1):
                    # read and reshape the current box data
                    box = np.array(list(boxes[ibox]),dtype=np.int)
                    shape = box[ndim:2*ndim] - box[0:ndim] + 1
                    shape = list(shape) + [ncomps]
                    data_old = fin[level][dset_name+':datatype=0'][offsets[ibox]:offsets[ibox+1]].reshape(shape,order='F')
                    # if this is the cell data, add the extra tracer field in
                    if dset_name == 'cell_data':
                        shape = box[ndim:2*ndim] - box[0:ndim] + 1 
                        shape = list(shape) + [ncomps+1]
                        data = np.zeros(shape, dtype=np.float, order='F')
                        for ii in range(0,ncomps):
                            inew = ii
                            if ii>= index: inew += 1
                            if ii == 7:
                                # the energy-density must be scaled according to the change in gamma
                                # to keep the pressure the same
                                #gamma_old = 1.0001
                                gamma_new = 1.66666667
                                pressure_fac = (gamma_old - 1.) / (gamma_new - 1.)
                                den = data_old[:,:,:,0]
                                v2 = (data_old[:,:,:,1]**2 + data_old[:,:,:,2]**2 + data_old[:,:,:,3]**2)/den**2
                                b2 = data_old[:,:,:,4]**2 + data_old[:,:,:,5]**2 + data_old[:,:,:,6]**2
                                ut = data_old[:,:,:,7] - 0.5*den*v2 - 0.5*b2
                                #print ut.min()
#                                assert(np.all(ut > 0.0))
                                ut = ut * pressure_fac
                                data[:,:,:,inew] = ut + 0.5*den*v2 + 0.5*b2
                            else:
                                data[:,:,:,inew] = data_old[:,:,:,ii]
                        # set the radiation temperature to 10 K.
                        data[:,:,:,index] = 7.5646e-15*temperature**4
                    else:
                        data = data_old
                    # flatten and write the data
                    fout[level][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                    # write the data offsets
                    fout[level][dset_name+':offsets=0'][ibox] = coarse_offset
                    fout[level][dset_name+':offsets=0'][ibox+1] = coarse_offset+data.size
                    coarse_offset += data.size

###################
# input parameters, read from command line
###################
parser = OptionParser()
parser.add_option('--infile', dest='infile', 
                  help='input file name')
parser.add_option('--outfile', dest='outfile', 
                  help='output file name')
parser.add_option('--index', dest='index', 
                  help='The index of the tracer field for the state vector in outfile.')
parser.add_option('--temperature', dest='temperature',
                  help='The constant temerature to use to set the radiaiton energy denstity.')
parser.add_option('--gamma_in', dest='gamma_in',
                  help='The value of gamma in the input file.')
(options, args) = parser.parse_args()
error = False
if options.infile == None: 
    error = True
    print "Error, no --infile option specified"
if options.outfile == None: 
    error = True
    print "Error, no --outfile option specified"
if options.index == None: 
    error = True
    print "Error, no --index option specified"
if options.temperature == None:
    error = True
    print "Error, no --temperature option specified"
if options.gamma_in == None:
    gamma_old = 1.0001
    print "Setting gamma_in = 1.0001"
else:
    gamma_old = float(options.gamma_in)
if error: sys.exit(-1)
###################
# begin computation
###################
print "infile = ", options.infile
fin=h5py.File(options.infile,'r')
fout=h5py.File(options.outfile,'w')
index = int(options.index)
temperature = float(options.temperature)
copy_groups('/')
copy_attrs('/', index)
print "copied groups and attributes!"
# add a root level attribute for the tracer name
ncomps = fout['level_0/cell_data_attributes'].attrs['comps']
fout['/'].attrs['component_'+str(index)] = np.string_('radiation-energy-density')
# fix-up the number of components attributes
ncomps = fout['/'].attrs['num_components'] + 1
fout['/'].attrs['num_components'] = ncomps
i = -1
while(True):
    i += 1
    level = 'level_%d'%i
    try:
        keys = fin[level].keys()
        fout[level+'/cell_data_attributes'].attrs['comps'] = ncomps
    except:
        # we've run out of levels
        break
# copy the data
copy_datasets(index)
fin.close()
fout.close()
