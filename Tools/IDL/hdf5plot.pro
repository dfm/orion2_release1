PRO hdf5plot,filename, noboxes = noboxes, _EXTRA=extra, var = var

 checkfile,filename

 rat = 1
 ifil = H5F_OPEN(filename)
 igen = H5G_OPEN(ifil,'/')

 inlev = H5A_OPEN_NAME(igen,'num_levels')
 nlev = H5A_READ(inlev)
 H5A_CLOSE,inlev

 icomp = H5A_OPEN_NAME(igen,'num_components')
 ncomp = H5A_READ (icomp)
 H5A_CLOSE,icomp

 lev = strarr(nlev)
 for i=0,nlev-1 do lev[i] = 'level_'+strcompress(string(i),/REMOVE_ALL)

freb = intarr(nlev)

FOR nl = nlev-1,0,-1 DO BEGIN

 ilev = H5G_OPEN(ifil,lev[nl])

 IF (nl EQ nlev-1) THEN BEGIN
   freb[nl] = 1
   iprobdom = H5A_OPEN_NAME(ilev,'prob_domain')
   pdom = H5A_READ(iprobdom)
   H5A_CLOSE,iprobdom
   ilo = pdom.lo_i
   jlo = pdom.lo_j
   ihi = pdom.hi_i
   jhi = pdom.hi_j
   nx = ihi-ilo+1
   ny = jhi-jlo+1
   idx = H5A_OPEN_NAME(ilev,'dx')
   dx = H5A_READ(idx)
   H5A_CLOSE,idx
 ENDIF ELSE BEGIN
  irat = H5A_OPEN_NAME(ilev,'ref_ratio')
  rat = H5A_READ(irat)
  H5A_CLOSE,irat
  freb[nl] = freb[nl+1]*rat
 ENDELSE

 H5G_CLOSE,ilev

ENDFOR

vars = dblarr(nx,ny,ncomp)
x = (findgen(nx)+0.5)*dx
y = (findgen(ny)+0.5)*dx


FOR nl = 0, NLEV - 1 DO BEGIN

xbox = rebin(x,nx/rat^(NLEV - 1 - nl))-10.;

  ilev = H5G_OPEN(ifil,lev[nl])

  idata = H5D_OPEN(ilev,'data:datatype=0')
  data  = H5D_READ(idata)
  H5D_CLOSE,idata

  iboxes = H5D_OPEN(ilev,'boxes')
  boxes = H5D_READ(iboxes)
  H5D_CLOSE,iboxes

  nbox = n_elements(boxes.lo_i)
  ncount = long(0)

  FOR ib = 0,nbox-1 DO BEGIN

    illoc = boxes[ib].lo_i
    ihloc = boxes[ib].hi_i
    jlloc = boxes[ib].lo_j
    jhloc = boxes[ib].hi_j

    FOR ic = 0,ncomp-1 DO BEGIN
    FOR j = jlloc,jhloc DO BEGIN
    FOR i = illoc,ihloc DO BEGIN

      FOR jsub = 0,freb[nl]-1 DO BEGIN
      FOR isub = 0,freb[nl]-1 DO BEGIN
        vars[i*freb[nl]+isub,j*freb[nl]+jsub,ic] = data[ncount]
;     vars[i*freb[nl]+isub,j*freb[nl]+jsub,ic] = data[ncount]/xbox[i]

      ENDFOR
      ENDFOR

      ncount = ncount + long(1)

    ENDFOR
    ENDFOR
    ENDFOR

  ENDFOR

 H5G_CLOSE,ilev

ENDFOR

H5G_CLOSE,igen
H5F_CLOSE,ifil


IF (NOT KEYWORD_SET (var)) THEN var = 0


print,min(vars(*,*,var)), max(vars(*,*,var))
display,alog10(vars(*,*,var)), x1 = x, x2 = y, _EXTRA = extra
;contour,vars(*,*,var),x,y,nlev=30

IF (NOT KEYWORD_SET (noboxes)) THEN BEGIN
  tvlct,r,g,b,/get
  loadct,0
  !P.COLOR = 255
  plotbox,filename
  tvlct,r,g,b

ENDIF

END
