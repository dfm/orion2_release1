pro plotarr, u, v, x, y, nxa = nxa, nya = nya 

IF (NOT KEYWORD_SET (nxa)) THEN nxa = 16
IF (NOT KEYWORD_SET (nya)) THEN nya = 16

a = u
xx = x
yy = y
regrid,a,xx,yy,n1=nxa,n2=nya

b = v
xx = x
yy = y
regrid,b,xx,yy,n1=nxa,n2=nya

hlength = (xx(1)-xx(0))*0.5

norm = sqrt(a^2 + b^2) + 1.e-6

for i = 0, nxa - 1 do begin
for j = 0, nya - 1 do begin

  lx = a(i,j)/norm(i,j)
  ly = b(i,j)/norm(i,j)

  xa = xx(i) + hlength*lx
  ya = yy(j) + hlength*ly

  arrow, xx(i), yy(j), xa, ya,/data,hsize=3.0
endfor
endfor

end



