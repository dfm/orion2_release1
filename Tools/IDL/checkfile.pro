pro checkfile,filename

finfo = file_info(filename)

if (finfo.exists eq 0) then begin

 print,'File '+strcompress(filename,/REMOVE_ALL)+' does not exist'
 retall

endif else begin

 ishdf = H5F_IS_HDF5(filename)

 if (ishdf eq 0) then begin

  print,'File '+strcompress(filename,/REMOVE_ALL)+' is not an HDF5 file'
  retall

 endif

endelse

return
end