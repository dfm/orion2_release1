; --------------------------------
;  make the userdef symbol filled
;  circle
; --------------------------------

npt  = 8
xsym = fltarr(npt)
ysym = fltarr(npt)

for i = 0,npt-1 DO BEGIN
  xsym(i) = sin(2.0*!PI*i/(npt*1.0))
  ysym(i) = cos(2.0*!PI*i/(npt*1.0))
ENDFOR
usersym,xsym,ysym,/fill
END
