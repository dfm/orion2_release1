  COMMON PLUTO_GRID,  n1,n2,n3,x1,x2,x3,$
                      dx1,dx2,dx3, geometry, xpos, ypos
  COMMON PLUTO_VAR, NVAR, rho, v1, v2, v3, b1, b2, b3, pr,$
                    b1s, b2s, b3s,$
                    b01, b02, b03, fneut, fion, fh2, $
                    tmp, tr1, tr2, tr3, tr4,$
                    fhI, fheI, fheII,$
                    fcI, fcII, fcIII, fcIV, fcV,$
                    fnI, fnII, fnIII, fnIV, fnV,$
                    foI, foII, foIII, foIV, foV,$
                    fneI, fneII, fneIII, fneIV, fneV,$
                    fsI, fsII, fsIII, fsIV, fsV

  COMMON PLUTO_RUN,  t, dt, nlast, dir, first_call

;+
;
; NAME:      pload
;
; AUTHOR:    Andrea Mignone
;
; REQUIRES:  load_pluto_data.pro
;
; PURPOSE:   - load a set of binary files written by PLUTO3 by storing them into 
;              memory or using file-association (for large dataset);
;            - define COMMON blocks containing problem specifications. 
;              The common blocks are:
;
;              PLUTO_GRID -> contains grid information: this is read using 
;                            the load_pluto_data function.
;
;               n1  =  number of points in the x1 direction 
;               n2  =  number of points in the x2 direction 
;               n3  =  number of points in the x3 direction 
;               x1  =  1-D array of n1 points containing the x1 zone centers 
;               x2  =  1-D array of n2 points containing the x2 zone centers 
;               x3  =  1-D array of n3 points containing the x3 zone centers 
;               dx1 =  1-D array of n1 points containing mesh widths   
;               dx2 =  1-D array of n2 points containing mesh widths  
;               dx3 =  1-D array of n3 points containing mesh widths    
;
;
;              PLUTO_VAR -> contains the number of variables and the corresponding
;                           file names:
;
;                 rho        = density
;                 v1, v2, v3 = velocity components
;                 b1, b2, b3 = magnetic field components
;                 pr         = pressure
;
;              PLUTO_RUN -> contains the number of variables and the corresponding
;
;                t     = time at the corresponding output number
;                dt    = time step at the corresponding output number
;                nlast = the last written file 
;
;            Data can be stored as 
;
;           1) single data-set, containing multiple variables. In this case
;               it is assumed that the file names are:
;
;              data.dbl.nnnn.out  OR  data.flt.nnnn.out  
;
;              for double OR single precision, respectively.
;
;            2) multiple files: separate file for each variable. In this case
;               variables are names
;
;                rho.dbl.nnnn.out, v1.dbl.nnnn.out, ...
;
;               or with ".flt." for single precision.
;
;           File and variables names are read in flt.out or dbl.out
;
;
; SYNTAX:    pload, nout[, /float][, /assoc,][ /verbose][, _EXTRA = extra]
;
; KEYWORD & PARAMETERS:
;
;           nout = the sequential output number,
;                  0,1,2,3,... :
;
;
;           /float  = set this keyword if you wish to read
;                     single precision data rather than double.
;                     The default will try to read dbl (if possible) and
;                     will automatically switch to /float if dbl.out is not found.
;
;           /assoc  = use IDL file-association rather than storing files into memory, 
;                     This can be particularly useful for large data sets.
;
;           /verbose  = set this keyword to print some nice output.
;
;
;  EXAMPLES:
;
;      pload,10
;
;            retrieves the 10th output data set and 
;            store variable contents into rho, v1, v2, ... 
;
;
; LAST MODIFIED
;
;  February 8, 2008 by A. Mignone (mignone@to.astro.it)
;
;-
PRO pload, nout, float=float,extension=extension,$
           assoc=assoc, verbose=verbose, _EXTRA=extra

  COMMON PLUTO_GRID
  COMMON PLUTO_VAR
  COMMON PLUTO_RUN

  IF (NOT KEYWORD_SET(first_call)) THEN BEGIN
    print, "> PLOAD"
    verbose = 1
    IF (!D.NAME EQ 'X') THEN BEGIN
      device, retain=2, decomposed=0, true_color = 24
    ENDIF ELSE BEGIN
      device, retain=2, decomposed=0
    ENDELSE

    first_call = 1
  ENDIF

  load_pluto_data,verbose=verbose

;+++++++++++++++++++++++++++++++++++++++++++
;  Try to read "dbl.out".
;  if it does not exist, try for "flt.out"
;+++++++++++++++++++++++++++++++++++++++++++

 fdbl = FILE_INFO("dbl.out")
 fflt = FILE_INFO("flt.out")

 ; return if neither one exists 

 IF (NOT KEYWORD_SET(float)) THEN BEGIN
   IF (NOT fdbl.exists) THEN BEGIN
     print," ! cannot find dbl.out"
     return
   ENDIF
   outname = "dbl.out"
 ENDIF

 IF (KEYWORD_SET(float)) THEN BEGIN
   IF (NOT fflt.exists) THEN BEGIN
     print," ! cannot find flt.out"
     return
   ENDIF
   outname = "flt.out"
 ENDIF

 scrh = read_ascii(outname,count = nlast)

 timestr = strarr(nlast)
 t       = fltarr(nlast)
 dt      = fltarr(nlast)
 mode    = strarr(nlast)
 prec    = strarr(nlast)
 endn    = strarr(nlast)

 numvar = intarr(nlast)
 names  = strarr(nlast,64)

 nlast = nlast - 1
 IF (nout GT nlast) THEN BEGIN
   print," ! Index out of range in "+outname+$
         " (nlast = ",strcompress(string(nlast)),')'
   return
 ENDIF

 CLOSE,/all
 OPENR,1, outname, ERROR = err
 READF, 1, timestr
 CLOSE,1

 FOR n = 0, nlast DO BEGIN

   q = strsplit (timestr(n),' ',/extract)
   t(n)     = q(1)
   dt(n)    = q(2)
   mode(n)  = q(4)
   endn(n)  = q(5)

   sq6 = size(q(6:*))
   numvar(n) = sq6[1]

   names(n, 0:numvar(n)-1) = q(6:*)

 ENDFOR

;+++++++++++++++++++++++++++++++++++++++
;     Open files for reading
;+++++++++++++++++++++++++++++++++++++++

 ext = string(format='(I4.4)', nout)
 ext = strcompress(ext,/remove_all)

 SINGLE_FILE = 0
 IF (mode(nout) EQ "single_file") THEN SINGLE_FILE = 1

  
 IF (prec(nout) EQ "float" OR KEYWORD_SET(float)) THEN BEGIN
   REAL   = 4
   NBYTES = 4
   ext = "."+ext+".flt"
 ENDIF ELSE BEGIN
   REAL   = 5; -- default is double --
   NBYTES = 8
   ext = "."+ext+".dbl"
 ENDELSE

 IF (KEYWORD_SET (verbose)) THEN BEGIN
   IF (NBYTES EQ 4) THEN BEGIN
     print, " > pload: using single precision data"
   ENDIF

   IF (NBYTES EQ 8) THEN BEGIN
     print, " > pload: using double precision data"
   ENDIF
 ENDIF


 IF (SINGLE_FILE) THEN BEGIN
   fname = "data"+ext
 ENDIF

 offset = ulong64(0)
 FOR nv = 0, numvar(nout) - 1 DO BEGIN

   IF (NOT SINGLE_FILE) THEN fname = names(nout,nv)+ext

   IF (NOT SINGLE_FILE OR nv EQ 0) THEN BEGIN
     IF (endn(nout) EQ "little") THEN BEGIN
       openr, U, fname, /GET_LUN, /SWAP_IF_BIG_ENDIAN, _EXTRA=extra, $
              bufsize=8, ERROR = err
     ENDIF ELSE BEGIN
       openr, U, fname, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, _EXTRA=extra, $
              bufsize=8,ERROR = err
     ENDELSE
     IF (err NE 0) THEN BEGIN
       print," ! Cannot open file "+fname 
     ENDIF

   ENDIF

   CASE names(nout,nv) OF
    "rho": rho = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "v1":  v1  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "v2":  v2  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "v3":  v3  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "b1":  b1  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "b2":  b2  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "b3":  b3  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "pr":  pr  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "tr1": tr1 = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "tr2": tr2 = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "tr3": tr3 = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "tr4": tr4 = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "b1s": b1s = assoc(U, make_array(n1+1,n2,n3, type = REAL),offset)
    "b2s": b2s = assoc(U, make_array(n1,n2+1,n3, type = REAL),offset)
    "b3s": b3s = assoc(U, make_array(n1,n2,n3+1, type = REAL),offset)
    "fneut": fneut = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "tmp":   tmp   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fHI":   fhI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fHeI":  fheI  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fHeII": fheII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fCI":   fcI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fCII":  fcII  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fCIII": fcIII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fCIV":  fcIV  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fCV":   fcV   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fNI":   fnI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNII":  fnII  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNIII": fnIII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNIV":  fnIV  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNV":   fnV   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fOI":   foI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fOII":  foII  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fOIII": foIII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fOIV":  foIV  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fOV":   foV   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fNeI":   fneI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNeII":  fneII  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNeIII": fneIII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNeIV":  fneIV  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fNeV":   fneV   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

    "fSI":   fsI   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fSII":  fsII  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fSIII": fsIII = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fSIV":  fsIV  = assoc(U, make_array(n1,n2,n3, type = REAL),offset)
    "fSV":   fsV   = assoc(U, make_array(n1,n2,n3, type = REAL),offset)

   ELSE:
   ENDCASE

; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;  store arrays into memory if the /assoc 
;  keyword has not been given
; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   IF (NOT KEYWORD_SET(assoc)) THEN BEGIN
     CASE names(nout,nv) OF
      "rho": rho = rho(0)
      "v1":  v1  = v1(0)
      "v2":  v2  = v2(0)
      "v3":  v3  = v3(0)
      "b1":  b1  = b1(0)
      "b2":  b2  = b2(0)
      "b3":  b3  = b3(0)
      "pr":  pr  = pr(0)
      "tr1": tr1 = tr1(0)
      "tr2": tr2 = tr2(0)
      "tr3": tr3 = tr3(0)
      "tr4": tr4 = tr4(0)
      "b1s": b1s = b1s(0)
      "b2s": b2s = b2s(0)
      "b3s": b3s = b3s(0)
      "fneut": fneut = fneut(0)
      "tmp":   tmp   = tmp(0)

      "fHI":   fhI   = fhI(0)
      "fHeI":  fheI  = fHeI(0)
      "fHeII": fheII = fHeII(0)

      "fCI":   fcI   = fcI(0)
      "fCII":  fcII  = fcII(0)
      "fCIII": fcIII = fcIII(0)
      "fCIV":  fcIV  = fcIV(0)
      "fCV":   fcV   = fcV(0) 

      "fNI":   fnI   = fnI(0)
      "fNII":  fnII  = fnII(0)
      "fNIII": fnIII = fnIII(0)
      "fNIV":  fnIV  = fnIV(0)
      "fNV":   fnV   = fnV(0) 

      "fOI":   foI   = foI(0)
      "fOII":  foII  = foII(0)
      "fOIII": foIII = foIII(0)
      "fOIV":  foIV  = foIV(0)
      "fOV":   foV   = foV(0) 

      "fNeI":   fneI   = fneI(0)
      "fNeII":  fneII  = fneII(0)
      "fNeIII": fneIII = fneIII(0)
      "fNeIV":  fneIV  = fneIV(0)
      "fNeV":   fneV   = fneV(0)

      "fSI":   fsI   = fsI(0)
      "fSII":  fsII  = fsII(0)
      "fSIII": fsIII = fsIII(0)
      "fSIV":  fsIV  = fsIV(0)
      "fSV":   fsV   = fsV(0)

     ELSE:
     ENDCASE
   ENDIF


   IF (SINGLE_FILE) THEN BEGIN
     n1_off = ulong64(n1)
     n2_off = ulong64(n2)
     n3_off = ulong64(n3)

     IF (names(nout,nv) EQ "b1s") THEN n1_off = ulong64(n1 + 1)
     IF (names(nout,nv) EQ "b2s") THEN n2_off = ulong64(n2 + 1)
     IF (names(nout,nv) EQ "b3s") THEN n3_off = ulong64(n3 + 1)

     offset = offset + ulong64(n1_off*n2_off*n3_off*NBYTES)
   ENDIF

 ENDFOR

END
