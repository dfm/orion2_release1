
PRO Movie, rho


sz = size(rho(0))
n1 = sz(1)
n2 = sz(2)
nlast = (fstat(rho)).size/(8*n1*n2) - 1

window,1,xsize=n1,ysize=n2
for n = 0,nlast do begin
  tvscl,rho(n)
endfor

end
