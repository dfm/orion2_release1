print ('loading...')  
import os
import os.path
import sys

# check if $ORION2_DIR is defined

try:
  orion2_dir = os.environ["ORION2_DIR"]
except:
  current_dir = os.getcwd()
#  print "---------------------------------------------------------------"
#  print " WARNING:"
#  print "  "
#  print " Your ORION2_DIR shell variable does not seem to be defined. " 
#  print " Please set ORION2_DIR as the directory name of your ORION2  "
#  print " distribution before proceeding, e.g.\n"
#  print '    > setenv ORION2_DIR "/home/user/ORION2"\n'
#  print " if you're using tcsh, or \n"
#  print '    > export ORION2_DIR="/home/user/ORION2"\n'
#  print " if you're using bash."
#  print "---------------------------------------------------------------"
#  sys.exit()


print ("ORION2 directory is defined as: ",orion2_dir)
sys.path.append(orion2_dir+'/Tools/Python')
import ut
import configure

#sys.stdout.write('loading...')
#sys.stdout.flush()
#orion2_dir   = os.getcwd();
#orion2_dir   = os.path.abspath(orion2_dir)

configure.check(orion2_dir)

#os.chdir(orion2_dir)


NO_GUI = False
for x in sys.argv:
  if (x == "--no-gui"):
    NO_GUI = True

if NO_GUI:
  ut.no_gui(orion2_dir)
else:
  ut.main_menu(orion2_dir)

